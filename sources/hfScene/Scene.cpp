#define TINYOBJLOADER_IMPLEMENTATION
#include <hfScene/Scene.inl>

std::string GetBaseDir(std::string& filepath) {
	std::regex reg("\\\\"); // \\ is escape seq and last two \\ is what to search for :DDD
	filepath = std::regex_replace(filepath, reg, "/");
	if (filepath.find_last_of("/") != std::string::npos)
		return filepath.substr(0, filepath.find_last_of("/")) + "/";
	return "";
}
