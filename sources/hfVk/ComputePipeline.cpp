/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/ComputePipeline.h>
#include <hfVk/PipelineCache.h>
#include <hfAx/Exception.h>
#include <utility>

using namespace hf::Vk;

/* -------CreateInfoClass ------- */
CreateInfo::ComputePipeline::operator vk::ComputePipelineCreateInfo() {
	return {
		vk::PipelineCreateFlags(flags),
		shaderStage,
		layout->getVkHandle(),
		basePipelineHandle,
		basePipelineIndex
	};
}

/* ------- Core Class ------- */
void ComputePipeline::init(CreateInfo::ComputePipeline& createInfo) {
	if (_pipeline != vk::Pipeline())
		return;

	vk::ComputePipelineCreateInfo pipeCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createComputePipelines(createInfo.pipelineCache->getVkHandle(), 1, &pipeCI, nullptr, &_pipeline);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk ComputePipeline");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::ComputePipeline(createInfo);
	else
		_description = new Info::ComputePipeline(createInfo);

}

void ComputePipeline::deinit() {
	// destroy vk object
	if (_pipeline == vk::Pipeline())
		_description->pDevice->getVkHandle().destroyPipeline(_pipeline, nullptr);

	// destroys info.
	if (_description == nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _pipeline = vk::Pipeline();
}

bool hf::Vk::ComputePipeline::rebuild(CreateInfo::ComputePipeline*& createInfo) {
	// reset 
	deinit();

	// rebuild
	if (createInfo != nullptr) 
		// $cam na osetreni vyjimky proste se vyskoci a konec...
		init(*createInfo);
	
	return true;
}

void ComputePipeline::move(ComputePipeline& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_pipeline != vk::Pipeline() || _description != nullptr)
		deinit();

	// move ptr on resources
	_pipeline = withdraw._pipeline;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._pipeline = vk::Pipeline();
	withdraw._description = nullptr;
	
}

bool ComputePipeline::assemble(VkPipeline vkpipe, Info::ComputePipeline info) {
	if (_pipeline != vk::Pipeline() || _description != nullptr)
		deinit();

	_description = new Info::ComputePipeline(info);
	_pipeline = vkpipe;

	return true;
}