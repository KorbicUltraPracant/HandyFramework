/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/Device.h>
#include <hfVk/DeviceQueues.h>
#include <hfAx/Exception.h>

using namespace std;
using namespace hf::Vk;

/* ------- QueuesInfoClass ------- */
Info::RequestQueues::operator vk::DeviceQueueCreateInfo() {
	// rovnomerna priorita queues
	queuePriorities.resize(count);
	queuePriorities.assign(count, (float) 1 / count);

	return vk::DeviceQueueCreateInfo(vk::DeviceQueueCreateFlags(0), qFamilyIdx, count, queuePriorities.data());
}

/* -------CreateInfoClass ------- */
CreateInfo::Device::operator vk::DeviceCreateInfo() {
	// queues to init info
	for (auto& entry : initializedQueuesInfo)
		queuesInfo.push_back(entry); 

	return vk::DeviceCreateInfo(
		vk::DeviceCreateFlags(0),
		(uint32_t) queuesInfo.size(),
		queuesInfo.data(),
		(uint32_t) requieredLayerNames.size(),
		requieredLayerNames.data(),
		(uint32_t)requieredExtensionNames.size(),
		requieredExtensionNames.data(),
		&requieredDeviceFeatures
	).setPNext(pNext);
}

/* ------- Auxiliary Methods ------- */
void Auxiliary::Device::enumerateAvailableExtensions(vk::PhysicalDevice psDevice, std::vector<vk::ExtensionProperties>& props) {
	uint32_t extensionsCount;
	vk::Result res = psDevice.enumerateDeviceExtensionProperties(nullptr, &extensionsCount, nullptr);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Device::enumerateAvailableExtensions -> Cannot obtain extensions count");

	props.resize(extensionsCount);
	res = psDevice.enumerateDeviceExtensionProperties(nullptr, &extensionsCount, props.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Device::enumerateAvailableExtensions -> Cannot obtain extensions info");

}

void Auxiliary::Device::enumerateAvailableLayers(vk::PhysicalDevice psDevice, std::vector<vk::LayerProperties>& props) {
	uint32_t layersCount;
	vk::Result res = psDevice.enumerateDeviceLayerProperties(&layersCount, nullptr);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Device::enumerateAvailableLayers -> Cannot obtain layers count");

	props.resize(layersCount);
	res = psDevice.enumerateDeviceLayerProperties(&layersCount, props.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Device::enumerateAvailableLayers -> Cannot obtain layers info");
}

bool Auxiliary::Device::extensionSupported(vk::PhysicalDevice psDevice, std::string name) {
	std::vector<vk::ExtensionProperties> props;
	Auxiliary::Device::enumerateAvailableExtensions(psDevice, props);
	for (auto entry : props) {
		if (!name.compare(entry.extensionName)) {
			return true;
		}
	}

	return false;
}

bool Auxiliary::Device::layerSupported(vk::PhysicalDevice psDevice, std::string name) {
	std::vector<vk::LayerProperties> props;
	Auxiliary::Device::enumerateAvailableLayers(psDevice, props);
	for (auto entry : props) {
		if (!name.compare(entry.layerName)) {
			return true;
		}
	}

	return false;
}

int Auxiliary::Device::extensionsSupported(vk::PhysicalDevice psDevice, std::vector<char*>& names) {
	for (int i = 0; i < names.size(); i++) {
		if (!Auxiliary::Device::extensionSupported(psDevice, names[i]))
			return i;
	}

	return names.size();
}

void Auxiliary::Device::extensionsSupported(vk::PhysicalDevice psDevice, std::vector<char*>& names, std::vector<bool>& resVec) {
	for (int i = 0; i < names.size(); i++) {
		if (Auxiliary::Device::extensionSupported(psDevice, names[i]))
			resVec.push_back(true);
		else
			resVec.push_back(false);
	}
}

int hf::Vk::Auxiliary::Device::layersSupported(vk::PhysicalDevice psDevice, std::vector<char*>& names) {
	for (int i = 0; i < names.size(); i++) {
		if (!Auxiliary::Device::layerSupported(psDevice, names[i]))
			return i;
	}

	return names.size();
}

void Auxiliary::Device::layersSupported(vk::PhysicalDevice psDevice, std::vector<char*>& names, std::vector<bool>& resVec) {
	for (int i = 0; i < names.size(); i++) {
		if (Auxiliary::Device::layerSupported(psDevice, names[i]))
			resVec.push_back(true);
		else
			resVec.push_back(false);
	}
}

bool Auxiliary::Device::checkPhysicalDeviceCapability(vk::PhysicalDevice psDevice, CreateInfo::Device& demands) {
	// check extensions support	
	int res = Auxiliary::Device::extensionsSupported(psDevice, demands.requieredExtensionNames);
	if (res < demands.requieredExtensionNames.size())
		return false; // psdevice nema vsechny pozadovane extensions 

	if (demands.useValidationLayers) { // pokud se maji pouzit validacni vrstvy
		// check layers support
		int res = Auxiliary::Device::layersSupported(psDevice, demands.requieredLayerNames);
		if (res < demands.requieredLayerNames.size())
			return false; // psdevice nema vsechny pozadovane layers
	}

	return true;
}

bool hf::Vk::Auxiliary::Device::checkGPUFeatures(vk::PhysicalDevice psDevice, vk::PhysicalDeviceFeatures featuresToCheck) {
	uint32_t len = sizeof(vk::PhysicalDeviceFeatures); // how many bools are in there
	// obtain psDevice features
	vk::PhysicalDeviceFeatures availableFeats;
	psDevice.getFeatures(&availableFeats);
	bool* avFeatsPtr = (bool*) &availableFeats;
	bool* reqFeatsPtr = (bool*) &featuresToCheck;

	for (int i = 0; i < len; i++) {
		// requiered Feature is not available on current GPU
		if (reqFeatsPtr[i] == true && reqFeatsPtr[i] != avFeatsPtr[i]) {
			return false;
		}
	}

	return true;
}

void hf::Vk::Device::featuresSupported(std::vector<vk::PhysicalDevice>& fromGPUs, vk::PhysicalDeviceFeatures reqFeatures) {
	std::vector<vk::PhysicalDevice> startVec(fromGPUs);
	fromGPUs.clear();
	for (auto& gpu : startVec) {
		if (Auxiliary::Device::checkGPUFeatures(gpu, reqFeatures))
			fromGPUs.push_back(gpu);
	}

}