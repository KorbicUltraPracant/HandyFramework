/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/Instance.inl>
#include <hfAx/Exception.h>

// for readability purpouse
using namespace hf::Vk;

/*  ----------------  Instance Auxiliary ----------------*/
void Auxiliary::Instance::enumeratePhysicalDevices(vk::Instance instance, std::vector<vk::PhysicalDevice>& devices) {
	uint32_t deviceCount;
	vk::Result res = instance.enumeratePhysicalDevices(&deviceCount, nullptr);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Instance::enumeratePhysicalDevices -> Cannot obtain physical device count");

	devices.resize(deviceCount);
	res = instance.enumeratePhysicalDevices(&deviceCount, devices.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Instance::enumeratePhysicalDevices -> Cannot obtain physical device info");
}

void Auxiliary::Instance::enumerateAvailableExtensions(std::vector<vk::ExtensionProperties>& props) {
	uint32_t extensionsCount;
	vk::Result res = vk::enumerateInstanceExtensionProperties(nullptr, &extensionsCount, nullptr);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Instance::enumerateAvailableExtensions -> Cannot obtain extensions count");

	props.resize(extensionsCount);
	res = vk::enumerateInstanceExtensionProperties(nullptr, &extensionsCount, props.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Instance::enumerateAvailableExtensions -> Cannot obtain extensions info");
}

void Auxiliary::Instance::enumerateAvailableLayers(std::vector<vk::LayerProperties>& props) {
	uint32_t layersCount;
	vk::Result res = vk::enumerateInstanceLayerProperties(&layersCount, nullptr);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Instance::enumerateAvailableLayers -> Cannot obtain layers count");

	props.resize(layersCount);
	res = vk::enumerateInstanceLayerProperties(&layersCount, props.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("Instance::enumerateAvailableLayers -> Cannot obtain layers info");
}

bool Auxiliary::Instance::extensionSupported(std::string name) {
	std::vector<vk::ExtensionProperties> props;
	Auxiliary::Instance::enumerateAvailableExtensions(props);
	for (auto entry : props) {
		if (!name.compare(entry.extensionName)) {
			return true;
		}
	}

	return false;
}

bool Auxiliary::Instance::layerSupported(std::string name) {
	std::vector<vk::LayerProperties> props;
	Auxiliary::Instance::enumerateAvailableLayers(props);
	for (auto entry : props) {
		if (!name.compare(entry.layerName)) {
			return true;
		}
	}

	return false;
}

void Auxiliary::Instance::extensionsSupported(std::vector<char*>& names, std::vector<bool>& resVec) {
	for (int i = 0; i < names.size(); i++) {
		if (Auxiliary::Instance::extensionSupported(names[i])) 
			resVec.push_back(true);
		else
			resVec.push_back(false);
	}
}

void Auxiliary::Instance::layersSupported(std::vector<char*>& names, std::vector<bool>& resVec) {
	for (int i = 0; i < names.size(); i++) {
		if (Auxiliary::Instance::layerSupported(names[i]))
			resVec.push_back(true);
		else
			resVec.push_back(false);
	}
}

VKAPI_ATTR vk::Bool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity, 
	VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData, void* pUserData) 
{
	std::cerr << "|#!|" << pCallbackData->pMessage << "|#|\n";
	return VK_FALSE;
}

VkResult createDebugUtilsMessengerEXT(VkInstance instance, const VkDebugUtilsMessengerCreateInfoEXT* pCreateInfo, const VkAllocationCallbacks* pAllocator, VkDebugUtilsMessengerEXT* pCallback) {
	auto func = (PFN_vkCreateDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT");
	if (func != nullptr) {
		return func(instance, pCreateInfo, pAllocator, pCallback);
	}
	else {
		return VK_ERROR_EXTENSION_NOT_PRESENT;
	}
}

void destroyDebugUtilsMessengerEXT(VkInstance instance, VkDebugUtilsMessengerEXT callback, const VkAllocationCallbacks* pAllocator) {
	auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(instance, "vkDestroyDebugUtilsMessengerEXT");
	if (func != nullptr) {
		func(instance, callback, pAllocator);
	}
}

/* ----------- Create Info Class -----------*/
hf::Vk::CreateInfo::Instance::operator vk::InstanceCreateInfo() {
	using namespace std;

	if (useValidationLayers) {
		enabledExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		enabledLayers.push_back("VK_LAYER_LUNARG_standard_validation");
	}

	appInfo = *this; // inicializuju pomocnou vk application info strukturu

	return vk::InstanceCreateInfo {
		vk::InstanceCreateFlags(0),
		&appInfo,
		(uint32_t) enabledLayers.size(),
		enabledLayers.data(),
		(uint32_t) enabledExtensions.size(),
		enabledExtensions.data(),
	};
}

/*  ----------------  Instance ----------------*/
Instance::Instance(vk::Instance& instance) {
	if (_instance != vk::Instance() || _description != nullptr)
		deinit();

	_instance = instance;

}

void Instance::init(CreateInfo::Instance& createInfo) {
	if (_instance != vk::Instance())
		return;

	vk::InstanceCreateInfo info = createInfo;
	vk::Result res = vk::createInstance(&info, nullptr, &_instance);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk Instance");

	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.abundantInfo)
		_description = new FurtherInfo::Instance(createInfo); // allokace heap struktury na heapu
	else 
		_description = new Info::Instance(createInfo); 


	if (_description->useValidationLayers) {
		VkDebugUtilsMessengerCreateInfoEXT debugCreate{};
		debugCreate.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		debugCreate.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		debugCreate.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		debugCreate.pfnUserCallback = debugCallback;

		VkResult res = createDebugUtilsMessengerEXT(_instance, &debugCreate, nullptr, &_debugCb);
		if (res != VK_SUCCESS)
			throw hf::Ax::Exception::Bad::Creation("hfVk Instance::DebugCallback");
	}
}

void hf::Vk::Instance::deinit() {
	if (_description == nullptr)
		return;

	if (_description->useValidationLayers)
		destroyDebugUtilsMessengerEXT(_instance, _debugCb, nullptr); // destroy debug callback object 

	if (_instance != vk::Instance())
		vkDestroyInstance(_instance, nullptr);

	// if here _description != nullptr
	delete _description; // uvolnim info structuru

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _instance = vk::Instance();
	
}

void Instance::move(Instance& withdraw) {
	if (_instance != vk::Instance() || _description != nullptr)
		deinit();

	// move ptr on resources
	_instance = withdraw._instance;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._instance = vk::Instance();
	withdraw._description = nullptr;

}