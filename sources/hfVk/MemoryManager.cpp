/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/MemoryManager.h>
#include <hfVk/MemoryPool.h>
#include <hfVk/Device.h>
#include <hfVk/MemoryBlock.h>
#include <hfVk/Image.h>
#include <hfVk/Buffer.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

void hf::Vk::MemoryManager::init(CreateInfo::MemoryManager& createInfo) {
	_description = new Info::MemoryManager(createInfo); 
}

void MemoryManager::deinit() {
	cleanUpStorage();
	if (_description != nullptr)
		delete _description;

}

vk::DeviceMemory MemoryManager::allocateMemory(vk::DeviceSize size, uint32_t memTypeIdx, vk::MemoryPropertyFlags memoryFlags) {
	vk::MemoryAllocateInfo allocInfo { 
		size,
		(uint32_t) memTypeIdx
	};
	
	vk::DeviceMemory memoryPtr;
	_description->pDevice->getVkHandle().allocateMemory(&allocInfo, nullptr, &memoryPtr);

	return memoryPtr;
}

void MemoryManager::releaseMemory(vk::DeviceMemory& memory) {
	if (memory != vk::DeviceMemory())
		_description->pDevice->getVkHandle().freeMemory(memory, nullptr);
}

void* MemoryManager::map(Info::MemoryBlock& memory) {
	// find the pool from which memory was allocated
	MemoryPool& pool = *memory.allocatedFrom;

	if (pool.getMemory() != vk::DeviceMemory() && (pool.getFlags() & vk::MemoryPropertyFlagBits::eDeviceLocal) != vk::MemoryPropertyFlagBits::eDeviceLocal) {
		void* dataPtr;
		// vkspecs state that memoryMapFlags must stay 0.
		_description->pDevice->getVkHandle().mapMemory(pool.getMemory(), memory.allocInfo.start, memory.allocInfo.size(), vk::MemoryMapFlags(0), &dataPtr);
		return dataPtr;
	}
	return nullptr;
}

void MemoryManager::unmap(Info::MemoryBlock& memory) {
	// find the pool from which memory was allocated
	MemoryPool& pool = *memory.allocatedFrom;

	if (pool.getMemory() != vk::DeviceMemory())
		_description->pDevice->getVkHandle().unmapMemory(pool.getMemory());

}

int MemoryManager::typeBitsToIndex(uint32_t typeBits, vk::MemoryPropertyFlags requirements) {
	vk::PhysicalDeviceMemoryProperties memProps;
	_description->pDevice->getPhysicalDevice().getMemoryProperties(&memProps);
	for (int i = 0; i < memProps.memoryTypeCount; i++) {
		if (((1 << i) & typeBits) && ((memProps.memoryTypes[i].propertyFlags & requirements) == requirements)) {
			return i;
		}
	}

	return -1;
}

void MemoryManager::assignMemoryToImage(Image& image, uint32_t allocationSize) {
	vk::MemoryRequirements memReqs = _description->pDevice->getVkHandle().getImageMemoryRequirements(image.getVkHandle());

	Info::MemoryRequierements reqs;
	reqs.memReqs = memReqs;
	reqs.allocationSize = allocationSize;
	reqs.memFlags = static_cast<Info::Image*>(image._description)->memFlags;

	bool res = allocateMemoryBlock(image._memoryInfo, reqs);

	vk::DeviceMemory bindTo = image._memoryInfo.allocatedFrom->getMemory();

	_description->pDevice->getVkHandle().bindImageMemory(image._image, bindTo, image._memoryInfo.allocInfo.start);
}

void MemoryManager::assignMemoryToBuffer(Buffer& buffer, uint32_t allocationSize) {
	vk::MemoryRequirements memReqs = _description->pDevice->getVkHandle().getBufferMemoryRequirements(buffer);

	Info::MemoryRequierements reqs;
	reqs.memReqs = memReqs;
	reqs.allocationSize = allocationSize;
	reqs.memFlags = buffer._description->propertyFlags;

	bool res = allocateMemoryBlock(buffer._memoryInfo, reqs);

	vk::DeviceMemory bindTo = buffer._memoryInfo.allocatedFrom->getMemory();

	return _description->pDevice->getVkHandle().bindBufferMemory(buffer._buffer, bindTo, buffer._memoryInfo.allocInfo.start);
}

void hf::Vk::MemoryManager::preAllocateMemory(Info::MemoryProperties info) {
	CreateInfo::MemoryPool crI;
	crI.memoryTypeIndex = info.memoryTypeIndex;
	crI.allocatedSpace = info.size;
	crI.unitSize = info.alignment;
	crI.propertyFlags = info.propertyFlags;

	insertPool(crI);
}

bool hf::Vk::MemoryManager::storeIntoGPU(Info::MemoryBlock & block) {
	return false;
}