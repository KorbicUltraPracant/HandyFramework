/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/RenderPass.inl>
#include <hfVk/Device.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

/* ------- Core Class ------- */
void RenderPass::init(CreateInfo::RenderPass& createInfo) {
	if (_renderPass != vk::RenderPass())
		return;

	vk::RenderPassCreateInfo rPCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createRenderPass(&rPCI, nullptr, &_renderPass);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk RenderPass");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::RenderPass(createInfo);
	else
		_description = new Info::RenderPass(createInfo);

}

void RenderPass::deinit() {
	// destroy vk buffer object
	if (_renderPass != vk::RenderPass())
		_description->pDevice->getVkHandle().destroyRenderPass(_renderPass, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _renderPass = vk::RenderPass();
}

void RenderPass::move(RenderPass& withdraw) {
	if (_renderPass != vk::RenderPass() || _description != nullptr)
		deinit();

	// move ptr on resources
	_renderPass = withdraw._renderPass;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._renderPass = vk::RenderPass();
	withdraw._description = nullptr;

}