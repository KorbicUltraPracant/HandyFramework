#include <hfVk/DescriptorSetLayout.h>
#include <hfVk/Device.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;
using namespace std;

/* ----------- Create Info Class ----------- */
CreateInfo::DescriptorSetLayout::operator vk::DescriptorSetLayoutCreateInfo() {
	return {
		vk::DescriptorSetLayoutCreateFlags(0),
		(uint32_t)layoutBindings.size(),
		layoutBindings.data()
	};
}

/* ------- Core Class ------- */
void DescriptorSetLayout::init(CreateInfo::DescriptorSetLayout& createInfo) {
	if (_layout != vk::DescriptorSetLayout())
		return;

	vk::DescriptorSetLayoutCreateInfo layoutCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createDescriptorSetLayout(&layoutCI, nullptr, &_layout);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk DescriptorSetLayout");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

							 // decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::DescriptorSetLayout(createInfo);
	else
		_description = new Info::DescriptorSetLayout(createInfo);
}

void DescriptorSetLayout::deinit() {
	// destroy vk object
	if (_layout != vk::DescriptorSetLayout())
		_description->pDevice->getVkHandle().destroyDescriptorSetLayout(_layout, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _layout = vk::DescriptorSetLayout();

}

void DescriptorSetLayout::move(DescriptorSetLayout & withdraw) {
	if (_layout != vk::DescriptorSetLayout() || _description != nullptr)
		deinit();

	// move ptr on resources
	_description = withdraw._description;
	_layout = withdraw._layout;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._layout = vk::DescriptorSetLayout();
	withdraw._description = nullptr;

}