/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/PipelineLayout.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

/* -------& createInfo Class ------- */
CreateInfo::PipelineLayout::operator vk::PipelineLayoutCreateInfo() {
	// convert descriptorSet's info into vkdescriptorsetlayout info
	descriptorLayouts.clear();
	for (auto& entry : pDescriptorSetLayouts) 
		descriptorLayouts.push_back(entry->getVkHandle());
	
	return {
		vk::PipelineLayoutCreateFlags(0),
		(uint32_t) descriptorLayouts.size(),
		descriptorLayouts.data(),
		(uint32_t) pushConstants.size(),
		pushConstants.data()
	};
}

/* ------- Core Class ------- */
void PipelineLayout::init(CreateInfo::PipelineLayout& createInfo) {
	if (_layout != vk::PipelineLayout())
		return;

	vk::PipelineLayoutCreateInfo layoutCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createPipelineLayout(&layoutCI, nullptr, &_layout);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk PipelineLayout");

	// allocate info struct memory
	if (_description != nullptr) 
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::PipelineLayout(createInfo);
	else
		_description = new Info::PipelineLayout(createInfo); 

}

void PipelineLayout::deinit() {
	// destroy vk object
	if (_layout != vk::PipelineLayout())
		_description->pDevice->getVkHandle().destroyPipelineLayout(_layout, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _layout = vk::PipelineLayout();
}

void PipelineLayout::move(PipelineLayout& withdraw) {
	if (_layout != vk::PipelineLayout() || _description != nullptr)
		deinit();

	// move ptr on resources
	_layout = withdraw._layout;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._layout = vk::PipelineLayout();
	withdraw._description = nullptr;

}