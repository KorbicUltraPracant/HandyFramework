/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/DesciptorsPool.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

/* ----------- Create Info Class ----------- */
CreateInfo::DescriptorPool::operator vk::DescriptorPoolCreateInfo() {
	return vk::DescriptorPoolCreateInfo(vk::DescriptorPoolCreateFlags(flags), maxSets, (uint32_t)allocateObjectsInfo.size(), allocateObjectsInfo.data());
}

/* ------- Core Class ------- */
void DescriptorPool::init(CreateInfo::DescriptorPool& createInfo) {
	if (_pool != vk::DescriptorPool())
		return;

	vk::DescriptorPoolCreateInfo poolCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createDescriptorPool(&poolCI, nullptr, &_pool);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk DescriptorPool");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::DescriptorPool(createInfo);

	// creating alloc map
	for (auto& entry : createInfo.allocateObjectsInfo) {
		auto x = _allocationAbleObjectsInfo.find(entry.type);
		if (x == _allocationAbleObjectsInfo.end())
			_allocationAbleObjectsInfo.emplace(entry.type, entry.descriptorCount);
		else
			_allocationAbleObjectsInfo[entry.type] += entry.descriptorCount;
	}

}

void hf::Vk::DescriptorPool::deinit() {
	// destroy vk buffer object
	if (_pool != vk::DescriptorPool())
		_description->pDevice->getVkHandle().destroyDescriptorPool(_pool, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _pool = vk::DescriptorPool(); _allocationAbleObjectsInfo.clear();
}