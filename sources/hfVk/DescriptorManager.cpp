/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/DescriptorManager.h>
#include <hfVk/DescriptorSet.inl>
#include <hfAx/Exception.h>
#include <unordered_map>

using namespace hf::Vk;

void hf::Vk::DescriptorManager::init(CreateInfo::DescriptorManager& createInfo) {
	if (_description != nullptr)
		return;

	_description = new Info::DescriptorManager(createInfo);

}

void hf::Vk::DescriptorManager::deinit() {
	for (auto& pool : _pools) 
		delete pool;
	
	if (_description != nullptr)
		delete _description;

	_pools.clear();
}

bool DescriptorManager::allocateDescriptorMemory(DescriptorSet& descriptorSet) {
	// searching for pool 
	for (unsigned i = 0; i < _pools.size(); i++) {
		bool suitablePool(true);
		// from which all descriptors can be allocated
		for (auto& memReq : descriptorSet._layoutMemReqs) {
			// pool has available at least memReq.second(count) resources of type memReq.first 
			if (!(memReq.second <= _pools[i]->_allocationAbleObjectsInfo[memReq.first])) {
				suitablePool = false;
				break; // not available in this poll jump to another one
			}
		}
		if (suitablePool == false)
			break; // jump to allocating pool

		// come into here? pool has available all resources
		descriptorSet._allocatedFrom = _pools[i];
		vk::DescriptorSetAllocateInfo allocInfo(*_pools[i], 1, &descriptorSet._mainLayout.getVkHandle());

		vk::Result res = _description->pDevice->getVkHandle().allocateDescriptorSets(&allocInfo, &descriptorSet._memory);
		if (res == vk::Result::eSuccess) {
			for (auto& memReq : descriptorSet._layoutMemReqs)
				_pools[i]->_allocationAbleObjectsInfo[memReq.first] -= memReq.second; // odectu pocet alokovanych resources

			return true;
		}
		else if (res != vk::Result::eErrorOutOfPoolMemory) // nejaka jina chyba nez nedostatek pameti v poolu, nedostatek pameti muze byt bud kvuli prekroceni povoleneho max set alloc nebo fragmentaci.
			return false;
	}
	
	/* 
		Suitable pool not found, must create new one...
	*/
	CreateInfo::DescriptorPool dpCI;
	dpCI.pDevice = _description->pDevice;
	dpCI.maxSets = _maxAllocationCountperPool; // assign it to draw worker threads
	dpCI.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
	for (auto& memReq : descriptorSet._layoutMemReqs) 
		dpCI.allocateObjectsInfo.push_back({ vk::DescriptorType(memReq.first), memReq.second * _maxAllocationCountperPool });
	
	_pools.push_back(new DescriptorPool(dpCI)); 
	
	descriptorSet._allocatedFrom = _pools.back(); 

	vk::DescriptorSetAllocateInfo allocInfo(*_pools.back(), 1, &descriptorSet._mainLayout.getVkHandle());
	vk::Result res = _description->pDevice->getVkHandle().allocateDescriptorSets(&allocInfo, &descriptorSet._memory);
	if (res == vk::Result::eSuccess) {
		for (auto& memReq : descriptorSet._layoutMemReqs)
			_pools.back()->_allocationAbleObjectsInfo[memReq.first] -= memReq.second; // odectu pocet alokovanych resources

		return true;
	}
	else
		return false;

	return false;
}

bool DescriptorManager::releaseDescriptorMemory(DescriptorSet& descriptorSet) {
	vk::Result res = _description->pDevice->getVkHandle().freeDescriptorSets(*descriptorSet._allocatedFrom, 1, &descriptorSet._memory);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("DescriptorManager::releaseDescriptorMemory -> Cannot free desciptor sets from pool");

	for (auto& memReq : descriptorSet._layoutMemReqs)
		descriptorSet._allocatedFrom->_allocationAbleObjectsInfo[memReq.first] += memReq.second;

	return true;
}