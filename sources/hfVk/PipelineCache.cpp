/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/PipelineCache.h>
#include <hfAx/FileOps.inl>
#include <hfAx/Exception.h>
#include <fstream>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* -------CreateInfoClass ------- */
CreateInfo::PipelineCache::operator vk::PipelineCacheCreateInfo() {
	// load cache data
	if (pathToData.size() > 0) { // want to load data from file
		if (!File::read(pathToData, _data))
			throw Exception::Bad::Load("Cannot obtain cache data");
	}

	return {
		vk::PipelineCacheCreateFlags(0),
		_data.size(),
		_data.data()
	};
}

/* ------- Core Class ------- */
void PipelineCache::init(CreateInfo::PipelineCache& createInfo) {
	if (_cache != vk::PipelineCache())
		return;

	vk::PipelineCacheCreateInfo pipecacheCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createPipelineCache(&pipecacheCI, nullptr, &_cache);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk PipelineCache");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::PipelineCache(createInfo);

}

void PipelineCache::deinit() {
	// destroy vk buffer object
	if (_cache != vk::PipelineCache())
		_description->pDevice->getVkHandle().destroyPipelineCache( _cache, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _cache = vk::PipelineCache();
}

bool hf::Vk::PipelineCache::store(std::string path) {
	fstream file(path, fstream::out | fstream::binary);
	if (!file.is_open())
		throw hf::Ax::Exception::Bad::Execution("PipelineCache::store -> Cannot open file");

	size_t dataSize;
	_description->pDevice->getVkHandle().getPipelineCacheData(_cache, &dataSize, nullptr);

	vector<uint8_t> data(dataSize);
	_description->pDevice->getVkHandle().getPipelineCacheData(_cache, &dataSize, data.data());

	file.write((char*) data.data(), dataSize);
	if (file.fail())
		throw hf::Ax::Exception::Bad::Execution("PipelineCache::store -> Can't write onto file");

	file.close();

	return true;
}