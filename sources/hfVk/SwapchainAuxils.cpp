/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/Swapchain.h>

using namespace std;
using namespace hf::Vk;

/* ------- Auxiliary Class ------- */
vk::SurfaceCapabilitiesKHR hf::Vk::Auxiliary::Swapchain::surfaceCapabilities(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface) {
	vk::SurfaceCapabilitiesKHR caps;
	psDevice.getSurfaceCapabilitiesKHR(surface, &caps);
	return caps;
}

bool Auxiliary::Swapchain::checkSurfaceFormatAvailability(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface, vk::SurfaceFormatKHR format) {
	uint32_t sfCount(0);
	psDevice.getSurfaceFormatsKHR(surface, &sfCount, nullptr);
	vector<vk::SurfaceFormatKHR> surfaceFormats(sfCount);
	psDevice.getSurfaceFormatsKHR(surface, &sfCount, surfaceFormats.data());

	for (auto x : surfaceFormats) {
		if ((x.colorSpace == format.colorSpace) && (x.format == format.format))
			return true;
	}

	return false;
}

vk::SurfaceFormatKHR Auxiliary::Swapchain::returnRandomAvailableSurfaceFormat(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface) {
	uint32_t sfCount(0);
	psDevice.getSurfaceFormatsKHR(surface, &sfCount, nullptr);
	vector<vk::SurfaceFormatKHR> surfaceFormats(sfCount);
	psDevice.getSurfaceFormatsKHR(surface, &sfCount, surfaceFormats.data());

	return surfaceFormats.front();
}

bool Auxiliary::Swapchain::checkPresentModeAvailability(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface, vk::PresentModeKHR mode) {
	uint32_t pmCount(0);
	psDevice.getSurfacePresentModesKHR(surface, &pmCount, nullptr);
	vector<vk::PresentModeKHR> presentModes(pmCount);
	psDevice.getSurfacePresentModesKHR(surface, &pmCount, presentModes.data());

	for (auto x : presentModes) {
		if (x == mode)
			return true;
	}

	// else
	return false;
}

vk::PresentModeKHR Auxiliary::Swapchain::returnRandomAvailablePresentMode(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface) {
	uint32_t pmCount(0);
	psDevice.getSurfacePresentModesKHR(surface, &pmCount, nullptr);
	vector<vk::PresentModeKHR> presentModes(pmCount);
	psDevice.getSurfacePresentModesKHR(surface, &pmCount, presentModes.data());

	return presentModes.front();
}

vk::DisplayPropertiesKHR hf::Vk::Auxiliary::Swapchain::singleDisplayProperties(vk::PhysicalDevice psDevice) {
	vk::DisplayPropertiesKHR displayProps;
	uint32_t pCount(0);
	psDevice.getDisplayPropertiesKHR(&pCount, nullptr);
	if (pCount == 1) {
		psDevice.getDisplayPropertiesKHR(&pCount, &displayProps);
		return displayProps;
	}
	displayProps.display = vk::DisplayKHR(); // sign invalid structure
	return displayProps;
}