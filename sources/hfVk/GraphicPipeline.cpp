/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/GraphicPipeline.h>
#include <hfVk/RenderPass.h>
#include <hfVk/PipelineCache.h>
#include <hfAx/Exception.h>
#include <utility>

using namespace hf::Vk;

/* -------CreateInfoClass ------- */
CreateInfo::GraphicPipeline::operator vk::PipelineVertexInputStateCreateInfo() {
	inputBinding.clear(); attriburesDescription.clear();
	// create input structures
	for (auto entry : vertexInput) {
		inputBinding.push_back(entry);
		attriburesDescription.push_back(entry);
	}
	
	return {
		vk::PipelineVertexInputStateCreateFlags(0),
		(uint32_t) inputBinding.size(),
		inputBinding.data(),
		(uint32_t) attriburesDescription.size(),
		attriburesDescription.data()
	};
}

CreateInfo::GraphicPipeline::operator vk::PipelineDynamicStateCreateInfo() {
	return {
		vk::PipelineDynamicStateCreateFlags(0),
		(uint32_t) dynamicStates.size(),
		dynamicStates.data()
	};
}

CreateInfo::GraphicPipeline::operator vk::GraphicsPipelineCreateInfo() {
	shaderStages.clear();
	shaderStages.resize(stagesInfo.size());
	for (unsigned i = 0; i < stagesInfo.size(); i++) 
		shaderStages[i] = stagesInfo[i];

	inputState = *this;
	assemblyState = primitivesAssembly;
	tessellationState = tessellation;
	viewportState = viewport;
	multisampleState = multisample;
	rasterizationState = rasterization;
	dynamicState = *this;
	colorBlendState = colorBlend;
	depthStencilState = depthStencil;

	return {
		flags,
		(uint32_t) stagesInfo.size(),
		shaderStages.data(),
		&inputState,
		&assemblyState,
		&tessellationState,
		&viewportState,
		&rasterizationState,
		&multisampleState,
		&depthStencilState,
		&colorBlendState,
		&dynamicState,
		*layout,
		*renderPass,
		usedForSubpass,
		basePipelineHandle,
		basePipelineIndex
	};
}

/* ------- Core Class ------- */
void GraphicPipeline::init(CreateInfo::GraphicPipeline& createInfo) {
	if (_pipeline != vk::Pipeline())
		return;

	vk::GraphicsPipelineCreateInfo pipeCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createGraphicsPipelines(createInfo.pipelineCache->getVkHandle(), 1, &pipeCI, nullptr, &_pipeline);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk GraphicPipeline");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::GraphicPipeline(createInfo);
	else
		_description = new Info::GraphicPipeline(createInfo);

}

void GraphicPipeline::deinit() {
	// destroy vk object
	if (_pipeline != vk::Pipeline())
		_description->pDevice->getVkHandle().destroyPipeline(_pipeline, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _pipeline = vk::Pipeline();
}

bool GraphicPipeline::rebuild(CreateInfo::GraphicPipeline*& createInfo) {
	// reset 
	deinit();

	// rebuild
	if (createInfo != nullptr)
		init(*createInfo);

	return true;
}

bool GraphicPipeline::renderPassCompatible(RenderPass& renderPassToCheck) {
	return false;
}

void hf::Vk::GraphicPipeline::move(GraphicPipeline& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_pipeline != vk::Pipeline() || _description != nullptr)
		deinit();

	// move ptr on resources
	_pipeline = withdraw._pipeline;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._pipeline = vk::Pipeline();
	withdraw._description = nullptr;

}

bool GraphicPipeline::assemble(vk::Pipeline& vkpipe, CreateInfo::GraphicPipeline& info) {
	if (_pipeline != vk::Pipeline() || _description != nullptr)
		deinit();

	_description = new Info::GraphicPipeline(info);
	_pipeline = vkpipe;

	return true;
}