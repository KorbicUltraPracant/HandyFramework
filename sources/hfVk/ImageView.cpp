/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/ImageView.h>
#include <hfVk/Image.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

/* -------CreateInfoClass ------- */
CreateInfo::ImageView::operator vk::ImageViewCreateInfo() {
	return {
		vk::ImageViewCreateFlags(0),
		image->getVkHandle(),
		viewType,
		format,
		componentMaping,
		subresourceRange
	};
}

/* ------- Core Class ------- */
void ImageView::init(CreateInfo::ImageView& createInfo) {
	if (_view != vk::ImageView())
		return;

	vk::ImageViewCreateInfo viewCI = createInfo;
	vk::Result res = createInfo.image->getDevice().getVkHandle().createImageView(&viewCI, nullptr, &_view);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk ImageView");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::ImageView(createInfo);

}

void ImageView::deinit() {
	// destroy vk buffer object
	if (_view != vk::ImageView())
		_description->image->getDevice().getVkHandle().destroyImageView(_view, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _view = vk::ImageView();

}

void ImageView::move(ImageView& withdraw) {
	if (_view != vk::ImageView() || _description != nullptr)
		deinit();

	// move ptr on resources
	_view = withdraw._view;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._view = vk::ImageView();
	withdraw._description = nullptr;

}