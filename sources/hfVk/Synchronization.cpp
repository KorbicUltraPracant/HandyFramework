/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/Synchronization.h>
#include <hfVk/Device.h>
#include <hfAx/Exception.h>
#include <stdlib.h> // sleep function

using namespace hf::Vk;

/* ------ Fence Core Class ------ */
CreateInfo::Fence::operator vk::FenceCreateInfo() {
	return {
		(createSignaled) ? vk::FenceCreateFlags(VK_FENCE_CREATE_SIGNALED_BIT) : vk::FenceCreateFlags(0)
	};
}

void Fence::init(CreateInfo::Fence& createInfo) {
	if (_fence != vk::Fence())
		return;

	vk::FenceCreateInfo info = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createFence(&info, nullptr, &_fence);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk Fence");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::Fence(createInfo);

}

void Fence::deinit() {
	// before do anything with fence, must wait until is not used by device anymore
	waitUntilSignaled();

	// destroy vk object
	if (_fence != vk::Fence())
		_description->pDevice->getVkHandle().destroyFence(_fence, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _fence = vk::Fence();
}

void Fence::move(Fence& withdraw) {
	// before do anything with fence, must wait until is not used by device anymore
	waitUntilSignaled();

	if (_fence != vk::Fence() || _description != nullptr)
		deinit();

	// move ptr on resources
	_fence = withdraw._fence;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._fence = vk::Fence();
	withdraw._description = nullptr;

}

vk::Fence Fence::passFenceToSignal() {
	_used = true;
	return _fence;
}

void Fence::waitUntilSignaled() {
	vk::Result res;
	if (_used) {
		res = _description->pDevice->getVkHandle().getFenceStatus(_fence);
		if (res == vk::Result::eNotReady)
			res = _description->pDevice->getVkHandle().waitForFences(1, &_fence, VK_TRUE, MAX_WAIT_TIMEOUT);
		
		else if (res != vk::Result::eSuccess) // device lost or something >> throw exepction
			throw hf::Ax::Exception::Bad::Execution("Fence::waitUntilSignaled -> WaitForFencesError");

		// reset signaled fence
		reset();
	}
}

void Fence::reset() {
	if (_used) {
		// reset signaled fence
		_description->pDevice->getVkHandle().resetFences(1, &_fence);

		// set it as unused
		_used = false;
	}
}

bool hf::Vk::Fence::isUsed() {
	// critical section
	//statusMutex.lock();

	vk::Result res = vk::Result::eSuccess;
	if (_used) {
		res = _description->pDevice->getVkHandle().getFenceStatus(_fence);
		if (res == vk::Result::eSuccess)
			reset(); // reset state
	}

	// out of critical section
	//statusMutex.unlock();

	return res == vk::Result::eNotReady; // not_ready -> not signaled so cmd buffs are still executed
}

bool hf::Vk::Fence::isSignaled() {
	vk::Result res = vk::Result::eSuccess;
	
	if (_used) {
		res = _description->pDevice->getVkHandle().getFenceStatus( _fence);
		if (res == vk::Result::eSuccess)
			reset(); // reset state
	}

	return res == vk::Result::eSuccess;
}

/* ------ Semaphore Core Class ------ */
CreateInfo::Semaphore::operator vk::SemaphoreCreateInfo() {
	return vk::SemaphoreCreateInfo();
}

void Semaphore::init(CreateInfo::Semaphore& createInfo) {
	if (_semaphore != vk::Semaphore())
		return;

	vk::SemaphoreCreateInfo info = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createSemaphore(&info, nullptr, &_semaphore);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk Semaphore");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::Semaphore(createInfo);

}

void Semaphore::deinit() {
	// destroy vk object
	if (_semaphore != vk::Semaphore())
		_description->pDevice->getVkHandle().destroySemaphore(_semaphore, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _semaphore = vk::Semaphore();
}

void Semaphore::move(Semaphore& withdraw) {
	if (_semaphore != vk::Semaphore() || _description != nullptr)
		deinit();

	// move ptr on resources
	_semaphore = withdraw._semaphore;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._semaphore = vk::Semaphore();
	withdraw._description = nullptr;

}

/* ------ Event Info Class ------ */
hf::Vk::CreateInfo::Event::operator vk::EventCreateInfo() {
	return vk::EventCreateInfo();
}

/* ------ Event Core Class ------ */
void hf::Vk::Event::init(CreateInfo::Event& createInfo) {
	if (_event != vk::Event())
		return;

	vk::EventCreateInfo info = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createEvent(&info, nullptr, &_event);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk Event");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::Event(createInfo);

	// always for insurance reset event
	createInfo.pDevice->getVkHandle().resetEvent(_event);

}

void hf::Vk::Event::deinit() {
	// destroy vk object
	if (_event != vk::Event())
		_description->pDevice->getVkHandle().destroyEvent(_event, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _event = vk::Event();
}

void hf::Vk::Event::move(Event& withdraw) {
	if (_event != vk::Event() || _description != nullptr)
		deinit();

	// move ptr on resources
	_event = withdraw._event;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._event = vk::Event();
	withdraw._description = nullptr;
}

bool hf::Vk::Event::isReset() {
	return _description->pDevice->getVkHandle().getEventStatus(_event) == vk::Result::eEventReset;
}

bool hf::Vk::Event::isSet() {
	return _description->pDevice->getVkHandle().getEventStatus(_event) == vk::Result::eEventSet;
}

void hf::Vk::Event::waitUntilSignaled() {
	vk::Result res;
	res = _description->pDevice->getVkHandle().getEventStatus(_event);
	if (res == vk::Result::eEventSet) {
		// active waiting :(
		while (_description->pDevice->getVkHandle().getEventStatus(_event) != vk::Result::eEventSet)
			hf::Ax::sleep(50); // space for experimenting to find suitable value...
		
	}
	else if (res != vk::Result::eEventReset) // device lost or something >> throw exepction
		throw hf::Ax::Exception::Bad::Execution("Event::waitUntilSignaled -> WaitForEventError");

}

void hf::Vk::Event::set() {
	_description->pDevice->getVkHandle().setEvent(_event);
}

void hf::Vk::Event::reset() {
	_description->pDevice->getVkHandle().resetEvent(_event);
}
