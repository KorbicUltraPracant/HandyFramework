#include <hfVk/DescriptorSet.inl>
#include <hfAx/Exception.h>
#include <utility>

using namespace hf::Vk;
using namespace std;

/* ------- Core Class ------- */
void DescriptorSet::init(CreateInfo::DescriptorSet& createInfo) {
	if (_memory != vk::DescriptorSet())
		return;

	// create main layout
	_mainLayout.describe(createInfo.mainLayoutInfo);

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::DescriptorSet(createInfo);

	// create _layoutMemReqs map -> requiered when allocating memory for set
	for (auto& entry : createInfo.mainLayoutInfo.layoutBindings) {
		auto x = _layoutMemReqs.find(entry.descriptorType);
		if (x == _layoutMemReqs.end())
			_layoutMemReqs.emplace(entry.descriptorType, entry.descriptorCount); // vytvori, dosadi
		else
			_layoutMemReqs[entry.descriptorType] += entry.descriptorCount; // aktualizuje hodnotu
	}

	// allocate memory
	if (!createInfo.pManager->allocateDescriptorMemory(*this))
		throw hf::Ax::Exception::Bad::Allocation("hfVk DescriptorSet's memory");

}

void DescriptorSet::deinit() {
	// release memory
	if (_memory != vk::DescriptorSet())
		_description->pManager->releaseDescriptorMemory(*this);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _memory = vk::DescriptorSet();
	_layoutMemReqs.clear(); _layoutViews.clear();

}

void DescriptorSet::move(DescriptorSet& withdraw) {
	if (_memory != vk::DescriptorSet() || _description != nullptr)
		deinit();

	// move ptr on resources
	_memory = withdraw._memory;
	_description = withdraw._description;
	_mainLayout = withdraw._mainLayout;
	_allocatedFrom = withdraw._allocatedFrom;
	_layoutMemReqs = std::move(withdraw._layoutMemReqs);

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._memory = vk::DescriptorSet();
	withdraw._description = nullptr;
	withdraw._allocatedFrom = nullptr;

}