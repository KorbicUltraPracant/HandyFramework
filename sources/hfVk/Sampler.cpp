#include <hfVk/Sampler.h>
#include <hfVk/Device.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

/* -------CreateInfoClass ------- */
CreateInfo::Sampler::operator vk::SamplerCreateInfo() {
	return { 
		vk::SamplerCreateFlags(0),
		magFilter,
		minFilter,
		mipmapMode,
		addressModeU,
		addressModeV,
		addressModeW,
		mipLodBias,
		anisotropyEnable,
		maxAnisotropy,
		compareEnable,
		compareOp,
		minLod,
		maxLod,
		borderColor,
		unnormalizedCoordinates
	};
}

/* ------- Core Class ------- */
void Sampler::init(CreateInfo::Sampler& createInfo) {
	if (_sampler != vk::Sampler())
		return;

	vk::SamplerCreateInfo samplerCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createSampler(&samplerCI, nullptr, &_sampler);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk Sampler");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::Sampler(createInfo);
	else
		_description = new Info::Sampler(createInfo);
}

void Sampler::deinit() {
	// destroy vk object
	if (_sampler != vk::Sampler())
		_description->pDevice->getVkHandle().destroySampler(_sampler, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _sampler = vk::Sampler();
}

void Sampler::move(Sampler& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_sampler != vk::Sampler() || _description != nullptr)
		deinit();

	// move ptr on resources
	_sampler = withdraw._sampler;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._sampler = vk::Sampler();
	withdraw._description = nullptr;

}