/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/CommandBuffer.inl>
#include <hfVk/Buffer.h>
#include <hfVk/Device.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

/* -------CreateInfoClass ------- */
CreateInfo::CommandBuffer::operator vk::CommandBufferAllocateInfo() {
	return vk::CommandBufferAllocateInfo(*pPool, level, 1);
}

/* ------- Core Class ------- */
void CommandBuffer::init(CreateInfo::CommandBuffer& createInfo) {
	if (_buffer != vk::CommandBuffer())
		return;

	vk::CommandBufferAllocateInfo cmdBuffCI = createInfo;
	vk::Result res = createInfo.pPool->getDescription().pDevice->getVkHandle().allocateCommandBuffers(&cmdBuffCI, &_buffer);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk CommandBuffer");

	// if secondary buffer, allocate aux structure
	if (createInfo.level == vk::CommandBufferLevel::eSecondary) {
		if (inheritanceInfo != nullptr)
			delete inheritanceInfo;

		inheritanceInfo = new vk::CommandBufferInheritanceInfo();
	}

	// allocate info struct memory
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::CommandBuffer(createInfo);

	// create lock
	if (_lock != nullptr)
		delete _lock;

	hf::Vk::CreateInfo::Fence fI;
	fI.pDevice = _description->pPool->getDescription().pDevice;
	_lock = new hf::Vk::Fence(fI);

}

void CommandBuffer::deinit() {
	// destroy vk buffer object
	if (_buffer != vk::CommandBuffer())
		_description->pPool->getDescription().pDevice->getVkHandle().freeCommandBuffers(*_description->pPool, 1, &_buffer);

	if (_description->level == vk::CommandBufferLevel::eSecondary)
		delete inheritanceInfo;

	// destroys info.
	if (_description != nullptr)
		delete _description;

	if (_lock != nullptr)
		delete _lock;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _buffer = vk::CommandBuffer(); _lock = nullptr;
}

void CommandBuffer::move(CommandBuffer& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_buffer != vk::CommandBuffer() || _description != nullptr)
		deinit();

	// move ptr on resources
	_buffer = withdraw._buffer;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._buffer = vk::CommandBuffer();
	withdraw._description = nullptr;
}

bool CommandBuffer::create(vk::CommandBuffer vkbuffer, CreateInfo::CommandBuffer& createInfo) {
	if (_buffer != vk::CommandBuffer() || _description != nullptr)
		deinit();

	_description = new Info::CommandBuffer(createInfo);
	_buffer = vkbuffer;

	return true;
}

/* Core Methods */
bool CommandBuffer::beginRecording(vk::CommandBufferUsageFlags usage) {
	vk::CommandBufferBeginInfo beginInfo(usage, inheritanceInfo);
	return _buffer.begin(&beginInfo) == vk::Result::eSuccess;
}

void CommandBuffer::adopt(vk::CommandBuffer buffer, CreateInfo::CommandBuffer& info) {
	if (_buffer != vk::CommandBuffer())
		return;

	// crate new description
	if (_description != nullptr)
		delete _description;
	
	_description = new Info::CommandBuffer(info);
	_buffer = buffer;

	if (_lock != nullptr)
		delete _lock;

	hf::Vk::CreateInfo::Fence fI;
	fI.pDevice = _description->pPool->getDescription().pDevice;
	_lock = new hf::Vk::Fence(fI);
}

void CommandBuffer::disinherit() {
	if (_description != nullptr) 
		delete _description;

	if (_lock != nullptr)
		delete _lock;

	_buffer = vk::CommandBuffer();
	_lock = nullptr;
	_description = nullptr;
}

void CommandBuffer::waitUntilExecutionFinished() {
	if (_lock == nullptr)
		return;

	_lock->waitUntilSignaled();
}

bool hf::Vk::CommandBuffer::isSubmited() {
	if (_lock == nullptr)
		return false;

	return _lock->isUsed();
}

/* Commands */
void CommandBuffer::bindDescriptorSets(Info::BindDescriptorSets info) {
	std::vector<vk::DescriptorSet> vksets;
	for (auto& set : info.pDescriptorSets)
		vksets.push_back(set->getVkHandle());

	_buffer.bindDescriptorSets(info.bindPoint, info.pPipelineLayout->getVkHandle(), info.firstSet, (uint32_t)vksets.size(), vksets.data(), info.dynamicOffsets.size(), (info.dynamicOffsets.size()) ? info.dynamicOffsets.data() : nullptr);
}