/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/Swapchain.h>
#include <hfAx/Exception.h>
#include <hfVk/MemoryManager.inl>
#include <hfVk/Image.inl>
#include <hfVk/CommandBuffer.inl>
#include <hfVk/CommandProcessor.h>
//#include <hfVk/RenderComponentsDB.h>

using namespace std;
using namespace hf::Vk;

/* ------- Auxiliary Class ------- */
hf::Vk::Info::PresentImage::operator vk::PresentInfoKHR() {
	if (imageIndexes.size() != waitSemaphores.size())
		throw hf::Ax::Exception::Bad::Execution("Info::PresentImage::operator VkPresentInfoKHR -> not every swapchain has and image assigned");

	vk::Result* resultPtr = nullptr;
	if (checkResults) {
		// resize results vector to result of every swapchain presentation is stored here
		results.clear();
		results.resize(swapchains.size());
		resultPtr = results.data();
	}

	return {
		(uint32_t) waitSemaphores.size(),
		waitSemaphores.data(),
		(uint32_t) swapchains.size(),
		swapchains.data(),
		imageIndexes.data(),
		resultPtr
	};
}

/* -------CreateInfoClass ------- */
CreateInfo::Swapchain::operator vk::SwapchainCreateInfoKHR() {
	return {
		vk::SwapchainCreateFlagBitsKHR(0),
		surface,
		minImageCount,
		surfaceFormat.format,
		surfaceFormat.colorSpace,
		{ width, height },
		imageArrayLayers,
		imageUsage,
		vk::SharingMode::eExclusive,
		0,
		nullptr,
		preTransform,
		compositeAlpha,
		presentMode,
		clipped,
		oldSwapchain
	};
}

/* ------- Core Class ------- */
void Swapchain::init(CreateInfo::Swapchain& createInfo) {
	if (_swapchain != vk::SwapchainKHR())
		return;

	vk::SwapchainCreateInfoKHR swapchainCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createSwapchainKHR(&swapchainCI, nullptr, &_swapchain);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk Swapchain");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::Swapchain(createInfo);

	initResources();
}

void Swapchain::deinit() {
	// clears hfVkimages
	returnImages();
	
	// destroy vk buffer object
	if (_swapchain != vk::SwapchainKHR())
		_description->pDevice->getVkHandle().destroySwapchainKHR(_swapchain, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _swapchain = vk::SwapchainKHR();

}

void Swapchain::obtainImages() {
	uint32_t imgCount(0);
	_description->pDevice->getVkHandle().getSwapchainImagesKHR(_swapchain, &imgCount, nullptr);
	
	_description->minImageCount = imgCount;

	vector<vk::Image> swapchainImgs(imgCount);
	_description->pDevice->getVkHandle().getSwapchainImagesKHR(_swapchain, &imgCount, swapchainImgs.data());

	for (auto& image: swapchainImgs) {
		_images.push_back(new SwapchainImage());
		// create empty hfVkimage
		_images.back()->_image = image; // assign ptr to vkimage
		_images.back()->_description = new Info::Image();
		_images.back()->_description->pDevice = _description->pDevice;
		_images.back()->_description->format = _description->surfaceFormat.format;

		// creating image views
		CreateInfo::ImageView viewCI;
		viewCI.setImage(&*_images.back())
		    .setViewType(vk::ImageViewType::e2D)
		    .setFormat(_description->surfaceFormat.format)
		    .setComponentMaping({ vk::ComponentSwizzle::eIdentity, })
		    .setSubresourceRange({ vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1 });

		_images.back()->_views.push_back(UPTR(ImageView, viewCI));
		// memory block must stay unitialized...
	}
}

void hf::Vk::Swapchain::returnImages() {
	// because hfVk Image objects are not allocated normaly but via swapchain object, swapchain object must also dealoce it
	for (auto& image : _images) {
		// delete views
		image->getViews().clear();

		// delete info
		if (image->_description != nullptr)
			delete image->_description;
	}

	_images.clear();
}

void hf::Vk::Swapchain::initResources() {
	// init sync components...
	CreateInfo::Semaphore semaphoreCI{ Info::Semaphore{ _description->pDevice } };
	_presentationLock.create(semaphoreCI);
	CreateInfo::Fence fenceCI{ Info::Fence{ _description->pDevice }, false };
	_obtainNextImageFence.create(fenceCI);

	// obtain images and create their views and framebuffers 
	obtainImages();

	// depthbuffer is created by user
	// framebuffers must be always created after renderpass!!!
}

void Swapchain::acquireNextImage() {
	VkFence waitFor = _obtainNextImageFence.getVkHandle();

	// acquire image with passive waiting
	_description->pDevice->getVkHandle().acquireNextImageKHR(_swapchain, MAX_WAIT_TIMEOUT, vk::Semaphore(), _obtainNextImageFence.passFenceToSignal(), &_currentImageIdx);
	
	// wait for acquire completed
	_obtainNextImageFence.waitUntilSignaled();

}

bool Swapchain::presentImage(hf::Vk::Queue& presentationQueue) {
	vk::PresentInfoKHR present_info{};
	present_info.waitSemaphoreCount = 0;
	present_info.pWaitSemaphores = nullptr;
	present_info.swapchainCount = 1;
	present_info.pSwapchains = &_swapchain;
	present_info.pImageIndices = &_currentImageIdx;
	present_info.pResults = VK_NULL_HANDLE;

	return presentationQueue.getVkHandle().presentKHR(&present_info) == vk::Result::eSuccess;
}

bool Swapchain::presentImage(Info::PresentImage& info) {
	vk::PresentInfoKHR tmp = info; 
	return info.presentationQueue->getVkHandle().presentKHR(&tmp) == vk::Result::eSuccess;
}

void Swapchain::changeResolution(uint32_t width, uint32_t height) {
	Info::Swapchain inheritInfo = *_description;
	CreateInfo::Swapchain swapchainCI(inheritInfo);
	swapchainCI.width = width;
	swapchainCI.height = height;
	swapchainCI.oldSwapchain = _swapchain; // current swapchain object

	Swapchain newSwapchain; // will be moved into old swapchain object
	newSwapchain.create(swapchainCI); // init new swapchain
	dismantle(); // this->dismantle ... delete old one
	*this = newSwapchain; // move new one over current one

}

void hf::Vk::Swapchain::move(Swapchain& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_swapchain != vk::SwapchainKHR() || _description != nullptr)
		deinit();

	// move ptr on resources
	_swapchain = withdraw._swapchain;
	_description = withdraw._description;
	
	// move resources
	_images.insert(_images.begin(), withdraw._images.begin(),withdraw._images.end());

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._swapchain = vk::SwapchainKHR();
	withdraw._description = nullptr;
	// clear ptr vectors
	withdraw._images.clear(); 

}

void Swapchain::clearColors(CommandBuffer& keepCommands, CommandProcessor& execCommands) {
	keepCommands.beginRecording();
	//_depthbuffer->clearDepthStencil({ 1.0f, 0, }, keepCommands);
	_images[_currentImageIdx]->clearColor(std::array<float, 4>{0.7f, 0.7f, 0.7f, 1.0f}, keepCommands);
	keepCommands.finishRecording();

	Info::PostSingleBuffer postI;
	postI.pBuffer = &keepCommands;
	execCommands.flushCmdBuffer(postI, &keepCommands.getLock());

	// because data ops need to wait until execution is finished
	keepCommands.waitUntilExecutionFinished();
}