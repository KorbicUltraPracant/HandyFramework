/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/Buffer.h>
#include <hfVk/MemoryManager.inl>
#include <hfVk/CommandBuffer.inl>
#include <hfVk/CommandProcessor.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* -------CreateInfoClass ------- */
CreateInfo::Buffer::Buffer(CreateInfo::Buffer::Usage bufferUsage, vk::MemoryPropertyFlagBits locationFlags) {
	switch (bufferUsage) {
		case CreateInfo::Buffer::Usage::VERTEXBUFFER: {
			usage = vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eTransferDst;
			propertyFlags = vk::MemoryPropertyFlagBits::eDeviceLocal;
		} break;
		case CreateInfo::Buffer::Usage::INDEXBUFFER: {
			usage = vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferDst;
			propertyFlags = vk::MemoryPropertyFlagBits::eDeviceLocal;
		} break; 
		case CreateInfo::Buffer::Usage::STAGGINGBUFFER: {
			usage = vk::BufferUsageFlagBits::eTransferSrc | vk::BufferUsageFlagBits::eTransferDst;
			propertyFlags = vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent;
		} break;
		case CreateInfo::Buffer::Usage::SHADERSTORAGE: {
			usage = vk::BufferUsageFlagBits::eStorageBuffer;
			propertyFlags = locationFlags;
		} break;
		case CreateInfo::Buffer::Usage::INDEXEDINDIRECTDRAW: {
			usage = vk::BufferUsageFlagBits::eIndirectBuffer | vk::BufferUsageFlagBits::eTransferDst;
			propertyFlags = vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent;
		} break;
		case CreateInfo::Buffer::Usage::UNIFORMBUFFER: {
			usage = vk::BufferUsageFlagBits::eUniformBuffer;
			propertyFlags = vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent;
		} break;
		default:
			break;
	}
}

CreateInfo::Buffer::operator vk::BufferCreateInfo() {
	return vk::BufferCreateInfo(vk::BufferCreateFlags(0), size, vk::BufferUsageFlags(usage));
}

/* ------- Core Class ------- */
void Buffer::init(CreateInfo::Buffer& createInfo) {
	if (_buffer != vk::Buffer())
		return; // buffer uz je inicializovan -> return
	
	vk::BufferCreateInfo buffCI = createInfo;
	vk::Result res = createInfo.pManager->getDevice().getVkHandle().createBuffer(&buffCI, nullptr, &_buffer);
	if (res != vk::Result::eSuccess)	
		throw Exception::Bad::Creation("hfVk Buffer");
	
	// allocate info struct memory
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::Buffer(createInfo);

	// allocate Vk memory for buffer
	if (createInfo.assignMemory)
		_description->pManager->assignMemoryToBuffer(*this, createInfo.allocationSize);

}

void Buffer::deinit() {
	// release memory block
	if (_memoryInfo.allocatedFrom != nullptr)
		_description->pManager->releaseBufferMemory(*this);

	// destroy vk buffer object
	if (_buffer != vk::Buffer())
		vkDestroyBuffer(_description->pManager->getDevice().getVkHandle(), _buffer, nullptr);

	// destroys info.
	if (_description == nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _buffer = vk::Buffer(); // init to VK_NULL_HANDLE
}

void* Buffer::map() {
	if ((_description->propertyFlags & vk::MemoryPropertyFlagBits::eHostVisible) == vk::MemoryPropertyFlagBits::eHostVisible)
		return _description->pManager->map(_memoryInfo);
	// else 
	return nullptr;
}

void Buffer::unmap() {
	if ((_description->propertyFlags & vk::MemoryPropertyFlagBits::eHostVisible) == vk::MemoryPropertyFlagBits::eHostVisible)
		_description->pManager->unmap(_memoryInfo);
}

void Buffer::move(Buffer& withdraw) {
	// if object has allocated some resources, they has to be cleaned
	if (_buffer != vk::Buffer() || _memoryInfo.allocatedFrom != nullptr || _description != nullptr)
		deinit();

	// move ptr on resources
	_buffer = withdraw._buffer;
	_description = withdraw._description;
	_memoryInfo = withdraw._memoryInfo;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._buffer = vk::Buffer();
	withdraw._description = nullptr;
	withdraw._memoryInfo.allocatedFrom = nullptr; 

}

void Buffer::copyMemory(Buffer& data, CommandBuffer& keepCommands, vk::BufferCopy* copyInfo) {
	uint32_t requieredSize = data._memoryInfo.allocInfo.size();
	// need to allocate more space
	if (requieredSize > _memoryInfo.allocInfo.size()) {
		CreateInfo::Buffer bufferCI;
		bufferCI.size = requieredSize;
		bufferCI.pManager = _description->pManager;
		bufferCI.propertyFlags = _description->propertyFlags;
		bufferCI.usage = _description->usage;

		// release attributes
		deinit();

		// init with new size
		init(bufferCI);
	}

	// data copy here is guaranteed that source buffer will fit inside this buffer memory...
	if (copyInfo == nullptr) {
		vk::BufferCopy copyRegion;
		copyRegion.srcOffset = 0;
		copyRegion.dstOffset = 0;
		copyRegion.size = requieredSize;
		keepCommands.getVkHandle().copyBuffer(data, *this, 1, &copyRegion);
	}
	else 
		keepCommands.getVkHandle().copyBuffer(data, *this, 1, copyInfo);

}

void Buffer::stashOnGPU(vector<uint8_t>& data, CommandBuffer& keepCommands, CommandProcessor& proccessCommands, uint32_t dstBufferOffset) {
	stashOnGPU(data.data(), data.size(), keepCommands, proccessCommands, dstBufferOffset);
}

void hf::Vk::Buffer::stashOnGPU(void* data, uint32_t dataLenInBytes, CommandBuffer& keepCommands, CommandProcessor& proccessCommands, uint32_t dstBufferOffset) {
	// unitialized object? -> return
	if (_buffer == vk::Buffer() || _description == nullptr)
		return;

	CreateInfo::Buffer bCI;
	bCI.pManager = _description->pManager;
	bCI.size = dataLenInBytes;
	bCI.propertyFlags = vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent;
	bCI.usage = vk::BufferUsageFlagBits::eTransferSrc | vk::BufferUsageFlagBits::eTransferDst;
	// create stagging buffer
	Buffer staggingBuffer(bCI);
	// recreate this buffer object if necessary
	if (dataLenInBytes > _description->size || ((_description->propertyFlags& vk::MemoryPropertyFlagBits::eDeviceLocal) != vk::MemoryPropertyFlagBits::eDeviceLocal)) {
		bCI = *_description;
		bCI.size = dataLenInBytes;
		bCI.propertyFlags &= vk::MemoryPropertyFlagBits::eHostVisible; // disable option for host visible memory
		bCI.propertyFlags |= vk::MemoryPropertyFlagBits::eDeviceLocal;
		// to inherited flags adding obligatory one
		bCI.usage |= vk::BufferUsageFlagBits::eTransferDst;
		// destroy and reinit object attribs...
		deinit();
		init(bCI);
	}
	// copy data to stagging buffer
	void* sbPtr = staggingBuffer.map();
	memcpy(sbPtr, data, dataLenInBytes);
	staggingBuffer.unmap();

	// copy from stagging buffer to this object stored in GPU local heap.
	keepCommands.waitUntilExecutionFinished();
	keepCommands.beginRecording();
	vk::BufferCopy copyInfo;
	copyInfo.dstOffset = dstBufferOffset;
	copyInfo.srcOffset = 0;
	copyInfo.size = dataLenInBytes;
	copyMemory(staggingBuffer, keepCommands, &copyInfo);
	keepCommands.finishRecording();

	// pass cmd buff into queue
	proccessCommands.flushCmdBuffer(keepCommands);

	// must wait until copy executes, because stagging buffer
	keepCommands.waitUntilExecutionFinished();

	// here should be object filled with data
}

void Buffer::getMemoryProperties(Info::MemoryProperties& result) {
	VkMemoryRequirements reqs;
	vkGetBufferMemoryRequirements(getDevice().getVkHandle(), _buffer, &reqs);

	result.size = reqs.size;
	result.memoryTypeIndex = _description->pManager->typeBitsToIndex(reqs.memoryTypeBits, _description->propertyFlags);
	result.alignment = reqs.alignment;
	result.propertyFlags = _description->propertyFlags;
}

void hf::Vk::Buffer::assignMemory() {
	_description->pManager->assignMemoryToBuffer(*this);
}