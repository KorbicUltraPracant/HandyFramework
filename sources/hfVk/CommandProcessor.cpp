/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/CommandProcessor.h>
#include <hfVk/CommandBuffer.inl>
#include <hfVk/Fence.h>
#include <hfAx/Exception.h>
#include <unordered_map>

using namespace std;
using namespace hf::Vk;

/* Info Classes */
Info::PostSingleBuffer::operator vk::SubmitInfo() {
	return {
		(uint32_t)waitOn.size(),
		(!waitOn.size()) ? nullptr : waitOn.data(),
		insideStages.data(),
		1,
		&pBuffer->getVkHandle(),
		(uint32_t)signalUponFinish.size(),
		(!signalUponFinish.size()) ? nullptr : signalUponFinish.data()
	};
}

/* Core */
void CommandProcessor::init(CreateInfo::CommandProcessor& createInfo) {
	if (_description != nullptr)
		delete _description;

	_description = new Info::CommandProcessor(createInfo);

}

void CommandProcessor::deinit() {
	if (_description != nullptr)
		delete _description;
}

vk::Queue CommandProcessor::findQueueToSubmit(uint32_t queueFamilyIdx) {
	auto queues = _description->pDevice->getQueueHandles(queueFamilyIdx);
	uint32_t idx = (queues.lastUsedQueue+1) % queues.availableQueues.size();
	return *queues.availableQueues[idx]; // converts object to vk object
}

// All buffers all posted into single queue
bool CommandProcessor::singleQueueCommandsPost(Info::PostCommandBuffers& buffers, Fence* signalUponFinish) {
	vector<vk::SubmitInfo> submitInfos;
	// create submit info for every command buffer
	for (auto execInfo : buffers.bulk) 
		submitInfos.push_back(execInfo);

	// all buffers are allocated from same pool -> have same QfamilyIdx
	vk::Queue submitTo = findQueueToSubmit(buffers.bulk.front().pBuffer->getFamilyIndex());

	return submitTo.submit((uint32_t)submitInfos.size(), submitInfos.data(), signalUponFinish->getVkHandle()) == vk::Result::eSuccess;
}

bool hf::Vk::CommandProcessor::multiQueueCommandsPost(Info::PostCommandBuffers& buffers) {
	vector<vk::SubmitInfo> submitInfos(buffers.bulk.size());
	// create submit info for every command buffer
	for (unsigned i = 0; i < buffers.bulk.size(); i++)
		submitInfos[i] = buffers.bulk[i];

	// Here obtain handles to all available queues and distribute execution of cmd buffers among them
	uint32_t submitStatus = (uint32_t) vk::Result::eSuccess; // if stays zero all submits were succesfull else -> some fault
	Info::QueueFamily queues = _description->pDevice->getQueueHandles(buffers.bulk.front().pBuffer->getFamilyIndex());

	// find proper balance between available queues and cmd buffers to be posted -> currently optimaly one cmd buff per queue
	uint32_t submitsPerQueue = submitInfos.size() / queues.availableQueues.size();
	// if submitInfos.size() > queues.availableQueues.size() -> use all queues, else use as much queues as submitInfos
	uint32_t useQueues = (submitsPerQueue > 0) ? (uint32_t)queues.availableQueues.size() : (uint32_t)submitInfos.size();
	submitsPerQueue = (submitsPerQueue > 0) ? submitsPerQueue : 1;

	for (unsigned i = 0; i < useQueues; i++) {
		uint32_t alreadySubmited = i * submitsPerQueue;
		// if is to submit less cmd buffer then it is submitsPerQeues
		uint32_t submitsNow = ((submitsPerQueue + alreadySubmited) > submitInfos.size()) ? submitInfos.size() - alreadySubmited : submitsPerQueue;
		submitStatus |= (uint32_t)queues.availableQueues[i]->getVkHandle().submit(submitsNow, submitInfos.data() + alreadySubmited, vk::Fence());
	}

	return submitStatus == (uint32_t) vk::Result::eSuccess;
}

bool CommandProcessor::multiQueueCommandsPost(Info::PostCommandBuffers& buffers, std::vector<Fence*>& pFenceVec) {
	vector<vk::SubmitInfo> submitInfos(buffers.bulk.size());
	// create submit info for every command buffer
	for (unsigned i = 0; i < buffers.bulk.size(); i++)
		submitInfos[i] = buffers.bulk[i];
	
	// Here obtain handles to all available queues and distribute execution of cmd buffers among them
	uint32_t submitStatus = (uint32_t)vk::Result::eSuccess; // if stays zero all submits were succesfull else -> some fault
	Info::QueueFamily queues = _description->pDevice->getQueueHandles(buffers.bulk.front().pBuffer->getFamilyIndex());
	if (queues.availableQueues.size() > pFenceVec.size())
		throw hf::Ax::Exception::Bad::Execution("CommandProcessor::multiQueueCommandsPost -> smaller FenceVector than actual Queues Count");

	// find proper balance between available queues and cmd buffers to be posted -> currently optimaly one cmd buff per queue
	uint32_t submitsPerQueue = uint32_t(submitInfos.size() / queues.availableQueues.size());
	// if submitInfos.size() > queues.availableQueues.size() -> use all queues, else use as much queues as submitInfos
	uint32_t useQueues = (submitsPerQueue > 0) ? queues.availableQueues.size() : submitInfos.size();
	submitsPerQueue = (submitsPerQueue > 0) ? submitsPerQueue : 1;

	for (unsigned i = 0; i < useQueues; i++) {
		uint32_t alreadySubmited = i * submitsPerQueue;
		// if is to submit less cmd buffer then it is submitsPerQeues
		uint32_t submitsNow = ((submitsPerQueue + alreadySubmited) > submitInfos.size()) ? submitInfos.size() - alreadySubmited : submitsPerQueue;
		submitStatus |= (uint32_t)queues.availableQueues[i]->getVkHandle().submit(submitsNow, submitInfos.data() + alreadySubmited, vk::Fence());
		// set fences ptr to cmd buffers to they can wait for...
		for (int j = 0; j < submitsNow; j++) {
			if (buffers.bulk[alreadySubmited + j].pBuffer->getLock().getVkHandle() != vk::Fence())
				buffers.bulk[alreadySubmited + j].pBuffer->getLock() = *pFenceVec[i];
		}
			
	}

	return submitStatus == (uint32_t)vk::Result::eSuccess;
}

bool CommandProcessor::flushCmdBuffer(Info::PostSingleBuffer& info, Fence* signalUponFinish) {
	vk::SubmitInfo submitInfo {
		(uint32_t)info.waitOn.size(),
		info.waitOn.data(),
		info.insideStages.data(),
		1,
		&info.pBuffer->getVkHandle(),
		(uint32_t)info.signalUponFinish.size(),
		info.signalUponFinish.data()
	};
	
	vk::Queue submitTo = findQueueToSubmit(info.pBuffer->getFamilyIndex());
	return submitTo.submit(1, &submitInfo, (signalUponFinish != nullptr) ? signalUponFinish->passFenceToSignal() : vk::Fence()) == vk::Result::eSuccess;
}

bool CommandProcessor::flushCmdBuffer(hf::Vk::CommandBuffer& buffer) {
	Info::PostSingleBuffer info;
	info.pBuffer = &buffer;

	vk::SubmitInfo submitInfo{
		(uint32_t)info.waitOn.size(),
		info.waitOn.data(),
		info.insideStages.data(),
		1,
		&info.pBuffer->getVkHandle(),
		(uint32_t)info.signalUponFinish.size(),
		info.signalUponFinish.data()
	};

	vk::Queue submitTo = findQueueToSubmit(info.pBuffer->getFamilyIndex());
	return submitTo.submit(1, &submitInfo, buffer.getLock().passFenceToSignal()) == vk::Result::eSuccess;
}