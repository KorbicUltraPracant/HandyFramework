/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/Framebuffer.h>
#include <hfVk/RenderPass.h>
#include <hfVk/ImageView.h>
#include <hfVk/Device.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

/* -------CreateInfoClass ------- */
CreateInfo::Framebuffer::operator vk::FramebufferCreateInfo() {
	_createInfoAttachmentRefs.clear();
	// covnert intern imagesview into VkImagesView
	for (auto entry : attachmentRefs)
		_createInfoAttachmentRefs.push_back(*entry); // operator ImageView::VkImageView()

	return {
		vk::FramebufferCreateFlags(0),
		activeRenderPass->getVkHandle(),
		(uint32_t) _createInfoAttachmentRefs.size(),
		_createInfoAttachmentRefs.data(),
		width,
		height,
		layersCount
	};
}

/* ------- Core Class ------- */
void Framebuffer::init(CreateInfo::Framebuffer& createInfo) {
	if (_framebuffer != vk::Framebuffer())
		return;

	vk::FramebufferCreateInfo framebuffCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createFramebuffer(&framebuffCI, nullptr, &_framebuffer);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk Framebuffer");

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	// decision to alloc futher info struct or just info
	if (createInfo.pDevice->abundantInfo())
		_description = new FurtherInfo::Framebuffer(createInfo);
	else
		_description = new Info::Framebuffer(createInfo);

}

void Framebuffer::deinit() {
	// destroy vk buffer object
	if (_framebuffer != vk::Framebuffer())
		_description->pDevice->getVkHandle().destroyFramebuffer(_framebuffer, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _framebuffer = vk::Framebuffer();
}

void Framebuffer::move(Framebuffer& withdraw) {
	if (_framebuffer != vk::Framebuffer() || _description != nullptr)
		deinit();

	// move ptr on resources
	_framebuffer = withdraw._framebuffer;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._framebuffer = vk::Framebuffer();
	withdraw._description = nullptr;

}