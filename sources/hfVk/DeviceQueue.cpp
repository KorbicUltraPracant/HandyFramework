/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/DeviceQueues.h>
#include <hfVk/Device.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

void Queue::init(CreateInfo::Queue& createInfo) {
	// obtain queue
	createInfo.pDevice->getVkHandle().getQueue(createInfo.queueFamilyIndex, createInfo.allocationIndex, &_queue);
	
	if (_description != nullptr)
		delete _description;

	_description = new Info::Queue(createInfo);

}

void hf::Vk::Queue::deinit() {
	// destroys info.
	if (_description != nullptr)
		delete _description;
}