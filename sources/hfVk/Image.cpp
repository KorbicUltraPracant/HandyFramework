/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/ImageInfo.h>
#include <hfVk/MemoryManager.inl>
#include <hfVk/CommandBuffer.inl>
#include <hfVk/CommandProcessor.h>
#include <hfAx/Exception.h>

using namespace std;
using namespace hf::Vk;

/* ------- MemoryBarrier ------- */
Info::Image::MemoryBarrier::operator vk::ImageMemoryBarrier() {
	return {
		srcAccessMask,
		dstAccessMask,
		oldLayout,
		newLayout,
		srcQueueFamilyIndex,
		dstQueueFamilyIndex,
		image,
		subresourceRange
	};
}

/* ------- ChangeLayout ------- */
Info::Image::ChangeLayout::ChangeLayout(TransferOp operation) {
	switch (operation) {
		case COLOR_ATTACHMENT: {
			oldLayout = vk::ImageLayout::eUndefined;
			newLayout = vk::ImageLayout::eColorAttachmentOptimal;
			srcAccessMask = vk::AccessFlagBits(0);
			dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			producentStage = vk::PipelineStageFlagBits::eTopOfPipe;
			consumentStage = vk::PipelineStageFlagBits::eColorAttachmentOutput;

		} break;

		case LOADING_TEX: {
			oldLayout = vk::ImageLayout::eUndefined;
			newLayout = vk::ImageLayout::eTransferDstOptimal;
			srcAccessMask = vk::AccessFlagBits(0);
			dstAccessMask = vk::AccessFlagBits::eTransferWrite;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			producentStage = vk::PipelineStageFlagBits::eTopOfPipe;
			consumentStage = vk::PipelineStageFlagBits::eTransfer;

		} break;

		case USE_TEX: {
			oldLayout = vk::ImageLayout::eTransferDstOptimal;
			newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
			srcAccessMask = vk::AccessFlagBits::eTransferWrite;
			dstAccessMask = vk::AccessFlagBits::eShaderRead;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			producentStage = vk::PipelineStageFlagBits::eTransfer;
			consumentStage = vk::PipelineStageFlagBits::eFragmentShader;

		} break;

		case IMG_INIT: {
			oldLayout = vk::ImageLayout::eUndefined;
			newLayout = vk::ImageLayout::ePresentSrcKHR;
			srcAccessMask = vk::AccessFlagBits(0);
			dstAccessMask = vk::AccessFlagBits::eMemoryRead;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			producentStage = vk::PipelineStageFlagBits::eTopOfPipe;
			consumentStage = vk::PipelineStageFlagBits::eTransfer;

		} break;

		case PRE_CLEAR_IMG: {
			oldLayout = vk::ImageLayout::ePresentSrcKHR;
			newLayout = vk::ImageLayout::eTransferDstOptimal;
			srcAccessMask = vk::AccessFlagBits::eMemoryRead;
			dstAccessMask = vk::AccessFlagBits::eTransferWrite;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			producentStage = vk::PipelineStageFlagBits::eTopOfPipe;
			consumentStage = vk::PipelineStageFlagBits::eTransfer;

		} break;

		case POST_CLEAR_IMG: {
			oldLayout = vk::ImageLayout::eTransferDstOptimal;
			newLayout = vk::ImageLayout::ePresentSrcKHR;
			srcAccessMask = vk::AccessFlagBits::eTransferWrite;
			dstAccessMask = vk::AccessFlagBits::eMemoryRead;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			producentStage = vk::PipelineStageFlagBits::eTransfer;
			consumentStage = vk::PipelineStageFlagBits::eTransfer;

		} break;

		case PRE_CLEAR_COLATT: {
			oldLayout = vk::ImageLayout::eColorAttachmentOptimal;
			newLayout = vk::ImageLayout::eTransferDstOptimal;
			srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
			dstAccessMask = vk::AccessFlagBits::eTransferWrite;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			producentStage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
			consumentStage = vk::PipelineStageFlagBits::eTransfer;

		} break;

		case POST_CLEAR_COLATT: {
			oldLayout = vk::ImageLayout::eTransferDstOptimal;
			newLayout = vk::ImageLayout::eColorAttachmentOptimal;
			srcAccessMask = vk::AccessFlagBits::eTransferWrite;
			dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			producentStage = vk::PipelineStageFlagBits::eTransfer;
			consumentStage = vk::PipelineStageFlagBits::eColorAttachmentOutput;

		} break;

		case DEPTH_TEST: {
			oldLayout = vk::ImageLayout::eUndefined;
			newLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
			srcAccessMask = vk::AccessFlagBits(0);
			dstAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentRead;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth; 
			producentStage = vk::PipelineStageFlagBits::eTopOfPipe;
			consumentStage = vk::PipelineStageFlagBits::eEarlyFragmentTests;

		} break;

		case PRE_CLEAR_DEPTH_A: {
			oldLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
			newLayout = vk::ImageLayout::eTransferDstOptimal;
			srcAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentRead;
			dstAccessMask = vk::AccessFlagBits::eTransferWrite;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth; 
			producentStage = vk::PipelineStageFlagBits::eEarlyFragmentTests;
			consumentStage = vk::PipelineStageFlagBits::eTransfer;

		} break;

		case POST_CLEAR_DEPTH_A: {
			oldLayout = vk::ImageLayout::eTransferDstOptimal;
			newLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
			srcAccessMask = vk::AccessFlagBits::eTransferWrite;
			dstAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentRead;
			subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
			producentStage = vk::PipelineStageFlagBits::eTransfer;
			consumentStage = vk::PipelineStageFlagBits::eEarlyFragmentTests;

		} break;
	}
}


vk::AccessFlags Image::layoutToFlags(vk::ImageLayout layout) {
	switch (layout) {
	case vk::ImageLayout::ePreinitialized:
		return vk::AccessFlagBits::eHostWrite;

	case vk::ImageLayout::eTransferDstOptimal:
		return vk::AccessFlagBits::eTransferWrite;

	case vk::ImageLayout::eTransferSrcOptimal:
		return vk::AccessFlagBits::eTransferRead;

	case vk::ImageLayout::eColorAttachmentOptimal:
		return vk::AccessFlagBits::eColorAttachmentWrite;

	case vk::ImageLayout::eDepthStencilAttachmentOptimal:
		return vk::AccessFlagBits::eDepthStencilAttachmentWrite;

	case vk::ImageLayout::eShaderReadOnlyOptimal:
		return vk::AccessFlagBits::eShaderRead | vk::AccessFlagBits::eInputAttachmentRead;

	case vk::ImageLayout::ePresentSrcKHR:
		return vk::AccessFlagBits::eMemoryRead;
	}
	return vk::AccessFlags(0);
}

/* -------CreateInfoClass ------- */
CreateInfo::Image::Image(CreateInfo::Image::Usage imgUsage, uint32_t width, uint32_t height, bool useMipmap, bool deviceLocal) {
	// keeps the same for the next ones
	type = vk::ImageType::e2D;
	memFlags = (deviceLocal) ? vk::MemoryPropertyFlagBits::eDeviceLocal : vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent;
	this->width = width;
	this->height = height;

	switch (imgUsage) {
		case CreateInfo::Image::Usage::COLORATTACHMENT: {
			usage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eStorage;
			//resourceDescription.aspectMask = vk::ImageAspectFlagBits::eColor;
		} break;
		case CreateInfo::Image::Usage::COLORBUFFER: {
			usage = vk::ImageUsageFlagBits::eColorAttachment;
			//resourceDescription.aspectMask = vk::ImageAspectFlagBits::eColor;
		} break;
		case CreateInfo::Image::Usage::COLORSAMPLED: {
			usage = vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eStorage;
			//resourceDescription.aspectMask = vk::ImageAspectFlagBits::eColor;
			mipLevelCount = (useMipmap) ? 1 /*+ floor(log2(max(width, height)))*/ : 1; // musim zjistit kde ma gpuengine definovane tyhle funkce a nebo si je napisu sam...
		} break;
		case CreateInfo::Image::Usage::DEPTHBUFFER: {
			usage = vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eTransferSrc;
			format = vk::Format::eD32Sfloat;
			tiling = vk::ImageTiling::eOptimal; 
			//resourceDescription.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		} break;
		case CreateInfo::Image::Usage::DEPTHATTACHMENT: {
			usage = vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eSampled;
			//resourceDescription.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
		} break;
		case CreateInfo::Image::Usage::TEXTURE: {
			currentLayout = vk::ImageLayout::eUndefined;
			tiling = vk::ImageTiling::eOptimal;
			usage = vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled;
			memFlags = vk::MemoryPropertyFlagBits::eDeviceLocal;
			format = vk::Format::eR8G8B8A8Unorm; // !!!!! UNORM always as a type... it costs me 2x 4 hours of mine life 
		} break;
		case CreateInfo::Image::Usage::CUBE: {
			this->width = 1024;
			this->height = 1024;
			layerCount = 6;
			createFlags = vk::ImageCreateFlagBits::eCubeCompatible;
			memFlags = vk::MemoryPropertyFlagBits::eDeviceLocal;
		} break;
	}
}

CreateInfo::Image::operator vk::ImageCreateInfo() {
	return {
		createFlags,
		type,
		format,
		{ width, height, depth }, // extent 3D
		mipLevelCount,
		layerCount,
		samples,
		tiling,
		usage,
		vk::SharingMode::eExclusive,
		0,
		nullptr,
		currentLayout
	};
}


/*  ------- Core Class ------- */
void Image::init(CreateInfo::Image& createInfo) {
	if (_image != vk::Image())
		return;

	createInfo.pDevice = &createInfo.pManager->getDevice();
	vk::ImageCreateInfo imageCI = createInfo;
	vk::Result res = createInfo.pManager->getDevice().getVkHandle().createImage(&imageCI, nullptr, &_image);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk Image");

	// allocate info struct memory
	if (_description != nullptr) 
		delete _description; // dealloc safety

	_description = new Info::Image(createInfo);

	if (createInfo.assignMemory)
		static_cast<Info::Image*>(_description)->pManager->assignMemoryToImage(*this, createInfo.allocationSize);

	if (createInfo.createView) {
		CreateInfo::ImageView viewI;
		viewI.format = _description->format;
		viewI.image = this;
		_views.push_back(UPTR(ImageView, viewI)); // vytvori se novy image view
	}

	/*
	if (createImageView) {
		if (pViewCreateInfo != nullptr) {
			pViewCreateInfo->format = _description->format;
			pViewCreateInfo->image = this;
			_views.push_back(new ImageView(*pViewCreateInfo)); // vytvori se novy image view
		}
		else {
			CreateInfo::ImageView viewI;
			viewI.format = _description->format;
			viewI.image = this;
			_views.push_back(new ImageView(viewI)); // vytvori se novy image view
		}
	}
	*/

}

void Image::deinit() {
	// release memory block
	if (_memoryInfo.allocatedFrom != nullptr)
		static_cast<Info::Image*>(_description)->pManager->releaseImageMemory(*this);
	
	// destroy vk buffer object
	if (_image != vk::Image())
		static_cast<Info::Image*>(_description)->pManager->getDevice().getVkHandle().destroyImage(_image, nullptr);

	// need to clear views first
	_views.clear();

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _image = vk::Image(); _views.clear();
}

void Image::move(Image& withdraw) {
	if (_image != vk::Image() || _memoryInfo.allocatedFrom != nullptr || _description != nullptr)
		deinit();

	// move ptr on resources
	_image = withdraw._image;
	_description = withdraw._description;
	_memoryInfo = withdraw._memoryInfo;
	_views = std::move(withdraw._views);

	// must change view pointers to this hfVk Image instead& withdraw image
	for (auto& view : _views)
		view->_description->image = this;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._image = vk::Image();
	withdraw._description = nullptr;
	withdraw._memoryInfo.allocatedFrom = nullptr;
	withdraw._views.clear();

}

void Image::copyMemory(Image& data, CommandBuffer& keepCommands, vk::ImageCopy* copyInfo) {
	uint32_t requieredSize = data._memoryInfo.allocInfo.size();
	// need to allocate more space
	if (requieredSize > _memoryInfo.allocInfo.size()) {
		CreateInfo::Image cI(*static_cast<Info::Image*>(data._description));
		cI.pManager = static_cast<Info::Image*>(_description)->pManager;

		// release attributes
		deinit();

		/* 
			init with new size
			if data has an image view, create one for this object also
		*/
		CreateInfo::ImageView viewCI(data._views.front()->getDescription());
		if (data._views.size() > 0) {
			init(cI);
			addView(viewCI);
		}
			
	}

	// data copy here is guaranteed that source buffer will fit inside this buffer memory...
	if (copyInfo == nullptr) {
		vk::ImageCopy copyRegion;
		copyRegion.srcOffset = vk::Offset3D( 0,0,0 );
		copyRegion.dstOffset = vk::Offset3D( 0,0,0 );
		copyRegion.srcSubresource = { vk::ImageAspectFlagBits::eColor, 0, 0, 1 };
		copyRegion.dstSubresource = { vk::ImageAspectFlagBits::eColor, 0, 0, 1 };
		copyRegion.extent = vk::Extent3D( data._description->width, data._description->height, static_cast<Info::Image*>(data._description)->layerCount );
		keepCommands.getVkHandle().copyImage(data.getVkHandle(), static_cast<Info::Image*>(data._description)->currentLayout, _image, static_cast<Info::Image*>(data._description)->currentLayout, 1, &copyRegion);
	}
	else 
		keepCommands.getVkHandle().copyImage(data.getVkHandle(), static_cast<Info::Image*>(data._description)->currentLayout, _image, static_cast<Info::Image*>(data._description)->currentLayout, 1, copyInfo);

}

void Image::copyMemory(Buffer& data, CommandBuffer& keepCommands, vk::BufferImageCopy* copyInfo) {
	uint32_t requieredSize = data.getMemoryInfo().allocInfo.size();
	// need to allocate more space
	if (requieredSize > _memoryInfo.allocInfo.size()) {
		return; // disallow image realloc

		/* but code for it is here for demonstration... CreateInfo::Image cI(*_description);
		cI.width = imageDimensions.width;
		cI.height = imageDimensions.height;
		cI.layerCount = imageDimensions.depth;

		// release attributes
		deinit();

		// init with new size
		if (_views.size() > 0) {
			// create image view with same parameters as before
			CreateInfo::ImageView viewCI(_views.front()->getDescription());
			init(cI, true, &viewCI);
		}
		else 
			init(cI);*/
	}

	// data copy here is guaranteed that source buffer will fit inside this buffer memory...
	if (copyInfo == nullptr) {
		vk::BufferImageCopy copyRegion;
		copyRegion.imageOffset = vk::Offset3D( 0,0,0 ); // Optional
		copyRegion.imageExtent = vk::Extent3D( _description->width, _description->height, 1 );
		copyRegion.imageSubresource = { vk::ImageAspectFlagBits::eColor, 0, 0, 1 };
		copyRegion.bufferOffset = 0;
		copyRegion.bufferRowLength = 0;
		copyRegion.bufferImageHeight = 0;
		keepCommands.getVkHandle().copyBufferToImage(data, _image, static_cast<Info::Image*>(_description)->currentLayout, 1, &copyRegion);
	}
	else
		keepCommands.getVkHandle().copyBufferToImage(data, _image, static_cast<Info::Image*>(_description)->currentLayout, 1, copyInfo);

}

void hf::Vk::Image::stashOnGPU(void* data, uint32_t dataLen, CommandBuffer& keepCommands, CommandProcessor& proccessCommands) {
	// unitialized object? -> return
	if (_image == vk::Image() || _description == nullptr)
		return;

	CreateInfo::Buffer bCI;
	bCI.pManager = static_cast<Info::Image*>(_description)->pManager;
	bCI.size = dataLen;
	bCI.propertyFlags = vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent;
	bCI.usage = vk::BufferUsageFlagBits::eTransferSrc | vk::BufferUsageFlagBits::eTransferDst;
	// create stagging buffer
	Buffer staggingBuffer(bCI);
	// if data.size != image.width * image.height -> return
	if (dataLen / _description->formatSize != _description->width * _description->height)
		throw hf::Ax::Exception::Bad::Execution("Image: stashOnGPU -> data size doesn't correspond with image dimensionality");

	// image not devicelocal -> recreate it 
	if ((static_cast<Info::Image*>(_description)->memFlags & vk::MemoryPropertyFlagBits::eDeviceLocal) != vk::MemoryPropertyFlagBits::eDeviceLocal) {
		CreateInfo::Image info(*static_cast<Info::Image*>(_description));
		info.memFlags &= ~(vk::MemoryPropertyFlagBits::eHostVisible); // disable option for host visible memory
		info.memFlags |= vk::MemoryPropertyFlagBits::eDeviceLocal;
		// recreate image
		deinit();
		init(info);
	}

	// copy data to stagging buffer
	void* sbPtr = staggingBuffer.map();
	memcpy(sbPtr, data, dataLen);
	staggingBuffer.unmap();

	// copy from stagging buffer to this object stored in GPU local heap.
	keepCommands.waitUntilExecutionFinished();
	keepCommands.beginRecording();
	copyMemory(staggingBuffer, keepCommands);
	keepCommands.finishRecording();

	Info::PostSingleBuffer postI;
	postI.pBuffer = &keepCommands;
	proccessCommands.flushCmdBuffer(postI, &keepCommands.getLock());

	// need to wait until copy is done before destroying stagging buffer... 
	keepCommands.waitUntilExecutionFinished();

	// here should be object filled with data
}

void Image::stashOnGPU(vector<uint8_t>& data, CommandBuffer& keepCommands, CommandProcessor& proccessCommands) {
	stashOnGPU(data.data(), data.size(), keepCommands, proccessCommands);
}

void hf::Vk::ImageBase::changeLayout(CommandBuffer& commandBuffer, Info::Image::ChangeLayout& info, CommandProcessor& proccessCommands) {
	commandBuffer.beginRecording();

	info.image = _image;
	vk::ImageMemoryBarrier memBarrier = info; // MemoryBarrier parrent of ChangeLayout and has convertor to vkimgbarrier
	commandBuffer.getVkHandle().pipelineBarrier(info.producentStage, info.consumentStage, vk::DependencyFlags(0), 0, VK_NULL_HANDLE, 0, VK_NULL_HANDLE, 1, &memBarrier);

	_description->currentLayout = memBarrier.newLayout;

	commandBuffer.finishRecording();

	Info::PostSingleBuffer postI;
	postI.pBuffer = &commandBuffer;
	proccessCommands.flushCmdBuffer(postI, &commandBuffer.getLock());

	// because data ops need to wait until execution is finished
	commandBuffer.waitUntilExecutionFinished();
}

void ImageBase::clearColor(vk::ClearColorValue clearValue, CommandBuffer& keepCommands, uint32_t layersCount) {
	vk::ImageSubresourceRange info { 
		vk::ImageAspectFlagBits::eColor,
		0,
		1,
		0,
		layersCount
	};

	// need to change image layout to proper one n back
	Info::Image::ChangeLayout changeL(Info::Image::ChangeLayout::PRE_CLEAR_IMG);
	changeL.subresourceRange = { vk::ImageAspectFlagBits::eColor, 0, 1, 0, layersCount };
	changeLayout(keepCommands, changeL);

	keepCommands.getVkHandle().clearColorImage(_image, vk::ImageLayout::eTransferDstOptimal, &clearValue, 1, &info);
	
	changeL = Info::Image::ChangeLayout(Info::Image::ChangeLayout::POST_CLEAR_IMG);
	changeL.subresourceRange = { vk::ImageAspectFlagBits::eColor, 0, 1, 0, layersCount };
	changeLayout(keepCommands, changeL);

}

void hf::Vk::ImageBase::clearColorAttachment(vk::ClearColorValue clearValue, CommandBuffer& keepCommands, uint32_t layersCount) {
	vk::ImageSubresourceRange info{
		vk::ImageAspectFlagBits::eColor,
		0,
		1,
		0,
		layersCount
	};

	// need to change image layout to proper one n back
	Info::Image::ChangeLayout changeL(Info::Image::ChangeLayout::PRE_CLEAR_COLATT);
	changeL.subresourceRange = { vk::ImageAspectFlagBits::eColor, 0, 1, 0, layersCount };
	changeLayout(keepCommands, changeL);

	keepCommands.getVkHandle().clearColorImage(_image, vk::ImageLayout::eTransferDstOptimal, &clearValue, 1, &info);

	changeL = Info::Image::ChangeLayout(Info::Image::ChangeLayout::POST_CLEAR_COLATT);
	changeL.subresourceRange = { vk::ImageAspectFlagBits::eColor, 0, 1, 0, layersCount };
	changeLayout(keepCommands, changeL);
}

void ImageBase::clearDepthStencil(vk::ClearDepthStencilValue clearValue, CommandBuffer& keepCommands, uint32_t layersCount) {
	vk::ImageSubresourceRange info;
	info.setAspectMask(vk::ImageAspectFlagBits::eDepth)
		.setBaseArrayLayer(0)
		.setBaseMipLevel(0)
		.setLayerCount(layersCount)
		.setLevelCount(1);

	// need to change image layout to proper one n back
	Info::Image::ChangeLayout changeL(Info::Image::ChangeLayout::PRE_CLEAR_DEPTH_A);
	changeL.subresourceRange = { vk::ImageAspectFlagBits::eDepth, 0, 1, 0, layersCount };
	changeLayout(keepCommands, changeL);
	keepCommands.getVkHandle().clearDepthStencilImage(_image, vk::ImageLayout::eTransferDstOptimal, &clearValue, 1, &info);
	changeL = Info::Image::ChangeLayout(Info::Image::ChangeLayout::POST_CLEAR_DEPTH_A);
	changeL.subresourceRange = { vk::ImageAspectFlagBits::eDepth, 0, 1, 0, layersCount };
	changeLayout(keepCommands, changeL);

}

void hf::Vk::ImageBase::clearColor(vk::ClearColorValue clearValue, CommandBuffer& keepCommands, CommandProcessor& execCommands) {
	keepCommands.beginRecording();
	clearColor(clearValue, keepCommands);
	keepCommands.finishRecording();

	Info::PostSingleBuffer postI;
	postI.pBuffer = &keepCommands;
	execCommands.flushCmdBuffer(postI, &keepCommands.getLock());

	// because data ops need to wait until execution is finished
	keepCommands.waitUntilExecutionFinished();
}

void hf::Vk::ImageBase::clearDepthStencil(vk::ClearDepthStencilValue clearValue, CommandBuffer& keepCommands, CommandProcessor& execCommands) {
	keepCommands.beginRecording();
	clearDepthStencil(clearValue, keepCommands);
	keepCommands.finishRecording();

	Info::PostSingleBuffer postI;
	postI.pBuffer = &keepCommands;
	execCommands.flushCmdBuffer(postI, &keepCommands.getLock());

	// because data ops need to wait until execution is finished
	keepCommands.waitUntilExecutionFinished();
}

void hf::Vk::ImageBase::addView(CreateInfo::ImageView& createInfo) {
	createInfo.format = _description->format;
	createInfo.image = this;
	_views.push_back(UPTR(ImageView, createInfo)); // vytvori se novy image view

}

void ImageBase::changeLayout(CommandBuffer& commandBuffer, Info::Image::ChangeLayout& info) {
	info.image = *this;
	vk::ImageMemoryBarrier memBarrier = info; // MemoryBarrier parrent of ChangeLayout and has convertor to vkimgbarrier
	commandBuffer.getVkHandle().pipelineBarrier(info.producentStage, info.consumentStage, vk::DependencyFlags(0), 0, nullptr, 0, nullptr, 1, &memBarrier);  
	_description->currentLayout = memBarrier.newLayout;
}