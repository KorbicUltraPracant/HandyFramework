/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/CommandPool.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;
using namespace std;

/* -------CreateInfoClass ------- */
CreateInfo::CommandPool::operator vk::CommandPoolCreateInfo() {
	return vk::CommandPoolCreateInfo((vk::CommandPoolCreateFlags) flags, queueFamilyIndex);
}

/* ------- Core Class ------- */
void hf::Vk::CommandPool::init(CreateInfo::CommandPool& createInfo) {
	if (_pool != vk::CommandPool())
		return;

	vk::CommandPoolCreateInfo poolCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createCommandPool(&poolCI, nullptr, &_pool);
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk CommandPool");


	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::CommandPool(createInfo);

}

void hf::Vk::CommandPool::deinit() {
	// destroy vk buffer object
	if (_pool != vk::CommandPool())
		_description->pDevice->getVkHandle().destroyCommandPool(_pool, nullptr);

	// destroys info.
	if (_description == nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_pool = vk::CommandPool(); _description = nullptr;
}