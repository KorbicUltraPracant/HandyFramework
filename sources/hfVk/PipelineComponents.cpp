/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/PipelineComponents.h>

using namespace hf::Vk;

Info::PipelineSetUp::ColorBlendState::operator vk::PipelineColorBlendStateCreateInfo() {
	return vk::PipelineColorBlendStateCreateInfo {
		vk::PipelineColorBlendStateCreateFlags(0),
		logicOpEnable,
		logicOp,
		(uint32_t) attachments.size(),
		attachments.data(),
		blendConstants
	};
}

Info::PipelineSetUp::ShaderStage::operator vk::PipelineShaderStageCreateInfo() {
	return {
		vk::PipelineShaderStageCreateFlags(0),
		stage,
		module->getVkHandle(),
		entryPoint.c_str(),
		pConstants
	};
}

Info::PipelineSetUp::ColorBlendAttachmentState::ColorBlendAttachmentState() {
	blendEnable = VK_FALSE;
	srcColorBlendFactor = vk::BlendFactor::eZero;
	dstColorBlendFactor = vk::BlendFactor::eZero;
	colorBlendOp = vk::BlendOp::eAdd;
	srcAlphaBlendFactor = vk::BlendFactor::eZero;
	dstAlphaBlendFactor = vk::BlendFactor::eZero;
	alphaBlendOp = vk::BlendOp::eAdd;
	colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
}