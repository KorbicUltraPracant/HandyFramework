/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVk/ShaderModule.h>
#include <hfVk/Device.h>
#include <hfAx/FileOps.inl>
#include <hfAx/Exception.h>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* ----------- Create Info Class ----------- */
CreateInfo::ShaderModule::operator vk::ShaderModuleCreateInfo() {
	// load shader data in spir-V format
	if (File::read(path, data, true) < 0) // 0 ... Sucess
		throw Exception::Bad::Load("ShaderModuleCI::Shader file");

	return {
		vk::ShaderModuleCreateFlagBits(0),
		(uint32_t) data.size() * sizeof(uint32_t),
		data.data()
	};
}

void ShaderModule::init(CreateInfo::ShaderModule& createInfo) {
	if (_shader != vk::ShaderModule())
		return;

	vk::ShaderModuleCreateInfo shaderCI = createInfo;
	vk::Result res = createInfo.pDevice->getVkHandle().createShaderModule(&shaderCI, nullptr, &_shader);
	if (res != vk::Result::eSuccess)
		throw Exception::Bad::Creation("hfVk ShaderModule");

	// uvolnim shader data, kod je ulozen ve vk driver's objektu
	createInfo.data.clear();

	// info struct allocation
	if (_description != nullptr)
		delete _description; // dealloc safety

	_description = new Info::ShaderModule(createInfo);

}

void ShaderModule::deinit() {
	// destroy vk buffer object
	if (_shader != vk::ShaderModule())
		_description->pDevice->getVkHandle().destroyShaderModule(_shader, nullptr);

	// destroys info.
	if (_description != nullptr)
		delete _description;

	// if object wouldn't be destroyed aftewards, this will be usefull for next init call
	_description = nullptr; _shader = vk::ShaderModule();
}

void ShaderModule::move(ShaderModule& withdraw) {
	if (_shader != vk::ShaderModule() || _description != nullptr)
		deinit();

	// move ptr on resources
	_shader = withdraw._shader;
	_description = withdraw._description;

	// disvalidate& withdraw's ptrs, when all set like this dtor of& withdraw object will not affect moved resources
	withdraw._shader = vk::ShaderModule();
	withdraw._description = nullptr;

}