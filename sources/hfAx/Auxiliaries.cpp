/**
* @author Radek Blahos
* @project hfAx Library
*
* @brief This file contains implementation of some specialized template functions
*/

#include <hfAx/hfAx.h>
#include <cstring>

template<>
bool hf::Ax::in(char* val, char** array, int arr_size) {
	for (int i = 0; i < arr_size; i++) {
		if (!strcmp(val, array[i]))
			return true;
	}
	return false;
}

template<>
bool hf::Ax::in(std::string val, std::string* array, int arr_size) {
	for (int i = 0; i < arr_size; i++) {
		if (!array[i].compare(val))
			return true;
	}
	return false;
}