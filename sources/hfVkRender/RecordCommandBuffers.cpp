#include <hfVkRender/Renderer.h>

void hf::Vk::Render::Renderer::recordDrawCommands(hf::Vk::CommandBuffer& keepCommands, hf::Vk::Info::RecordDrawCommands& info) {
	/* begin recording */
	keepCommands.beginRecording(vk::CommandBufferUsageFlagBits::eSimultaneousUse);

	/* clear current image */
	if (info.clearImage)
		presenter.clearRenderResources(info.currentSwapchainImageIdx, keepCommands);

	/* begin renderPass */
	keepCommands.beginRenderPass(info.renderPassInfo);

	/* record shared commands */
	recordCoreCommands(keepCommands, info);

	/* record draw command */
	keepCommands.draw(info.drawInfo);

	/* end renderpass */
	keepCommands.endRenderPass();

	/* finish recording */
	keepCommands.finishRecording();
}

void hf::Vk::Render::Renderer::recordCoreCommands(hf::Vk::CommandBuffer& keepCommands, hf::Vk::Info::RecordDrawCommandsCore& info) {
	/* bind Pipeline */
	keepCommands.bindGraphicPipeline(*info.pGraphicPipeline);

	/* set viewport and scissor test */
	Info::SetViewport viewport;
	viewport.width = info.renderWidth;
	viewport.height = info.renderHeight;
	keepCommands.setViewport(viewport);

	Info::SetScissor scissor;
	scissor.width = info.renderWidth;
	scissor.height = info.renderHeight;
	keepCommands.setScissor(scissor);

	/* bind vertexBuffers */
	if (info.vertexBuffersInfo.pBuffers.size() > 0)
		keepCommands.bindVertexBuffers(info.vertexBuffersInfo);

	/* bind descriptors */
	if (info.descriptorSetsInfo.pDescriptorSets.size() > 0)
		keepCommands.bindDescriptorSets(info.descriptorSetsInfo);

	/* push constants */
	if (info.pushConstants.pLayout != nullptr)
		keepCommands.pushConstants(info.pushConstants);
}

void hf::Vk::Render::Renderer::recordIndexedDrawCommands(hf::Vk::CommandBuffer& keepCommands, hf::Vk::Info::RecordIndexedDrawCommands& info) {
	/* begin recording */
	keepCommands.beginRecording(vk::CommandBufferUsageFlagBits::eSimultaneousUse);

	/* clear current image */
	if (info.clearImage)
		presenter.clearRenderResources(info.currentSwapchainImageIdx, keepCommands);

	/* begin renderPass */
	keepCommands.beginRenderPass(info.renderPassInfo);

	/* record shared commands */
	recordCoreCommands(keepCommands, info);

	/* bind indexBuffer */
	keepCommands.bindIndexBuffer(info.indexBufferInfo);

	/* record draw command */
	keepCommands.drawIndexed(info.drawInfo);

	/* end renderpass */
	keepCommands.endRenderPass();

	/* finish recording */
	keepCommands.finishRecording();

	cerr << "recorded buffer: " << endl;
}

void hf::Vk::Render::Renderer::recordIndexedIndirectDrawCommands(hf::Vk::CommandBuffer& keepCommands, hf::Vk::Info::RecordIndexedIndirectDrawCommands& info) {
	/* begin recording */
	keepCommands.beginRecording(vk::CommandBufferUsageFlagBits::eSimultaneousUse);

	/* clear current image */
	if (info.clearImage)
		presenter.clearRenderResources(info.currentSwapchainImageIdx, keepCommands);

	/* begin renderPass */
	keepCommands.beginRenderPass(info.renderPassInfo);

	/* record shared commands */
	recordCoreCommands(keepCommands, info);

	/* bind indexBuffer */
	keepCommands.bindIndexBuffer(info.indexBufferInfo);

	/* draw command */
	keepCommands.drawIndexedIndirect(info.drawInfo);

	/* end renderpass */
	keepCommands.endRenderPass();

	/* finish recording */
	keepCommands.finishRecording();
}