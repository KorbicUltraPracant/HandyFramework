/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVkRender/PipelineDatabase.h>
#include <hfAx/AuxInlines.inl>
#include <hfAx/Exception.h>
#include <thread>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* ------- Core Class ------- */
void hf::Vk::PipelineDatabase::buildGraphicPipelines(std::vector<CreateInfo::GraphicPipeDBEntry>& graphicPipesCreateInfos) {
	if (!graphicPipesCreateInfos.size() || _description == nullptr)
		return;

	// Pipelines Creation constants
	uint32_t maxPipesPerThread = 4;
	uint32_t gThreadCount = ceil((double)graphicPipesCreateInfos.size() / maxPipesPerThread);

	// Caches loading
	CreateInfo::PipelineCache cacheCI;
	cacheCI.pDevice = _description->pDevice;
	cacheCI.pathToData = _description->cacheStoragePath;

	vector<PipelineCache*> caches;
	// create caches, one per thread all share same input data
	for (int i = 0; i < gThreadCount; i++) {
		caches.push_back(new PipelineCache());
		caches[i]->create(cacheCI);
	}

	// assemble graphic pipes
	vector<vector<vk::Pipeline>> resVec(gThreadCount);
	vector<thread> graphicWorkers;
	vk::PipelineCache passTo; 
	
	// create worker threads
	for (uint32_t i = 0; i < gThreadCount; i++) {
		passTo = caches[i]->getVkHandle();
		graphicWorkers.push_back(thread(assembleGraphicPipelinesWorker, &graphicPipesCreateInfos, _description->pDevice, i * maxPipesPerThread, (i + 1)*maxPipesPerThread, passTo, &resVec[i]));
	}
	// join them and check results 
	for (uint32_t i = 0; i < gThreadCount; i++) {
		graphicWorkers[i].join();

		// resVec is filled with requested created pipes, so time to push them into DB
		// from i * maxPipesPerThread to resVec.size()
		int counter = i * maxPipesPerThread;
		// must be 0(vk::Result::eSuccess) to pass so thats why !resVec.size() -> if some error occurs during creation resVec will be cleared in worker method...
		if (resVec[i].size() == 0)
			throw hf::Ax::Exception::Bad::Creation("hfVk PipeDB::GraphicPipelines");

		for (auto& vkpipeline : resVec[i]) {
			_graphicPipesDB[graphicPipesCreateInfos[counter].name]; // vytvorim prazdny pipeline objekt
			_graphicPipesDB[graphicPipesCreateInfos[counter].name].assemble(vkpipeline, graphicPipesCreateInfos[counter].graphicPipeCreateInfo);
			counter++;
		}
	}

	// store cache data, first merge them together into newly created empty one...
	// cacheCI.pathToData.clear(); // memory saving?
	// creating cache where all data will be merged
	caches.push_back(new PipelineCache());
	caches.back()->create(cacheCI);

	vector<vk::PipelineCache> vkcaches;
	for (uint32_t i = 0; i < caches.size() - 1; i++)
		vkcaches.push_back(*caches[i]);

	vk::Result res = _description->pDevice->getVkHandle().mergePipelineCaches(caches.back()->getVkHandle(), vkcaches.size(), vkcaches.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("hfVk PipeDB: MergePipelineCache");

	if (!caches.back()->store(_description->cacheStoragePath))
		throw hf::Ax::Exception::Bad::Execution("hfVk PipeDB: StorePipelineCache");

	for (auto& cache : caches) {
		delete cache;
	}

}

void hf::Vk::PipelineDatabase::buildComputePipelines(std::vector<CreateInfo::ComputePipeDBEntry>& computePipesCreateInfos) {
	if (!computePipesCreateInfos.size() || _description == nullptr)
		return;

	// Pipelines Creation constants
	uint32_t maxPipesPerThread = 4;
	uint32_t cThreadCount = ceil((double)computePipesCreateInfos.size() / maxPipesPerThread);

	// Caches loading
	CreateInfo::PipelineCache cacheCI;
	cacheCI.pDevice = _description->pDevice;
	cacheCI.pathToData = _description->cacheStoragePath;

	vector<PipelineCache*> caches;
	// create caches, one per thread all share same input data
	for (int i = 0; i < cThreadCount; i++) {
		caches.push_back(new PipelineCache());
		caches[i]->create(cacheCI);
	}

	// compute ones here
	vector <vector<vk::Pipeline>> resVec(cThreadCount);
	vector<thread> computeWorkers;
	vk::PipelineCache passTo;

	// create worker threads
	for (uint32_t i = 0; i < cThreadCount; i++) {
		passTo = caches[i]->getVkHandle();
		computeWorkers.push_back(thread(assembleComputePipelinesWorker, &computePipesCreateInfos, _description->pDevice, i * maxPipesPerThread, (i + 1)*maxPipesPerThread, passTo, &resVec[i]));
	}

	// join them and check results
	for (uint32_t i = 0; i < cThreadCount; i++) {
		computeWorkers[i].join();
		if (resVec[i].size() == 0)
			throw hf::Ax::Exception::Bad::Creation("hfVk PipeDB::ComputePipelines");

		int counter = i * maxPipesPerThread;
		for (auto& vkpipeline : resVec[i]) {
			_computePipesDB[computePipesCreateInfos[counter].name];
			_computePipesDB[computePipesCreateInfos[counter].name].assemble(vkpipeline, computePipesCreateInfos[counter].computePipeCreateInfo);
			counter++;
		}
	}

	// store cache data, first merge them together into newly created empty one...
	// cacheCI.pathToData.clear(); // memory saving?
	// creating cache where all data will be merged
	caches.push_back(new PipelineCache());
	caches.back()->create(cacheCI);

	vector<vk::PipelineCache> vkcaches;
	for (uint32_t i = 0; i < caches.size() - 1; i++)
		vkcaches.push_back(*caches[i]);

	vk::Result res = _description->pDevice->getVkHandle().mergePipelineCaches(caches.back()->getVkHandle(), vkcaches.size(), vkcaches.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("hfVk PipeDB: MergePipelineCache");

	if (!caches.back()->store(_description->cacheStoragePath))
		throw hf::Ax::Exception::Bad::Execution("hfVk PipeDB: StorePipelineCache");

	for (auto& cache : caches) {
		delete cache;
	}

}

void hf::Vk::PipelineDatabase::addComputePipeline(CreateInfo::ComputePipeDBEntry& info) {
	CreateInfo::PipelineCache cacheCI;
	cacheCI.pDevice = _description->pDevice;
	cacheCI.pathToData = _description->cacheStoragePath;

	PipelineCache cache(cacheCI);
	info.computePipeCreateInfo.pipelineCache = &cache;

	_computePipesDB.emplace(info.name, info.computePipeCreateInfo);
}

void hf::Vk::PipelineDatabase::addGraphicPipeline(CreateInfo::GraphicPipeDBEntry& info) {
	CreateInfo::PipelineCache cacheCI;
	cacheCI.pDevice = _description->pDevice;
	cacheCI.pathToData = _description->cacheStoragePath;

	PipelineCache cache(cacheCI);
	info.graphicPipeCreateInfo.pipelineCache = &cache;

	_graphicPipesDB.emplace(info.name, info.graphicPipeCreateInfo);
}

void PipelineDatabase::init(CreateInfo::PipelineDatabase& createInfo) {
	// Pipelines Creation constants
	uint32_t maxPipesPerThread = 4;
	uint32_t gThreadCount = ceil((double)createInfo.graphicPipesCreateInfos.size() / maxPipesPerThread);
	uint32_t cThreadCount = ceil((double)createInfo.computePipesCreateInfos.size() / maxPipesPerThread);
	uint32_t reqSize = hf::Ax::max(gThreadCount, cThreadCount);

	// Caches loading
	CreateInfo::PipelineCache cacheCI;
	cacheCI.pDevice = createInfo.pDevice;
	cacheCI.pathToData = createInfo.cacheStoragePath;

	vector<PipelineCache*> caches;
	// create caches, one per thread all share same input data
	for (int i = 0; i < reqSize; i++) {
		caches.push_back(new PipelineCache());
		caches[i]->create(cacheCI);
	}
	
	// assemble graphic pipes
	vector<vector<vk::Pipeline>> resVec(reqSize);
	vector<thread> graphicWorkers;
	vk::PipelineCache passTo; // for some unexplained reasons visual++ thread implementation captured hfVk Pipeline Cache object(caches[i]) not vk::PipelineCache ptr (*caches[i]) and then destoyed it? ffs wtf? :( 
	// create worker threads
	for (uint32_t i = 0; i < gThreadCount; i++) {
		passTo = *caches[i];
		graphicWorkers.push_back(thread(assembleGraphicPipelinesWorker, &createInfo.graphicPipesCreateInfos, createInfo.pDevice, i * maxPipesPerThread, (i + 1)*maxPipesPerThread, passTo, &resVec[i]));
	}
	// join them and check results 
	for (uint32_t i = 0; i < gThreadCount; i++) {
		graphicWorkers[i].join();
		// must be 0(vk::Result::eSuccess) to pass so thats why !resVec.size() -> if some error occurs during creation resVec will be cleared in worker method...
		if (resVec[i].size() == 0) 
			throw hf::Ax::Exception::Bad::Creation("hfVk PipeDB::GraphicPipelines");
			
		// resVec is filled with requested created pipes, so time to push them into DB
		// from i * maxPipesPerThread to resVec.size()
		int counter = i * maxPipesPerThread;
		for (auto& vkpipeline : resVec[i]) {
			_graphicPipesDB[createInfo.graphicPipesCreateInfos[counter].name]; // vytvorim prazdny pipeline objekt
			_graphicPipesDB[createInfo.graphicPipesCreateInfos[counter].name].assemble(vkpipeline, createInfo.graphicPipesCreateInfos[counter].graphicPipeCreateInfo);
			counter++;
		}
	}

	// compute ones here
	vector<thread> computeWorkers;
	// create worker threads
	for (uint32_t i = 0; i < cThreadCount; i++) {
		passTo = *caches[i];
		computeWorkers.push_back(thread(assembleComputePipelinesWorker, &createInfo.computePipesCreateInfos, createInfo.pDevice, i * maxPipesPerThread, (i + 1)*maxPipesPerThread, passTo, &resVec[i]));
	}

	// join them and check results
	for (uint32_t i = 0; i < cThreadCount; i++) {
		computeWorkers[i].join();
		if (resVec[i].size() == 0)
			throw hf::Ax::Exception::Bad::Creation("hfVk PipeDB::ComputePipelines");

		int counter = i * maxPipesPerThread;
		for (auto& vkpipeline : resVec[i]) {
			_computePipesDB[createInfo.computePipesCreateInfos[counter].name];
			_computePipesDB[createInfo.computePipesCreateInfos[counter].name].assemble(vkpipeline, createInfo.computePipesCreateInfos[counter].computePipeCreateInfo);
			counter++;
		}
	}

	// allocate description
	if (_description != nullptr)
		delete _description;

	_description = new Info::PipelineDatabase(createInfo);

	// store cache data, first merge them together into newly created empty one...
	// cacheCI.pathToData.clear(); // memory saving?
	// creating cache where all data will be merged
	caches.push_back(new PipelineCache());
	caches.back()->create(cacheCI); 

	vector<vk::PipelineCache> vkcaches;
	for (uint32_t i = 0; i < caches.size()-1; i++)
		vkcaches.push_back(*caches[i]);

	vk::Result res = createInfo.pDevice->getVkHandle().mergePipelineCaches(caches.back()->getVkHandle(), vkcaches.size(), vkcaches.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Execution("hfVk PipeDB: MergePipelineCache");

	if (!caches.back()->store(_description->cacheStoragePath))
		throw hf::Ax::Exception::Bad::Execution("hfVk PipeDB: StorePipelineCache");

	for (auto& cache : caches) {
		delete cache;
	}

}

void PipelineDatabase::deinit() {
	if (_description != nullptr)
		delete _description;

	_description = nullptr;
	
	_graphicPipesDB.clear();
	_computePipesDB.clear();
}

// multithreaded pipeline creation
void PipelineDatabase::assembleGraphicPipelinesWorker(std::vector<CreateInfo::GraphicPipeDBEntry>* graphicPipesCreateInfos, Device* pDevice, uint32_t from, uint32_t to, vk::PipelineCache cache, std::vector<vk::Pipeline>* pipesPtr) {
	uint32_t resize = ((graphicPipesCreateInfos->size() - from) < (to - from)) ? graphicPipesCreateInfos->size() - from : (to - from);
	vector<vk::GraphicsPipelineCreateInfo> createInfos(resize); // number of create infos to be submited
	for (int i = from; i < to && i < graphicPipesCreateInfos->size(); i++)
		createInfos[i - from] = graphicPipesCreateInfos->operator[](i).graphicPipeCreateInfo;
	
	pipesPtr->clear();
	pipesPtr->resize(resize);

	vk::Result res = pDevice->getVkHandle().createGraphicsPipelines(cache, createInfos.size(), createInfos.data(), nullptr, pipesPtr->data());
	if (res != vk::Result::eSuccess) {
		pipesPtr->clear();
		return;
	}
}

void PipelineDatabase::assembleComputePipelinesWorker(std::vector<CreateInfo::ComputePipeDBEntry>* computePipesCreateInfos, Device* pDevice, uint32_t from, uint32_t to, vk::PipelineCache cache, std::vector<vk::Pipeline>* pipesPtr) {
	// multithreaded pipeline creation
	uint32_t resize = ((computePipesCreateInfos->size() - from) < (to - from)) ? computePipesCreateInfos->size() - from : (to - from);
	vector<vk::ComputePipelineCreateInfo> createInfos(resize);
	for (int i = from; i < to && i < computePipesCreateInfos->size(); i++)
		createInfos[i - from] = computePipesCreateInfos->operator[](i).computePipeCreateInfo;

	pipesPtr->clear();
	pipesPtr->resize(resize);

	vk::Result res = pDevice->getVkHandle().createComputePipelines(cache, createInfos.size(), createInfos.data(), nullptr, pipesPtr->data());
	if (res != vk::Result::eSuccess) {
		pipesPtr->clear();
		return;
	}
}