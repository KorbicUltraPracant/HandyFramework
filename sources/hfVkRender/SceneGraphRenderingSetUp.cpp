#include <hfVkRender/Renderer.h>
#include <hfAx/AuxInlines.inl>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* componentsDB.renderStrategies["name"] will be always created if not existed in first acess call... so no need to create it manualy */

/*
void hf::Vk::Render::Renderer::sceneGraph_FillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo) {
	createInfo.pDevice = &device;
	createInfo.layout = &componentsDB.renderPasses["axis"];
	createInfo.renderPass = &componentsDB.renderPasses["axis"];

	createInfo.vertexInput.resize(2);
	// position
	createInfo.vertexInput[0].location = 0;
	createInfo.vertexInput[0].stride = 3 * sizeof(float);

	// normal
	createInfo.vertexInput[1].location = 1;
	createInfo.vertexInput[1].stride = 3 * sizeof(float);

	// texture coords
	createInfo.vertexInput[2].location = 2;
	createInfo.vertexInput[2].stride = 2 * sizeof(float);
	createInfo.vertexInput[2].format = vk::Format::eR32G32Sfloat;

	// reusing baked scene shaders
	createInfo.stagesInfo.resize(2);
	createInfo.stagesInfo[0].stage = vk::ShaderStageFlagBits::eVertex;
	createInfo.stagesInfo[0].module = &*componentsDB.renderStrategies["bakedScene"].shaders[0];

	createInfo.stagesInfo[1].stage = vk::ShaderStageFlagBits::eFragment;
	createInfo.stagesInfo[1].module = &*componentsDB.renderStrategies["bakedScene"].shaders[1];

	createInfo.viewport.scissors.resize(1);
	createInfo.viewport.scissors[0].extent = { 0, 0 };
	createInfo.viewport.scissors[0].offset = { (int32_t)presenter.getSwapchain().getDescription().width, (int32_t)presenter.getSwapchain().getDescription().height };

	createInfo.viewport.viewports.resize(1);
	createInfo.viewport.viewports[0].width = (float)presenter.getSwapchain().getDescription().width;
	createInfo.viewport.viewports[0].height = (float)presenter.getSwapchain().getDescription().height;
	createInfo.viewport.viewports[0].minDepth = createInfo.viewport.viewports[0].x = createInfo.viewport.viewports[0].y = 0.0f;
	createInfo.viewport.viewports[0].maxDepth = 1.0f;

	// dynamic states
	createInfo.dynamicStates.insert(createInfo.dynamicStates.begin(), { vk::DynamicState::eViewport, vk::DynamicState::eScissor });

	// blend attachment states
	createInfo.colorBlend.attachments.push_back(Info::PipelineSetUp::ColorBlendAttachmentState());

	// depth test
	createInfo.depthStencil.depthTestEnable = VK_TRUE;
	createInfo.depthStencil.depthWriteEnable = VK_TRUE;
	createInfo.depthStencil.depthCompareOp = vk::CompareOp::eLessOrEqual; 
}

void hf::Vk::Render::Renderer::sceneGraph_SetUpDS() {
	/*hf::Vk::CreateInfo::DescriptorSet info;
	info.pManager = &descManager;
	info.mainLayoutInfo.pDevice = &device;
	info.mainLayoutInfo.layoutBindings.resize(2);

	// vertex shader uniformbuffer
	info.mainLayoutInfo.layoutBindings[0].binding = 0;
	info.mainLayoutInfo.layoutBindings[0].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	info.mainLayoutInfo.layoutBindings[0].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[0].stageFlags = vk::ShaderStageFlagBits::eVertex;

	// fragment shader uniformbuffer
	info.mainLayoutInfo.layoutBindings[1].binding = 1;
	info.mainLayoutInfo.layoutBindings[1].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[1].descriptorType = vk::DescriptorType::eUniformBuffer;
	info.mainLayoutInfo.layoutBindings[1].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[1].stageFlags = vk::ShaderStageFlagBits::eFragment;

	// fragment shader image
	info.mainLayoutInfo.layoutBindings[2].binding = 2;
	info.mainLayoutInfo.layoutBindings[2].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[2].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	info.mainLayoutInfo.layoutBindings[2].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[2].stageFlags = vk::ShaderStageFlagBits::eFragment;

	// create descriptor set
	componentsDB.renderStrategies["sceneGraph"].descriptorSets.push_back(UPTR(hf::Vk::DescriptorSet, info)); // 0

}

void hf::Vk::Render::Renderer::sceneGraph_WriteDS() {
	Info::UpdateDescriptorSets updateInfo;
	updateInfo.buffersInfo.resize(2);

	// vsUniforms
	updateInfo.buffersInfo[0].buffer = vsUbo.getBuffer();
	updateInfo.buffersInfo[0].offset = 0;
	updateInfo.buffersInfo[0].range = sizeof(hf::Vk::ShaderResources::UniformBuffer::VertexShader);

	// fsUniforms
	updateInfo.buffersInfo[1].buffer = fsUbo.getBuffer();
	updateInfo.buffersInfo[1].offset = 0;
	updateInfo.buffersInfo[1].range = sizeof(hf::Vk::ShaderResources::UniformBuffer::FragmentShader);

	// sampler update info
	// texture
	updateInfo.imagesInfo.resize(1);
	updateInfo.imagesInfo[0] = vk::DescriptorImageInfo()
		.setSampler(*componentsDB.samplers[0])
		.setImageView(vk::ImageView()) // tady doplnit texture buffer az bude hotovy!!!
		.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);

	// -- write set infos -- // 
	updateInfo.writeInfo.resize(2);

	// vsUniforms
	updateInfo.writeInfo[0].dstSet = componentsDB.renderStrategies["sceneGraph"].descriptorSets[0]->getVkHandle();
	updateInfo.writeInfo[0].dstBinding = 0;
	updateInfo.writeInfo[0].dstArrayElement = 0;
	updateInfo.writeInfo[0].descriptorCount = 1;
	updateInfo.writeInfo[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	updateInfo.writeInfo[0].pBufferInfo = &updateInfo.buffersInfo[0];

	// fsUniforms
	updateInfo.writeInfo[1].dstSet = componentsDB.renderStrategies["sceneGraph"].descriptorSets[0]->getVkHandle();
	updateInfo.writeInfo[1].dstBinding = 1;
	updateInfo.writeInfo[1].dstArrayElement = 0;
	updateInfo.writeInfo[1].descriptorCount = 1;
	updateInfo.writeInfo[1].descriptorType = vk::DescriptorType::eUniformBuffer;
	updateInfo.writeInfo[1].pBufferInfo = &updateInfo.buffersInfo[1];

	// texture
	updateInfo.writeInfo[2] = hf::Vk::Info::WriteDescriptorSet()
		.setDstSet(componentsDB.renderStrategies["sceneGraph"].descriptorSets[0]->getVkHandle())
		.setDstBinding(2)
		.setDstArrayElement(0)
		.setDescriptorCount(1)
		.setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
		.setPImageInfo(&updateInfo.imagesInfo[0]);

	componentsDB.renderStrategies["sceneGraph"].descriptorSets[0]->update(updateInfo);
}

void hf::Vk::Render::Renderer::sceneGraph_CreatePipelineLayout() {
	/*hf::Vk::CreateInfo::PipelineLayout layoutCI;
	layoutCI.pDevice = &device;

	layoutCI.pDescriptorSetLayouts.push_back(&componentsDB.renderStrategies["sceneGraph"].descriptorSets[0]->getLayout());
	componentsDB.renderStrategies["sceneGraph"].pipelineLayout.describe(layoutCI); // textured 0 
}

void hf::Vk::Render::Renderer::sceneGraph_LoadShaders() {
	// shaders
	hf::Vk::CreateInfo::ShaderModule basicCI;
	basicCI.pDevice = &device;

	string dirName(RENDER_RESOURCES);

	basicCI.path = dirName + string("/shaders/sceneGraph_vert.spv");
	componentsDB.renderStrategies["sceneGraph"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedV 0

	basicCI.path = dirName + string("/shaders/sceneGraph_frag.spv");
	componentsDB.renderStrategies["sceneGraph"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedF 1
}

void hf::Vk::Render::Renderer::sceneGraph_RecordCommands() {
	
}
*/