#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_nonuniform_qualifier : enable

layout(location = 0) in vec3 fragmentPosition;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 texCoords;

layout(set = 0, binding = 1) uniform ubo1 {
	vec4 color;
	vec4 objectsColor;
	vec4 position;
	vec4 direction;
	vec4 eyePosition;
	float cutOff;
	uint lightingMode;
	uint samplerIdx;
	float farPlane;
} lightInfo;

layout(set = 1, binding = 0) uniform sampler2D texSamplers[];
layout(set = 1, binding = 1) uniform samplerCube shadowSampler;

layout(location = 0) out vec4 finalColor;

float shadowCalculation(vec3 fragPos) {
	// get vector between fragment position and light position
	vec3 fragToLight = fragPos - lightInfo.position.xyz;

	// use the light to fragment vector to sample from the depth map
	float closestDepth = texture(shadowSampler, fragToLight).r;

	// it is currently in linear range between [0,1]. Re-transform back to original value
	closestDepth *= lightInfo.farPlane;

	// now get current linear depth as the length between the fragment and light position
	float currentDepth = length(fragToLight);

	// now test for shadows
	float bias = 0.05;
	float shadow = currentDepth -  bias > closestDepth ? 1.0 : 0.0;

	return shadow;
}

void main(void) {
	if (lightInfo.samplerIdx == -1)
		finalColor = lightInfo.objectsColor;
	else
		finalColor = texture(texSamplers[lightInfo.samplerIdx], texCoords);

	if (lightInfo.lightingMode == 0) // if lighting is off
		return;

	/* Vars used in computations */
	vec3 N = normalize(normal);
	vec3 L = normalize(lightInfo.position.xyz - fragmentPosition); // vector/dir from fragment to light
	vec3 V = normalize(lightInfo.eyePosition.xyz - fragmentPosition); // vector/dir from fragment to eye
	vec3 R = reflect(-L, N);
	vec3 HV = normalize(L + V); // half-way vector for blinn/phong

	float ambientStrength = 0.02;
	float specularFactor = 64;

	vec3 ambient = ambientStrength * lightInfo.color.xyz;
	vec3 diffuse = max(dot(N, L), 0.0) * finalColor.xyz;
	vec3 specular = pow(max(dot(HV, N), 0.0), specularFactor) * lightInfo.color.xyz;

	// calculate shadow
	// get vector between fragment position and light position
	vec3 fragToLight = fragmentPosition - lightInfo.position.xyz;

	// use the light to fragment vector to sample from the depth map
	float closestDepth = texture(shadowSampler, fragToLight).r;

    //float shadow = shadowCalculation(fragmentPosition);

	finalColor = vec4(ambient + (diffuse + specular), 1.0); /*vec4(closestDepth, 0.0, 0.0, 1.0);*/ //vec4(ambient + (1.0 - shadow) * (diffuse + specular), 1.0);
}

// TD
