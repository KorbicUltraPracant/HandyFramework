#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;
layout (location = 0) out vec3 outColor;

layout (location = 0) in vec3 inNormal[];

void main() {
	float normalLength = 1;
	for(int i = 0; i < gl_in.length(); i++) {
		gl_Position = gl_in[i].gl_Position;
		outColor = vec3(1.0, 0.0, 0.0);
		EmitVertex();

		gl_Position = gl_in[i].gl_Position + vec4(inNormal[i] * normalLength, 0.0);
		outColor = vec3(0.0, 0.0, 1.0);
		EmitVertex();

		EndPrimitive();
	}
}
