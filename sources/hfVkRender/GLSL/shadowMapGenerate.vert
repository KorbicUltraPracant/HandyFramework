#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex {
    vec4 gl_Position;
};
layout(location = 0) out vec3 fragPos;

layout(location = 0) in vec3 inPosition;

layout(set = 0, binding = 0) uniform ubo1 {
	mat4 viewProjection;
	mat4 worldSpaceTr;
	mat4 normalsFix; // because memory alligment this is mat comes so much fucked up into renderer
} ubo;

layout(set = 1, binding = 0) uniform ubo2 {
    mat4 sDepthProjection[6];
} uboMat;

layout(push_constant) uniform push_constants {
	uint matIdx;
} pushConstants;

void main() {
    // final position of vertex in clip space
    gl_Position = uboMat.sDepthProjection[pushConstants.matIdx] * ubo.worldSpaceTr * vec4(inPosition, 1.0);

    fragPos = inPosition;
}
