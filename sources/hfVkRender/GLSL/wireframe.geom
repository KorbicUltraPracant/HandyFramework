#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (triangles) in;
layout (line_strip, max_vertices = 6) out;

void main() {
	for(int i = 0; i < gl_in.length(); i++) {
		gl_Position = gl_in[i].gl_Position;
		EmitVertex();

		gl_Position = gl_in[(i+1) % gl_in.length()].gl_Position;
		EmitVertex();

		EndPrimitive();
	}
}
