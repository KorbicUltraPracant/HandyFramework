#version 450
#extension GL_ARB_separate_shader_objects : enable

out gl_PerVertex {
    vec4 gl_Position;
};

//layout(location = 0) in vec3 vertexPos;
layout(location = 0) out vec3 fragColor;

layout(binding = 0) uniform ubo1 {
	mat4 viewProjection;
	mat4 worldSpaceTr;
	mat3 normalsFix;
} ubo;

vec3 positions[6] = vec3[](
  vec3(10.0, 0.0, 0.0),
  vec3(0.0, 0.0, 0.0),
  vec3(0.0, -10.0, 0.0),
  vec3(0.0, 0.0, 0.0),
  vec3(0.0, 0.0, 10.0),
  vec3(0.0, 0.0, 0.0)
);

vec3 colors[3] = vec3[](
    vec3(1.0, 0.0, 0.0),
    vec3(0.0, 1.0, 0.0),
    vec3(0.0, 0.0, 1.0)
);

void main(void) {
  	gl_Position = ubo.viewProjection * vec4(positions[gl_VertexIndex], 1.0); // ubo.worldSpaceTr * vec4(vertexPos, 1.0);
    fragColor = colors[(gl_VertexIndex%6)/2]; // to index 0...5 into 0...2
}
