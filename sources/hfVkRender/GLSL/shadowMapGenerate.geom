#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (triangles) in;
layout (triangle_strip, max_vertices=18) out;

layout(set = 1, binding = 0) uniform uboGS {
	mat4 shadowProj[6];
} ubo;

layout(location = 0) out vec4 fragPos; // FragPos from GS (output per emitvertex)

void main() {
	for(int face = 0; face < 6; face++) {
        gl_Layer = face; // built-in variable that specifies to which face we render.
        for(int i = 0; i < 3; ++i) { // for each triangle's vertices
            fragPos = gl_in[i].gl_Position;
            gl_Position = ubo.shadowProj[face] * fragPos;
            EmitVertex();
        }
        EndPrimitive();
    }
}

/* https://github.com/sydneyzh/variance_shadow_mapping_vk/tree/gh-vsm-master/shadow */
