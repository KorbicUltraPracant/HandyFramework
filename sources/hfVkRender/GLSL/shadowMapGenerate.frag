#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 fragPos;

layout(set = 0, binding = 1) uniform ubo1 {
	vec4 color;
	vec4 objectsColor;
	vec4 position;
	vec4 direction;
	vec4 eyePosition;
	float cutOff;
	uint lightingMode;
	uint samplerIdx;
	float farPlane;
} lightInfo;

void main(void) {
	// get distance between fragment and light source
	float lightDistance = length(fragPos - lightInfo.position.xyz);

	// map to [0;1] range by dividing by far_plane
	lightDistance = lightDistance / lightInfo.farPlane;

	// write this as modified depth
	gl_FragDepth = 0.0; //lightDistance;
}
