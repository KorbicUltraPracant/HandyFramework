/**
* @author Radek Blahos
* @project hfVkWidget Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfAx/ExceptionMacros.h>
#include <hfAx/AuxInlines.inl>
#include <hfVkRender/Scene.h>
#include <fstream>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <hfVkRender/CommandBufferSet.h>

using namespace hf::Ax;
using namespace hf::Vk;
using namespace std;

hf::Vk::Render::Scene::~Scene() {

}

void hf::Vk::Render::Scene::storeMeshes() {
	hf::Vk::CreateInfo::Buffer bI(hf::Vk::CreateInfo::Buffer::Usage::VERTEXBUFFER);
	bI.assignMemory = true;
	bI.pManager = _description->pManager;
	bI.size = sceneData->vertices.size();

	// obtain cmd buff
	CommandBuffer& keepComands = _description->pCmdSet->getNextBuffer();

	// store vertices
	buffers.push_back(UPTR(hf::Vk::Buffer, bI));
	buffers[0]->stashOnGPU((void*)sceneData->vertices.data(), uint32_t(sceneData->vertices.size() * sizeof(float)), keepComands, *_description->pCmdProcessor);

	// store normals
	bI.size = sceneData->normals.size();
	buffers.push_back(UPTR(hf::Vk::Buffer, bI));
	buffers[1]->stashOnGPU((void*)sceneData->normals.data(), uint32_t(sceneData->normals.size() * sizeof(float)), keepComands, *_description->pCmdProcessor);

	// store texcoords
	bI.size = sceneData->normals.size();
	buffers.push_back(UPTR(hf::Vk::Buffer, bI));
	buffers[2]->stashOnGPU((void*)sceneData->texcoords.data(), uint32_t(sceneData->texcoords.size() * sizeof(float)), keepComands, *_description->pCmdProcessor);

	// store indices
	bI.size = indices.size();
	bI.usage = vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferDst;
	bI.propertyFlags = vk::MemoryPropertyFlagBits::eDeviceLocal;
	buffers.push_back(UPTR(hf::Vk::Buffer, bI));
	buffers[3]->stashOnGPU((void*)indices.data(), uint32_t(indices.size() * sizeof(float)), keepComands, *_description->pCmdProcessor, 0); // s texturama idx 3

	// return cmd buffer
	_description->pCmdSet->returnCurrentBuffer();

}

void hf::Vk::Render::Scene::stashTextureOnGPU(void* data, uint32_t width, uint32_t height) {
	// nasyp whole image into vk image
	// buffer texture into GPU
	CreateInfo::Image imageCI(CreateInfo::Image::Usage::TEXTURE);
	imageCI.setPtrManager(_description->pManager)
		.setWidth(width)
		.setHeight(height)
		.setFormatSize(4 * sizeof(uint8_t));
	imageCI.createView = true;

	// create default geVkImage object and create it
	bakedTextures.push_back(UPTR(hf::Vk::Image, imageCI));

	// copy data to baked texture
	// obtain cmd buff
	CommandBuffer& keepComands = _description->pCmdSet->getNextBuffer();

	// change layout from uninit to transfer dst
	Info::Image::ChangeLayout changeI(Info::Image::ChangeLayout::TransferOp::LOADING_TEX);
	bakedTextures.back()->changeLayout(keepComands, changeI, *_description->pCmdProcessor);

	// fills this buffer with data provided in vector
	bakedTextures.back()->stashOnGPU(data, width * height * 4, keepComands, *_description->pCmdProcessor);

	// change layout from transfer dst to sampler readonly
	changeI = Info::Image::ChangeLayout(Info::Image::ChangeLayout::TransferOp::USE_TEX);
	bakedTextures.back()->changeLayout(keepComands, changeI, *_description->pCmdProcessor);

	// return cmd buffer
	_description->pCmdSet->returnCurrentBuffer();
}

void hf::Vk::Render::Scene::storeTextures() {
	// acquiere max allowed dimensions of texture on ps device
	//vk::PhysicalDeviceProperties props = _description->pManager->getDevice().getPhysicalDevice().getProperties();
	// props.limits.maxImageDimension2D max width || height allowed on that device

	std::unordered_map<std::string, uint32_t> textureLocation;

	// Load n GPU Store diffuse materials
	for (auto& mat : sceneData->materials) {
		if (mat.second->diffuseTexname.length() > 0) {
			// Only load the texture if it is not already loaded
			if (sceneData->materials.find(mat.second->diffuseTexname) == sceneData->materials.end()) {
				int w, h;
				int comp;

				std::string texture_filename = mat.second->diffuseTexname;
				std::fstream textureFile(texture_filename);
				if (!textureFile.is_open()) {
					// Append base dir.
					texture_filename = mat.second->baseDir + mat.second->diffuseTexname;
					textureFile.open(texture_filename);
					if (!textureFile.is_open()) {
						std::cerr << "Unable to find file: " << texture_filename << ":(.\n Press any key to exit...\n";
						exit(1);
					}
				}

				unsigned char* image = stbi_load(texture_filename.c_str(), &w, &h, &comp, STBI_rgb_alpha);
				// fail or not RGB image
				if (!image) {
					std::cerr << "Unable to load texture: " << texture_filename << ":(.\n Press any key to exit...\n";
					exit(1);
				}
			
				stashTextureOnGPU(image, w, h);
				textureLocation[mat.second->diffuseTexname] = bakedTextures.size() - 1;

				stbi_image_free(image);
			}
		}
	}

	// redistribute tex locs to meshes
	for (auto& mesh : meshes) {
		if (mesh.second.material_name.size())
			mesh.second.bakedTextureId = textureLocation[mesh.second.material_name];
	}

	// load dummy texture if there is no textured model
	if (!bakedTextures.size())
		storeDummyTexture();

}

void hf::Vk::Render::Scene::storeDummyTexture() {
	// nasyp whole image into vk image
	// buffer texture into GPU
	
	// fills this buffer with data provided in vector -> generate Default grey texture
	std::array<uint8_t, 16> image{};
	for (int i = 0; i < 4; i++) {
		image[i] = image[i + 1] = image[i + 2] = image[i + 3] = 77;
	}
	
	stashTextureOnGPU(image.data(), 2, 2);
}

void hf::Vk::Render::Scene::createSceneGraph() {

}

void hf::Vk::Render::Scene::stashOnGPU() {
	storeMeshes();
	storeTextures();
	sceneData = nullptr; // dealocate scene data same as call sceneData.reset()
}

/*
	vector<uint8_t> singleBakedTexture;
	// if there is not possible to create any bigger texture, due device caps
	if ((singleBakedTexture.size() + w * h) / 2 >= props.limits.maxImageDimension2D) {
		// stash on gpu
		uint32_t dimension = sqrt(singleBakedTexture.size());
		stashOnGPU(singleBakedTexture.data(), dimension, dimension);

		// modify texture coords

		// clear dimension map

		// clear single baked texture
		singleBakedTexture.clear();
	}
	// there is still space on baked texture
	else {
		// add texture to baked one
		singleBakedTexture.insert(singleBakedTexture.end(), image, image + w * h);
		// write dimension map for texture coords redistribution
		dimensionMap[mat.first] = vk::Extent2D((uint32_t)w, (uint32_t)h);

		// there is last texture to store and last baked texture was not stored, because code is inside this condition
		if (counter + 1 == sceneData->materials.size())
			stashOnGPU(image, w, h);
	}
	// recalculate texture coords for all meshes to point this newly created baked texture
	/*
		need to transform original normalized tx coords into 1d buffer offset as offset = (v*tx.height * tx.width) + u*tx.width [y*width + x]
		and then find new normalized baked tx coords as x = offset%ultimateSize, y = offset/ultimateSize, where texWidth && textHeight = ultimateSize;
		and finaly normalize x,y with ultimateSize to obtain new uv coords
		Ok one last thing since the origin of texture coordinates in Vulkan is the top-left corner, whereas the OBJ format assumes the bottom-left corner,
		flipping the vertical component of the texture coordinates is requiered [reason: https://vulkan-tutorial.com/Loading_models]
	* /
	uint32_t ultimateSize = singleBakedTexture.size() / 8; // /2 half size * 4 bacause rgb[3 values] need to index per one
	for (auto& mesh : meshes) {
		for (int i = mesh.second.indicesPos.start; i < mesh.second.indicesPos.end; i++) {
			uint32_t idx = indices[i];
			float u(sceneData->texcoords[2 * idx + 0]), v(sceneData->texcoords[2 * idx + 1]);
			uint32_t offset = (v * dimensionMap[mesh.second.material_name].height * dimensionMap[mesh.second.material_name].width) + u * dimensionMap[mesh.second.material_name].width;
			//cerr << offset << endl;
			//float y = offset / ultimateSize;
			//float x = offset % ultimateSize;
			//cerr << x << " " << y << endl;

			// final new coords
			sceneData->texcoords[2 * idx + 0] = float(offset % ultimateSize) / float(ultimateSize);
			sceneData->texcoords[2 * idx + 1] = 1 - float(offset / ultimateSize) / (float)ultimateSize;

			//scene.texcoords[2 * i + 0] = (scene.texcoords[2 * i + 0] < 0) ? 0 : scene.texcoords[2 * i + 0];
			//scene.texcoords[2 * i + 1] = (scene.texcoords[2 * i + 1] < 0) ? 0 : scene.texcoords[2 * i + 1];

			//cerr << scene.texcoords[2 * i + 0] << " " << scene.texcoords[2 * i + 1] << endl;
		}
	}
*/
