/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVkRender/Presenter.h>
#include <hfAx/AuxInlines.inl>
#include <hfAx/Exception.h>
#include <hfVk/ImageInfo.h>
#include <thread>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* ------- Core Class ------- */
/*
	Rozdelit na 2 casti init swapchain alone, pak potreba vytvorit renderpass na kterem budou stavit framebuffery!! a pak clean color rp attachmentu
*/
void hf::Vk::Presenter::init(hf::Vk::CreateInfo::Presenter& createInfo) {
	createInfo.swapchainCreateInfo.surface = _presentSurface;

	// init swapchain
	_swapchain = UPTR(hf::Vk::Swapchain, createInfo.swapchainCreateInfo);

	if (_description != nullptr)
		delete _description;

	_description = new hf::Vk::Info::Presenter(createInfo);

	// init depthbuffer
	createDepthBuffer();

}

void hf::Vk::Presenter::addFramebuffers(RenderPass& renderPass) {
	CreateInfo::Framebuffer framebuffCI;
	framebuffCI.pDevice = &_description->pMemManager->getDevice();
	framebuffCI.activeRenderPass = &renderPass;
	framebuffCI.width = _swapchain->getDescription().width;
	framebuffCI.height = _swapchain->getDescription().height;
	framebuffCI.layersCount = _swapchain->getDescription().imageArrayLayers;

	// for single renderpass can be used just single Framebuffers, be carefull here you overwrite _description->renderPass_FramebuffersMapPtr for another swapchain 
	if (_description->renderPass_FramebuffersMapPtr->find(&renderPass) != _description->renderPass_FramebuffersMapPtr->end())
		_description->renderPass_FramebuffersMapPtr->erase(&renderPass);

	for (unsigned i = 0; i < _swapchain->getImagesVec().size(); i++) {
		framebuffCI.attachmentRefs.clear();
		framebuffCI.attachmentRefs.insert(framebuffCI.attachmentRefs.begin(), { &*_swapchain->getImagesVec()[i]->getViews().front(), &*_depthbuffer->getViews().front() });

		// create framebuff for all swapchain images
		(*_description->renderPass_FramebuffersMapPtr)[&renderPass].push_back(UPTR(Framebuffer, framebuffCI));
	}
}

void Presenter::createDepthBuffer() {
	// create info se vyplni automaticky na zaklade konstuktoru 
	CreateInfo::Image depthbuffCI(CreateInfo::Image::Usage::DEPTHBUFFER, _swapchain->getDescription().width, _swapchain->getDescription().height, false, true);
	depthbuffCI.pManager = _description->pMemManager;

	// alloc image object on heap
	_depthbuffer = UPTR(Image, depthbuffCI);

	// ctor image view
    CreateInfo::ImageView viewCI;
    viewCI.setImage(&*_depthbuffer)
        .setViewType(vk::ImageViewType::e2D)
        .setFormat(vk::Format::eD32Sfloat)
        .setComponentMaping({})
        .setSubresourceRange({ vk::ImageAspectFlagBits::eDepth, 0, 1, 0, 1 });

	_depthbuffer->addView(viewCI);

}

void hf::Vk::Presenter::transformImagesToProperLayout(hf::Vk::CommandBuffer& buffer) {
	// change images
	Info::Image::ChangeLayout changeI(Info::Image::ChangeLayout::TransferOp::IMG_INIT);

	for (auto& image : _swapchain->getImagesVec()) {
		changeI.image = *image;
		image->changeLayout(buffer, changeI);
	}

	// change depthbuffer
	changeI = Info::Image::ChangeLayout(Info::Image::ChangeLayout::TransferOp::DEPTH_TEST);
	changeI.image = *_depthbuffer;
	_depthbuffer->changeLayout(buffer, changeI);
}

void hf::Vk::Presenter::clearRenderResources(uint32_t swapchainImageIdx, hf::Vk::CommandBuffer& buffer) {
	_swapchain->getImage(swapchainImageIdx).clearColor(std::array<float, 4>{0.7f, 0.7f, 0.7f, 1.0f}, buffer);
	_depthbuffer->clearDepthStencil({1.0f, 0 }, buffer);
}

