/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVkRender/VertexBuffer.h>

using namespace hf::Vk;

void hf::Vk::Render::VertexBuffer::create(CreateInfo::VertexBuffer& createInfo) {
	_data.create(createInfo.bufferInfo);
	_description = std::move(createInfo.vertexInputdescription);

	// if memory is host visible map it
	if ((createInfo.bufferInfo.propertyFlags & vk::MemoryPropertyFlagBits::eHostVisible) == vk::MemoryPropertyFlagBits::eHostVisible) {
		_viewPtr = _data.map();

		// if defined any data to be copied inside this memory
		if (createInfo.dataLenInBytes > 0 && createInfo.sourceData != nullptr)
			memcpy(_viewPtr, createInfo.sourceData, createInfo.dataLenInBytes);
	}
	// buffer is defined to be stashed inside gpu
	else if (createInfo.dataLenInBytes > 0 && createInfo.sourceData != nullptr && createInfo.execBuffer && createInfo.copyAgent) {
		// data buffer's memory is provided from only gpu visible heap
		_data.stashOnGPU(createInfo.sourceData, createInfo.dataLenInBytes, *createInfo.execBuffer, *createInfo.copyAgent, 0);
	}

}