#include <hfVkRender/Renderer.h>
#include <hfVkRender/CommandBufferRecording.h>

void hf::Vk::CommandBufferRecording::Draw::Core::record(hf::Vk::CommandBuffer& keepCommands) {
	/* bind Pipeline */
	keepCommands.bindGraphicPipeline(*pGraphicPipeline);

	/* set viewport and scissor test */
	Info::SetViewport viewport = *this;
	keepCommands.setViewport(viewport);

	Info::SetScissor scissor;
	scissor.width = renderWidth;
	scissor.height = renderHeight;
	keepCommands.setScissor(scissor);

	/* bind vertexBuffers */
	if (vertexBuffersInfo.pBuffers.size() > 0)
		keepCommands.bindVertexBuffers(vertexBuffersInfo);

	/* bind descriptors */
	if (descriptorSetsInfo.pDescriptorSets.size() > 0)
		keepCommands.bindDescriptorSets(descriptorSetsInfo);

	/* push constants */
	if (pushConstants.pLayout != nullptr)
		keepCommands.pushConstants(pushConstants);
}

void hf::Vk::CommandBufferRecording::Draw::Draw::record(hf::Vk::CommandBuffer& keepCommands) {
	// let parent record its commands
	hf::Vk::CommandBufferRecording::Draw::Core::record(keepCommands);

	/* record draw command */
	keepCommands.draw(drawInfo);
}

void hf::Vk::CommandBufferRecording::Draw::Indexed::record(hf::Vk::CommandBuffer& keepCommands) {
	// let parent record its commands
	hf::Vk::CommandBufferRecording::Draw::Core::record(keepCommands);

	/* bind indexBuffer */
	keepCommands.bindIndexBuffer(indexBufferInfo);

	/* record draw command */
	keepCommands.drawIndexed(drawInfo);
}

void hf::Vk::CommandBufferRecording::Draw::IndexedIndirect::record(hf::Vk::CommandBuffer& keepCommands) {
	// let parent record its commands
	hf::Vk::CommandBufferRecording::Draw::Core::record(keepCommands);

	/* bind indexBuffer */
	keepCommands.bindIndexBuffer(indexBufferInfo);

	/* draw command */
	keepCommands.drawIndexedIndirect(drawInfo);
}