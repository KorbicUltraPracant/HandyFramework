/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVkRender/CommandBufferSet.h>
#include <hfVk/CommandBuffer.inl>
#include <hfAx/Exception.h>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

void Utility::Set<CommandBuffer>::init(CreateInfo::CommandBufferSet& createInfo) {
	// create command pool
	CreateInfo::CommandPool poolCI;
	poolCI.pDevice = createInfo.pDevice;
	poolCI.queueFamilyIndex = createInfo.familyQueueIdx;
	poolCI.flags = vk::CommandPoolCreateFlagBits::eTransient | vk::CommandPoolCreateFlagBits::eResetCommandBuffer;
	_pool.construct(poolCI); // throws it own exception

	// create cmdbuffers in bulk
	vk::CommandBufferAllocateInfo allocBuffers {
		_pool,
		vk::CommandBufferLevel::ePrimary,
		createInfo.initialBufferCount
	};

	// allocate cmd buffers 
	vector<vk::CommandBuffer> tmpVec;
	tmpVec.resize(allocBuffers.commandBufferCount);
	vk::Result res = createInfo.pDevice->getVkHandle().allocateCommandBuffers(&allocBuffers, tmpVec.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk CommandBufferSet:AllocateBuffers");

	if (_description != nullptr)
		delete _description;

	_description = new Info::CommandBufferSet(createInfo);

	// create hfVk cmdbuff from vk ones
	CreateInfo::CommandBuffer buffCI;
	buffCI.pPool = &_pool;
	buffCI.level = vk::CommandBufferLevel::ePrimary;
	for (auto& vkcmdbuff : tmpVec) {
		_buffers.push_back(UPTR(CommandBuffer, ));
		_buffers.back()->adopt(vkcmdbuff, buffCI);
	}

	_bufferUsage.init(allocBuffers.commandBufferCount);

}

void Utility::Set<CommandBuffer>::deinit() {
	// init function was not called upon object
	if (!_buffers.size())
		return;

	// before destroy attributes, must wait for queues finished work with cmdbuffers
	/*if (_description->createFences) {
		vector<VkFence> vkfences;
		for (auto& cmdbuffer : _buffers) {
			// only if buffer was passed into queue
			if (cmdbuffer->isSubmited()) {
				if (!cmdbuffer->getDescription().useEvent)
					vkfences.push_back(cmdbuffer->getLock().getPrimitive<Fence>().getVkFence());
			}
		}

		if (vkfences.size())
			vkWaitForFences(*_pool.getDescription().pDevice, _buffers.size(), vkfences.data(), VK_TRUE, MAX_WAIT_TIMEOUT);
	}*/

	if (_description != nullptr)
		delete _description;

	// destroy cmd buffers
	_buffers.clear(); _bufferUsage.reset(); _description = nullptr;

}

int Utility::Set<CommandBuffer>::getNextBufferIdx() {
	int freeIdx;
	while (1) {
		freeIdx = _bufferUsage.getNextFreeIndex();
		// search every available buffers and find one which is not operated with 
		if (freeIdx != -1) {
			if (!_buffers[freeIdx]->isSubmited())
				break;
			// else search continues
		}
		else {
			_buffers.front()->waitUntilExecutionFinished();

			freeIdx = 0;
			break;
		}
	}

	return freeIdx;
}

void Utility::Set<CommandBuffer>::addBuffers(uint32_t count) {
	// create cmdbuffers in bulk
	vk::CommandBufferAllocateInfo allocBuffers {
		_pool,
		vk::CommandBufferLevel::ePrimary,
		count
	};

	// allocate cmd buffers 
	vector<vk::CommandBuffer> tmpVec;
	tmpVec.resize(allocBuffers.commandBufferCount);
	vk::Result res = _description->pDevice->getVkHandle().allocateCommandBuffers(&allocBuffers, tmpVec.data());
	if (res != vk::Result::eSuccess)
		throw hf::Ax::Exception::Bad::Creation("hfVk CommandBufferSet:AllocateBuffers");

	// create hfVk cmdbuff from vk ones
	CreateInfo::CommandBuffer buffCI;
	buffCI.pPool = &_pool;
	buffCI.level = vk::CommandBufferLevel::ePrimary;

	for (auto& vkcmdbuff : tmpVec) {
		_buffers.push_back(UPTR(CommandBuffer, ));
		_buffers.back()->adopt(vkcmdbuff, buffCI);
	}

	_bufferUsage.resize(count);
}

/*
void Utility::Set<CommandBuffer>::deleteFencePointers() {
	for (auto& buffers : _buffers) {
		if (buffers->getLockPtr() != nullptr && !buffers->getDescription().useEvent)
			buffers->getLock().getPrimitivePtr() = nullptr;
	}
} */
