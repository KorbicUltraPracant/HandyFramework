#include <hfVkRender/Renderer.h>
#include <hfVkRender/CommandBufferRecording.h>
#include <hfAx/AuxInlines.inl>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* componentsDB.renderStrategies["name"] will be always created if not existed in first acess call... so no need to create it manualy */

void hf::Vk::Render::Renderer::bakedScene_FillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo) {
	createInfo.pDevice = &device;
	createInfo.layout = &componentsDB.pipelineLayouts["bakedScene"];
	createInfo.renderPass = &componentsDB.renderPasses["axis"];

	createInfo.vertexInput.resize(3);
	// position
	createInfo.vertexInput[0].location = 0;
	createInfo.vertexInput[0].stride = 3 * sizeof(float);

	// normal
	createInfo.vertexInput[1].location = 1;
	createInfo.vertexInput[1].stride = 3 * sizeof(float);

	// texture coords
	createInfo.vertexInput[2].location = 2;
	createInfo.vertexInput[2].stride = 2 * sizeof(float);
	createInfo.vertexInput[2].format = vk::Format::eR32G32Sfloat;

	createInfo.stagesInfo.resize(2);
	createInfo.stagesInfo[0].stage = vk::ShaderStageFlagBits::eVertex;
	createInfo.stagesInfo[0].module = &*componentsDB.renderStrategies["bakedScene"].shaders[0];

	createInfo.stagesInfo[1].stage = vk::ShaderStageFlagBits::eFragment;
	createInfo.stagesInfo[1].module = &*componentsDB.renderStrategies["bakedScene"].shaders[1];

	createInfo.viewport.scissors.resize(1);
	createInfo.viewport.scissors[0].extent = vk::Extent2D( 0, 0 );
	createInfo.viewport.scissors[0].offset = vk::Offset2D( (int32_t)presenter.getSwapchain().getDescription().width, (int32_t)presenter.getSwapchain().getDescription().height );

	createInfo.viewport.viewports.resize(1);
	createInfo.viewport.viewports[0].width = (float)presenter.getSwapchain().getDescription().width;
	createInfo.viewport.viewports[0].height = (float)presenter.getSwapchain().getDescription().height;
	createInfo.viewport.viewports[0].minDepth = createInfo.viewport.viewports[0].x = createInfo.viewport.viewports[0].y = 0.0f;
	createInfo.viewport.viewports[0].maxDepth = 1.0f;

	// dynamic states
	createInfo.dynamicStates.insert(createInfo.dynamicStates.begin(), { vk::DynamicState::eViewport, vk::DynamicState::eScissor });

	// blend attachment states
	createInfo.colorBlend.attachments.push_back(Info::PipelineSetUp::ColorBlendAttachmentState());

	// depth test
	createInfo.depthStencil.depthTestEnable = VK_TRUE;
	createInfo.depthStencil.depthWriteEnable = VK_TRUE;
	createInfo.depthStencil.depthCompareOp = vk::CompareOp::eLessOrEqual;
	
	// used in first subpass
	createInfo.usedForSubpass = 0;
}

void hf::Vk::Render::Renderer::bakedScene_SetUpDS() {
	hf::Vk::CreateInfo::DescriptorSet info;
	info.pManager = &descManager;
	info.mainLayoutInfo.pDevice = &device;
	info.mainLayoutInfo.layoutBindings.resize(2);

	// vertex shader uniformbuffer
	info.mainLayoutInfo.layoutBindings[0].binding = 0;
	info.mainLayoutInfo.layoutBindings[0].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	info.mainLayoutInfo.layoutBindings[0].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[0].stageFlags = vk::ShaderStageFlagBits::eVertex;

	// fragment shader uniformbuffer
	info.mainLayoutInfo.layoutBindings[1].binding = 1;
	info.mainLayoutInfo.layoutBindings[1].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[1].descriptorType = vk::DescriptorType::eUniformBuffer;
	info.mainLayoutInfo.layoutBindings[1].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[1].stageFlags = vk::ShaderStageFlagBits::eFragment;

	// create descriptor set
	componentsDB.renderStrategies["bakedScene"].descriptorSets.push_back(UPTR(hf::Vk::DescriptorSet, info)); // 0
}

void hf::Vk::Render::Renderer::bakedScene_SetUpTextureDS() {
	// create one combined sampler per baked texture
	hf::Vk::CreateInfo::DescriptorSet info;
	info.pManager = &descManager;
	info.mainLayoutInfo.pDevice = &device;
	info.mainLayoutInfo.layoutBindings.resize(1);
	info.mainLayoutInfo.layoutBindings[0].binding = 0;
	info.mainLayoutInfo.layoutBindings[0].descriptorCount = scene.bakedTextures.size();
	info.mainLayoutInfo.layoutBindings[0].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	info.mainLayoutInfo.layoutBindings[0].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[0].stageFlags = vk::ShaderStageFlagBits::eFragment;

	// create descriptor set
	componentsDB.renderStrategies["bakedScene"].descriptorSets.push_back(UPTR(hf::Vk::DescriptorSet, info));
}

void hf::Vk::Render::Renderer::bakedScene_WriteDS() {
	Info::UpdateDescriptorSets updateInfo;
	updateInfo.buffersInfo.resize(2);

	// vsUniforms
	updateInfo.buffersInfo[0].buffer = vsUbo.getBuffer();
	updateInfo.buffersInfo[0].offset = 0;
	updateInfo.buffersInfo[0].range = sizeof(hf::Vk::ShaderResources::UniformBuffer::VertexShader);

	// fsUniforms
	updateInfo.buffersInfo[1].buffer = fsUbo.getBuffer();
	updateInfo.buffersInfo[1].offset = 0;
	updateInfo.buffersInfo[1].range = sizeof(hf::Vk::ShaderResources::UniformBuffer::FragmentShader);

	/* write set infos */
	updateInfo.writeInfo.resize(2);

	// vsUniforms
	updateInfo.writeInfo[0].dstSet = componentsDB.renderStrategies["bakedScene"].descriptorSets[0]->getVkHandle();
	updateInfo.writeInfo[0].dstBinding = 0;
	updateInfo.writeInfo[0].dstArrayElement = 0;
	updateInfo.writeInfo[0].descriptorCount = 1;
	updateInfo.writeInfo[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	updateInfo.writeInfo[0].pBufferInfo = &updateInfo.buffersInfo[0];

	// fsUniforms
	updateInfo.writeInfo[1].dstSet = componentsDB.renderStrategies["bakedScene"].descriptorSets[0]->getVkHandle();
	updateInfo.writeInfo[1].dstBinding = 1;
	updateInfo.writeInfo[1].dstArrayElement = 0;
	updateInfo.writeInfo[1].descriptorCount = 1;
	updateInfo.writeInfo[1].descriptorType = vk::DescriptorType::eUniformBuffer;
	updateInfo.writeInfo[1].pBufferInfo = &updateInfo.buffersInfo[1];

	componentsDB.renderStrategies["bakedScene"].descriptorSets[0]->update(updateInfo);
}

void hf::Vk::Render::Renderer::bakedScene_WriteTextureDS() {
	if (!scene.bakedTextures.size())
		return;

	// sampler update info
	Info::UpdateDescriptorSets updateInfo;
	updateInfo.imagesInfo.resize(scene.bakedTextures.size());
	// for every image inside baked textures, write info
	for (int i = 0; i < scene.bakedTextures.size(); i++) {
		updateInfo.imagesInfo[i] = vk::DescriptorImageInfo()
			.setSampler(*componentsDB.samplers[0])
			.setImageView(scene.bakedTextures[i]->getView().getVkHandle()) // tady doplnit texture buffer az bude hotovy!!!
			.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
	}

	// write struct
	updateInfo.writeInfo.resize(1);
	updateInfo.writeInfo[0] = hf::Vk::Info::WriteDescriptorSet()
		.setDstSet(componentsDB.renderStrategies["bakedScene"].descriptorSets[1]->getVkHandle())
		.setDstBinding(0)
		.setDstArrayElement(0)
		.setDescriptorCount(scene.bakedTextures.size())
		.setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
		.setPImageInfo(&updateInfo.imagesInfo[0]);

	componentsDB.renderStrategies["bakedScene"].descriptorSets[1]->update(updateInfo);
}

void hf::Vk::Render::Renderer::bakedScene_CreatePipelineLayout() {
	hf::Vk::CreateInfo::PipelineLayout layoutCI;
	layoutCI.pDevice = &device;

	layoutCI.pDescriptorSetLayouts.push_back(&componentsDB.renderStrategies["bakedScene"].descriptorSets[0]->getLayout());
	layoutCI.pDescriptorSetLayouts.push_back(&componentsDB.renderStrategies["bakedScene"].descriptorSets[1]->getLayout());
	
	componentsDB.pipelineLayouts.emplace("bakedScene", layoutCI);
}

void hf::Vk::Render::Renderer::bakedScene_LoadShaders() {
	// shaders
	hf::Vk::CreateInfo::ShaderModule basicCI;
	basicCI.pDevice = &device;

	string dirName(RENDER_RESOURCES);

	basicCI.path = dirName + string("/shaders/bakedScene_vert.spv");
	componentsDB.renderStrategies["bakedScene"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedV 0

	basicCI.path = dirName + string("/shaders/bakedScene_frag.spv");
	componentsDB.renderStrategies["bakedScene"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedF 1
}

void hf::Vk::Render::Renderer::bakedScene_RecordCommands() {
	hf::Vk::CommandBufferRecording::Batch<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect> allCmds;
	allCmds.renderPasses.push_back(UPTR(hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect>, ));
	hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect>& rp = *allCmds.renderPasses.front();
	hf::Vk::CommandBufferRecording::Draw::IndexedIndirect dI;

	// pipeline for textured models
	dI.renderWidth = presenter.getSwapchain().getDescription().width;
	dI.renderHeight = presenter.getSwapchain().getDescription().height;
	dI.pGraphicPipeline = &pipelineDB.obtainGraphicPipeline("bakedScene");
	// render pass info
	rp.renderPassInfo.pRenderPass = &componentsDB.renderPasses["axis"];
	rp.renderPassInfo.renderArea.extent = vk::Extent2D(dI.renderWidth, dI.renderHeight);
	rp.renderPassInfo.clearColors.emplace_back(vk::ClearColorValue(std::array<float, 4>({ { 0.2f, 0.2f, 0.2f, 0.2f } })));
	rp.renderPassInfo.clearColors.emplace_back(vk::ClearDepthStencilValue(1.0f, 0u));

	// index buffer
	dI.indexBufferInfo.pBuffer = &*scene.buffers[3]; // nezapomenut prepnut na 3

	// vertex buffers
	dI.vertexBuffersInfo.firstBinding = 0;
	dI.vertexBuffersInfo.offsets.push_back(0);
	dI.vertexBuffersInfo.pBuffers.push_back(scene.buffers[0]->getVkHandle());
	dI.vertexBuffersInfo.offsets.push_back(0);
	dI.vertexBuffersInfo.pBuffers.push_back(scene.buffers[1]->getVkHandle());
	dI.vertexBuffersInfo.offsets.push_back(0);
	dI.vertexBuffersInfo.pBuffers.push_back(scene.buffers[2]->getVkHandle());

	// clear image
	allCmds.clearImage = false;

	// descriptors
	dI.descriptorSetsInfo.bindPoint = vk::PipelineBindPoint::eGraphics;
	dI.descriptorSetsInfo.firstSet = 0;
	dI.descriptorSetsInfo.pPipelineLayout = &componentsDB.pipelineLayouts["bakedScene"];
	dI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.renderStrategies["bakedScene"].descriptorSets[0]);
	dI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.renderStrategies["bakedScene"].descriptorSets[1]);

	// draw info 
	dI.drawInfo = hf::Vk::Info::DrawIndexedIndirect()
		.setBuffer(indirectDrawParameters.getBuffer().getVkHandle())
		.setDrawCount(1)
		.setOffset(0)
		.setStride(sizeof(vk::DrawIndexedIndirectCommand));

	rp.drawInfos.resize(1);
	for (int i = 0; i < presenter.getImageCount(); i++) {
		// assign framebuffer for swapchain image i image
		rp.renderPassInfo.pFramebuffer = &presenter.getFramebuffer(componentsDB.renderPasses["axis"], i);

		// n record commands for swapchain image i
		rp.drawInfos[0] = dI;

		allCmds.record(componentsDB.renderStrategies["bakedScene"].commandBuffers.getBuffer(i));
	}
}
