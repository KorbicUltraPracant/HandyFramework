#include <hfVkRender/Renderer.h>
#include <hfVkRender/CommandBufferRecording.h>
#include <hfAx/AuxInlines.inl>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* componentsDB.renderStrategies["name"] will be always created if not existed in first acess call... so no need to create it manualy */

void hf::Vk::Render::Renderer::shadowMapping_CreatePipelineLayouts() {
	hf::Vk::CreateInfo::PipelineLayout layoutCI;
	layoutCI.pDevice = &device;

	/* shadow map generation */
	layoutCI.pDescriptorSetLayouts.push_back(&componentsDB.renderStrategies["bakedScene"].descriptorSets[0]->getLayout());
	layoutCI.pDescriptorSetLayouts.push_back(&componentsDB.renderStrategies["shadowMapping"].descriptorSets[0]->getLayout());
	// !!!! tohle si x krat overit ze to vazne assignuje spravne 
	layoutCI.pushConstants.push_back(
		vk::PushConstantRange()
		.setOffset(0)
		.setSize(sizeof(uint32_t))
		.setStageFlags(vk::ShaderStageFlagBits::eVertex)
	);
	componentsDB.pipelineLayouts.emplace("shadowMapGen", layoutCI);

	/* render scene */
	layoutCI.pDescriptorSetLayouts[1] = &componentsDB.renderStrategies["shadowMapping"].descriptorSets[1]->getLayout();
	componentsDB.pipelineLayouts.emplace("shadowMapping", layoutCI);
}

/* generate depth map pipeline */
void hf::Vk::Render::Renderer::shadowMapping_GenerateMapFillPipelineInfo(std::vector<hf::Vk::CreateInfo::GraphicPipeDBEntry>& graphicPipesCreateInfos) {
	hf::Vk::CreateInfo::GraphicPipeline createInfo;
	createInfo.pDevice = &device;
	createInfo.layout = &componentsDB.pipelineLayouts["shadowMapGen"];
	createInfo.renderPass = &componentsDB.renderPasses["shadowMapping"];

	createInfo.vertexInput.resize(1);
	// position
	createInfo.vertexInput[0].location = 0;
	createInfo.vertexInput[0].stride = 3 * sizeof(float);

	createInfo.stagesInfo.resize(2);
	createInfo.stagesInfo[0].stage = vk::ShaderStageFlagBits::eVertex;
	createInfo.stagesInfo[0].module = &*componentsDB.renderStrategies["shadowMapping"].shaders[0];

	createInfo.stagesInfo[1].stage = vk::ShaderStageFlagBits::eFragment;
	createInfo.stagesInfo[1].module = &*componentsDB.renderStrategies["shadowMapping"].shaders[1];

	createInfo.viewport.scissors.resize(1);
	createInfo.viewport.scissors[0].extent = vk::Extent2D(0, 0);
	createInfo.viewport.scissors[0].offset = vk::Offset2D((int32_t)presenter.getSwapchain().getDescription().width, (int32_t)presenter.getSwapchain().getDescription().height);

	createInfo.viewport.viewports.resize(1);
	createInfo.viewport.viewports[0].width = (float)presenter.getSwapchain().getDescription().width;
	createInfo.viewport.viewports[0].height = (float)presenter.getSwapchain().getDescription().height;
	createInfo.viewport.viewports[0].minDepth = createInfo.viewport.viewports[0].x = createInfo.viewport.viewports[0].y = 0.0f;
	createInfo.viewport.viewports[0].maxDepth = 1.0f;

	// dynamic states
	createInfo.dynamicStates.insert(createInfo.dynamicStates.begin(), { vk::DynamicState::eViewport, vk::DynamicState::eScissor });

	// blend attachment states
	//createInfo.colorBlend.attachments.push_back(Info::PipelineSetUp::ColorBlendAttachmentState());

	// depth test
	createInfo.depthStencil.depthTestEnable = VK_TRUE;
	createInfo.depthStencil.depthWriteEnable = VK_TRUE;
	createInfo.depthStencil.depthCompareOp = vk::CompareOp::eLessOrEqual;

	/* 
		for each subpass need to create its own pipeline :(,
		i hope its because subpasses are executed independently(i haven't set any dependencies between them), 
		and so not co create concurence over single pipeline, use pipeline for each sp
	*/
	/*uint32_t parentIdx = graphicPipesCreateInfos.size(); // graphicPipesCreateInfos.size() - 1 + 1 will be idx of first shadow depth map subpass pipeline
	for (int i = 0; i < 6; i++) {
		graphicPipesCreateInfos.push_back(hf::Vk::CreateInfo::GraphicPipeDBEntry());

		// used in first subpass
		createInfo.usedForSubpass = i;

		if (i > 0) 
			createInfo.setBasePipelineIndex(parentIdx);

		graphicPipesCreateInfos.back().name = "shadowMapGenSub" + std::to_string(i);
		graphicPipesCreateInfos.back().graphicPipeCreateInfo = createInfo;
	}*/

	graphicPipesCreateInfos.push_back(hf::Vk::CreateInfo::GraphicPipeDBEntry());

	// used in first subpass
	createInfo.usedForSubpass = 0;

	graphicPipesCreateInfos.back().name = "shadowMapGenSub" + std::to_string(0);
	graphicPipesCreateInfos.back().graphicPipeCreateInfo = createInfo;
}

/* render to swapchain image pipeline */
void hf::Vk::Render::Renderer::shadowMapping_RenderSceneFillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo) {
	createInfo.pDevice = &device;
	createInfo.layout = &componentsDB.pipelineLayouts["shadowMapping"];
	createInfo.renderPass = &componentsDB.renderPasses["axis"];

	createInfo.vertexInput.resize(3);
	// position
	createInfo.vertexInput[0].location = 0;
	createInfo.vertexInput[0].stride = 3 * sizeof(float);

	// normal
	createInfo.vertexInput[1].location = 1;
	createInfo.vertexInput[1].stride = 3 * sizeof(float);

	// texture coords
	createInfo.vertexInput[2].location = 2;
	createInfo.vertexInput[2].stride = 2 * sizeof(float);
	createInfo.vertexInput[2].format = vk::Format::eR32G32Sfloat;

	createInfo.stagesInfo.resize(2);
	createInfo.stagesInfo[0].stage = vk::ShaderStageFlagBits::eVertex;
	createInfo.stagesInfo[0].module = &*componentsDB.renderStrategies["shadowMapping"].shaders[2];

	createInfo.stagesInfo[1].stage = vk::ShaderStageFlagBits::eFragment;
	createInfo.stagesInfo[1].module = &*componentsDB.renderStrategies["shadowMapping"].shaders[3];

	createInfo.viewport.scissors.resize(1);
	createInfo.viewport.scissors[0].extent = vk::Extent2D(0, 0);
	createInfo.viewport.scissors[0].offset = vk::Offset2D((int32_t)presenter.getSwapchain().getDescription().width, (int32_t)presenter.getSwapchain().getDescription().height);

	createInfo.viewport.viewports.resize(1);
	createInfo.viewport.viewports[0].width = (float)presenter.getSwapchain().getDescription().width;
	createInfo.viewport.viewports[0].height = (float)presenter.getSwapchain().getDescription().height;
	createInfo.viewport.viewports[0].minDepth = createInfo.viewport.viewports[0].x = createInfo.viewport.viewports[0].y = 0.0f;
	createInfo.viewport.viewports[0].maxDepth = 1.0f;

	// dynamic states
	createInfo.dynamicStates.insert(createInfo.dynamicStates.begin(), { vk::DynamicState::eViewport, vk::DynamicState::eScissor });

	// blend attachment states
	createInfo.colorBlend.attachments.push_back(Info::PipelineSetUp::ColorBlendAttachmentState());

	// depth test
	createInfo.depthStencil.depthTestEnable = VK_TRUE;
	createInfo.depthStencil.depthWriteEnable = VK_TRUE;
	createInfo.depthStencil.depthCompareOp = vk::CompareOp::eLessOrEqual;

	// used in first subpass
	createInfo.usedForSubpass = 0;
}

void hf::Vk::Render::Renderer::shadowMapping_SetUpDS() {
	hf::Vk::CreateInfo::DescriptorSet info;
	info.pManager = &descManager;
	info.mainLayoutInfo.pDevice = &device;

	/* generating map DS */
	/* separate gsubo ds */
	info.mainLayoutInfo.layoutBindings.resize(1);

	// shadowDepthCast matrices
	info.mainLayoutInfo.layoutBindings[0].binding = 0;
	info.mainLayoutInfo.layoutBindings[0].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	info.mainLayoutInfo.layoutBindings[0].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[0].stageFlags = vk::ShaderStageFlagBits::eVertex;
					
	// create descriptor set
	componentsDB.renderStrategies["shadowMapping"].descriptorSets.push_back(UPTR(hf::Vk::DescriptorSet, info));

	/* rendering DS */
	info.mainLayoutInfo.layoutBindings.clear();
	info.mainLayoutInfo.layoutBindings.resize(2);

	// textures
	info.mainLayoutInfo.layoutBindings[0].binding = 0;
	info.mainLayoutInfo.layoutBindings[0].descriptorCount = scene.bakedTextures.size();
	info.mainLayoutInfo.layoutBindings[0].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	info.mainLayoutInfo.layoutBindings[0].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[0].stageFlags = vk::ShaderStageFlagBits::eFragment;

	// depth map
	info.mainLayoutInfo.layoutBindings[1].binding = 1;
	info.mainLayoutInfo.layoutBindings[1].descriptorCount = 1; // 6
	info.mainLayoutInfo.layoutBindings[1].descriptorType = vk::DescriptorType::eCombinedImageSampler;
	info.mainLayoutInfo.layoutBindings[1].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[1].stageFlags = vk::ShaderStageFlagBits::eFragment;

	// create descriptor set
	componentsDB.renderStrategies["shadowMapping"].descriptorSets.push_back(UPTR(hf::Vk::DescriptorSet, info));
}

void hf::Vk::Render::Renderer::shadowMapping_WriteDS() {
	Info::UpdateDescriptorSets updateInfo;
	/* generating map DS */
	updateInfo.buffersInfo.resize(1);

	// geometry ubo
	updateInfo.buffersInfo[0].buffer = gsUbo.getBuffer();
	updateInfo.buffersInfo[0].offset = 0;
	updateInfo.buffersInfo[0].range = sizeof(hf::Vk::ShaderResources::UniformBuffer::GeometryShader);

	// shadow map
	updateInfo.imagesInfo.resize(scene.bakedTextures.size() + 1);
	updateInfo.imagesInfo[0] = vk::DescriptorImageInfo()
		.setSampler(*componentsDB.samplers[0])
		.setImageView(shadowCubeMap.getView().getVkHandle()) // tady doplnit texture buffer az bude hotovy!!!
		.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);

	// textures
	// for every image inside baked textures, write info
	for (int i = 0; i < scene.bakedTextures.size(); i++) {
		updateInfo.imagesInfo[1 + i] = vk::DescriptorImageInfo()
			.setSampler(*componentsDB.samplers[0])
			.setImageView(scene.bakedTextures[i]->getView().getVkHandle()) // tady doplnit texture buffer az bude hotovy!!!
			.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
	}

	/* write set infos */
	updateInfo.writeInfo.resize(1);
	// gs ubo
	updateInfo.writeInfo[0].dstSet = componentsDB.renderStrategies["shadowMapping"].descriptorSets[0]->getVkHandle();
	updateInfo.writeInfo[0].dstBinding = 0;
	updateInfo.writeInfo[0].dstArrayElement = 0;
	updateInfo.writeInfo[0].descriptorCount = 1;
	updateInfo.writeInfo[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	updateInfo.writeInfo[0].pBufferInfo = &updateInfo.buffersInfo[0];

	/* generating */
	componentsDB.renderStrategies["shadowMapping"].descriptorSets[0]->update(updateInfo);

	updateInfo.writeInfo.clear();
	updateInfo.writeInfo.resize(2);
	// shadow map
	updateInfo.writeInfo[0] = hf::Vk::Info::WriteDescriptorSet()
	.setDstSet(componentsDB.renderStrategies["shadowMapping"].descriptorSets[1]->getVkHandle())
	.setDstBinding(1)
	.setDstArrayElement(0)
	.setDescriptorCount(1)
	.setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
	.setPImageInfo(&updateInfo.imagesInfo[0]);

	// textures
	updateInfo.writeInfo[1] = hf::Vk::Info::WriteDescriptorSet()
	.setDstSet(componentsDB.renderStrategies["shadowMapping"].descriptorSets[1]->getVkHandle())
	.setDstBinding(0)
	.setDstArrayElement(0)
	.setDescriptorCount(scene.bakedTextures.size())
	.setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
	.setPImageInfo(&updateInfo.imagesInfo[1]);

	/* rendering DS */
	componentsDB.renderStrategies["shadowMapping"].descriptorSets[1]->update(updateInfo);

}

void hf::Vk::Render::Renderer::shadowMapping_SetUpRenderPass() {
	hf::Vk::CreateInfo::RenderPass rpCI;
	rpCI.pDevice = &device;
	rpCI.attachments.resize(1);

	// depth Cube image
	rpCI.attachments[0].format = shadowCubeMap.getDescription().format;
	rpCI.attachments[0].samples = vk::SampleCountFlagBits::e1;
	rpCI.attachments[0].loadOp = vk::AttachmentLoadOp::eLoad;
	rpCI.attachments[0].storeOp = vk::AttachmentStoreOp::eStore;
	rpCI.attachments[0].stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
	rpCI.attachments[0].stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	rpCI.attachments[0].initialLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
	rpCI.attachments[0].finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	/* ref to each cube face */
	rpCI.attachmentReferences.resize(1);
	rpCI.attachmentReferences[0] = { 0, vk::ImageLayout::eDepthStencilAttachmentOptimal };

	// create six same subpasses... only data will differ(shadow map cast matrix)
	rpCI.subpasses.resize(1);
	rpCI.subpasses[0].pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
	rpCI.subpasses[0].colorAttachmentCount = 0;
	rpCI.subpasses[0].pColorAttachments = VK_NULL_HANDLE;
	rpCI.subpasses[0].pDepthStencilAttachment = &rpCI.attachmentReferences[0];
	/*
	for (uint32_t i = 1; i < 6; i++) {
		// all the attachments have same description
		rpCI.attachments[i] = rpCI.attachments[0];
		// create attachment ref to attachment i
		rpCI.attachmentReferences[i] = { i, vk::ImageLayout::eDepthStencilReadOnlyOptimal };
		// all the subpasses are the same
		rpCI.subpasses[i] = rpCI.subpasses[0];
		// set face i as color attachment
		rpCI.subpasses[i].pDepthStencilAttachment = &rpCI.attachmentReferences[0]; 
	}
	*/

	componentsDB.renderPasses.emplace("shadowMapping", rpCI);
}

void hf::Vk::Render::Renderer::shadowMapping_CreateFramebuffers() {
	/* Shadow depth map */
	CreateInfo::Framebuffer framebuffCI;
	framebuffCI.pDevice = &device;
	framebuffCI.activeRenderPass = &componentsDB.renderPasses["shadowMapping"];
	framebuffCI.width = shadowCubeMap.getDescription().width;
	framebuffCI.height = shadowCubeMap.getDescription().height;
	framebuffCI.layersCount = 1; //shadowCubeMap.getDescription().layerCount;

	// for single renderpass can be used just single Framebuffers, be carefull here you overwrite _description->renderPass_FramebuffersMapPtr for another swapchain 
	if (componentsDB.renderPass_FramebuffersMap.find(&componentsDB.renderPasses["shadowMapping"]) != componentsDB.renderPass_FramebuffersMap.end())
		componentsDB.renderPass_FramebuffersMap.erase(&componentsDB.renderPasses["shadowMapping"]);

	framebuffCI.attachmentRefs.resize(1);
	/* create view for each cube face */
	for (uint32_t i = 0; i < 6; i++) {
		CreateInfo::ImageView viewCI;
		viewCI.setImage(&shadowCubeMap)
			.setViewType(vk::ImageViewType::eCube)
			.setFormat(shadowCubeMap.getDescription().format)
			.setComponentMaping({})
			.setSubresourceRange({ vk::ImageAspectFlagBits::eDepth, 0, 1, i, 1 }); // view for each layer

		shadowCubeMap.addView(viewCI);

		// n crate its framebuffer
		framebuffCI.attachmentRefs[0] = &*shadowCubeMap.getViews()[1 + i];

		// create framebuffer
		componentsDB.renderPass_FramebuffersMap[&componentsDB.renderPasses["shadowMapping"]].push_back(UPTR(Framebuffer, framebuffCI));
	}
}

void hf::Vk::Render::Renderer::shadowMapping_LoadShaders() {
	// shaders
	hf::Vk::CreateInfo::ShaderModule basicCI;
	basicCI.pDevice = &device;

	string dirName(RENDER_RESOURCES);

	basicCI.path = dirName + string("/shaders/shadowMapGenerate_vert.spv");
	componentsDB.renderStrategies["shadowMapping"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedV 0

	basicCI.path = dirName + string("/shaders/shadowMapGenerate_frag.spv");
	componentsDB.renderStrategies["shadowMapping"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedF 1

	basicCI.path = dirName + string("/shaders/shadowMapping_vert.spv");
	componentsDB.renderStrategies["shadowMapping"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedV 0

	basicCI.path = dirName + string("/shaders/shadowMapping_frag.spv");
	componentsDB.renderStrategies["shadowMapping"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedF 1
}

// --------------------------------- cube shadow map generation render pass --------------------------------- //
void hf::Vk::Render::Renderer::shadowMapping_ShadowMap_RecordCommands() {
	hf::Vk::CommandBufferRecording::Batch<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect> allCmds;
	hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect> rp1C;

	hf::Vk::CommandBufferRecording::Draw::IndexedIndirect sMGI;
	// render pass info
	sMGI.renderWidth = shadowCubeMap.getDescription().width;
	sMGI.renderHeight = shadowCubeMap.getDescription().height;
	rp1C.renderPassInfo.pRenderPass = &componentsDB.renderPasses["shadowMapping"];
	rp1C.renderPassInfo.renderArea.extent = vk::Extent2D(shadowCubeMap.getDescription().width, shadowCubeMap.getDescription().height);
	rp1C.renderPassInfo.clearColors.emplace_back(vk::ClearColorValue(std::array<float, 4>({ { 0.2f, 0.2f, 0.2f, 0.2f } })));
	rp1C.renderPassInfo.clearColors.emplace_back(vk::ClearDepthStencilValue(1.0f, 0u));

	// index buffer
	sMGI.indexBufferInfo.pBuffer = &*scene.buffers[3]; // nezapomenut prepnut na 3

	// vertex buffers
	sMGI.vertexBuffersInfo.firstBinding = 0;
	sMGI.vertexBuffersInfo.offsets.push_back(0);
	sMGI.vertexBuffersInfo.pBuffers.push_back(scene.buffers[0]->getVkHandle());

	// clear image
	allCmds.clearImage = false;

	// descriptors
	sMGI.descriptorSetsInfo.bindPoint = vk::PipelineBindPoint::eGraphics;
	sMGI.descriptorSetsInfo.firstSet = 0;
	sMGI.descriptorSetsInfo.pPipelineLayout = &componentsDB.pipelineLayouts["shadowMapGen"];
	sMGI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.renderStrategies["bakedScene"].descriptorSets[0]);
	sMGI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.renderStrategies["shadowMapping"].descriptorSets[0]);

	// draw info 
	sMGI.drawInfo = hf::Vk::Info::DrawIndexedIndirect()
		.setBuffer(indirectDrawParameters.getBuffer().getVkHandle())
		.setDrawCount(1)
		.setOffset(0)
		.setStride(sizeof(vk::DrawIndexedIndirectCommand));

	// pipeline for depth map generation
	sMGI.pGraphicPipeline = &pipelineDB.obtainGraphicPipeline("shadowMapGenSub" + std::to_string(0));

	// create draw infos for shadow depth map generation rp
	for (int i = 0; i < 6; i++) {
		// assign push constant to each subpass to index proper shadowdepth proj mat
		sMGI.pushConstants
			.setPValues(&pushConstants.matIdx[i])
			.setOffset(0)
			.setSize(sizeof(uint32_t))
			.setShaderStages(vk::ShaderStageFlagBits::eVertex)
			.setPipelineLayout(&componentsDB.pipelineLayouts["shadowMapGen"]);

		// assign framebuffer for cube map
		rp1C.renderPassInfo.pFramebuffer = &*componentsDB.renderPass_FramebuffersMap[&componentsDB.renderPasses["shadowMapping"]][i];
		
		allCmds.renderPasses.push_back(UPTR(hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect>, ));
		*allCmds.renderPasses.back() = rp1C;

		allCmds.renderPasses.back()->drawInfos.push_back(sMGI);	
	}

	// record cmd buff
	allCmds.record(componentsDB.renderStrategies["shadowMapping"].commandBuffers.getLastBuffer());
}

// --------------------------------- rendering with shadow map --------------------------------- //
void hf::Vk::Render::Renderer::shadowMapping_Render_RecordCommands() {
	hf::Vk::CommandBufferRecording::Batch<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect> allCmds;
	allCmds.renderPasses.push_back(UPTR(hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect>, ));
	hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect>& rp2C = *allCmds.renderPasses.back();

	hf::Vk::CommandBufferRecording::Draw::IndexedIndirect drawI;

	// pipeline for textured models
	drawI.pGraphicPipeline = &pipelineDB.obtainGraphicPipeline("shadowMapping");
	// render pass info
	drawI.renderWidth = presenter.getSwapchain().getDescription().width;
	drawI.renderHeight = presenter.getSwapchain().getDescription().height;
	rp2C.renderPassInfo.pRenderPass = &componentsDB.renderPasses["axis"];
	rp2C.renderPassInfo.renderArea.extent = vk::Extent2D(drawI.renderWidth, drawI.renderHeight);
	rp2C.renderPassInfo.clearColors.emplace_back(vk::ClearColorValue(std::array<float, 4>({ { 0.2f, 0.2f, 0.2f, 0.2f } })));
	rp2C.renderPassInfo.clearColors.emplace_back(vk::ClearDepthStencilValue(1.0f, 0u));

	// index buffer
	drawI.indexBufferInfo.pBuffer = &*scene.buffers[3]; // nezapomenut prepnut na 3

	// vertex buffers
	drawI.vertexBuffersInfo.firstBinding = 0;
	drawI.vertexBuffersInfo.offsets.push_back(0);
	drawI.vertexBuffersInfo.pBuffers.push_back(scene.buffers[0]->getVkHandle());
	drawI.vertexBuffersInfo.offsets.push_back(0);
	drawI.vertexBuffersInfo.pBuffers.push_back(scene.buffers[1]->getVkHandle());
	drawI.vertexBuffersInfo.offsets.push_back(0);
	drawI.vertexBuffersInfo.pBuffers.push_back(scene.buffers[2]->getVkHandle());

	// clear image
	allCmds.clearImage = false;

	// descriptors
	drawI.descriptorSetsInfo.bindPoint = vk::PipelineBindPoint::eGraphics;
	drawI.descriptorSetsInfo.firstSet = 0;
	drawI.descriptorSetsInfo.pPipelineLayout = &componentsDB.pipelineLayouts["shadowMapping"];
	drawI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.renderStrategies["bakedScene"].descriptorSets[0]);
	drawI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.renderStrategies["shadowMapping"].descriptorSets[1]);

	// draw info 
	drawI.drawInfo = hf::Vk::Info::DrawIndexedIndirect()
		.setBuffer(indirectDrawParameters.getBuffer().getVkHandle())
		.setDrawCount(1)
		.setOffset(0)
		.setStride(sizeof(vk::DrawIndexedIndirectCommand));

	// push draw info
	rp2C.drawInfos.push_back(drawI);

	// --------------------------------- for every swapchain image create cmd buffer --------------------------------- //
	for (int i = 0; i < presenter.getImageCount(); i++) {
		// assign framebuffer for swapchain image i image
		rp2C.renderPassInfo.pFramebuffer = &presenter.getFramebuffer(componentsDB.renderPasses["axis"], i);
		
		// record cmd buff for current swapchain img
		allCmds.record(componentsDB.renderStrategies["shadowMapping"].commandBuffers.getBuffer(i));
	}
}

void hf::Vk::Render::Renderer::shadowMapping_switchCubeImageLayout(CommandBuffer& commandBuffer) {
	static bool switchToSampling(true);

	// change to texture read friendly layout
	if (switchToSampling) {
		hf::Vk::Info::Image::ChangeLayout chI;
		chI.setProducentStage(vk::PipelineStageFlagBits::eLateFragmentTests)
			.setConsumentStage(vk::PipelineStageFlagBits::eFragmentShader)
			.setImage(shadowCubeMap.getVkHandle())
			.setSubresourceRange(vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eDepth, 0 , 1, 0, 6))
			.setSrcAccessMask(vk::AccessFlagBits::eDepthStencilAttachmentWrite)
			.setOldLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal)
			.setDstAccessMask(vk::AccessFlagBits::eShaderRead)
			.setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal);
			

		shadowCubeMap.changeLayout(commandBuffer, chI);

	}
	// switch to depth read/write friendly layout
	else {
		hf::Vk::Info::Image::ChangeLayout chI;
		chI.setProducentStage(vk::PipelineStageFlagBits::eAllGraphics)
			.setConsumentStage(vk::PipelineStageFlagBits::eEarlyFragmentTests)
			.setImage(shadowCubeMap.getVkHandle())
			.setSubresourceRange(vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eDepth, 0 , 1, 0, 6))
			.setSrcAccessMask(vk::AccessFlagBits::eShaderRead)
			.setOldLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
			.setDstAccessMask(vk::AccessFlagBits::eDepthStencilAttachmentWrite | vk::AccessFlagBits::eDepthStencilAttachmentRead)
			.setNewLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal);

		shadowCubeMap.changeLayout(commandBuffer, chI);
	}

	// switch to change
	switchToSampling = !switchToSampling;
}

/* Tutorial
Main:
https://learnopengl.com/Advanced-Lighting/Shadows/Point-Shadows

Cube Mapping:
https://en.wikipedia.org/wiki/Cube_mapping

Vulkan Cube Map example:
https://github.com/SaschaWillems/Vulkan
https://github.com/SaschaWillems/Vulkan/tree/master/data/shaders/shadowmapomni

udajne screw up on geometry shader use single renderpass with 6 subpasess to render into each cube face, mozna by bylo lepsi vyuzit i 
nejaky vicerohedron(podstava by nebyla ctverec ale nejaky viceuhelnik) aby se pokrylo vice uhlu na ktere se renderuji mapy(navrh na expansion)
ve vk tu bude mega ez. 
takze ve framebufferu bude n(6) image, proste nabindovat kazdou vrstvu genshadowmap image. pak v kazdem subpass pouzit push constantu
vkcmdpushconstant a nahrat tam aktualni matici -> lip nechat ty matice jako descriptor(ubo) a posilat tam jenom index do pole ktera se 
ma aktualne pouzit, ez. vsechno bakenu do jednoho cmdbufferu a pojedu jak pan... love it :)

ok uz vim kde je ten problem, prvni musim projit vsechny objekty sceny a s nima vytvorit shadow depth mapu a az teprve pak udelat
rendering vsech meshes :( 


// begin cmd buffer
componentsDB.renderStrategies["shadowMapping"].commandBuffers.getBuffer(i).beginRecording(vk::CommandBufferUsageFlagBits::eSimultaneousUse);

// record commands for shadow map generating
rp1C.record(componentsDB.renderStrategies["shadowMapping"].commandBuffers.getBuffer(i));

// record commands for changing cube map layout for sampling
shadowMapping_switchCubeImageLayout(componentsDB.renderStrategies["shadowMapping"].commandBuffers.getBuffer(i));

// n record commands for rendering into swapchain image i
rp2C.record(componentsDB.renderStrategies["shadowMapping"].commandBuffers.getBuffer(i));

// record commands for changing cube map layout for attaching as depth buffer
shadowMapping_switchCubeImageLayout(componentsDB.renderStrategies["shadowMapping"].commandBuffers.getBuffer(i));

// end cmd buffer
componentsDB.renderStrategies["shadowMapping"].commandBuffers.getBuffer(i).finishRecording();
*/
