/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVkRender/SharedBuffer.h>

using namespace hf::Vk;

void hf::Vk::Render::SharedBuffer::create(vk::DeviceSize size, vk::BufferUsageFlagBits usage, hf::Vk::MemoryManager* managerPtr) {
	hf::Vk::CreateInfo::Buffer bI = hf::Vk::CreateInfo::Buffer()
		.setAllocationSize(size)
		.setAssignMemory(true)
		.setManagerPtr(managerPtr)
		.setPropertyFlags(vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent)
		.setUsage(usage);
		
	// create buffer
	_data.create(bI);

	// map data to ptr
	_viewPtr = _data.map();
}

void hf::Vk::Render::SharedBuffer::flushData() {
	_data.unmap();
	_viewPtr = _data.map();
}
