#include <hfVkRender/Renderer.h>
#include <hfAx/AuxInlines.inl>
#include <glm/gtc/matrix_transform.hpp>

#define DEBUG

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

void hf::Vk::Render::Renderer::obtainRequiredInstanceExtensions(std::vector<std::string>& extensionNames) {
	extensionNames.push_back("VK_KHR_surface");

// switch surface extensions via used OS
#ifdef _WIN32
	extensionNames.push_back("VK_KHR_win32_surface");
#elif defined(__ANDROID__)
	extensionNames.push_back("VK_KHR_win32_surface");
#elif defined(VK_USE_PLATFORM_WAYLAND_KHR)
	extensionNames.push_back("VK_KHR_win32_surface");
#else // XCB
	extensionNames.push_back("VK_KHR_xcb_surface");
#endif  // _WIN32

}

void hf::Vk::Render::Renderer::initInstance() {
	hf::Vk::CreateInfo::Instance cI;
	cI.abundantInfo = false;
	cI.appInfo.apiVersion = VK_API_VERSION_1_1;

	std::vector<std::string> extensionNames;
	obtainRequiredInstanceExtensions(extensionNames);

	for (int i = 0; i < extensionNames.size(); i++)
		cI.enabledExtensions.push_back((char*)extensionNames[i].data());

#ifdef DEBUG
	cI.useValidationLayers = true;
	cI.abundantInfo = true;
#endif // DEBUG

	instance.create(cI);
}

void hf::Vk::Render::Renderer::initManagers() {
	// init memory manager
	hf::Vk::CreateInfo::MemoryManager memManagerCI;
	memManagerCI.pDevice = &device;

	memManager.setUp(memManagerCI);

	// init command processor
	hf::Vk::CreateInfo::CommandProcessor cpCI;
	cpCI.pDevice = &device;
	commandProcessor.startUp(cpCI);

	// init descriptor manager
	hf::Vk::CreateInfo::DescriptorManager mCI;
	mCI.pDevice = &device;
	descManager.setUp(mCI);
}

void hf::Vk::Render::Renderer::setUpRenderPasses() {
	hf::Vk::CreateInfo::RenderPass rpCI;
	rpCI.pDevice = &device;

	rpCI.attachments.resize(2);
	// swapchain img
	rpCI.attachments[0].format = presenter.getSwapchain().getImageFormat();
	rpCI.attachments[0].samples = vk::SampleCountFlagBits::e1;
	rpCI.attachments[0].loadOp = vk::AttachmentLoadOp::eLoad;
	rpCI.attachments[0].storeOp = vk::AttachmentStoreOp::eStore;
	rpCI.attachments[0].stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
	rpCI.attachments[0].stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	rpCI.attachments[0].initialLayout = vk::ImageLayout::ePresentSrcKHR;
	rpCI.attachments[0].finalLayout = vk::ImageLayout::ePresentSrcKHR;

	// depth image
	rpCI.attachments[1].format = vk::Format::eD32Sfloat;
	rpCI.attachments[1].samples = vk::SampleCountFlagBits::e1;
	rpCI.attachments[1].loadOp = vk::AttachmentLoadOp::eLoad;
	rpCI.attachments[1].storeOp = vk::AttachmentStoreOp::eStore;
	rpCI.attachments[1].stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
	rpCI.attachments[1].stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	rpCI.attachments[1].initialLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
	rpCI.attachments[1].finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	rpCI.attachmentReferences.resize(2);
	rpCI.attachmentReferences[0] = { 0, vk::ImageLayout::eColorAttachmentOptimal };
	rpCI.attachmentReferences[1] = { 1, vk::ImageLayout::eDepthStencilAttachmentOptimal };

	rpCI.subpasses.resize(1);
	rpCI.subpasses[0].pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
	rpCI.subpasses[0].colorAttachmentCount = 1;
	rpCI.subpasses[0].pColorAttachments = &rpCI.attachmentReferences[0];
	rpCI.subpasses[0].pDepthStencilAttachment = &rpCI.attachmentReferences[1];

	componentsDB.renderPasses.emplace("axis", hf::Vk::RenderPass(rpCI));
}

void hf::Vk::Render::Renderer::createSamplers() {
	CreateInfo::Sampler ci;
	ci.pDevice = &device;

	//for (int i = 0; i < scene.bakedTextures.size(); i++)
	componentsDB.samplers.push_back(UPTR(Sampler, ci));
}

void hf::Vk::Render::Renderer::createBuffers() {
	// allocated memory pools for uniform buffers
	hf::Vk::CreateInfo::Buffer vsUCI(hf::Vk::CreateInfo::Buffer::Usage::UNIFORMBUFFER);
	vsUCI.pManager = &memManager;

	// create Uniform buffers
	vsUbo.create(vsUCI);
	fsUbo.create(vsUCI);
	gsUbo.create(vsUCI);

	// create indirect draw buffer
	hf::Vk::CreateInfo::Buffer bI(hf::Vk::CreateInfo::Buffer::Usage::INDEXEDINDIRECTDRAW);
	bI.pManager = &memManager;
	indirectDrawParameters.create(bI);

	if (description.requieredFeatures.shadowRendering) {
		// create CubeMap
		hf::Vk::CreateInfo::Image cubeCI(hf::Vk::CreateInfo::Image::Usage::CUBE);
		cubeCI.format = vk::Format::eD32Sfloat;
		cubeCI.usage = vk::ImageUsageFlagBits::eDepthStencilAttachment | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eSampled;
		cubeCI.setPtrManager(&memManager);
		shadowCubeMap.create(cubeCI);

		// and create its view
		CreateInfo::ImageView viewCI;
		viewCI.setImage(&shadowCubeMap)
			.setViewType(vk::ImageViewType::eCube)
			.setFormat(shadowCubeMap.getDescription().format)
			.setComponentMaping({})
			.setSubresourceRange({ vk::ImageAspectFlagBits::eDepth, 0, 1, 0, 6 });

		shadowCubeMap.addView(viewCI);
	}
}

void hf::Vk::Render::Renderer::createCmdBuffers() {
	// with peace in mind I can without any trouble create cmb buffers here after all previous object connected to strategies are allocated...
	hf::Vk::CreateInfo::CommandBufferSet cbsI;
	cbsI.initialBufferCount = presenter.getImageCount();
	cbsI.pDevice = &device;
	cbsI.familyQueueIdx = componentsDB.queueFamilyTags[VkQueueFlagBits(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT)];
	// create cmd buffer set
	componentsDB.renderStrategies["bakedScene"].commandBuffers.create(cbsI);

	if (description.requieredFeatures.normalRendering)
		componentsDB.renderStrategies["normals"].commandBuffers.create(cbsI);
	
	if (description.requieredFeatures.wireframeRendering)
		componentsDB.renderStrategies["wireframe"].commandBuffers.create(cbsI);
	
	if (description.requieredFeatures.axisRendering)
		componentsDB.renderStrategies["axis"].commandBuffers.create(cbsI);
		
	if (description.requieredFeatures.shadowRendering) {
		cbsI.initialBufferCount = presenter.getImageCount() + 1;
		componentsDB.renderStrategies["shadowMapping"].commandBuffers.create(cbsI);
	}

}

void hf::Vk::Render::Renderer::recordCmdBuffers() {
	if (description.requieredFeatures.axisRendering)
		axis_RecordCommands();
}

void hf::Vk::Render::Renderer::recordSceneCmdBuffers() {
	bakedScene_RecordCommands();

	if (description.requieredFeatures.normalRendering)
		normals_RecordCommands();
	
	if (description.requieredFeatures.wireframeRendering)
		wireframe_RecordCommands();
	
	if (description.requieredFeatures.shadowRendering) {
		shadowMapping_ShadowMap_RecordCommands();
		shadowMapping_Render_RecordCommands();
	}
	
}

void hf::Vk::Render::Renderer::loadShaders() {
	bakedScene_LoadShaders();

	if (description.requieredFeatures.normalRendering)
		normals_LoadShaders();

	if (description.requieredFeatures.wireframeRendering)
		wireframe_LoadShaders();

	if (description.requieredFeatures.axisRendering)
		axis_LoadShaders();

	if (description.requieredFeatures.shadowRendering)
		shadowMapping_LoadShaders();

}

void hf::Vk::Render::Renderer::setUpDescriptorSets() {
	bakedScene_SetUpDS();
	
	if (description.requieredFeatures.normalRendering)
		normals_SetUpDS();
	
	if (description.requieredFeatures.wireframeRendering)
		wireframe_SetUpDS();
	
	if (description.requieredFeatures.axisRendering)
		axis_SetUpDS();
}

void hf::Vk::Render::Renderer::createPipelineLayouts() {
	if (description.requieredFeatures.normalRendering)
		normals_CreatePipelineLayout();
	
	if (description.requieredFeatures.wireframeRendering)
		wireframe_CreatePipelineLayout();
	
	if (description.requieredFeatures.axisRendering)
		axis_CreatePipelineLayout();
}

void hf::Vk::Render::Renderer::fillPipelineDBCreateInfo(hf::Vk::CreateInfo::PipelineDatabase& createInfo) {
	if (description.requieredFeatures.axisRendering) {
		createInfo.graphicPipesCreateInfos.push_back(hf::Vk::CreateInfo::GraphicPipeDBEntry());
		createInfo.graphicPipesCreateInfos.back().name = "axis";
		axis_FillPipelineInfo(createInfo.graphicPipesCreateInfos.back().graphicPipeCreateInfo);
	}

	if (description.requieredFeatures.normalRendering) {
		createInfo.graphicPipesCreateInfos.push_back(hf::Vk::CreateInfo::GraphicPipeDBEntry());
		createInfo.graphicPipesCreateInfos.back().name = "normals";
		normals_FillPipelineInfo(createInfo.graphicPipesCreateInfos.back().graphicPipeCreateInfo);
	}

	if (description.requieredFeatures.wireframeRendering) {
		createInfo.graphicPipesCreateInfos.push_back(hf::Vk::CreateInfo::GraphicPipeDBEntry());
		createInfo.graphicPipesCreateInfos.back().name = "wireframe";
		wireframe_FillPipelineInfo(createInfo.graphicPipesCreateInfos.back().graphicPipeCreateInfo);
	}

}

void hf::Vk::Render::Renderer::writeDescriptorSets() {	
	bakedScene_WriteDS();
	
	if (description.requieredFeatures.axisRendering)
		axis_WriteDS();

	if (description.requieredFeatures.normalRendering)
		normals_WriteDS();

	if (description.requieredFeatures.wireframeRendering)
		wireframe_WriteDS();

}

void hf::Vk::Render::Renderer::initPresenter(uint32_t width, uint32_t height, uint32_t swapchainImgCount) {
	hf::Vk::CreateInfo::Presenter cI;
	cI.renderPass_FramebuffersMapPtr = &componentsDB.renderPass_FramebuffersMap;
	cI.pMemManager = &memManager;
	cI.presentationQueue = device.getQueueHandles(componentsDB.queueFamilyTags[VkQueueFlagBits(VK_QUEUE_GRAPHICS_BIT | VK_QUEUE_COMPUTE_BIT | VK_QUEUE_TRANSFER_BIT)]).availableQueues.back();

	vk::SurfaceFormatKHR reqSurfaceFormat{ vk::Format::eB8G8R8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear };
	// getting surface capabilities
	vk::SurfaceCapabilitiesKHR surfCaps = Auxiliary::Swapchain::surfaceCapabilities(device.getPhysicalDevice(), presenter.getSurface());

	cI.swapchainCreateInfo.pDevice = &device;
	cI.swapchainCreateInfo.surfaceFormat = (Auxiliary::Swapchain::checkSurfaceFormatAvailability(device.getPhysicalDevice(), presenter.getSurface(), reqSurfaceFormat)) ? reqSurfaceFormat : Auxiliary::Swapchain::returnRandomAvailableSurfaceFormat(device.getPhysicalDevice(), presenter.getSurface());// check surface format capability ...  :(
	cI.swapchainCreateInfo.presentMode = (Auxiliary::Swapchain::checkPresentModeAvailability(device.getPhysicalDevice(), presenter.getSurface(), vk::PresentModeKHR::eMailbox)) ? vk::PresentModeKHR::eMailbox : Auxiliary::Swapchain::returnRandomAvailablePresentMode(device.getPhysicalDevice(), presenter.getSurface()); // check present mode availibility...  :(
	cI.swapchainCreateInfo.minImageCount = (uint32_t)swapchainImgCount; //!! opravit na widlach hf::Ax::min(surfCaps.maxImageCount, (uint32_t)swapchainImgCount);
	cI.swapchainCreateInfo.width = hf::Ax::min(width, surfCaps.maxImageExtent.width);
	cI.swapchainCreateInfo.height = hf::Ax::min(height, surfCaps.maxImageExtent.height);
	cI.swapchainCreateInfo.imageArrayLayers = 1;
	cI.swapchainCreateInfo.imageUsage = vk::ImageUsageFlagBits::eColorAttachment | vk::ImageUsageFlagBits::eTransferDst;
	cI.swapchainCreateInfo.preTransform = vk::SurfaceTransformFlagBitsKHR::eIdentity;
	cI.swapchainCreateInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
	cI.swapchainCreateInfo.clipped = VK_TRUE;

	presenter.init(cI);
}

void hf::Vk::Render::Renderer::imagesLayoutTransfer() {
	CommandBuffer& cmdbuff = componentsDB.commonCommandBufferSet.getNextBuffer();
	cmdbuff.beginRecording();
	// must convert swapchain img from undefined layout -> presentKHR
	presenter.transformImagesToProperLayout(cmdbuff);

	if (description.requieredFeatures.shadowRendering) {
		// convert cube mat to proper layout
		Info::Image::ChangeLayout changeI(Info::Image::ChangeLayout::TransferOp::DEPTH_TEST);
		changeI.image = shadowCubeMap.getVkHandle();
		changeI.subresourceRange = { vk::ImageAspectFlagBits::eDepth, 0, 1, 0, 6 };
		shadowCubeMap.changeLayout(cmdbuff, changeI);
	}

	cmdbuff.finishRecording();

	// post cmds
	Info::PostSingleBuffer postI;
	postI.pBuffer = &cmdbuff;
	commandProcessor.flushCmdBuffer(postI, &cmdbuff.getLock());
	componentsDB.commonCommandBufferSet.returnCurrentBuffer();
}

void hf::Vk::Render::Renderer::afterSceneLoadedActions() {
	bakedScene_SetUpTextureDS();
	bakedScene_WriteTextureDS();

	// create bakedScene pipeline 
	// Baked Scene Strategy
	bakedScene_CreatePipelineLayout(); // first create layout

	// build pipelines
	std::vector<CreateInfo::GraphicPipeDBEntry> pipeInfos;
	pipeInfos.push_back(CreateInfo::GraphicPipeDBEntry());
	pipeInfos.back().name = "bakedScene";
	bakedScene_FillPipelineInfo(pipeInfos.back().graphicPipeCreateInfo);

	if (description.requieredFeatures.shadowRendering) {
		shadowMapping_SetUpRenderPass();
		shadowMapping_CreateFramebuffers();
		shadowMapping_SetUpDS();
		shadowMapping_WriteDS();
		shadowMapping_CreatePipelineLayouts();

		// fill pipe info for shadow cast rp
		shadowMapping_GenerateMapFillPipelineInfo(pipeInfos);

		// swapchain image rendering rp
		pipeInfos.push_back(CreateInfo::GraphicPipeDBEntry());
		pipeInfos.back().name = "shadowMapping";
		shadowMapping_RenderSceneFillPipelineInfo(pipeInfos.back().graphicPipeCreateInfo);
	}

	// than create pipeline itself
	pipelineDB.buildGraphicPipelines(pipeInfos);

	// record cmd buffers
	recordSceneCmdBuffers();
}

/* 
	Need to rebuild this strategy !!!
	First at all I must create all resources for strategy pipelines, descriptors render passes etc...
	then i must create its cmd buffers 
	and then record them :) 

	init strategies will contain initNameStrategy function which handles all ptr assignment and other things... EZ!
*/