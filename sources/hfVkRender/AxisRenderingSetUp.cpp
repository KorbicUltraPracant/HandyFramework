#include <hfVkRender/Renderer.h>
#include <hfVkRender/CommandBufferRecording.h>
#include <hfAx/AuxInlines.inl>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* componentsDB.renderStrategies["name"] will be always created if not existed in first acess call... so no need to create it manualy */

void hf::Vk::Render::Renderer::axis_FillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo) {
	createInfo.pDevice = &device;
	createInfo.layout = &componentsDB.pipelineLayouts["axis"];
	createInfo.renderPass = &componentsDB.renderPasses["axis"];

	createInfo.primitivesAssembly.topology = vk::PrimitiveTopology::eLineList;

	// vertex input
	//createInfo.vertexInput.push_back(axisCoords.getInputDescription());

	createInfo.stagesInfo.resize(2);
	createInfo.stagesInfo[0].stage = vk::ShaderStageFlagBits::eVertex;
	createInfo.stagesInfo[0].module = &*componentsDB.renderStrategies["axis"].shaders[0];

	createInfo.stagesInfo[1].stage = vk::ShaderStageFlagBits::eFragment;
	createInfo.stagesInfo[1].module = &*componentsDB.renderStrategies["axis"].shaders[1];

	createInfo.viewport.scissors.resize(1);
	createInfo.viewport.scissors[0].extent = vk::Extent2D( presenter.getSwapchain().getDescription().width, presenter.getSwapchain().getDescription().height );
	createInfo.viewport.scissors[0].offset = vk::Offset2D( 0, 0 );

	createInfo.viewport.viewports.resize(1);
	createInfo.viewport.viewports[0].width = (float)presenter.getSwapchain().getDescription().width;
	createInfo.viewport.viewports[0].height = (float)presenter.getSwapchain().getDescription().height;
	createInfo.viewport.viewports[0].minDepth = createInfo.viewport.viewports[0].x = createInfo.viewport.viewports[0].y = 0.0f;
	createInfo.viewport.viewports[0].maxDepth = 1.0f;

	// dynamic states
	createInfo.dynamicStates.insert(createInfo.dynamicStates.begin(), { vk::DynamicState::eViewport, vk::DynamicState::eScissor });

	// blend attachment states
	createInfo.colorBlend.attachments.push_back(Info::PipelineSetUp::ColorBlendAttachmentState());

	// depth test
	createInfo.depthStencil.depthTestEnable = VK_TRUE;
	createInfo.depthStencil.depthWriteEnable = VK_TRUE;
	createInfo.depthStencil.depthCompareOp = vk::CompareOp::eLessOrEqual;

	// used in first subpass
	createInfo.usedForSubpass = 0;
}

void hf::Vk::Render::Renderer::axis_SetUpDS() {
	hf::Vk::CreateInfo::DescriptorSet info;
	info.pManager = &descManager;
	info.mainLayoutInfo.pDevice = &device;
	info.mainLayoutInfo.layoutBindings.resize(1);

	// vertex shader uniformbuffer
	info.mainLayoutInfo.layoutBindings[0].binding = 0;
	info.mainLayoutInfo.layoutBindings[0].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	info.mainLayoutInfo.layoutBindings[0].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[0].stageFlags = vk::ShaderStageFlagBits::eVertex;

	// create descriptor set
	componentsDB.renderStrategies["axis"].descriptorSets.push_back(UPTR(hf::Vk::DescriptorSet, info));
}

void hf::Vk::Render::Renderer::axis_WriteDS() {
	Info::UpdateDescriptorSets updateInfo;
	updateInfo.buffersInfo.resize(1);

	// vsUniforms
	updateInfo.buffersInfo[0].buffer = vsUbo.getBuffer();
	updateInfo.buffersInfo[0].offset = 0;
	updateInfo.buffersInfo[0].range = sizeof(hf::Vk::ShaderResources::UniformBuffer::VertexShader);

	/* write set infos */
	updateInfo.writeInfo.resize(1);

	// vsUniforms
	updateInfo.writeInfo[0].dstSet = componentsDB.renderStrategies["axis"].descriptorSets[0]->getVkHandle();
	updateInfo.writeInfo[0].dstBinding = 0;
	updateInfo.writeInfo[0].dstArrayElement = 0;
	updateInfo.writeInfo[0].descriptorCount = 1;
	updateInfo.writeInfo[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	updateInfo.writeInfo[0].pBufferInfo = &updateInfo.buffersInfo[0];

	componentsDB.renderStrategies["axis"].descriptorSets[0]->update(updateInfo);
}

void hf::Vk::Render::Renderer::axis_CreatePipelineLayout() {
	hf::Vk::CreateInfo::PipelineLayout layoutCI;
	layoutCI.pDevice = &device;

	layoutCI.pDescriptorSetLayouts.push_back(&componentsDB.renderStrategies["axis"].descriptorSets[0]->getLayout());
	componentsDB.pipelineLayouts.emplace("axis", layoutCI);
}

void hf::Vk::Render::Renderer::axis_LoadShaders() {
	// shaders
	hf::Vk::CreateInfo::ShaderModule basicCI;
	basicCI.pDevice = &device;

	string dirName(RENDER_RESOURCES);

	basicCI.path = dirName + string("/shaders/axisRender_vert.spv");
	componentsDB.renderStrategies["axis"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedV 0

	basicCI.path = dirName + string("/shaders/axisRender_frag.spv");
	componentsDB.renderStrategies["axis"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedF 1
}

void hf::Vk::Render::Renderer::axis_RecordCommands() {
	hf::Vk::CommandBufferRecording::Batch<hf::Vk::CommandBufferRecording::Draw::Draw> allCmds;
	allCmds.renderPasses.push_back(UPTR(hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::Draw>, ));
	hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::Draw>& rp = *allCmds.renderPasses.front();
	hf::Vk::CommandBufferRecording::Draw::Draw dI;

	// pipeline for textured models
	dI.renderWidth = presenter.getSwapchain().getDescription().width;
	dI.renderHeight = presenter.getSwapchain().getDescription().height;
	dI.pGraphicPipeline = &pipelineDB.obtainGraphicPipeline("axis");
	// render pass info
	rp.renderPassInfo.pRenderPass = &componentsDB.renderPasses["axis"];
	rp.renderPassInfo.renderArea.extent = vk::Extent2D( dI.renderWidth, dI.renderHeight );
	rp.renderPassInfo.clearColors.emplace_back(vk::ClearColorValue(std::array<float, 4>({ { 0.2f, 0.2f, 0.2f, 0.2f } })));
	rp.renderPassInfo.clearColors.emplace_back(vk::ClearDepthStencilValue(1.0f, 0u));
	// index buffer
	// vertex buffers
	/*rI.vertexBuffersInfo.firstBinding = 0;
	rI.vertexBuffersInfo.offsets.push_back(0);
	rI.vertexBuffersInfo.pBuffers.push_back(axisCoords.getBuffer().getVkHandle());*/

	// clear image
	allCmds.clearImage = true;

	// descriptors
	dI.descriptorSetsInfo.bindPoint = vk::PipelineBindPoint::eGraphics;
	dI.descriptorSetsInfo.firstSet = 0;
	dI.descriptorSetsInfo.pPipelineLayout = &componentsDB.pipelineLayouts["axis"];
	dI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.renderStrategies["axis"].descriptorSets[0]);

	// draw info
	dI.drawInfo.firstInstance = 0;
	dI.drawInfo.firstVertex = 0;
	dI.drawInfo.instanceCount = 1;
	dI.drawInfo.vertexCount = 6;

	rp.drawInfos.resize(1);
	for (int i = 0; i < presenter.getImageCount(); i++) {
		// assign framebuffer for swapchain image i image
		rp.renderPassInfo.pFramebuffer = &presenter.getFramebuffer(componentsDB.renderPasses["axis"], i);

		// n record commands for swapchain image i
		rp.drawInfos[0] = dI;

		allCmds.record(componentsDB.renderStrategies["axis"].commandBuffers.getBuffer(i), this, i);
	}
}