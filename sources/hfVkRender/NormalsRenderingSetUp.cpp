#include <hfVkRender/Renderer.h>
#include <hfVkRender/CommandBufferRecording.h>
#include <hfAx/AuxInlines.inl>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* componentsDB.renderStrategies["name"] will be always created if not existed in first acess call... so no need to create it manualy */

void hf::Vk::Render::Renderer::normals_FillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo) {
	createInfo.pDevice = &device;
	createInfo.layout = &componentsDB.pipelineLayouts["normals"];
	createInfo.renderPass = &componentsDB.renderPasses["axis"];

	createInfo.vertexInput.resize(2);
	// position
	createInfo.vertexInput[0].location = 0;
	createInfo.vertexInput[0].stride = 3 * sizeof(float);

	// normal
	createInfo.vertexInput[1].location = 1;
	createInfo.vertexInput[1].stride = 3 * sizeof(float);

	createInfo.stagesInfo.resize(3);
	createInfo.stagesInfo[0].stage = vk::ShaderStageFlagBits::eVertex;
	createInfo.stagesInfo[0].module = &*componentsDB.renderStrategies["normals"].shaders[0];

	createInfo.stagesInfo[1].stage = vk::ShaderStageFlagBits::eGeometry;
	createInfo.stagesInfo[1].module = &*componentsDB.renderStrategies["normals"].shaders[1];

	createInfo.stagesInfo[2].stage = vk::ShaderStageFlagBits::eFragment;
	createInfo.stagesInfo[2].module = &*componentsDB.renderStrategies["normals"].shaders[2];

	createInfo.viewport.scissors.resize(1);
	createInfo.viewport.scissors[0].extent = vk::Extent2D( 0, 0 );
	createInfo.viewport.scissors[0].offset = vk::Offset2D( (int32_t)presenter.getSwapchain().getDescription().width, (int32_t)presenter.getSwapchain().getDescription().height );

	createInfo.viewport.viewports.resize(1);
	createInfo.viewport.viewports[0].width = (float)presenter.getSwapchain().getDescription().width;
	createInfo.viewport.viewports[0].height = (float)presenter.getSwapchain().getDescription().height;
	createInfo.viewport.viewports[0].minDepth = createInfo.viewport.viewports[0].x = createInfo.viewport.viewports[0].y = 0.0f;
	createInfo.viewport.viewports[0].maxDepth = 1.0f;

	// dynamic states
	createInfo.dynamicStates.insert(createInfo.dynamicStates.begin(), { vk::DynamicState::eViewport, vk::DynamicState::eScissor });

	// blend attachment states
	createInfo.colorBlend.attachments.push_back(Info::PipelineSetUp::ColorBlendAttachmentState());

	// depth test
	createInfo.depthStencil.depthTestEnable = VK_TRUE;
	createInfo.depthStencil.depthWriteEnable = VK_TRUE;
	createInfo.depthStencil.depthCompareOp = vk::CompareOp::eLessOrEqual;

	// used in first subpass
	createInfo.usedForSubpass = 0;
}

void hf::Vk::Render::Renderer::normals_SetUpDS() {
	hf::Vk::CreateInfo::DescriptorSet info;
	info.pManager = &descManager;
	info.mainLayoutInfo.pDevice = &device;
	info.mainLayoutInfo.layoutBindings.resize(1);

	// vertex shader uniformbuffer
	info.mainLayoutInfo.layoutBindings[0].binding = 0;
	info.mainLayoutInfo.layoutBindings[0].descriptorCount = 1;
	info.mainLayoutInfo.layoutBindings[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	info.mainLayoutInfo.layoutBindings[0].pImmutableSamplers = VK_NULL_HANDLE;
	info.mainLayoutInfo.layoutBindings[0].stageFlags = vk::ShaderStageFlagBits::eVertex;

	// create descriptor set
	componentsDB.renderStrategies["normals"].descriptorSets.push_back(UPTR(hf::Vk::DescriptorSet, info)); // 0
}

void hf::Vk::Render::Renderer::normals_WriteDS() {
	Info::UpdateDescriptorSets updateInfo;
	updateInfo.buffersInfo.resize(1);

	// vsUniforms
	updateInfo.buffersInfo[0].buffer = vsUbo.getBuffer();
	updateInfo.buffersInfo[0].offset = 0;
	updateInfo.buffersInfo[0].range = sizeof(hf::Vk::ShaderResources::UniformBuffer::VertexShader);

	/* write set infos */
	updateInfo.writeInfo.resize(1);

	// vsUniforms
	updateInfo.writeInfo[0].dstSet = componentsDB.renderStrategies["normals"].descriptorSets[0]->getVkHandle();
	updateInfo.writeInfo[0].dstBinding = 0;
	updateInfo.writeInfo[0].dstArrayElement = 0;
	updateInfo.writeInfo[0].descriptorCount = 1;
	updateInfo.writeInfo[0].descriptorType = vk::DescriptorType::eUniformBuffer;
	updateInfo.writeInfo[0].pBufferInfo = &updateInfo.buffersInfo[0];

	componentsDB.renderStrategies["normals"].descriptorSets[0]->update(updateInfo);
}

void hf::Vk::Render::Renderer::normals_CreatePipelineLayout() {
	hf::Vk::CreateInfo::PipelineLayout layoutCI;
	layoutCI.pDevice = &device;

	layoutCI.pDescriptorSetLayouts.push_back(&componentsDB.renderStrategies["normals"].descriptorSets[0]->getLayout());
	componentsDB.pipelineLayouts.emplace("normals", layoutCI); // textured 0
}

void hf::Vk::Render::Renderer::normals_LoadShaders() {
	// shaders
	hf::Vk::CreateInfo::ShaderModule basicCI;
	basicCI.pDevice = &device;

	string dirName(RENDER_RESOURCES);

	basicCI.path = dirName + string("/shaders/normals_vert.spv");
	componentsDB.renderStrategies["normals"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedV 0

	basicCI.path = dirName + string("/shaders/normals_geom.spv");
	componentsDB.renderStrategies["normals"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); 

	basicCI.path = dirName + string("/shaders/normals_frag.spv");
	componentsDB.renderStrategies["normals"].shaders.push_back(UPTR(hf::Vk::ShaderModule, basicCI)); // texturedF 1
}

void hf::Vk::Render::Renderer::normals_RecordCommands() {
	hf::Vk::CommandBufferRecording::Batch<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect> allCmds;
	allCmds.renderPasses.push_back(UPTR(hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect>, ));
	hf::Vk::CommandBufferRecording::RenderPass<hf::Vk::CommandBufferRecording::Draw::IndexedIndirect>& rp = *allCmds.renderPasses.front();
	hf::Vk::CommandBufferRecording::Draw::IndexedIndirect dI;

	// pipeline for textured models
	// pipeline for textured models
	dI.renderWidth = presenter.getSwapchain().getDescription().width;
	dI.renderHeight = presenter.getSwapchain().getDescription().height;
	dI.pGraphicPipeline = &pipelineDB.obtainGraphicPipeline("normals");
	// render pass info
	rp.renderPassInfo.pRenderPass = &componentsDB.renderPasses["axis"];
	rp.renderPassInfo.renderArea.extent = vk::Extent2D( dI.renderWidth, dI.renderHeight );
	rp.renderPassInfo.clearColors.emplace_back(vk::ClearColorValue(std::array<float, 4>({ { 0.2f, 0.2f, 0.2f, 0.2f } })));
	rp.renderPassInfo.clearColors.emplace_back(vk::ClearDepthStencilValue(1.0f, 0u));
	// index buffer
	dI.indexBufferInfo.pBuffer = &*scene.buffers[3]; // nezapomenut prepnut na 3

	// vertex buffers
	dI.vertexBuffersInfo.firstBinding = 0;
	dI.vertexBuffersInfo.offsets.push_back(0);
	dI.vertexBuffersInfo.pBuffers.push_back(scene.buffers[0]->getVkHandle());
	dI.vertexBuffersInfo.offsets.push_back(0);
	dI.vertexBuffersInfo.pBuffers.push_back(scene.buffers[1]->getVkHandle());

	// clear image
	allCmds.clearImage = false;

	// descriptors
	dI.descriptorSetsInfo.bindPoint = vk::PipelineBindPoint::eGraphics;
	dI.descriptorSetsInfo.firstSet = 0;
	dI.descriptorSetsInfo.pPipelineLayout = &componentsDB.pipelineLayouts["normals"];
	dI.descriptorSetsInfo.pDescriptorSets.push_back(&*componentsDB.renderStrategies["normals"].descriptorSets[0]);

	// draw info
	dI.drawInfo = hf::Vk::Info::DrawIndexedIndirect()
		.setBuffer(indirectDrawParameters.getBuffer().getVkHandle())
		.setDrawCount(1)
		.setOffset(0)
		.setStride(sizeof(vk::DrawIndexedIndirectCommand));

	rp.drawInfos.resize(1);
	for (int i = 0; i < presenter.getImageCount(); i++) {
		// assign framebuffer for swapchain image i image
		rp.renderPassInfo.pFramebuffer = &presenter.getFramebuffer(componentsDB.renderPasses["axis"], i);

		// n record commands for swapchain image i
		rp.drawInfos[0] = dI;

		allCmds.record(componentsDB.renderStrategies["normals"].commandBuffers.getBuffer(i));
	}
}