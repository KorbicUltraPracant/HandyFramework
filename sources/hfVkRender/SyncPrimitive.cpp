/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVkRender/SyncPrimitive.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

/* ------ SyncPrimitive Util Class ------ */
void FencePrimitive::create(hf::Vk::Device& device) {
	CreateInfo::Fence info;
	info.pDevice = &device;
	_lockPtr = (void*) new Fence(info);
}

void FencePrimitive::destroy() {
	Fence* ptr = (Fence*)_lockPtr;
	delete ptr;
}

void EventPrimitive::create(hf::Vk::Device& device) {
	CreateInfo::Event info;
	info.pDevice = &device;
	_lockPtr = (void*) new Event(info);
}

void EventPrimitive::destroy() {
	Event* ptr = (Event*)_lockPtr;
	delete ptr;
}
