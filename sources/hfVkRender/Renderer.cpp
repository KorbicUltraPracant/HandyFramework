#include <hfVkRender/Renderer.h>
#include <hfAx/AuxInlines.inl>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

/* Zde musim mit nadefinovanou hfVkScene... ta bude obecna pro ruzne reprezentace a bude soucasti hfVk */
void hf::Vk::Render::Renderer::checkGPUFeatures(std::vector<vk::PhysicalDevice>& hostGPUs) {
	vector<vk::PhysicalDevice> start_vec(hostGPUs); hostGPUs.clear();
	for (auto& gpu : start_vec) {
		// properties
		/*
		Tady uzivatel kontroluje dostupne pozadavky:
		-> Pokud zarizeni vyhovuje serii pozadavkum (if statements) ulozi se ptr na gpu do res vectoru HostGPUs
		se kterymi se dale operuje po skonceni teto funkce
		*/
		vk::PhysicalDeviceProperties props;
		gpu.getProperties(&props);

		if (props.deviceType != vk::PhysicalDeviceType::eDiscreteGpu)
			continue;

		hostGPUs.push_back(gpu);
	}
}

void hf::Vk::Render::Renderer::resizePresenter(uint32_t width, uint32_t height) {
	/*
		must wait to all ops upon device(all its queues) are completed, because honestly i had no time to implement
		watchdog which tracks which command buffer operates with swapchains framebuffers... :(
	*/
	device.getVkHandle().waitIdle();
	
	// recreate Swapchain
	vk::SurfaceCapabilitiesKHR surfCaps = Auxiliary::Swapchain::surfaceCapabilities(device.getPhysicalDevice(), presenter.getSurface());
	width = hf::Ax::min(width, surfCaps.maxImageExtent.width);
	height = hf::Ax::min(height, surfCaps.maxImageExtent.height);
	presenter.getSwapchain().changeResolution(width, height);

	// recreate depth buffer
	presenter.createDepthBuffer();

	// recreates swapchain's framebuffers
	presenter.addFramebuffers(componentsDB.renderPasses["axis"]);

	// transfer images to proper format
	imagesLayoutTransfer();

	// need to recreate all cmd buffers
	recordCmdBuffers();
	
	if (scene.buffers.size())
		recordSceneCmdBuffers();

}

void hf::Vk::Render::Renderer::clearRPAttachments(uint32_t swapchainImageIdx, hf::Vk::CommandBuffer& buffer) {
	presenter.clearRenderResources(swapchainImageIdx, buffer);

	if (description.requieredFeatures.shadowRendering)
		shadowCubeMap.clearDepthStencil({ 1.0, 0 }, buffer, 6);
}

void Render::Renderer::operator()(hf::Vk::Render::Scene* scene) {
	
}
