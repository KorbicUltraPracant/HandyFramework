include(${HandyFramework_SOURCE_DIR}/CMakeModules/ucm.cmake)
# use macro to set library runtime as static
ucm_set_runtime(STATIC)

cmake_minimum_required(VERSION 3.5)

if (TARGET_${LIB_NAME})
	return()
endif()

find_package(glm REQUIRED)

project(hfVkRender)

SET(CMAKE_CXX_STANDARD 14)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_INCLUDE_CURRENT_DIR_IN_INTERFACE ON)

set(MAJOR_VERSION 1)
set(MINOR_VERSION 0)
set(REVISION_VERSION 0)

include(GenerateExportHeader)

set(LIB_NAME hfVkRender)

set(HEADER_PATH ${HandyFramework_SOURCE_DIR}/include/${LIB_NAME})

file(GLOB INCLUDES
	"${HEADER_PATH}/*.h"
	"${HEADER_PATH}/*.inl"
)

file(GLOB_RECURSE SOURCES
	"*.cpp"
)

# set and export glsl code dir
file(GLOB SHADER_SOURCE_FILES
  "${CMAKE_CURRENT_SOURCE_DIR}/GLSL/*.frag"
  "${CMAKE_CURRENT_SOURCE_DIR}/GLSL/*.vert"
  "${CMAKE_CURRENT_SOURCE_DIR}/GLSL/*.geom" # geometry shader
  "${CMAKE_CURRENT_SOURCE_DIR}/GLSL/*.tess" # tesselation shader
  "${CMAKE_CURRENT_SOURCE_DIR}/GLSL/*.comp" # compute shader
)

add_library(${LIB_NAME} STATIC ${SOURCES} ${INCLUDES} ${ADDITIONAL_HEADERS_PATH} ${GLM_INCLUDE_DIRS})
add_library(hf::VkRender ALIAS ${LIB_NAME})

target_link_libraries(${LIB_NAME} PUBLIC hf::Vk hf::Scene)

target_include_directories(${LIB_NAME} PUBLIC $<INSTALL_INTERFACE:include>)
target_include_directories(${LIB_NAME} PUBLIC $<BUILD_INTERFACE:${HandyFramework_SOURCE_DIR}/include/>)
target_include_directories(${LIB_NAME} PUBLIC $<BUILD_INTERFACE:${Vulkan_INCLUDE_DIR}>)
target_include_directories(${LIB_NAME} PUBLIC $<BUILD_INTERFACE:${GLM_INCLUDE_DIRS}>)

set(LIB_NAME_LOWER)
string(TOLOWER ${LIB_NAME} LIB_NAME_LOWER)

generate_export_header(${LIB_NAME} EXPORT_FILE_NAME ${LIB_NAME}/${LIB_NAME_LOWER}_export.h)

set(${LIB_NAME}_VERSION ${MAJOR_VERSION}.${MINOR_VERSION}.${REVISION_VERSION})

set_property(TARGET ${LIB_NAME} PROPERTY VERSION ${${LIB_NAME}_VERSION})
set_property(TARGET ${LIB_NAME} PROPERTY SOVERSION 1)
set_property(TARGET ${LIB_NAME} PROPERTY INTERFACE_${LIB_NAME}_MAJOR_VERSION 1)
set_property(TARGET ${LIB_NAME} APPEND PROPERTY COMPATIBLE_INTERFACE_STRING ${LIB_NAME}_MAJOR_VERSION)

# shaders handling
# Add and Compile GLSL Shaders
if (WIN32)
	if (${CMAKE_HOST_SYSTEM_PROCESSOR} STREQUAL "AMD64")
	  set(GLSL_VALIDATOR "$ENV{VULKAN_SDK}/Bin/glslangValidator.exe")
	else()
  		set(GLSL_VALIDATOR "$ENV{VULKAN_SDK}/Bin32/glslangValidator.exe")
	endif()
else()
	find_path(GLSL_VALIDATOR "*")
endif()

set(RENDER_PREQ_DIR "${CMAKE_INSTALL_PREFIX}/lib/${LIB_NAME}")
#set(RENDER_RESOURCES "${RENDER_PREQ_DIR}" CACHE PATH "Relative or absolute path to Application resources.")
set(RENDER_RESOURCES ${RENDER_PREQ_DIR})
set_target_properties(${LIB_NAME} PROPERTIES COMPILE_DEFINITIONS "RENDER_RESOURCES=\"${RENDER_RESOURCES}\"")

# create shaders dir inside output dir tree
# GLSL_SOURCE_FILES defined in QtgevkWidget's CMakeList.txt
foreach(GLSL ${SHADER_SOURCE_FILES})
	get_filename_component(FILE_NAME ${GLSL} NAME)

	# get rid of dot in shader name
	string(REPLACE "." "_" SNAME ${FILE_NAME})

	set(SPIRV "${RENDER_PREQ_DIR}/shaders/${SNAME}.spv")

	add_custom_command(
	OUTPUT ${SPIRV}
	COMMAND ${CMAKE_COMMAND} -E make_directory "${RENDER_PREQ_DIR}/shaders/"
	COMMAND ${GLSL_VALIDATOR} -V ${GLSL} -o ${SPIRV}
	DEPENDS ${GLSL})
	list(APPEND SPIRV_BINARY_FILES ${SPIRV})
endforeach(GLSL)

add_custom_target(
    Shaders
    DEPENDS ${SPIRV_BINARY_FILES}
)

add_dependencies(${LIB_NAME} Shaders)

# create others folder
if(NOT EXISTS ${RENDER_PREQ_DIR})
	file(MAKE_DIRECTORY ${RENDER_PREQ_DIR})
endif(NOT EXISTS ${RENDER_PREQ_DIR})

# let it execute when generating sln not after every compilation...
if(NOT EXISTS "${RENDER_PREQ_DIR}/others/pipeline.cache")
	file(WRITE "${RENDER_PREQ_DIR}/others/pipeline.cache" "")
endif(NOT EXISTS "${RENDER_PREQ_DIR}/others/pipeline.cache")

# ---- Install ----
#install shaderu !!!
#install(FILES ${SPIRV_BINARY_FILES} DESTINATION shaders # install spir-v inside shader folder COMPONENT Devel)

install(TARGETS ${LIB_NAME} EXPORT ${LIB_NAME}Targets
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  RUNTIME DESTINATION bin
  INCLUDES DESTINATION include
  )

install(
  FILES
  ${INCLUDES}
  ${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME_LOWER}_export.h
  DESTINATION
  include/${LIB_NAME}
  COMPONENT
  Devel
  )

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}ConfigVersion.cmake
  VERSION ${${LIB_NAME}_VERSION}
  COMPATIBILITY SameMajorVersion
  )

export(EXPORT ${LIB_NAME}Targets
  FILE ${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}Targets.cmake
  NAMESPACE ${LIB_NAME}::
  )

set(ConfigPackageLocation lib/cmake/${LIB_NAME})
install(EXPORT ${LIB_NAME}Targets
  FILE
  ${LIB_NAME}Targets.cmake
  NAMESPACE
  ${LIB_NAME}::
  DESTINATION
  ${ConfigPackageLocation}
  )

file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}Config.cmake
  "include($" "{CMAKE_CURRENT_LIST_DIR}/${LIB_NAME}Targets.cmake)"
  )

install(
  FILES
  ${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}Config.cmake
  ${CMAKE_CURRENT_BINARY_DIR}/${LIB_NAME}/${LIB_NAME}ConfigVersion.cmake
  DESTINATION
  ${ConfigPackageLocation}
  COMPONENT
  Devel
  )
