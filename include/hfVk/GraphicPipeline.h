/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Graphic pipeline class

Chovani objektu odpovida chovani vk::Pipeline objektu jak je ve specifikaci.
Objektu se v konstruktoru preda, povinne, struktura popisujici pipeline a v ramci
behu ctoru se vytvori finalni VkPipe. Pote uz objekt slouzi akorat ke cteni parametru pipeline,
ziskani vk::Pipeline ptr. Je zde pridana moznost, zmenit kompletne pipeline na zaklade rebuild metody.
Ta ma ale velkou rezii, jelikoz se musi z GPU odstranit cela puvodni pipeline a nahrat se tam nova

*/

#ifndef HFVKGRAPHICPIPE_H
#define HFVKGRAPHICPIPE_H

#include <hfVk/PipelineComponents.inl>
#include <hfVk/PipelineLayout.h>
#include <hfVk/Pipeline.h>
#include <vector>

/* ------- Info Class ------- */
class hf::Vk::Info::GraphicPipeline : public hf::Vk::Info::Pipeline {
// attributes
public:
	// renderpass
	hf::Vk::RenderPass* renderPass;

	// used in subpass no.
	uint32_t usedForSubpass = 0;

// virtual dtor
public:
	virtual ~GraphicPipeline() = default;

	GraphicPipeline& setRenderPass(hf::Vk::RenderPass * renderPass_) {
		renderPass = renderPass_;
		return *this;
	}

	GraphicPipeline& setUsedForSubpass(uint32_t usedForSubpass_) {
		usedForSubpass = usedForSubpass_;
		return *this;
	}
};

class hf::Vk::FurtherInfo::GraphicPipeline : public hf::Vk::Info::GraphicPipeline {
// attributes
public:
	std::vector<Info::PipelineSetUp::ShaderStage> stagesInfo;
	std::vector<Info::PipelineSetUp::VertexInputDescription> vertexInput; // must be congruent with shaders, so in current state cant use HFVKMesh to fill it, must be hardcored. I going to solve this somehow in my future work.
	hf::Vk::Info::PipelineSetUp::InputAssemblyState primitivesAssembly;
	hf::Vk::Info::PipelineSetUp::TessellationState tessellation;
	hf::Vk::Info::PipelineSetUp::ViewportState viewport;
	hf::Vk::Info::PipelineSetUp::RasterizationState rasterization;
	hf::Vk::Info::PipelineSetUp::MultisampleState multisample;
	hf::Vk::Info::PipelineSetUp::DepthStencilState depthStencil;
	hf::Vk::Info::PipelineSetUp::StencilOpState stencilFrontOp;
	hf::Vk::Info::PipelineSetUp::StencilOpState stencilBackOp;
	hf::Vk::Info::PipelineSetUp::ColorBlendState colorBlend;
	std::vector<hf::Vk::Info::PipelineSetUp::DynamicState> dynamicStates;

	GraphicPipeline& setPrimitivesAssembly(hf::Vk::Info::PipelineSetUp::InputAssemblyState primitivesAssembly_) {
		primitivesAssembly = primitivesAssembly_;
		return *this;
	}

	GraphicPipeline& setTessellation(hf::Vk::Info::PipelineSetUp::TessellationState tessellation_) {
		tessellation = tessellation_;
		return *this;
	}

	GraphicPipeline& setViewport(hf::Vk::Info::PipelineSetUp::ViewportState viewport_) {
		viewport = viewport_;
		return *this;
	}

	GraphicPipeline& setRasterization(hf::Vk::Info::PipelineSetUp::RasterizationState rasterization_) {
		rasterization = rasterization_;
		return *this;
	}

	GraphicPipeline& setMultisample(hf::Vk::Info::PipelineSetUp::MultisampleState multisample_) {
		multisample = multisample_;
		return *this;
	}

	GraphicPipeline& setDepthStencil(hf::Vk::Info::PipelineSetUp::DepthStencilState depthStencil_) {
		depthStencil = depthStencil_;
		return *this;
	}

	GraphicPipeline& setStencilFrontOp(hf::Vk::Info::PipelineSetUp::StencilOpState stencilFrontOp_) {
		stencilFrontOp = stencilFrontOp_;
		return *this;
	}

	GraphicPipeline& setStencilBackOp(hf::Vk::Info::PipelineSetUp::StencilOpState stencilBackOp_) {
		stencilBackOp = stencilBackOp_;
		return *this;
	}

	GraphicPipeline& setColorBlend(hf::Vk::Info::PipelineSetUp::ColorBlendState colorBlend_) {
		colorBlend = colorBlend_;
		return *this;
	}

};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::GraphicPipeline : public hf::Vk::FurtherInfo::GraphicPipeline, public hf::Vk::CreateInfo::Pipeline {
// attributes
private:
	std::vector<vk::PipelineShaderStageCreateInfo> shaderStages;
	std::vector<vk::VertexInputBindingDescription> inputBinding;
	std::vector<vk::VertexInputAttributeDescription> attriburesDescription;
	vk::PipelineVertexInputStateCreateInfo inputState;
	vk::PipelineInputAssemblyStateCreateInfo assemblyState;
	vk::PipelineTessellationStateCreateInfo tessellationState;
	vk::PipelineViewportStateCreateInfo viewportState;
	vk::PipelineRasterizationStateCreateInfo rasterizationState;
	vk::PipelineMultisampleStateCreateInfo multisampleState;
	vk::PipelineDepthStencilStateCreateInfo depthStencilState;
	vk::PipelineColorBlendStateCreateInfo colorBlendState;
	vk::PipelineDynamicStateCreateInfo dynamicState;

// methods
public:
	operator vk::GraphicsPipelineCreateInfo();
	operator vk::PipelineVertexInputStateCreateInfo();
	operator vk::PipelineDynamicStateCreateInfo();
};

/* ------- Core Class ------- */
class hf::Vk::GraphicPipeline : public hf::Vk::Pipeline {
// attributes
private:
	// pipeline's info
	hf::Vk::Info::GraphicPipeline* _description = nullptr;

// methods
private:
	void init(CreateInfo::GraphicPipeline& createInfo);
	void deinit();
	void move(GraphicPipeline& withdraw);

public:
	// Ctor 'n Dtor
	GraphicPipeline() {}
	GraphicPipeline(CreateInfo::GraphicPipeline& createInfo) { init(createInfo); } // testnout jak se bude chovat vector na move-assignment(p
	GraphicPipeline(GraphicPipeline&& init) { move(init); }
	~GraphicPipeline() { deinit(); }
	
	/* getters */
	Info::GraphicPipeline& getDescription() { return *_description; }

	/* operators */
	GraphicPipeline& operator=(GraphicPipeline& copy) { move(copy); return *this; }

	/* Core Methods */
	// object's lifetime
	void assemble(CreateInfo::GraphicPipeline& createInfo) { init(createInfo); }
	bool assemble(vk::Pipeline& vkpipe, CreateInfo::GraphicPipeline& info); // used in pipelineDB
	void dismantle() { deinit(); }
	
	// Explicit Object's Reset, object is keept in same memory, but attributes are restored in state as in the Start of Ctor. 
	bool rebuild(CreateInfo::GraphicPipeline*& createInfo); // if& createInfo == nullptr -> reset else rebuild via& createInfo

	// renderpass compatibility check
	bool renderPassCompatible(hf::Vk::RenderPass& renderPassToCheck);

};

#endif