/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Image wrapper
*/

#ifndef HFVKIMAGE_H
#define HFVKIMAGE_H

#include <hfVk/MemoryManager.h>
#include <hfVk/MemoryBlock.h>
#include <hfVk/ImageView.h>
#include <vector>

/* ------- Info Class ------- */
class hf::Vk::Info::ImageBase {
public:
	hf::Vk::Device* pDevice = nullptr;

	vk::ImageType type = vk::ImageType::e2D;
	vk::Format format;
	size_t formatSize; // user must define format size
	uint32_t width;
	uint32_t height;
	vk::ImageLayout currentLayout = vk::ImageLayout::eUndefined; // when creating serves as definition for initial layout but after that keeps track of actual image layout runtime

	ImageBase& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	ImageBase& setType(vk::ImageType type_) {
		type = type_;
		return *this;
	}

	ImageBase& setFormat(vk::Format format_) {
		format = format_;
		return *this;
	}

	ImageBase& setFormatSize(size_t formatSize_) {
		formatSize = formatSize_;
		return *this;
	}

	ImageBase& setWidth(uint32_t width_) {
		width = width_;
		return *this;
	}

	ImageBase& setHeight(uint32_t height_) {
		height = height_;
		return *this;
	}

	ImageBase& setCurrentLayout(vk::ImageLayout currentLayout_) {
		currentLayout = currentLayout_;
		return *this;
	}
};

#undef MemoryBarrier // bacause windows-vs bshit
class hf::Vk::Info::Image : public Info::ImageBase {
public:
// same namespace classes
	class MemoryBarrier;
	class ChangeLayout;

// attributes
public:	
	hf::Vk::MemoryManager* pManager = nullptr;

	vk::ImageTiling tiling = vk::ImageTiling::eOptimal;
	vk::ImageUsageFlags usage;
	vk::MemoryPropertyFlags memFlags = vk::MemoryPropertyFlags(0);
	vk::SampleCountFlagBits samples = vk::SampleCountFlagBits::e1;
	uint32_t depth = 1;
	uint32_t mipLevelCount = 1;
	uint32_t layerCount = 1;

	Image& setPtrManager(hf::Vk::MemoryManager * pManager_) {
		pManager = pManager_;
		return *this;
	}

	Image& setTiling(vk::ImageTiling tiling_) {
		tiling = tiling_;
		return *this;
	}

	Image& setUsage(vk::ImageUsageFlags usage_) {
		usage = usage_;
		return *this;
	}

	Image& setMemFlags(vk::MemoryPropertyFlags memFlags_) {
		memFlags = memFlags_;
		return *this;
	}

	Image& setSamples(vk::SampleCountFlagBits samples_) {
		samples = samples_;
		return *this;
	}

	Image& setMipLevelCount(uint32_t mipLevelCount_) {
		mipLevelCount = mipLevelCount_;
		return *this;
	}

	Image& setLayerCount(uint32_t layerCount_) {
		layerCount = layerCount_;
		return *this;
	}
};

class hf::Vk::Info::SwapchainImage : public Info::ImageBase {};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::Image : public hf::Vk::Info::Image {
public:
	enum Usage { GENERAL, DEPTHBUFFER, COLORBUFFER, DEPTHATTACHMENT, COLORATTACHMENT, COLORSAMPLED, TEXTURE, CUBE };

// attributes
public:
	bool createView = false;
	bool assignMemory = true;
	vk::DeviceSize allocationSize = 0;
	vk::ImageCreateFlags createFlags = vk::ImageCreateFlags(0);

// methods
public:
	// Ctor
	Image(CreateInfo::Image::Usage usage = hf::Vk::CreateInfo::Image::Usage::GENERAL, uint32_t width = 0, uint32_t height = 0, bool use_mipmap = false, bool device_local = true);
	Image(Info::Image info) : Info::Image(info) {}

	// Convertor
	operator vk::ImageCreateInfo();
	
	Image& setCreateView(bool createView_) {
		createView = createView_;
		return *this;
	}

	Image& setAssignMemory(bool assignMemory_) {
		assignMemory = assignMemory_;
		return *this;
	}

};

/* ------- Core Class ------- */
class hf::Vk::ImageBase {
protected:
	// Img Description
	hf::Vk::Info::ImageBase* _description = nullptr;

	// vk Image object
	vk::Image _image;

	// views storage
	hf::Ax::PtrVector<ImageView> _views;

public:
	/* Core Methods */
	void addView(CreateInfo::ImageView& createInfo);

	// changing layout
	void changeLayout(CommandBuffer& commandBuffer, Info::Image::ChangeLayout& info);
	void changeLayout(CommandBuffer& commandBuffer, Info::Image::ChangeLayout& info, CommandProcessor& proccessCommands);

	// clear Image's color
	// only add proper commands to keepCommands buffer
	void clearColor(vk::ClearColorValue clearValue, CommandBuffer& keepCommands, uint32_t layersCount = 1);
	void clearColorAttachment(vk::ClearColorValue clearValue, CommandBuffer& keepCommands, uint32_t layersCount = 1);
	// it also begin and finish cmd buffer and execute it instantly
	void clearColor(vk::ClearColorValue clearValue, CommandBuffer& keepCommands, CommandProcessor& execCommands);

	// clear depth stencil image
	// only add proper commands to keepCommands buffer
	void clearDepthStencil(vk::ClearDepthStencilValue clearValue, CommandBuffer& keepCommands, uint32_t layersCount = 1);
	// it also begin and finish cmd buffer and execute it instantly
	void clearDepthStencil(vk::ClearDepthStencilValue clearValue, CommandBuffer& keepCommands, CommandProcessor& execCommands);

	/* Getters */
	vk::Image& getVkHandle() { return _image; }
	//vk::ImageView& getVkView() { return _views.back()->getVkHandle(); } // vetsinou jediny view -> samostatny getter
	ImageView& getView() { return *_views.front(); }
	hf::Ax::PtrVector<ImageView>& getViews() { return _views; }
	Device& getDevice() { return *_description->pDevice; }

	/* Operators */
	operator vk::Image() { return _image; }

};

class hf::Vk::Image : public ImageBase {
//friend class
	friend class MemoryManager;

// attributes
private:
	// GPU Memory info
	Info::MemoryBlock _memoryInfo;

// methods
private:
	void init(CreateInfo::Image& createInfo);  // vytvori zaroven imageview
	void deinit();
	//bool initCompleted() { return _image != VK_NULL_HANDLE && _memory.allocatedFrom != nullptr; }
	void move(Image& withdraw);

	// changing layout
	vk::AccessFlags layoutToFlags(vk::ImageLayout layout);


public:
	/* Ctors 'n Dtor */
	Image() {}
	Image(CreateInfo::Image& createInfo) { init(createInfo); }
	Image(Image&& init) { move(init); }
	~Image() { deinit(); }
	// when using this, image must have flag DeviceLocal
	void stashOnGPU(void* data, uint32_t dataLen, CommandBuffer& keepCommands, CommandProcessor& proccessCommands);
	void stashOnGPU(std::vector<uint8_t>& data, CommandBuffer& keepCommands, CommandProcessor& proccessCommands);

	/* Core Methods */
	// object lifetime handles
	void create(CreateInfo::Image& createInfo) { return init(createInfo); }
	void discard() { deinit(); }

	// assign memory
	inline void assignMemory();

	// set data
	void setData(void* data);
	
	/*
		post commands to copy memory from one HFVKBuffer to this Image, source data keeps untouched,
		its user responsibility to begin command buffer before posting it to this method and after this method's call cmd buffer must be finished and posted by user...
	*/
	void copyMemory(Buffer& data, CommandBuffer& keepCommands, vk::BufferImageCopy* copyInfo = nullptr);
	// Image -> Image
	void copyMemory(Image& data, CommandBuffer& keepCommands, vk::ImageCopy* copyInfo = nullptr);
	
	/* Operators */
	Image& operator=(Image& assign) { move(assign); return *this; }
	
	/* Getters */
	Info::MemoryBlock& getMemoryInfo() { return _memoryInfo; }
	Info::Image& getDescription() { return *static_cast<Info::Image*>(_description); }
	vk::DeviceMemory& getMemory() { return _memoryInfo.allocatedFrom->getMemory(); } // gets whole backup device memory 
	inline void getMemoryProperties(Info::MemoryProperties& result); // getting image's memory requierements 

	/* Status Checkers */

};

class hf::Vk::SwapchainImage : public ImageBase { 
	// friend class
	friend class Swapchain;

// attributes
private:

// methods
public:
	SwapchainImage() {}
	~SwapchainImage() {}

	/* getters */
	Info::SwapchainImage& getDescription() { return *static_cast<Info::SwapchainImage*>(_description); }

};

#endif
