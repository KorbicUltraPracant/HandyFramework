/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Descriptor Set Layout wrapper
*/

#ifndef HFVKDESCRIPTORSETLAYOUT_H
#define HFVKDESCRIPTORSETLAYOUT_H

#include <hfVk/hfVk.h>
#include <vector>

/* ----------- Info Class ----------- */
class hf::Vk::Info::DescriptorSetLayout {
// attributes
public:
	hf::Vk::Device* pDevice;

// virtual dtor
public:
	virtual ~DescriptorSetLayout() = default;

	DescriptorSetLayout& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}
};


class hf::Vk::FurtherInfo::DescriptorSetLayout : public hf::Vk::Info::DescriptorSetLayout {
// attributes
public:
	std::vector<vk::DescriptorSetLayoutBinding> layoutBindings; // bindings

};

/* ----------- Create Info Class ----------- */
class hf::Vk::CreateInfo::DescriptorSetLayout : public hf::Vk::FurtherInfo::DescriptorSetLayout {
// attributes
public:

// methods
public:
	operator vk::DescriptorSetLayoutCreateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::DescriptorSetLayout {
// attributes
private:
	Info::DescriptorSetLayout* _description = nullptr;
	vk::DescriptorSetLayout _layout;

// methods
private:
	void init(CreateInfo::DescriptorSetLayout& createInfo);
	void deinit();
	//bool initCompleted();
	void move(DescriptorSetLayout& withdraw);

public:
	DescriptorSetLayout() {}
	DescriptorSetLayout(CreateInfo::DescriptorSetLayout& createInfo) { init(createInfo); }
	DescriptorSetLayout(DescriptorSetLayout&& init) { move(init); }
	~DescriptorSetLayout() { deinit(); }

	/* Getters */
	vk::DescriptorSetLayout& getVkHandle() { return _layout; }

	/* Core Methods */
	void describe(CreateInfo::DescriptorSetLayout& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

	/* Operators */
	operator vk::DescriptorSetLayout() { return _layout; }
	DescriptorSetLayout& operator=(DescriptorSetLayout& assign) { move(assign); return *this; }

};

#endif

