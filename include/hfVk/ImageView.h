/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief ImageView wrapper
*/

#ifndef HFVKIMAGEVIEW_H
#define HFVKIMAGEVIEW_H

#include <hfVk/hfVk.h>

/* ------- Info Class ------- */
class hf::Vk::Info::ImageView {
// attributes
public:
	hf::Vk::ImageBase* image; // view to
	vk::ImageViewType viewType = vk::ImageViewType(VK_IMAGE_VIEW_TYPE_2D);
	vk::Format format;
	vk::ComponentMapping componentMaping;
	vk::ImageSubresourceRange subresourceRange = { vk::ImageAspectFlags(vk::ImageAspectFlagBits::eColor), 0, 1, 0, 1 };

	ImageView& setImage(hf::Vk::ImageBase * image_) {
		image = image_;
		return *this;
	}

	ImageView& setViewType(vk::ImageViewType viewType_) {
		viewType = viewType_;
		return *this;
	}

	ImageView& setFormat(vk::Format format_) {
		format = format_;
		return *this;
	}

	ImageView& setComponentMaping(vk::ComponentMapping componentMaping_) {
		componentMaping = componentMaping_;
		return *this;
	}

    ImageView& setSubresourceRange(vk::ImageSubresourceRange imageSubresourceRange_) {
        subresourceRange = imageSubresourceRange_;
        return *this;
    }
};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::ImageView : public hf::Vk::Info::ImageView {
// attributes
public:

// methods
public:
	ImageView() {}
	ImageView(Info::ImageView& parent) : Info::ImageView(parent) {}
	operator vk::ImageViewCreateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::ImageView { 
// friend class
	friend class Image;

// attributes
private:
	hf::Vk::Info::ImageView* _description = nullptr;
	vk::ImageView _view;

// methods
private:
	void init(CreateInfo::ImageView& createInfo);
	void deinit();
	void move(ImageView& withdraw);

public:
	/* Ctors 'n Dtor */
	ImageView() {}
	ImageView(CreateInfo::ImageView& createInfo) { init(createInfo); }
	ImageView(ImageView&& init) { move(init); }
	~ImageView() { deinit(); }

	/* Getters */
	Info::ImageView& getDescription() { return *_description; }
	vk::ImageView& getVkHandle() { return _view; }

	/* Core Methods */
	void create(CreateInfo::ImageView& createInfo) { init(createInfo); }
	void destroy(){ deinit(); }

	/* Operators */
	ImageView& operator=(ImageView& assign) { move(assign); return *this; }
	operator vk::ImageView() { return _view; }

	/* Status Checkers */
	

};

#endif