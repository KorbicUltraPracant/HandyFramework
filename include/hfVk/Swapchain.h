/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Swapchain wrapper
*/

#ifndef HFVKSWAPCHAIN_H
#define HFVKSWAPCHAIN_H

#include <hfVk/hfVk.h>
#include <hfVk/Image.h>
#include <hfVk/Synchronization.h>

/* ------- Auxiliary Class ------- */
class hf::Vk::Auxiliary::Swapchain {
// methods
public:
	static bool checkSurfaceFormatAvailability(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface, vk::SurfaceFormatKHR format);
	static vk::SurfaceFormatKHR returnRandomAvailableSurfaceFormat(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface);
	static bool checkPresentModeAvailability(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface, vk::PresentModeKHR mode);
	static vk::PresentModeKHR returnRandomAvailablePresentMode(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface);
	static vk::SurfaceCapabilitiesKHR surfaceCapabilities(vk::PhysicalDevice psDevice, vk::SurfaceKHR surface);
	static vk::DisplayPropertiesKHR singleDisplayProperties(vk::PhysicalDevice psDevice);
};

class hf::Vk::Info::PresentImage {
// methods
public:
	hf::Vk::Queue* presentationQueue;
	std::vector<vk::Semaphore> waitSemaphores;
	std::vector<vk::SwapchainKHR> swapchains;
	std::vector<uint32_t> imageIndexes;
	bool checkResults = false;
	std::vector<vk::Result> results;

	operator vk::PresentInfoKHR();

	PresentImage& setPresentationQueue(hf::Vk::Queue * presentationQueue_) {
		presentationQueue = presentationQueue_;
		return *this;
	}

	PresentImage& setWaitSemaphores(std::vector<vk::Semaphore> waitSemaphores_) {
		waitSemaphores = waitSemaphores_;
		return *this;
	}

	PresentImage& setSwapchains(std::vector<vk::SwapchainKHR> swapchains_) {
		swapchains = swapchains_;
		return *this;
	}

	PresentImage& setImageIndexes(std::vector<uint32_t> imageIndexes_) {
		imageIndexes = imageIndexes_;
		return *this;
	}

	PresentImage& setCheckResults(bool checkResults_) {
		checkResults = checkResults_;
		return *this;
	}

	PresentImage& setResults(std::vector<vk::Result> results_) {
		results = results_;
		return *this;
	}
};

/* ------- Info Class ------- */
class hf::Vk::Info::Swapchain {
// attributes
public:
	hf::Vk::Device* pDevice;
	vk::SwapchainCreateFlagsKHR createFlags;
	vk::SurfaceKHR surface;
	uint32_t minImageCount;
	vk::SurfaceFormatKHR surfaceFormat;
	uint32_t width;
	uint32_t height;
	uint32_t imageArrayLayers;
	vk::ImageUsageFlags imageUsage;
	vk::SurfaceTransformFlagBitsKHR preTransform;
	vk::CompositeAlphaFlagBitsKHR compositeAlpha;
	vk::PresentModeKHR presentMode;
	vk::Bool32 clipped;
	//VkSurfaceCapabilitiesKHR surfaceCaps;

	// advanced usage
	//VkSharingMode imageSharingMode = VK_SHARING_MODE_EXCLUSIVE; // Image share between numerous queues

	Swapchain& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	Swapchain& setCreateFlags(vk::SwapchainCreateFlagsKHR createFlags_) {
		createFlags = createFlags_;
		return *this;
	}

	Swapchain& setSurface(vk::SurfaceKHR surface_) {
		surface = surface_;
		return *this;
	}

	Swapchain& setMinImageCount(uint32_t minImageCount_) {
		minImageCount = minImageCount_;
		return *this;
	}

	Swapchain& setSurfaceFormat(vk::SurfaceFormatKHR surfaceFormat_) {
		surfaceFormat = surfaceFormat_;
		return *this;
	}

	Swapchain& setWidth(uint32_t width_) {
		width = width_;
		return *this;
	}

	Swapchain& setHeight(uint32_t height_) {
		height = height_;
		return *this;
	}

	Swapchain& setImageArrayLayers(uint32_t imageArrayLayers_) {
		imageArrayLayers = imageArrayLayers_;
		return *this;
	}

	Swapchain& setImageUsage(vk::ImageUsageFlags imageUsage_) {
		imageUsage = imageUsage_;
		return *this;
	}

	Swapchain& setPreTransform(vk::SurfaceTransformFlagBitsKHR preTransform_) {
		preTransform = preTransform_;
		return *this;
	}

	Swapchain& setCompositeAlpha(vk::CompositeAlphaFlagBitsKHR compositeAlpha_) {
		compositeAlpha = compositeAlpha_;
		return *this;
	}

	Swapchain& setPresentMode(vk::PresentModeKHR presentMode_) {
		presentMode = presentMode_;
		return *this;
	}

	Swapchain& setClipped(vk::Bool32 clipped_) {
		clipped = clipped_;
		return *this;
	}
};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::Swapchain : public hf::Vk::Info::Swapchain {
// attributes
public:
	vk::SwapchainKHR oldSwapchain; // for recreating swapchain via old one

// methods
public:
	Swapchain() {}
	Swapchain(Info::Swapchain info) : Info::Swapchain(info) {}
	operator vk::SwapchainCreateInfoKHR();

	Swapchain& setOldSwapchain(vk::SwapchainKHR oldSwapchain_) {
		oldSwapchain = oldSwapchain_;
		return *this;
	}
};

/* ------- Core Class ------- */
class hf::Vk::Swapchain {
// attributes
private:
	Info::Swapchain* _description = nullptr; // custom alloc in ctor
	vk::SwapchainKHR _swapchain;

	std::vector<SwapchainImage*> _images; // imageView is part of hf::Vk::Image
	uint32_t _currentImageIdx = -1; // max uint number -> none image acquired.
	
	Semaphore _presentationLock; // On GPU Synchronization wait for presentation of previous image comes to the end
	Fence _obtainNextImageFence; // to create it once not every obtainNextImage method call...

// methods
private:
	void init(CreateInfo::Swapchain& createInfo);
	void deinit();
	void move(Swapchain& withdraw);
	//bool initCompleted();

	// obtain swapchain images and init image views for them
	void obtainImages(); 
	// before creating framebuffers used render pass must be created
	void returnImages(); // clears all HFVKimages(set them null, because swapchain cares of dtoring them)
	// dtory na objekty nejsou treba... zaridi si to objekty sami ;) 
	
public:
	/* Ctors 'n Dtor */
	Swapchain() {}
	Swapchain(CreateInfo::Swapchain& createInfo) { init(createInfo); }
	Swapchain(Swapchain&& init) { move(init); }
	~Swapchain() { deinit(); };

	/* Object's Lifetime Methods */
	void create(CreateInfo::Swapchain& createInfo) { init(createInfo); }
	void dismantle() { deinit(); }
	
	/* Getters */
	void acquireNextImage(); // rekne swapchain aby ziskala dalsi image, vrati se s presentImage...
	SwapchainImage& getImage() { if (_currentImageIdx != -1)  return *_images[_currentImageIdx]; }	
	SwapchainImage& getImage(uint32_t idx) { if (idx != -1)  return *_images[idx]; }
	std::vector<SwapchainImage*> getImagesVec() { return _images; }
	vk::Format getImageFormat() { return _description->surfaceFormat.format; }
	vk::SurfaceKHR getPresentationSurface() { return _description->surface; }
	Info::Swapchain& getDescription() { return *_description; }
	vk::SwapchainKHR getVkHandle() { return _swapchain; }
	int getCurrentImageIdx() { return _currentImageIdx; }

	/* Core Methods */
	void initResources();
	bool presentImage(hf::Vk::Queue& presentationQueue); //bool waitPresentationEnd = false
	bool presentImage(Info::PresentImage& info);
	void changeResolution(uint32_t width, uint32_t height);
	void transformImagesToProperLayout(CommandBuffer& postSingleCmdBufferHere);
	void clearColors(CommandBuffer& keepCommands, CommandProcessor& execCommands); // clear colors for swapchain image and deptbuffer
	void recordClearColorsCommandForCurrentImage(CommandBuffer& keepCommands) { if (_currentImageIdx != uint32_t(-1)) _images[_currentImageIdx]->clearColor(std::array<float, 4>{0.7f, 0.7f, 0.7f, 1.0f}, keepCommands); }

	/* Operators */
	operator vk::SwapchainKHR() { return _swapchain; }
	Swapchain& operator=(Swapchain& assign) { move(assign); return *this; }

	/* Status Checkers */
	
};

#endif

/* 
// Choose Swapchain's surface compatibility with GPU, zada se pozadovany, a v res vectoru se vrati nalezeny pro kazde gpu nebo prazdna struktura
void SurfaceCompatibleGPUs(vk::PhysicalDevice psDevice, VkSurfaceFormatKHR& format);
*/