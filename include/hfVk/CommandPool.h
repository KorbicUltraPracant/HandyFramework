/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief CommandPool object 
*/

#ifndef HFVKCOMMANDPOOL_H
#define HFVKCOMMANDPOOL_H

#include <hfVk/hfVk.h>
#include <hfVk/Device.h>

/* ------- Info Class ------- */
class hf::Vk::Info::CommandPool {
// attributes
public:
	hf::Vk::Device* pDevice;
	vk::CommandPoolCreateFlags flags = vk::CommandPoolCreateFlags(0);
	uint32_t queueFamilyIndex;

	CommandPool& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	CommandPool& setFlags(vk::CommandPoolCreateFlags flags_) {
		flags = flags_;
		return *this;
	}

	CommandPool& setQueueFamilyIndex(uint32_t queueFamilyIndex_) {
		queueFamilyIndex = queueFamilyIndex_;
		return *this;
	}
};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::CommandPool : public hf::Vk::Info::CommandPool {
// attributes
public:

// methods
public:
	operator vk::CommandPoolCreateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::CommandPool {
//attributes
private:
	vk::CommandPool _pool;
	Info::CommandPool* _description = nullptr;

//methods
private:
	void init(CreateInfo::CommandPool& createInfo);
	void deinit();

public:
	/* Ctors 'n Dtor */
	CommandPool() {}
	CommandPool(CreateInfo::CommandPool& createInfo) { init(createInfo); }
	~CommandPool() { deinit(); }

	/* Getters */
	vk::CommandPool& getVkHandle() { return _pool; }
	Info::CommandPool& getDescription() { return *_description; }

	/* Core Methods */
	void construct(CreateInfo::CommandPool& createInfo) { init(createInfo); }
	void wipeout() { deinit(); }

	/* Operators */
	operator vk::CommandPool() { return _pool; }

};

#endif
