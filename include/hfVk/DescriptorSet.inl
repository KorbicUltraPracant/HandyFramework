/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Inlines for Descriptor Set
*/

#ifndef HFVKDESCRIPTOR_INL
#define HFVKDESCRIPTOR_INL

#include <hfVk/DescriptorSet.h>

inline hf::Vk::Info::WriteDescriptorSet::operator vk::WriteDescriptorSet() {
	return vk::WriteDescriptorSet {
		dstSet,
		dstBinding,
		dstArrayElement,
		descriptorCount,
		descriptorType,
		pImageInfo,
		pBufferInfo,
		pTexelBufferView
	};
}

/* --------- Core --------- */
void hf::Vk::DescriptorSet::update(Info::UpdateDescriptorSets info) {
	std::vector<vk::WriteDescriptorSet> writeInfos;
	for (auto& writeI : info.writeInfo)
		writeInfos.push_back(writeI);

	_description->pManager->getDevice().getVkHandle().updateDescriptorSets((uint32_t)writeInfos.size(), writeInfos.data(), 0, VK_NULL_HANDLE);
}

#endif