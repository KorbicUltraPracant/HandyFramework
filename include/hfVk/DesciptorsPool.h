/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Descriptor Pool wrapper object
*/

#ifndef HFVKDESCRIPTORPOOL_H
#define HFVKDESCRIPTORPOOL_H

#include <hfVk/Device.h>
#include <vector>
#include <unordered_map>

/* ----------- Info Class ----------- */
class hf::Vk::Info::DescriptorPool {
// attributes
public:
	hf::Vk::Device* pDevice;
	uint32_t maxSets; // maximum number of sets which can be allocated from pool
	vk::DescriptorPoolCreateFlags flags;

	DescriptorPool& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	DescriptorPool& setMaxSets(uint32_t maxSets_) {
		maxSets = maxSets_;
		return *this;
	}

	DescriptorPool& setFlags(vk::DescriptorPoolCreateFlags flags_) {
		flags = flags_;
		return *this;
	}

};

/* ----------- Create Info Class ----------- */
class hf::Vk::CreateInfo::DescriptorPool : public hf::Vk::Info::DescriptorPool {
// attributes
public:
	std::vector<vk::DescriptorPoolSize> allocateObjectsInfo;
	uint32_t maxAllocationCount = 1;

// methods
public:
	operator vk::DescriptorPoolCreateInfo();

	DescriptorPool& setMaxAllocationCount(uint32_t maxAllocationCount_) {
		maxAllocationCount = maxAllocationCount_;
		return *this;
	}
};

/* ------- Core Class ------- */
class hf::Vk::DescriptorPool {
// friend class 
	friend class hf::Vk::DescriptorManager;

// attributes
private:
	Info::DescriptorPool* _description = nullptr;
	vk::DescriptorPool _pool;
	std::unordered_map<vk::DescriptorType, uint32_t> _allocationAbleObjectsInfo;

// methods
private:
	void init(CreateInfo::DescriptorPool& createInfo);
	void deinit();

public:
	/* Ctors 'n Dtor */
	DescriptorPool() {}
	DescriptorPool(CreateInfo::DescriptorPool& createInfo) { init(createInfo); }
	~DescriptorPool() { deinit(); }

	/* Core Methods */
	void construct(CreateInfo::DescriptorPool& createInfo) { init(createInfo); }
	void wipeout() { deinit(); }

	/* Getters */
	vk::DescriptorPool& getVkHandle() { return _pool; }

	/* Operators */
	operator vk::DescriptorPool() { return _pool; }

};


#endif
