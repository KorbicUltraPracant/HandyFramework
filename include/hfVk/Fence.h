/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Fence wrapper object
*/

#ifndef HFVKFENCE_H
#define HFVKFENCE_H

#include <hfVk/hfVk.h>
//#include <mutex>

/* ------ Info Classes ------ */
class hf::Vk::Info::Fence {
// attributes
public:
	hf::Vk::Device* pDevice;

};

class hf::Vk::CreateInfo::Fence : public hf::Vk::Info::Fence {
// attributes
public:
	bool createSignaled = false;

// methods
public:
	Fence() {}
	Fence(Info::Fence info, bool signaled = false) : Info::Fence(info), createSignaled(signaled) {}
	operator vk::FenceCreateInfo();

	Fence& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	Fence& setCreateSignaled(bool createSignaled_) {
		createSignaled = createSignaled_;
		return *this;
	}
};

/* ------ Fence Core Class ------ */
class hf::Vk::Fence {
// attributes
private:
	Info::Fence* _description = nullptr;
	vk::Fence _fence;
	bool _used = false; // fence usage flag

// methods
private:
	void init(CreateInfo::Fence& createInfo);
	void deinit();
	void move(Fence& withdraw);

public:
	/* Ctors 'n Dtor */
	Fence() {}
	Fence(CreateInfo::Fence& createInfo) { init(createInfo); }
	Fence(Fence&& init) { move(init); }
	~Fence() { deinit(); }

	/* Getters */
	vk::Fence& getVkHandle() { return _fence; }
	Info::Fence& getDescription() { return *_description; }

	/* Core Methods */
	void create(CreateInfo::Fence& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

	vk::Fence passFenceToSignal();
	void waitUntilSignaled();
	void reset();
	void markAsUsed() { _used = true; }

	/* Operators */
	Fence& operator=(Fence& assign) { move(assign); return *this; }
	operator vk::Fence() { return _fence; }

	/* Status Checkers */
	bool isUsed();
	bool isSignaled();

};

#endif