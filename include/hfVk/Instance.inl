/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Inlines for Instance methods 
*/

#ifndef HFVK_INSTANCE_INL
#define HFVK_INSTANCE_INL

#include <hfVk/Instance.h>

inline uint32_t hf::Vk::Auxiliary::Instance::makeVersion(uint16_t major, uint16_t minor, uint16_t patch) {
	return (((major) << 22) | ((minor) << 12) | (patch));
}

inline std::string hf::Vk::Auxiliary::Instance::getVersion(uint32_t encodedIn) {
	return std::string(std::to_string(encodedIn >> 22) + "." + std::to_string((encodedIn << 10) >> 22) + "." + std::to_string((encodedIn << 20) >> 20));
}

inline hf::Vk::CreateInfo::Instance::operator vk::ApplicationInfo() {
	return appInfo;
}

#endif // !
