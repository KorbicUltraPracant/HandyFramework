/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Command buffer Info sturctures used for CommandBuffer's methods
*/

#ifndef HFVKCOMMANDBUFFERINFOSTRUCTS_H
#define HFVKCOMMANDBUFFERINFOSTRUCTS_H

#include <hfVk/hfVk.h>
#include <vector>

class hf::Vk::Info::BeginRenderPass {
// attributes
public:
	hf::Vk::RenderPass* pRenderPass = nullptr;
	hf::Vk::Framebuffer* pFramebuffer = nullptr;
	vk::Rect2D renderArea = { { 0,0 }, { 0,0 } };
	std::vector<vk::ClearValue> clearColors;
	vk::SubpassContents subpassContents = vk::SubpassContents::eInline; // defines if during first subpass will be used secondary cmd buffers or not

// methods
public:
	operator vk::RenderPassBeginInfo();

	BeginRenderPass& setPRenderPass(hf::Vk::RenderPass * pRenderPass_) {
		pRenderPass = pRenderPass_;
		return *this;
	}

	BeginRenderPass& setPFramebuffer(hf::Vk::Framebuffer * pFramebuffer_) {
		pFramebuffer = pFramebuffer_;
		return *this;
	}

	BeginRenderPass& setClearColors(std::vector<vk::ClearValue> clearColors_) {
		clearColors = clearColors_;
		return *this;
	}

	BeginRenderPass& setSubpassContents(vk::SubpassContents subpassContents_) {
		subpassContents = subpassContents_;
		return *this;
	}

};

class hf::Vk::Info::BindIndexBuffer {
// attributes
public:
	hf::Vk::Buffer* pBuffer = nullptr;
	vk::DeviceSize offset = 0;
	vk::IndexType indexType = vk::IndexType::eUint32;

	BindIndexBuffer& setPBuffer(hf::Vk::Buffer * pBuffer_) {
		pBuffer = pBuffer_;
		return *this;
	}

	BindIndexBuffer& setOffset(vk::DeviceSize offset_) {
		offset = offset_;
		return *this;
	}

	BindIndexBuffer& setIndexType(vk::IndexType indexType_) {
		indexType = indexType_;
		return *this;
	}

};

class hf::Vk::Info::BindVertexBuffers {
// attributes
public:
	uint32_t firstBinding;
	std::vector<vk::Buffer> pBuffers;
	std::vector<vk::DeviceSize> offsets;

	BindVertexBuffers& setFirstBinding(uint32_t firstBinding_) {
		firstBinding = firstBinding_;
		return *this;
	}

	BindVertexBuffers& setPBuffers(std::vector<vk::Buffer> pBuffers_) {
		pBuffers = pBuffers_;
		return *this;
	}

	BindVertexBuffers& setOffsets(std::vector<vk::DeviceSize> offsets_) {
		offsets = offsets_;
		return *this;
	}

};

class hf::Vk::Info::BindDescriptorSets {
// attributes
public:
	std::vector<hf::Vk::DescriptorSet*> pDescriptorSets;
	uint32_t firstSet;
	hf::Vk::PipelineLayout* pPipelineLayout = nullptr;
	vk::PipelineBindPoint bindPoint = vk::PipelineBindPoint::eGraphics;
	std::vector<uint32_t> dynamicOffsets;

// methods
public:
	BindDescriptorSets& setPDescriptorSets(std::vector<hf::Vk::DescriptorSet *> pDescriptorSets_) {
		pDescriptorSets = pDescriptorSets_;
		return *this;
	}

	BindDescriptorSets& setFirstSet(uint32_t firstSet_) {
		firstSet = firstSet_;
		return *this;
	}

	BindDescriptorSets& setPPipelineLayout(hf::Vk::PipelineLayout * pPipelineLayout_) {
		pPipelineLayout = pPipelineLayout_;
		return *this;
	}

	BindDescriptorSets& setBindPoint(vk::PipelineBindPoint bindPoint_) {
		bindPoint = bindPoint_;
		return *this;
	}

	BindDescriptorSets& setDynamicOffsets(std::vector<uint32_t> dynamicOffsets_) {
		dynamicOffsets = dynamicOffsets_;
		return *this;
	}

};

class hf::Vk::Info::Draw {
// attributes
public:
	uint32_t vertexCount;
	uint32_t firstVertex = 0;
	uint32_t instanceCount = 1;
	uint32_t firstInstance = 0;

	Draw& setVertexCount(uint32_t vertexCount_) {
		vertexCount = vertexCount_;
		return *this;
	}

	Draw& setFirstVertex(uint32_t firstVertex_) {
		firstVertex = firstVertex_;
		return *this;
	}

	Draw& setInstanceCount(uint32_t instanceCount_) {
		instanceCount = instanceCount_;
		return *this;
	}

	Draw& setFirstInstance(uint32_t firstInstance_) {
		firstInstance = firstInstance_;
		return *this;
	}

};

class hf::Vk::Info::DrawIndexed {
// attributes
public:
	uint32_t indexCount;
	uint32_t firstIndex = 0;
	int32_t vertexOffset = 0;
	uint32_t instanceCount = 1;
	uint32_t firstInstance = 0;

	DrawIndexed& setIndexCount(uint32_t indexCount_) {
		indexCount = indexCount_;
		return *this;
	}

	DrawIndexed& setFirstIndex(uint32_t firstIndex_) {
		firstIndex = firstIndex_;
		return *this;
	}

	DrawIndexed& setVertexOffset(int32_t vertexOffset_) {
		vertexOffset = vertexOffset_;
		return *this;
	}

	DrawIndexed& setInstanceCount(uint32_t instanceCount_) {
		instanceCount = instanceCount_;
		return *this;
	}

	DrawIndexed& setFirstInstance(uint32_t firstInstance_) {
		firstInstance = firstInstance_;
		return *this;
	}

};

class hf::Vk::Info::DrawIndexedIndirect {
// attributes
public:
	vk::Buffer buffer;
	vk::DeviceSize offset;
	uint32_t drawCount;
	uint32_t stride;

	DrawIndexedIndirect& setBuffer(vk::Buffer buffer_) {
		buffer = buffer_;
		return *this;
	}

	DrawIndexedIndirect& setOffset(vk::DeviceSize offset_) {
		offset = offset_;
		return *this;
	}

	DrawIndexedIndirect& setDrawCount(uint32_t drawCount_) {
		drawCount = drawCount_;
		return *this;
	}

	DrawIndexedIndirect& setStride(uint32_t stride_) {
		stride = stride_;
		return *this;
	}
};

class hf::Vk::Info::PushConstants {
// attributes
public:
	hf::Vk::PipelineLayout* pLayout = nullptr; // if nullptr not error but push constants are not used at all
	vk::ShaderStageFlags usedInStages;
	uint32_t offset = 0;
	uint32_t size;
	void* pValues;

public:
	PushConstants& setPipelineLayout(hf::Vk::PipelineLayout* pLayout_) {
		pLayout = pLayout_;
		return *this;
	}
	PushConstants& setShaderStages(vk::ShaderStageFlags usedInStages_) {
		usedInStages = usedInStages_;
		return *this;
	}
	PushConstants& setOffset(uint32_t offset_) {
		offset = offset_;
		return *this;
	}
	PushConstants& setSize(uint32_t size_) {
		size = size_;
		return *this;
	}
	PushConstants& setPValues(void* pValues_) {
		pValues = pValues_;
		return *this;
	}
};

class hf::Vk::Info::SetViewport {
// attributes
public:
	float x = 0;
	float y = 0;
	float width;
	float height;
	float minDepth = 0.0f;
	float maxDepth = 1.0f;

// methods
public:
	operator vk::Viewport() {
		return { x, y, width, height, minDepth, maxDepth };
	}

	SetViewport& setX(float x_) {
		x = x_;
		return *this;
	}

	SetViewport& setY(float y_) {
		y = y_;
		return *this;
	}

	SetViewport& setWidth(float width_) {
		width = width_;
		return *this;
	}

	SetViewport& setHeight(float height_) {
		height = height_;
		return *this;
	}

	SetViewport& setMinDepth(float minDepth_) {
		minDepth = minDepth_;
		return *this;
	}

	SetViewport& setMaxDepth(float maxDepth_) {
		maxDepth = maxDepth_;
		return *this;
	}
};

class hf::Vk::Info::SetScissor {
// attributes
public:
	int32_t x = 0;
	int32_t y = 0;
	uint32_t width;
	uint32_t height;

// methods
public:
	operator vk::Rect2D() { return { { x, y }, { width, height } }; }

	SetScissor& setX(float x_) {
		x = x_;
		return *this;
	}

	SetScissor& setY(float y_) {
		y = y_;
		return *this;
	}

	SetScissor& setWidth(float width_) {
		width = width_;
		return *this;
	}

	SetScissor& setHeight(float height_) {
		height = height_;
		return *this;
	}
};

#endif
