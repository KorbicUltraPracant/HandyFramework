/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Framebuffer wrapper
*/

#ifndef HFVKFRAMEBUFFER_H
#define HFVKFRAMEBUFFER_H

#include <hfVk/hfVk.h>
#include <vector>

/* ------- Info Class ------- */
class hf::Vk::Info::Framebuffer {
// attributes
public:
	hf::Vk::Device* pDevice;
	hf::Vk::RenderPass* activeRenderPass;
	uint32_t width;
	uint32_t height;
	uint32_t layersCount; // or dimensions of framebuffer if you want

// virtual dtor
public:
	virtual ~Framebuffer() = default;

	Framebuffer& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	Framebuffer& setActiveRenderPass(hf::Vk::RenderPass * activeRenderPass_) {
		activeRenderPass = activeRenderPass_;
		return *this;
	}

	Framebuffer& setWidth(uint32_t width_) {
		width = width_;
		return *this;
	}

	Framebuffer& setHeight(uint32_t height_) {
		height = height_;
		return *this;
	}

	Framebuffer& setLayersCount(uint32_t layersCount_) {
		layersCount = layersCount_;
		return *this;
	}

};

class hf::Vk::FurtherInfo::Framebuffer : public hf::Vk::Info::Framebuffer {
// attributes
public:
	std::vector<hf::Vk::ImageView*> attachmentRefs;

};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::Framebuffer : public hf::Vk::FurtherInfo::Framebuffer {
// attributes
private:
	std::vector<vk::ImageView> _createInfoAttachmentRefs;

// methods
public:
	operator vk::FramebufferCreateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::Framebuffer {
// attributes
private:
	hf::Vk::Info::Framebuffer* _description = nullptr;
	vk::Framebuffer _framebuffer;

public:

// methods
private:
	void init(CreateInfo::Framebuffer& createInfo);
	void deinit();
	void move(Framebuffer& withdraw);

public:
	/* Ctors 'n Dtor */
	Framebuffer() {}
	Framebuffer(CreateInfo::Framebuffer& createInfo) { init(createInfo); }
	Framebuffer(Framebuffer&& init) { move(init); }
	~Framebuffer() { deinit(); }

	/* Getters */
	Info::Framebuffer& getDescription() { return *_description; }
	vk::Framebuffer& getVkHandle() { return _framebuffer; }

	/* Operators */
	Framebuffer& operator=(Framebuffer& assign) { move(assign); return *this; }
	operator vk::Framebuffer() { return _framebuffer; }

	/* Core Methods */
	// nickname for init
	void assemble(CreateInfo::Framebuffer& createInfo) { init(createInfo); }
	void dismantle() { deinit(); };

}; 

#endif
