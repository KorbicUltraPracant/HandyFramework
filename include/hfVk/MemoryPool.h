/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief MemoryPoll object
*/

#ifndef HFVKMEMORYPOOL_H
#define HFVKMEMORYPOOL_H

#include <hfVk/hfVk.h>
#include <hfVk/MemoryManager.h>
#include <hfVk/MemoryBlock.h>
#include <list>

/* ------- Info Class ------- */
class hf::Vk::Info::MemoryPool {
// attributes
public:
	//hf::Vk::Device* pDevice;
	vk::DeviceSize allocatedSpace = 0; // how much memory will be allocated inside pool
	vk::DeviceSize unitSize; // size of baseResource for which was pool originaly allocated
	uint32_t memoryTypeIndex;
	vk::MemoryPropertyFlags propertyFlags;

	MemoryPool& setAllocatedSpace(vk::DeviceSize allocatedSpace_) {
		allocatedSpace = allocatedSpace_;
		return *this;
	}

	MemoryPool& setUnitSize(vk::DeviceSize unitSize_) {
		unitSize = unitSize_;
		return *this;
	}

	MemoryPool& setMemoryTypeIndex(uint32_t memoryTypeIndex_) {
		memoryTypeIndex = memoryTypeIndex_;
		return *this;
	}

	MemoryPool& setPropertyFlags(vk::MemoryPropertyFlags propertyFlags_) {
		propertyFlags = propertyFlags_;
		return *this;
	}
};

class hf::Vk::CreateInfo::MemoryPool : public hf::Vk::Info::MemoryPool {
// attributes
public:

};

class hf::Vk::MemoryPool {
//friend classes
	friend class MemoryManager;

// attributes
private:
	hf::Vk::Info::MemoryPool* _description = nullptr;

	/* Memory Allocation Info */
	//std::list<AllocationInfo> _allocationMap; // vk::DeviceSize Start, vk::DeviceSize offset
	std::list<AllocationInfo> _allocationAbleMap; // free spaces inside memory

	vk::DeviceMemory _memory;
	uint32_t _allocationCount = 0; // used in deallocation

// methods
private:
public:
	/* Ctors 'n Dtor */
	MemoryPool() {}

	/* Getters */
	vk::DeviceMemory& getMemory() { return _memory; }
	vk::MemoryPropertyFlags getFlags() { return _description->propertyFlags; }
	std::list<AllocationInfo>const& getFreeSpaceInfo() { return _allocationAbleMap; }
	hf::Vk::Info::MemoryPool& getDescription() { return *_description; }
	uint32_t getAllocationCount() { return _allocationCount; }

	/* Core Methods */

	/* Status Checkers */
	bool initialized() { return _memory != vk::DeviceMemory(); };
};

#endif
