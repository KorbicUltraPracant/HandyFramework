/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Command buffer object
*/

#ifndef HFVKCOMMANDBUFFER_H
#define HFVKCOMMANDBUFFER_H

#include <hfVk/hfVk.h>
#include <hfVk/CommandPool.h>
#include <hfVk/Synchronization.h>
#include <hfVk/CommandBufferInfo.h>

/* ------- Info Class ------- */
class hf::Vk::Info::CommandBuffer {
// attributes
public:
	hf::Vk::CommandPool* pPool;
	vk::CommandBufferLevel level;

	CommandBuffer& setPtrPool(hf::Vk::CommandPool * pPool_) {
		pPool = pPool_;
		return *this;
	}

	CommandBuffer& setLevel(vk::CommandBufferLevel level_) {
		level = level_;
		return *this;
	}

};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::CommandBuffer : public hf::Vk::Info::CommandBuffer {
// attributes
public:
	//bool createFences = false;

// methods
public:
	operator vk::CommandBufferAllocateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::CommandBuffer {
// attributes
private:
	Info::CommandBuffer* _description = nullptr;
	vk::CommandBuffer _buffer;
	Fence* _lock = nullptr; // queue submit fence, not initing or anything

public:
	vk::CommandBufferInheritanceInfo* inheritanceInfo = nullptr; // only if _description->level == Secondary

// methods
private:
	void init(CreateInfo::CommandBuffer& createInfo);
	void deinit();
	void move(CommandBuffer& withdraw);

public:
	/* Ctors 'n Dtor */
	CommandBuffer() {}
	CommandBuffer(CreateInfo::CommandBuffer& createInfo) { init(createInfo); }
	CommandBuffer(CommandBuffer&& init) { move(init); }
	CommandBuffer(vk::CommandBuffer buffer,CreateInfo::CommandBuffer& info) { adopt(buffer, info); }
	~CommandBuffer() { deinit(); }

	/* Getters */
	vk::CommandBuffer& getVkHandle() { return _buffer; }
	Info::CommandBuffer& getDescription() { return *_description; }
	uint32_t getFamilyIndex() { return _description->pPool->getDescription().queueFamilyIndex; }
	Fence& getLock() { return *_lock; }

	/* Core Methods */
	void create(CreateInfo::CommandBuffer& createInfo) { init(createInfo); }
	bool create(vk::CommandBuffer vkbuffer, CreateInfo::CommandBuffer& createInfo);
	void discard() { deinit(); }

	// adopt and disinherit vk::Commandbuffer object
	void adopt(vk::CommandBuffer buffer, CreateInfo::CommandBuffer& info);
	void disinherit();

	/* Custom vk::CommandBuffer Commands */
	bool beginRecording(vk::CommandBufferUsageFlags usage = vk::CommandBufferUsageFlagBits::eOneTimeSubmit); // secondary buffers must preinit their inheritance struct before calling this...
	void finishRecording() { return _buffer.end(); }
	void reset(vk::CommandBufferResetFlags resetFlag = vk::CommandBufferResetFlagBits::eReleaseResources) { return _buffer.reset(vk::CommandBufferResetFlags(resetFlag)); }
	void beginRenderPass(Info::BeginRenderPass beginInfo);
	void endRenderPass() { vkCmdEndRenderPass(_buffer); }
	void setViewport(Info::SetViewport info);
	void setScissor(Info::SetScissor info);
	void bindVertexBuffers(Info::BindVertexBuffers info);
	void bindIndexBuffer(Info::BindIndexBuffer info);
	void bindIndexBuffer(vk::Buffer indexBuffer, vk::DeviceSize offset, vk::IndexType indexType = vk::IndexType::eUint32);
	void bindPipeline(Pipeline& pipeline, vk::PipelineBindPoint bindPoint);
	void bindGraphicPipeline(GraphicPipeline& pipeline);
	void bindComputePipeline(ComputePipeline& pipeline);
	void bindDescriptorSets(Info::BindDescriptorSets info);
	void pushConstants(Info::PushConstants info);
	void setEvent(vk::Event event, vk::PipelineStageFlags stageMask);
	void resetEvent(vk::Event event, vk::PipelineStageFlags stageMask);
	// in future, waitEvent, but this command not only waits for event to be signaled but also with it activate memory barrier
	void nextSubpass(vk::SubpassContents contents = vk::SubpassContents::eInline) { _buffer.nextSubpass(contents); }

	// variations of draw commands
	void draw(Info::Draw& info) { vkCmdDraw(_buffer, info.vertexCount, info.instanceCount, info.firstVertex, info.firstInstance); }
	void draw(uint32_t vertexCount, uint32_t firstVertex = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0) { vkCmdDraw(_buffer, vertexCount, instanceCount, firstVertex, firstInstance); }
	void drawIndexed(Info::DrawIndexed& info) { vkCmdDrawIndexed(_buffer, info.indexCount, info.instanceCount, info.firstIndex, info.vertexOffset, info.firstInstance); }
	void drawIndexed(uint32_t indexCount, uint32_t firstIndex = 0, int32_t vertexOffset = 0, uint32_t instanceCount = 1, uint32_t firstInstance = 0) { vkCmdDrawIndexed(_buffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance); }
	void drawIndexedIndirect(vk::Buffer buffer, vk::DeviceSize offset, uint32_t drawCount, uint32_t stride);
	void drawIndexedIndirect(Info::DrawIndexedIndirect& info) { drawIndexedIndirect(info.buffer, info.offset, info.drawCount, info.stride); }

	/* Operators */
	CommandBuffer& operator=(CommandBuffer& assign) { move(assign); return *this; }
	//operator vk::CommandBuffer() { return _buffer; }

	/* Lock using operations */
	void waitUntilExecutionFinished();
	bool isSubmited();

};

#endif
