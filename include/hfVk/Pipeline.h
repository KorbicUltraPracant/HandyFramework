/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Pipeline abstract class
*/

#ifndef HFVKPIPE_H
#define HFVKPIPE_H

#include <hfVk/hfVk.h>

/* ------- Info Class ------- */
class hf::Vk::Info::Pipeline {
// attributes
public:
	// parent device
	hf::Vk::Device* pDevice;

	// used shader layout
	hf::Vk::PipelineLayout* layout;

	// flags
	vk::PipelineCreateFlags flags = vk::PipelineCreateFlags(0);

	Pipeline& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	Pipeline& setLayout(hf::Vk::PipelineLayout * layout_) {
		layout = layout_;
		return *this;
	}

	Pipeline& setFlags(vk::PipelineCreateFlags flags_) {
		flags = flags_;
		return *this;
	}
};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::Pipeline {
// attributes
public:
	// used cache
	hf::Vk::PipelineCache* pipelineCache = nullptr;

	// pipes derivations
	VkPipeline basePipelineHandle = VK_NULL_HANDLE;
	int32_t basePipelineIndex = -1;

	Pipeline& setPipelineCache(hf::Vk::PipelineCache * pipelineCache_) {
		pipelineCache = pipelineCache_;
		return *this;
	}

	Pipeline& setBasePipelineHandle(VkPipeline basePipelineHandle_) {
		basePipelineHandle = basePipelineHandle_;
		return *this;
	}

	Pipeline& setBasePipelineIndex(int32_t basePipelineIndex_) {
		basePipelineIndex = basePipelineIndex_;
		return *this;
	}

};

/* ------- Core Class ------- */
class hf::Vk::Pipeline {
// attributes
public:
	vk::Pipeline _pipeline;

// methods
public:
	/* Ctors 'n Dtor */

	/* Getters */
	vk::Pipeline& getVkHandle() { return _pipeline; }

	/* Core Methods */

	/* Operators */
	operator vk::Pipeline() { return _pipeline; }

};


#endif
