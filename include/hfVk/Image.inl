/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Inlines for Image's methods
*/

#ifndef HFVKIMAGE_INL
#define HFVKIMAGE_INL

#include <hfVk/Image.h>
#include <hfVk/ImageInfo.h>
#include <hfVk/ImageView.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;

inline void Image::getMemoryProperties(Info::MemoryProperties& result) {
	vk::MemoryRequirements reqs;
	getDevice().getVkHandle().getImageMemoryRequirements(_image, &reqs);

	result.size = reqs.size;
	result.memoryTypeIndex = static_cast<Info::Image*>(_description)->pManager->typeBitsToIndex(reqs.memoryTypeBits, static_cast<Info::Image*>(_description)->memFlags);
	result.alignment = reqs.alignment;
	result.propertyFlags = static_cast<Info::Image*>(_description)->memFlags;
}

inline void hf::Vk::Image::assignMemory() {
	static_cast<Info::Image*>(_description)->pManager->assignMemoryToImage(*this);
}

#endif