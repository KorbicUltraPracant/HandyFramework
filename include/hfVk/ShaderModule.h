/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Shader module wrapper
*/

#ifndef HFVKSHADERMODULE_H
#define HFVKSHADERMODULE_H

#include <hfVk/hfVk.h>
#include <string>
#include <vector>

/* ----------- Info Class ----------- */
class hf::Vk::Info::ShaderModule {
	friend class hf::Vk::CreateInfo::ShaderModule;
	friend class hf::Vk::ShaderModule;

// attributes
private:
	std::vector<uint32_t> data;

public:
	hf::Vk::Device* pDevice;
	std::string path;

	ShaderModule& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	ShaderModule& setPath(std::string path_) {
		path = path_;
		return *this;
	}
};

/* ----------- Create Info Class ----------- */
class hf::Vk::CreateInfo::ShaderModule : public hf::Vk::Info::ShaderModule {
// attributes
private:
public:

// methods
public:
	/* Operators */
	operator vk::ShaderModuleCreateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::ShaderModule {
// attributes
private:
	Info::ShaderModule* _description = nullptr;
	vk::ShaderModule _shader;

// methods
private:
	void init(CreateInfo::ShaderModule& createInfo);
	void deinit();
	//bool initCompleted() { return _shader != VK_NULL_HANDLE && _description != nullptr; }
	void move(ShaderModule& withdraw);

public:
	/* Ctors 'n Dtor */
	ShaderModule() {}
	ShaderModule(CreateInfo::ShaderModule& createInfo) { init(createInfo); }
	ShaderModule(ShaderModule&& init) { move(init); }
	~ShaderModule() { deinit(); }

	/* Getters */
	VkShaderModule getVkHandle() { return _shader; }

	/* Core Methods */
	void create(CreateInfo::ShaderModule& createInfo) { init(createInfo); }
	void destroy() { deinit(); }
	
	/* Operators */
	operator VkShaderModule() { return _shader; }
	ShaderModule& operator=(ShaderModule& assign) { move(assign); return *this; }

	/* Status Checkers */

};

#endif