/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Semaphore object
*/

#ifndef HFVKSEMAPHORE_H
#define HFVKSEMAPHORE_H

#include <hfVk/hfVk.h>

/* ------ Semaphore Info Classes ------ */
class hf::Vk::Info::Semaphore {
// attributes
public:
	hf::Vk::Device* pDevice;

	Semaphore& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}
};

class hf::Vk::CreateInfo::Semaphore : public hf::Vk::Info::Semaphore {
// methods
public:
	Semaphore() {}
	Semaphore(Info::Semaphore info) : Info::Semaphore(info) {}
	operator vk::SemaphoreCreateInfo();

};

/* ------ Semaphore Core Class ------ */
class hf::Vk::Semaphore {
// attributes
private:
	vk::Semaphore _semaphore;
	Info::Semaphore* _description = nullptr;

// methods
private:
	void init(CreateInfo::Semaphore& createInfo);
	void deinit();
	//bool initCompleted() { return _semaphore != VK_NULL_HANDLE; }
	void move(Semaphore& withdraw);

public:
	/* Ctors 'n Dtor */
	Semaphore() {}
	Semaphore(CreateInfo::Semaphore& createInfo) { init(createInfo); }
	Semaphore(Semaphore&& init) { move(init); }
	~Semaphore() { deinit(); }

	/* Getters */
	vk::Semaphore& getVkSemaphore() { return _semaphore; } 

	/* Core Methods */
	void create(CreateInfo::Semaphore& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

	/* Operators */
	operator vk::Semaphore() { return _semaphore; }
	Semaphore& operator=(Semaphore& assign) { move(assign); return *this; }

};

#endif
