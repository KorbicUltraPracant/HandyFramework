/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Descriptor Manager, stores descriptor pool and redistributes their memory.
*/

#ifndef HFVKDESCRIPTORSMANAGER_H
#define HFVKDESCRIPTORSMANAGER_H

#include <hfVk/DesciptorsPool.h>
#include <hfVk/Device.h>
#include <vector>

/* ----------- Info Class ----------- */
class hf::Vk::Info::DescriptorManager {
// attributes
public:
	hf::Vk::Device* pDevice;

	DescriptorManager& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

};

/* ----------- Create Info Class ----------- */
class hf::Vk::CreateInfo::DescriptorManager : public hf::Vk::Info::DescriptorManager {
// attributes
public:

// methods
public:

};

/* ------- Core Class ------- */
class hf::Vk::DescriptorManager {
// attributes
private:
	hf::Vk::Info::DescriptorManager* _description = nullptr; // create info description
	std::vector<DescriptorPool*> _pools; // allowing possibly unlimited allocation of sets
	uint32_t _maxAllocationCountperPool = 1;

// methods
private:
	void init(CreateInfo::DescriptorManager& createInfo);
	void deinit();

public:
	/* Ctors 'n Dtor */
	DescriptorManager() {}
	DescriptorManager(CreateInfo::DescriptorManager& createInfo) { init(createInfo); }
	~DescriptorManager() { deinit(); }

	/* Core Methods */
	void setUp(CreateInfo::DescriptorManager& createInfo) { init(createInfo); }
	void shutdown() { deinit(); }

	// desciptors allocation
	bool allocateDescriptorMemory(DescriptorSet& descriptorSet);
	bool releaseDescriptorMemory(DescriptorSet& descriptorSet);

	/* Getters */
	hf::Vk::Device& getDevice() { return *_description->pDevice; }

	/* Setters */
	// will influence only descriptor pools created after this call!
	void setCurrentAllocationCountperPool(uint32_t allocationCount) { _maxAllocationCountperPool = allocationCount; }

	/* Status Checkers */

};

#endif
