/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Pipeline Cache object
*/

#ifndef HFVKPIPECACHE_H
#define HFVKPIPECACHE_H

#include <hfVk/hfVk.h>
#include <hfVk/Device.h>
#include <vector>
#include <string>

/* ------- Info Class ------- */
class hf::Vk::Info::PipelineCache {
// attributes
public:
	hf::Vk::Device* pDevice;

	PipelineCache& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}
};


/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::PipelineCache : public hf::Vk::Info::PipelineCache {
// attributes
private:
	std::vector<uint8_t> _data;

public:
	std::string pathToData;

// methods
public:
	operator vk::PipelineCacheCreateInfo();

	PipelineCache& setPathToData(std::string pathToData_) {
		pathToData = pathToData_;
		return *this;
	}
};

/* ------- Core Class ------- */
class hf::Vk::PipelineCache {
// attributes
private:
	Info::PipelineCache* _description = nullptr;
	vk::PipelineCache _cache;

// methods
private:
	void init(CreateInfo::PipelineCache& createInfo);
	void deinit();
	//bool initCompleted() { return _cache != VK_NULL_HANDLE; }

public:
	/* Ctors 'n Dtor */
	PipelineCache() {}
	PipelineCache(CreateInfo::PipelineCache& createInfo) { init(createInfo); }
	~PipelineCache() { deinit(); }

	/* Operators */
	operator vk::PipelineCache() { return _cache; }

	/* Getters */
	vk::PipelineCache& getVkHandle() { return _cache; }
	
	/* Core Methods */
	void create(CreateInfo::PipelineCache& createInfo) { init(createInfo); }
	void destroy() { deinit(); }
	bool store(std::string path);


	/* Status Checkers */
	
};

#endif
