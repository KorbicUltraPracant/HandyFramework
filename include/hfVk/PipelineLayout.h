/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Pipeline Layout object
*/

#ifndef HFVKPIPELAYOUT_H
#define HFVKPIPELAYOUT_H

#include <hfVk/DescriptorSet.inl>
#include <hfVk/hfVk.h>
#include <vector>

/* ------- Info Class ------- */
class hf::Vk::Info::PipelineLayout {
// attributes
public:
	hf::Vk::Device* pDevice;

// virtual dtor
public:
	virtual ~PipelineLayout() = default;

	PipelineLayout& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}
};

class hf::Vk::FurtherInfo::PipelineLayout : public hf::Vk::Info::PipelineLayout {
// attributes
public:
	std::vector<hf::Vk::DescriptorSetLayout*> pDescriptorSetLayouts; // extract layouts and other info about descriptor
	std::vector<vk::PushConstantRange> pushConstants;

};


/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::PipelineLayout : public hf::Vk::FurtherInfo::PipelineLayout {
// attributes
private:
	std::vector<vk::DescriptorSetLayout> descriptorLayouts;

// methods
public:
	operator vk::PipelineLayoutCreateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::PipelineLayout {
// attributes
private:
	hf::Vk::Info::PipelineLayout* _description = nullptr;
	vk::PipelineLayout _layout;


// methods
private:	
	void init(CreateInfo::PipelineLayout& createInfo);
	void deinit();
	void move(PipelineLayout& withdraw);

public:
	/* Ctors 'n Dtor */
	PipelineLayout() {}
	PipelineLayout(CreateInfo::PipelineLayout& createInfo) { init(createInfo); }
	PipelineLayout(PipelineLayout&& init) { move(init); }
	~PipelineLayout() { deinit(); }

	/* Getters */
	vk::PipelineLayout& getVkHandle() { return _layout; }

	/* Operators */
	PipelineLayout& operator=(PipelineLayout& assign) { move(assign); return *this; }
	operator vk::PipelineLayout() { return _layout; }

	/* Core */
	void describe(CreateInfo::PipelineLayout& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

};

#endif

