/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Wrapper over Device's Queues
*/

#ifndef DQUEUE_H
#define DQUEUE_H

#include <hfVk/hfVk.h>
#include <hfAx/UsageRegister.h>
#include <hfVk/Synchronization.h>
#include <vector>

/* ------- Info Class ------- */
class hf::Vk::Info::RequestQueues {
// attributes
public:
	uint32_t qFamilyIdx;
	uint32_t count;
	VkQueueFlagBits flags;
	std::vector<float> queuePriorities;

// methods
public:
	operator vk::DeviceQueueCreateInfo();

	RequestQueues& setQFamilyIdx(uint32_t qFamilyIdx_) {
		qFamilyIdx = qFamilyIdx_;
		return *this;
	}

	RequestQueues& setCount(uint32_t count_) {
		count = count_;
		return *this;
	}

	RequestQueues& setFlags(VkQueueFlagBits flags_) {
		flags = flags_;
		return *this;
	}

	RequestQueues& setQueuePriorities(std::vector<float> queuePriorities_) {
		queuePriorities = queuePriorities_;
		return *this;
	}
};

class hf::Vk::Info::Queue {
// attributes
public:
	hf::Vk::Device* pDevice;
	uint32_t queueFamilyIndex;
	uint32_t allocationIndex;

	Queue& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	Queue& setQueueFamilyIndex(uint32_t queueFamilyIndex_) {
		queueFamilyIndex = queueFamilyIndex_;
		return *this;
	}

	Queue& setAllocationIndex(uint32_t allocationIndex_) {
		allocationIndex = allocationIndex_;
		return *this;
	}
};

class hf::Vk::CreateInfo::Queue : public hf::Vk::Info::Queue {
// attributes
public:

};

class hf::Vk::Queue {
// attributes
private:
	vk::Queue _queue;
	Info::Queue* _description = nullptr;

// methods
private:
	void init(CreateInfo::Queue& createInfo);
	void deinit();
	//bool initCompleted() { return _queue != VK_NULL_HANDLE && _description != nullptr; }

public:
	/* Ctors 'n Dtor */
	Queue() {}
	Queue(CreateInfo::Queue& createInfo) { init(createInfo); };
	~Queue() { deinit(); }

	/* Getters */
	vk::Queue getVkHandle() { return _queue; }
	Info::Queue& getDescription() { return *_description; }

	/* Core Methods */
	void obtain(CreateInfo::Queue& createInfo) { init(createInfo); }
	void release() { deinit(); }

	/* Operators */
	operator vk::Queue() { return _queue; }

	/* Status Checkers */
	

};

class hf::Vk::Info::QueueFamily {
// attributes
public:
	std::vector<hf::Vk::Queue*> availableQueues;
	int lastUsedQueue = -1;

// methods
public:

};

#endif