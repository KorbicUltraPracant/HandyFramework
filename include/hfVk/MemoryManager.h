/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Memory Manager class
*/

#ifndef HFVKMEMORYMANAGER_H
#define HFVKMEMORYMANAGER_H

#include <hfVk/MemoryPool.h>
#include <hfVk/Device.h>
#include <unordered_map>

/* ------- Info Class ------- */
class hf::Vk::Info::MemoryManager {
// attributes
public:
	hf::Vk::Device* pDevice = nullptr; // ptr to device

	MemoryManager& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}
};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::MemoryManager : public hf::Vk::Info::MemoryManager {

};

/* ------- Execution Class ------- */
class hf::Vk::MemoryManager {
// attributes
private:
	hf::Vk::Info::MemoryManager* _description = nullptr;
	std::unordered_map<uint32_t, std::vector<hf::Vk::MemoryPool*>> _poolStorage; // uint32_t MemoryTypeIdx

public:

// methods
private:
	void init(CreateInfo::MemoryManager& createInfo);
	void deinit();
	//bool initCompleted() { return _description->pDevice != nullptr; }

	/* Memory Pool Events */
	void allocatePool(hf::Vk::CreateInfo::MemoryPool& createInfo, hf::Vk::MemoryPool& pool);
	void releasePool(hf::Vk::MemoryPool& pool);
	bool provideMemoryBlock(hf::Vk::Info::MemoryBlock& resBlock, hf::Vk::MemoryPool& pool, vk::DeviceSize requieredSize);

	/* Memory Pool Storage */
	hf::Vk::MemoryPool* insertPool(hf::Vk::CreateInfo::MemoryPool& createInfo);
	void cleanUpStorage();   
	// Find suitable pool for storing resource
	hf::Vk::MemoryPool* findSuitableMemoryPool(hf::Vk::Info::MemoryRequierements& reqs);
	

public:
	/* Ctors 'n Dtor */
	MemoryManager() {}
	MemoryManager(CreateInfo::MemoryManager& createInfo) { init(createInfo); }
	~MemoryManager() { deinit(); }

	// Auxiliary
	int typeBitsToIndex(uint32_t typeBits, vk::MemoryPropertyFlags requirements);

	/* Core Methods */
	// Init 
	void setUp(CreateInfo::MemoryManager& createInfo) { init(createInfo); } //return _description = new Info::MemoryManager(createInfo)
	void shutdown() { deinit(); }
	
	// alocate memory to pool
	vk::DeviceMemory allocateMemory(vk::DeviceSize size, uint32_t memTypeIdx, vk::MemoryPropertyFlags memoryFlags);
	void releaseMemory(vk::DeviceMemory& memory);

	// assign or release memory from pool
	bool allocateMemoryBlock(hf::Vk::Info::MemoryBlock& resBlock, hf::Vk::Info::MemoryRequierements memReqs);
	void restoreMemoryBlock(hf::Vk::Info::MemoryBlock& memory); // memory pool ptr is part of memory block

	// assign memory block to object
	void assignMemoryToImage(Image& image, uint32_t allocationSize = 0); // preAllocationCount -- kolikrat se ma baseRscSize naalokovat 
	void assignMemoryToBuffer(Buffer& buffer, uint32_t allocationSize = 0);
	inline void releaseImageMemory(Image& image);
	inline void releaseBufferMemory(Buffer& buffer);
	
	// store data from host coherent memory into device local memory(GPU) -> copy between proper memory pools
	bool storeIntoGPU(Info::MemoryBlock& block); // block will be modified to point into GPU memory, host coherent mem will be freed... 

	// assign host|device shared memory to resource 
	void setSubData(void* data, vk::DeviceSize size, vk::DeviceSize offset = 0);
	void* map(Info::MemoryBlock& memory);
	void unmap(Info::MemoryBlock& memory);

	// auxiliary
	//bool findProperFormatForBuffer(Info::Buffer& buffer); // vk::PhysicalDevice psDevice, VkFormat reqFormat
	//bool findProperFormatForImage(Info::Image& img);

	// preallocate memory pool 
	void preAllocateMemory(Info::MemoryProperties info);

	// Truncate, mempools which dont store any data
	void truncatePools();

	/* Getters */
	hf::Vk::Device& getDevice() { return *_description->pDevice; }

	/* Status Checkers */
	
};

#endif