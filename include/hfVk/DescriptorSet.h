/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Descriptor Set wrapper
*/

#ifndef HFVKDESCRIPTOR_H
#define HFVKDESCRIPTOR_H

#include <hfVk/DescriptorManager.h>
#include <hfVk/DescriptorSetLayout.h>
#include <hfVk/DescriptorSetAuxils.h>
#include <vector>
#include <unordered_map>

/* ----------- Info Class ----------- */
class hf::Vk::Info::DescriptorSet {
// attributes
public:
	hf::Vk::DescriptorManager* pManager;

	DescriptorSet& setManagerPtr(hf::Vk::DescriptorManager * pManager_) {
		pManager = pManager_;
		return *this;
	}
};

/* ----------- Create Info Class ----------- */
class hf::Vk::CreateInfo::DescriptorSet : public hf::Vk::Info::DescriptorSet {
// attributes
public:
	CreateInfo::DescriptorSetLayout mainLayoutInfo;

};

/* ------- Core Class ------- */
class hf::Vk::DescriptorSet {
// friend class 
	friend class hf::Vk::DescriptorManager;

// attributes
private:
	Info::DescriptorSet* _description = nullptr;
	std::unordered_map<vk::DescriptorType, uint32_t> _layoutMemReqs;

	DescriptorPool* _allocatedFrom;
	DescriptorSetLayout _mainLayout; // layout which can be allocated from pool in memory manager
	hf::Ax::PtrVector<DescriptorSetLayout> _layoutViews; // contains layouts which are parents of main layout(ommiting some descriptor from main layout...)
	vk::DescriptorSet _memory; // ptrs to GPU memory where layouts are stored -> is initialized after passing them into desciptors manager

// methods
private:
	void init(CreateInfo::DescriptorSet& createInfo);
	void deinit();
	void move(DescriptorSet& withdraw);

public:
	/* Ctors 'n Dtor */
	DescriptorSet() {}
	DescriptorSet(CreateInfo::DescriptorSet& createInfo) { init(createInfo); }
	DescriptorSet(DescriptorSet&& init) { move(init); }
	~DescriptorSet() { deinit(); }

	/* Getters */
	DescriptorSetLayout& getLayout() { return _mainLayout; }
	DescriptorSetLayout& getLayoutView(uint32_t idx) { if (idx < _layoutViews.size()) return *_layoutViews[idx]; }
	vk::DescriptorSet& getVkHandle() { return _memory; }
	hf::Ax::PtrVector<DescriptorSetLayout>& getLayoutViews() { return _layoutViews; }

	/* Operators */
	DescriptorSet& operator=(DescriptorSet& assign) { move(assign); return *this; }

	/* Core Methods */
	void create(CreateInfo::DescriptorSet& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

	// update descriptor set data
	inline void update(Info::UpdateDescriptorSets info);
	void addLayoutView(CreateInfo::DescriptorSetLayout& layoutInfo) { _layoutViews.push_back(UPTR(DescriptorSetLayout, layoutInfo)); }

	/* Operators */
	operator vk::DescriptorSet() { return _memory; }

	/* Status Checkers */

};
#endif