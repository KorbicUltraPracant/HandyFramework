﻿/**
* @author Radek Blahos
* @project Handy Framework's Vulkan Library 
*
* @brief HFVK Forward declarations
*/

#ifndef HFVK_H
#define HFVK_H

/*
*
*
# HFVK's Basic Object structure
	If you know or will read something about Vulkan you'll find out that vulkan has several
	API defined object which most of them have their counterpart inside HFVK implementation.
	Some object, these which requires allocating resources from pools, are merged together with
	them into single HFVK object which handles their memory management.
	All class list you can read below with brief description in form of Doxygen commentary.
	
	Every HFVK object has 3 types of classes defining objects.
	Core,& createInfo, Info. Info class is inherited by& createInfo and is also stored 
	as private _description attribute inside Core class. Why so? because some attributes needs to be
	in izomorf relationship between& createInfo and Core class. In Core class their mostly representing 
	static read-only status where user can obtain runtime info about object depending& createInfo input.
	Its more programming friendly for me as library writer. And object conversions are making my life easier here to.
*/

#include <vulkan/vulkan.hpp> // vulkan API
#include <cstdint> // base types (uint32_t eg.)
#include <hfVk/UsefulMacros.h>

namespace hf {
	namespace Vk {
		// wrapper classes
		class Instance;
		class Device;
		class Queue;
		class CommandProcessor;
		class CommandBuffer;
		class CommandPool;
		class Pipeline;
		class GraphicPipeline;
		class ComputePipeline;
		class PipelineLayout;
		class PipelineCache;
		class DescriptorManager;
		class DescriptorSet;
		class DescriptorSetLayout;
		class DescriptorPool;
		class RenderPass;
		class MemoryManager;
		class MemoryPool;
		class Buffer;
		class BufferView;
		class ImageBase;
		class Image;
		class SwapchainImage;
		class ImageView;
		class Swapchain;
		class Framebuffer;
		class Semaphore;
		class Fence;
		class Event;
		class ShaderModule;
		class Sampler;
		
		namespace CreateInfo {
			class Core; // abstract class
			class Instance;
			class Device;
			class Queue;
			class CommandProcessor;
			class CommandBuffer;
			class CommandPool;
			class Pipeline;
			class GraphicPipeline;
			class ComputePipeline;
			class PipelineLayout;
			class PipelineCache;
			struct GraphicPipeDBEntry;
			struct ComputePipeDBEntry;
			class DescriptorSet;
			class DescriptorSetLayout;
			class DescriptorPool;
			class DescriptorManager;
			class RenderPass;
			class MemoryManager;
			class MemoryPool;
			class MemoryPoolStorage;
			class Buffer;
			class Image;
			class ImageView;
			class Framebuffer;
			class Swapchain;
			class Event;
			class Semaphore;
			class Fence;
			class ShaderModule;
			class Sampler;

		}

		namespace FurtherInfo {
			class Instance;
			class Device;
			class ComputePipeline;
			class GraphicPipeline;
			class DescriptorSetLayout;
			class RenderPass;
			class Framebuffer;
			class PipelineLayout;
			class Sampler;

		}

		namespace Info {
			class Instance;
			class Device;
			class Queue;
			class QueueFamily;
			class MemoryBlock;
			class DescriptorManager;
			class DescriptorPool;
			class DescriptorSet;
			class DescriptorSetLayout;
			class CommandBuffer;
			class CommandPool;
			class CommandProcessor;
			class Pipeline;
			class ComputePipeline;
			class GraphicPipeline;
			class PipelineCache;
			class PipelineLayout;
			class Buffer;
			class ImageBase;
			class Image;
			class SwapchainImage;
			class ImageView;
			class RenderPass;
			class Framebuffer;
			class Swapchain;
			class RequestQueues; // used as Info for HFVKQueue
			class MemoryPool;
			class MemoryManager;
			class MemoryRequierements;
			class MemoryProperties;
			class Semaphore;
			class Event;
			class Fence;
			class ShaderModule;
			class Sampler;

			// class inhering and modifiing original vk structures
			class WriteDescriptorSet;

			// methods info
			class PostCommandBuffers;
			class PostSingleBuffer;
			class UpdateDescriptorSets;
			class PresentImage;

			// Commands 
			class BeginRenderPass;
			class PushConstants;
			class BindDescriptorSets;
			class BindVertexBuffers;
			class BindIndexBuffer;
			class Draw;
			class DrawIndexed;
			class DrawIndexedIndirect;
			class SetViewport;
			class SetScissor;

			// vk flags shortcuts

			namespace PipelineSetUp {
				class ShaderStage;
				class VertexInputDescription;
				class InputAssemblyState;
				class TessellationState;
				class ViewportState;
				class RasterizationState;
				class MultisampleState;
				class DepthStencilState;
				class StencilOpState;
				class ColorBlendAttachmentState;
				class ColorBlendState;
				using DynamicState = vk::DynamicState; 
			}
		}

		// Utility 
		namespace Utility {
			class SyncPrimitive;
			class GPUPropertiesViewer;
		}

		// Auxilliary classes
		namespace Auxiliary {
			class Instance;
			class Device;
			class Swapchain;
		}
	}
}

#endif