/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Instance wrapper
*/

#ifndef HFVK_INSTANCE_H
#define HFVK_INSTANCE_H

#include <hfVk/hfVk.h>
#include <string>
#include <vector>

/* ----------- Info Class ----------- */
class hf::Vk::Info::Instance {
/* attributes */
public:
	bool useValidationLayers = false;
	bool abundantInfo = false;

// virtual dtor
public:
	virtual ~Instance() = default;

	Instance& setUseValidationLayers(bool useValidationLayers_) {
		useValidationLayers = useValidationLayers_;
		return *this;
	}

	Instance& setAbundantInfo(bool abundantInfo_) {
		abundantInfo = abundantInfo_;
		return *this;
	}

};

class hf::Vk::FurtherInfo::Instance : public hf::Vk::Info::Instance {
/* attributes */
public:
	vk::ApplicationInfo appInfo;
	std::vector<char*> enabledExtensions;
	std::vector<char*> enabledLayers;

	Instance& setAppInfo(vk::ApplicationInfo appInfo_) {
		appInfo = appInfo_;
		return *this;
	}
};

/* ----------- Create Info Class ----------- */
class hf::Vk::CreateInfo::Instance : public hf::Vk::FurtherInfo::Instance {
/* attributes */

/* methods */
public:
	/* Operators */
	inline operator vk::ApplicationInfo();
	operator vk::InstanceCreateInfo();

};

/* ------- Auxiliary Class ------- */
class hf::Vk::Auxiliary::Instance {
// methods
public:
	// makes version from specified numbers
	static inline uint32_t makeVersion(uint16_t major, uint16_t minor, uint16_t patch);
	static inline std::string getVersion(uint32_t encodedIn);

	// enumerate all available PhysicalDevices
	static void enumeratePhysicalDevices(vk::Instance instance, std::vector<vk::PhysicalDevice>& devices);
	// enumerate extensions
	static void enumerateAvailableExtensions(std::vector<vk::ExtensionProperties>& props);
	// enumerate layers
	static void enumerateAvailableLayers(std::vector<vk::LayerProperties>& props);
	// Query for Support
	static bool extensionSupported(std::string name);
	static void extensionsSupported(std::vector<char*>& names, std::vector<bool>& resVec);
	static bool layerSupported(std::string name);
	static void layersSupported(std::vector<char*>& names, std::vector<bool>& resVec);
};

/* ------- Core Class ------- */
class hf::Vk::Instance : public hf::Vk::Auxiliary::Instance {
// attributes
private:
	hf::Vk::Info::Instance* _description = nullptr;
	vk::Instance _instance;
	VkDebugUtilsMessengerEXT _debugCb;

// methods
private:
	void init(CreateInfo::Instance& createInfo);
	void deinit();
	void move(Instance& withdraw);

	/* Debug Callback Handling 
	static VKAPI_ATTR vk::Bool32 VKAPI_CALL debugCallback(
		VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType,
		const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData);
	*/

public:
	/* Ctors 'n Dtor */
	Instance() { ; }
	Instance(CreateInfo::Instance& createInfo) { init(createInfo); }
	Instance(Instance&& init) { move(init); }
	Instance(vk::Instance& instance); 
	~Instance() { deinit(); }

	/* Getters */
	vk::Instance& getVkHandle() { return _instance; }
	Info::Instance& getDescription() { return *_description; }
	bool useValidationLayers() { return _description->useValidationLayers; }
	bool abundantInfo() { return _description->abundantInfo; }

	/* Operators */
	Instance& operator=(Instance& assign) { move(assign); return *this; }
	operator vk::Instance() { return _instance; }

	/* Core Methods */
	// Initialization on demand
	void create(CreateInfo::Instance& createInfo) { init(createInfo); }
	// Objects dtor on demand
	void destroy() { deinit(); }

	/* Capatibility checkers */
	// enumerate Psdevices
	void enumeratePhysicalDevices(std::vector<vk::PhysicalDevice>& devices) { hf::Vk::Auxiliary::Instance::enumeratePhysicalDevices(_instance, devices); }
	// enumerate extensions
	void enumerateAvailableExtensions(std::vector<vk::ExtensionProperties>& props) { hf::Vk::Auxiliary::Instance::enumerateAvailableExtensions(props); }
	// enumerate layers
	void enumerateAvailableLayers(std::vector<vk::LayerProperties>& props) { hf::Vk::Auxiliary::Instance::enumerateAvailableLayers(props); }
	// Query for Support
	bool extensionSupported(std::string name) { return hf::Vk::Auxiliary::Instance::extensionSupported(name); }
	// v resVec je ulozena bool hodnota jestli je extension podporovano nebo ne
	void extensionsSupported(std::vector<char*>& names, std::vector<bool>& resVec) { hf::Vk::Auxiliary::Instance::extensionsSupported(names, resVec); }
	bool layerSupported(std::string name) { return hf::Vk::Auxiliary::Instance::layerSupported(name); }
	void layersSupported(std::vector<char*>& names, std::vector<bool>& resVec) { hf::Vk::Auxiliary::Instance::layersSupported(names, resVec); }

	/* Status Checkers */

};

#endif
