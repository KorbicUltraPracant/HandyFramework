/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Infos for Image's methods
*/

#ifndef HFVKIMAGEINFO_H
#define HFVKIMAGEINFO_H

#include <hfVk/Image.h>

class hf::Vk::Info::Image::MemoryBarrier {
public:
	vk::AccessFlags srcAccessMask; // to which action to wait for
	vk::AccessFlags dstAccessMask; // which action waits 
	vk::ImageLayout oldLayout;
	vk::ImageLayout newLayout;
	uint32_t srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	uint32_t dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	vk::Image image;
	vk::ImageSubresourceRange subresourceRange = { vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1 };

public:
	operator vk::ImageMemoryBarrier();

	MemoryBarrier& setSrcAccessMask(vk::AccessFlags srcAccessMask_) {
		srcAccessMask = srcAccessMask_;
		return *this;
	}

	MemoryBarrier& setDstAccessMask(vk::AccessFlags dstAccessMask_) {
		dstAccessMask = dstAccessMask_;
		return *this;
	}

	MemoryBarrier& setOldLayout(vk::ImageLayout oldLayout_) {
		oldLayout = oldLayout_;
		return *this;
	}

	MemoryBarrier& setNewLayout(vk::ImageLayout newLayout_) {
		newLayout = newLayout_;
		return *this;
	}

	MemoryBarrier& setSrcQueueFamilyIndex(uint32_t srcQueueFamilyIndex_) {
		srcQueueFamilyIndex = srcQueueFamilyIndex_;
		return *this;
	}

	MemoryBarrier& setDstQueueFamilyIndex(uint32_t dstQueueFamilyIndex_) {
		dstQueueFamilyIndex = dstQueueFamilyIndex_;
		return *this;
	}

	MemoryBarrier& setImage(vk::Image image_) {
		image = image_;
		return *this;
	}

	MemoryBarrier& setSubresourceRange(vk::ImageSubresourceRange subresourceRange_) {
		subresourceRange = subresourceRange_;
		return *this;
	}

};

class hf::Vk::Info::Image::ChangeLayout : public hf::Vk::Info::Image::MemoryBarrier {
public:
// ctor enum
enum TransferOp { COLOR_ATTACHMENT, IMG_INIT, DEPTH_TEST, LOADING_TEX, USE_TEX, PRE_CLEAR_IMG, POST_CLEAR_IMG, PRE_CLEAR_COLATT, POST_CLEAR_COLATT, PRE_CLEAR_DEPTH_A, POST_CLEAR_DEPTH_A };

public:
	vk::PipelineStageFlags producentStage;
	vk::PipelineStageFlags consumentStage;

public:
	ChangeLayout() {}
	ChangeLayout(TransferOp operation);

	/* 
		Tady pri inicializovani bacha ze ty ostatni atributy vraci Memory barrier struct... mozna by to bylo dobre nejak overridnout !!, a nebude to jenom tady
		urcite i u jinych struktur, takze dodelat!!!
	*/
	ChangeLayout& setProducentStage(vk::PipelineStageFlags waitToStage_) {
		producentStage = waitToStage_;
		return *this;
	}

	ChangeLayout& setConsumentStage(vk::PipelineStageFlags completeBeforeStage_) {
		consumentStage = completeBeforeStage_;
		return *this;
	}
};

#endif
