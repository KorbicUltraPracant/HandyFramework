/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Class wrapping Vulkan Buffer object. 
*/

#ifndef HFVKBUFFER_H
#define HFVKBUFFER_H

#include <hfVk/MemoryManager.h>
#include <hfVk/MemoryBlock.h>
#include <hfVk/hfVk.h>
#include <vector>

/* ------- Info Class ------- */
class hf::Vk::Info::Buffer {
// attributes
public:
	hf::Vk::MemoryManager* pManager = nullptr; // zpristupnuje memory operace a zaroven urcuje odkud se vezne pamet
	vk::DeviceSize size; // buffer's size
	vk::BufferUsageFlags usage;
	vk::MemoryPropertyFlags propertyFlags;

	Buffer& setManagerPtr(hf::Vk::MemoryManager * pManager_) {
		pManager = pManager_;
		return *this;
	}
	Buffer& setSize(vk::DeviceSize size_) {
		size = size_;
		return *this;
	}
	Buffer& setUsage(vk::BufferUsageFlags usage_) {
		usage = usage_;
		return *this;
	}
	Buffer& setPropertyFlags(vk::MemoryPropertyFlags propertyFlags_) {
		propertyFlags = propertyFlags_;
		return *this;
	}
};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::Buffer : public hf::Vk::Info::Buffer {
public:
	enum Usage { GENERAL, VERTEXBUFFER, UNIFORMBUFFER, INDEXBUFFER, STAGGINGBUFFER, SHADERSTORAGE, INDEXEDINDIRECTDRAW };

// attributes
public:
	bool assignMemory = true;
	vk::DeviceSize allocationSize = 0; // if 0 Info::size memory will be allocated else defines how much memory will be allocated from pool

// methods
public:
	// Ctor
	Buffer(Info::Buffer info) : Info::Buffer(std::move(info)) {}
	Buffer(Usage bufferUsage = Usage::GENERAL, vk::MemoryPropertyFlagBits locationFlags = vk::MemoryPropertyFlagBits::eDeviceLocal);
	
	operator vk::BufferCreateInfo();

	Buffer& setAssignMemory(bool assignMemory_) {
		assignMemory = assignMemory_;
		return *this;
	}
	Buffer& setAllocationSize(vk::DeviceSize allocationSize_) {
		allocationSize = allocationSize_;
		return *this;
	}

};

/* ------- Core Class ------- */
class hf::Vk::Buffer {
//friend class
	friend class MemoryManager;

// attributes
private:
	Info::MemoryBlock _memoryInfo; // device memory block info -> info about assigned memory from large pool
	Info::Buffer* _description = nullptr;
	vk::Buffer _buffer;

// methods
private:
	void init(CreateInfo::Buffer& createInfo);
	void deinit();
	void move(Buffer& withdraw);
	
public:
	/* Ctors 'n Dtor */
	Buffer() {}
	Buffer(CreateInfo::Buffer& createInfo) { init(createInfo); }
	Buffer(Buffer&& init) { move(init); } // same as move ctor
	~Buffer() { deinit(); }

	/* Getters */
	vk::Buffer& getVkHandle() { return _buffer; }
	Info::MemoryBlock& getMemoryInfo() { return _memoryInfo; }
	Info::Buffer& getDescription() { return *_description; }
	Device& getDevice() { return _description->pManager->getDevice(); }

	/* Operators */
	operator vk::Buffer() { return _buffer; }
	Buffer& operator=(Buffer& copy) { move(copy); return *this; } // "move" -> shallow assign

	/* Core Methods */
	// object lifetime
	void create(CreateInfo::Buffer& createInfo) { init(createInfo); }
	void discard() { deinit(); }

	// when using this, buffer must have flag DeviceLocal
	void stashOnGPU(std::vector<uint8_t>& data, CommandBuffer& keepCommands, CommandProcessor& proccessCommands, uint32_t dstBufferOffset = 0);
	void stashOnGPU(void* data, uint32_t dataLenInBytes, CommandBuffer& keepCommands, CommandProcessor& proccessCommands, uint32_t dstBufferOffset = 0);

	/* 
		post commands to copy memory from one HFVKBuffer to another, source's data keeps untouched,
		its user responsibility to begin command buffer before posting it to this method and after this method's call cmd buffer must be finished and posted by user...
	*/
	void copyMemory(Buffer& data, CommandBuffer& keepCommands, vk::BufferCopy* copyInfo = nullptr);

	// memory maping VRAM -> RAM
	void* map();
	void unmap();

	// getting buffer's memory requierements 
	void getMemoryProperties(Info::MemoryProperties& result);
	void assignMemory();

	// changing layout

};

#endif
