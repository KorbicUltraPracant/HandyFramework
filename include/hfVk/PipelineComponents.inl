/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Pipeline componets used inside CreateInfo::GraphicPipeline ...
*/

#ifndef HFVKPIPECOMPONENTS_INL
#define HFVKPIPECOMPONENTS_INL

#include <hfVk/PipelineComponents.h>

using namespace hf::Vk;

inline Info::PipelineSetUp::VertexInputDescription::operator vk::VertexInputBindingDescription() {
	return vk::VertexInputBindingDescription {
		location,
		stride,
		inputRate
	};
}

inline Info::PipelineSetUp::VertexInputDescription::operator vk::VertexInputAttributeDescription() {
	return vk::VertexInputAttributeDescription{
		location,
		location,
		format,
		offset
	};
}

inline Info::PipelineSetUp::ViewportState::operator vk::PipelineViewportStateCreateInfo() {
	return vk::PipelineViewportStateCreateInfo {
		vk::PipelineViewportStateCreateFlags(0),
		(uint32_t) viewports.size(),
		viewports.data(),
		(uint32_t)scissors.size(),
		scissors.data()
	};
}

#endif