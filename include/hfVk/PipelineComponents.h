/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Pipeline Componets used inside CreateInfo::PipelineLayout
*/

#ifndef HFVKPIPECOMPONENTS_H
#define HFVKPIPECOMPONENTS_H

#include <hfVk/hfVk.h>
#include <hfVk/ShaderModule.h>
#include <hfVk/Device.h>
#include <vector>
#include <string>

class hf::Vk::Info::PipelineSetUp::ShaderStage {
// attributes
public:
	vk::ShaderStageFlagBits stage;
	hf::Vk::ShaderModule* module = nullptr;
	std::string entryPoint = "main";
	vk::SpecializationInfo* pConstants = nullptr; // shader constants, dynamic allocation only when needed

// methods
public:
	~ShaderStage() { if (pConstants != nullptr) delete pConstants; }
	operator vk::PipelineShaderStageCreateInfo();

	ShaderStage& setStage(vk::ShaderStageFlagBits stage_) {
		stage = stage_;
		return *this;
	}
	ShaderStage& setModule(hf::Vk::ShaderModule * module_) {
		module = module_;
		return *this;
	}
	ShaderStage& setEntryPoint(std::string entryPoint_) {
		entryPoint = entryPoint_;
		return *this;
	}
	ShaderStage& setPConstants(vk::SpecializationInfo * pConstants_) {
		pConstants = pConstants_;
		return *this;
	}

};

class hf::Vk::Info::PipelineSetUp::VertexInputDescription {
// attributes
public:
	//uint32_t binding; 
	uint32_t location = -1; // [binding] - unassigned
	uint32_t stride; // size of single vertex attrib in bytes
	vk::VertexInputRate inputRate = vk::VertexInputRate(VK_VERTEX_INPUT_RATE_VERTEX);
	vk::Format format = vk::Format(VK_FORMAT_R32G32B32_SFLOAT);
	uint32_t offset = 0;

// methods
public:
	inline operator vk::VertexInputBindingDescription();
	inline operator vk::VertexInputAttributeDescription();

	VertexInputDescription& setLocation(uint32_t location_) {
		location = location_;
		return *this;
	}
	VertexInputDescription& setStride(uint32_t stride_) {
		stride = stride_;
		return *this;
	}
	VertexInputDescription& setInputRate(vk::VertexInputRate inputRate_) {
		inputRate = inputRate_;
		return *this;
	}
	VertexInputDescription& setFormat(vk::Format format_) {
		format = format_;
		return *this;
	}
	VertexInputDescription& setOffset(uint32_t offset_) {
		offset = offset_;
		return *this;
	}

};

class hf::Vk::Info::PipelineSetUp::InputAssemblyState : public vk::PipelineInputAssemblyStateCreateInfo {
	// attributes
public:
	InputAssemblyState() : vk::PipelineInputAssemblyStateCreateInfo(vk::PipelineInputAssemblyStateCreateFlags(0), vk::PrimitiveTopology::eTriangleList, VK_FALSE) {}

};

class hf::Vk::Info::PipelineSetUp::ColorBlendAttachmentState 
	: public vk::PipelineColorBlendAttachmentState 
{
public:
	ColorBlendAttachmentState();

};

class hf::Vk::Info::PipelineSetUp::ColorBlendState {
// attributes
public:
	vk::Bool32 logicOpEnable = VK_FALSE;
	vk::LogicOp logicOp = vk::LogicOp::eCopy;
	std::vector<ColorBlendAttachmentState> attachments;
	std::array<float, 4> blendConstants = { { 0, 0, 0, 0 } };

// methods
public:
	operator vk::PipelineColorBlendStateCreateInfo();

	ColorBlendState& setLogicOpEnable(vk::Bool32 logicOpEnable_) {
		logicOpEnable = logicOpEnable_;
		return *this;
	}

	ColorBlendState& setLogicOp(vk::LogicOp logicOp_) {
		logicOp = logicOp_;
		return *this;
	}

	ColorBlendState& setBlendConstants(std::array<float, 4> blendConstants_) {
		blendConstants = blendConstants_;
		return *this;
	}

};

class hf::Vk::Info::PipelineSetUp::StencilOpState : public vk::StencilOpState {};

class hf::Vk::Info::PipelineSetUp::DepthStencilState : public vk::PipelineDepthStencilStateCreateInfo {};

class hf::Vk::Info::PipelineSetUp::MultisampleState : public vk::PipelineMultisampleStateCreateInfo {};

class hf::Vk::Info::PipelineSetUp::RasterizationState : public vk::PipelineRasterizationStateCreateInfo {
// methods
public:
	RasterizationState() { lineWidth = 1.0f; }

};

class hf::Vk::Info::PipelineSetUp::TessellationState : public vk::PipelineTessellationStateCreateInfo {};

class hf::Vk::Info::PipelineSetUp::ViewportState {
// attributes
public:
	std::vector<vk::Viewport> viewports;
	std::vector<vk::Rect2D> scissors;

// methods
public:
	inline operator vk::PipelineViewportStateCreateInfo();

};

#endif