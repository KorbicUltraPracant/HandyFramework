/**
* @author Radek Blahos
* @project HFVK Library
*
* @brief Sampler wrapper
*/

#ifndef HFVK_SAMPLER_H
#define HFVK_SAMPLER_H

#include <hfVk/hfVk.h>

/* ------- Info Class ------- */
class hf::Vk::Info::Sampler {
// attributes
public:
	hf::Vk::Device* pDevice;

	Sampler& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}
};

class hf::Vk::FurtherInfo::Sampler : public hf::Vk::Info::Sampler {
// attributes
public:
	vk::Filter magFilter = vk::Filter::eLinear;
	vk::Filter minFilter = vk::Filter::eLinear;
	vk::SamplerMipmapMode mipmapMode = vk::SamplerMipmapMode::eLinear;
	vk::SamplerAddressMode addressModeU = vk::SamplerAddressMode::eRepeat;
	vk::SamplerAddressMode addressModeV = vk::SamplerAddressMode::eRepeat;
	vk::SamplerAddressMode addressModeW = vk::SamplerAddressMode::eRepeat;
	float mipLodBias = 0;
	vk::Bool32 anisotropyEnable = VK_FALSE;
	float maxAnisotropy = 1;
	vk::Bool32 compareEnable = VK_FALSE; 
	vk::CompareOp compareOp = vk::CompareOp::eNever;
	float minLod = 0;
	float maxLod = 0;
	vk::BorderColor borderColor = vk::BorderColor::eFloatTransparentBlack;
	vk::Bool32 unnormalizedCoordinates = VK_FALSE;

};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::Sampler : public hf::Vk::FurtherInfo::Sampler {
// attributes
public:
// methods
public:
	operator vk::SamplerCreateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::Sampler {
// attributes
private:
	Info::Sampler* _description = nullptr;
	vk::Sampler _sampler;

// methods
private:
	void init(CreateInfo::Sampler& createInfo);
	void deinit();
	void move(Sampler& withdraw);

public:
	/* Ctors 'n Dtor */
	Sampler() {}
	Sampler(CreateInfo::Sampler& createInfo) { init(createInfo); }
	Sampler(Sampler&& init) { move(init); }
	~Sampler() { deinit(); }

	/* Getters */

	/* Core Methods */
	void create(CreateInfo::Sampler& createInfo) { init(createInfo); }
	void destroy() { deinit(); }

	/* Operators */
	Sampler& operator=(Sampler& assign) { move(assign); return *this; }
	operator vk::Sampler() { return _sampler; }
	vk::Sampler& getVkHandle() { return _sampler; }
	Info::Sampler& getDescription() { return *_description; }

};

#endif
