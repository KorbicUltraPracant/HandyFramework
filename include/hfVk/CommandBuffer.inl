/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief inlines for command buffer.h
*/

#ifndef HFVKCOMMANDBUFFERINFO_INL
#define HFVKCOMMANDBUFFERINFO_INL

#include <hfVk/CommandBufferInfo.h>
#include <hfVk/CommandBuffer.h>
#include <hfVk/RenderPass.h>
#include <hfVk/Framebuffer.h>
#include <hfVk/GraphicPipeline.h>
#include <hfVk/ComputePipeline.h>
#include <hfVk/Buffer.h>

using namespace hf::Vk;
using namespace std;

/* info inlines */
inline Info::BeginRenderPass::operator vk::RenderPassBeginInfo() {
	return vk::RenderPassBeginInfo(pRenderPass->getVkHandle(), pFramebuffer->getVkHandle(), renderArea, (uint32_t)clearColors.size(), clearColors.data());
}

/* Commands */
inline void CommandBuffer::beginRenderPass(Info::BeginRenderPass beginInfo) {
	vk::RenderPassBeginInfo info = std::move(beginInfo);
	_buffer.beginRenderPass(&info, beginInfo.subpassContents);
}

inline void CommandBuffer::bindPipeline(Pipeline& pipeline, vk::PipelineBindPoint bindPoint) {
	_buffer.bindPipeline(bindPoint, pipeline);
}

inline void CommandBuffer::bindGraphicPipeline(GraphicPipeline& pipeline) {
	_buffer.bindPipeline(vk::PipelineBindPoint(VK_PIPELINE_BIND_POINT_GRAPHICS), pipeline.getVkHandle());
}

inline void CommandBuffer::bindComputePipeline(ComputePipeline& pipeline) {
	_buffer.bindPipeline(vk::PipelineBindPoint(VK_PIPELINE_BIND_POINT_COMPUTE), pipeline);
}

inline void CommandBuffer::setViewport(Info::SetViewport info) {
	vk::Viewport tmp = info;
	_buffer.setViewport(0, 1, &tmp);
}

inline void CommandBuffer::setScissor(Info::SetScissor info) {
	vk::Rect2D tmp = info;
	_buffer.setScissor(0, 1, &tmp);
}

inline void CommandBuffer::bindVertexBuffers(Info::BindVertexBuffers info) {
	_buffer.bindVertexBuffers(info.firstBinding, (uint32_t)info.pBuffers.size(), info.pBuffers.data(), info.offsets.data());
}

inline void hf::Vk::CommandBuffer::bindIndexBuffer(Info::BindIndexBuffer info) {
	_buffer.bindIndexBuffer(*info.pBuffer, info.offset, info.indexType);
}

inline void CommandBuffer::bindIndexBuffer(vk::Buffer indexBuffer, vk::DeviceSize offset, vk::IndexType indexType) {
	_buffer.bindIndexBuffer(indexBuffer, offset, indexType);
}

inline void CommandBuffer::pushConstants(Info::PushConstants info) {
	_buffer.pushConstants(*info.pLayout, vk::ShaderStageFlags(info.usedInStages), info.offset, info.size, info.pValues);
}

inline void CommandBuffer::setEvent(vk::Event event, vk::PipelineStageFlags stageMask) {
	_buffer.setEvent(event, stageMask);
}

inline void CommandBuffer::resetEvent(vk::Event event, vk::PipelineStageFlags stageMask) {
	_buffer.resetEvent(event, stageMask);
}

inline void hf::Vk::CommandBuffer::drawIndexedIndirect(vk::Buffer buffer, vk::DeviceSize offset, uint32_t drawCount, uint32_t stride) {
	_buffer.drawIndexedIndirect(buffer, offset, drawCount, stride);
}

#endif
