/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Inlines for RenderPass
*/

#ifndef HFVKRENDERPASS_INL
#define HFVKRENDERPASS_INL

#include <hfVk/RenderPass.h>

/* -------CreateInfoClass ------- */
inline hf::Vk::CreateInfo::RenderPass::operator vk::RenderPassCreateInfo() {
	return {
		vk::RenderPassCreateFlagBits(0),
		(uint32_t)attachments.size(),
		attachments.data(),
		(uint32_t)subpasses.size(),
		subpasses.data(),
		(uint32_t)subpassDependencies.size(),
		subpassDependencies.data()
	};
}

#endif
