/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Info structure used with Memory manager methods.
*/

#ifndef HFVKMEMORYBLOCK_H
#define HFVKMEMORYBLOCK_H

#include <hfVk/hfVk.h>
#include <list>

/* ---- Aux Memory Alloc Map Struct ---- */
struct AllocationInfo {
	vk::DeviceSize start;
	vk::DeviceSize offset;

	vk::DeviceSize size() { return offset - start; }

	AllocationInfo& setStart(vk::DeviceSize start_) {
		start = start_;
		return *this;
	}

	AllocationInfo& setOffset(vk::DeviceSize offset_) {
		offset = offset_;
		return *this;
	}
};

/* ------- Info Classes ------- */
class hf::Vk::Info::MemoryBlock {
//attributes
public:
	AllocationInfo allocInfo; 
	hf::Vk::MemoryPool* allocatedFrom = nullptr;

// methods
public:
	MemoryBlock& setAllocInfo(AllocationInfo allocInfo_) {
		allocInfo = allocInfo_;
		return *this;
	}

	MemoryBlock& setAllocatedFrom(hf::Vk::MemoryPool * allocatedFrom_) {
		allocatedFrom = allocatedFrom_;
		return *this;
	}

};

class hf::Vk::Info::MemoryRequierements {
//attributes
public:
	vk::MemoryRequirements memReqs;
	vk::MemoryPropertyFlags memFlags;
	vk::DeviceSize allocationSize;

	MemoryRequierements& setMemReqs(vk::MemoryRequirements memReqs_) {
		memReqs = memReqs_;
		return *this;
	}

	MemoryRequierements& setMemFlags(vk::MemoryPropertyFlags memFlags_) {
		memFlags = memFlags_;
		return *this;
	}

	MemoryRequierements& setAllocationSize(vk::DeviceSize allocationSize_) {
		allocationSize = allocationSize_;
		return *this;
	}
};

class hf::Vk::Info::MemoryProperties {
//attributes
public:
	vk::DeviceSize size;
	vk::DeviceSize alignment;
	uint32_t memoryTypeIndex;
	vk::MemoryPropertyFlags propertyFlags;

	MemoryProperties& setAlignment(vk::DeviceSize alignment_) {
		alignment = alignment_;
		return *this;
	}

	MemoryProperties& setMemoryTypeIndex(uint32_t memoryTypeIndex_) {
		memoryTypeIndex = memoryTypeIndex_;
		return *this;
	}

	MemoryProperties& setPropertyFlags(vk::MemoryPropertyFlags propertyFlags_) {
		propertyFlags = propertyFlags_;
		return *this;
	}
};

#endif
