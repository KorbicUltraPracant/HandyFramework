/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Device wrapper object
*/

#ifndef HFVK_DEVICE_H
#define HFVK_DEVICE_H

#include <hfVk/DeviceQueues.h>
#include <hfVk/Instance.h>
#include <string>
#include <vector>
#include <unordered_map>

/* ------- Auxiliary Class ------- */
class hf::Vk::Auxiliary::Device {
// methods
public:
	// enumerate extensions
	static void enumerateAvailableExtensions(vk::PhysicalDevice psDevice, std::vector<vk::ExtensionProperties>& props);
	// enumerate layers
	static void enumerateAvailableLayers(vk::PhysicalDevice psDevice, std::vector<vk::LayerProperties>& props);
	// Query for Support
	static bool extensionSupported(vk::PhysicalDevice psDevice, std::string name);
	static int extensionsSupported(vk::PhysicalDevice psDevice, std::vector<char*>& names);
	static void extensionsSupported(vk::PhysicalDevice psDevice, std::vector<char*>& names, std::vector<bool>& resVec);
	static bool layerSupported(vk::PhysicalDevice psDevice, std::string name);
	static int layersSupported(vk::PhysicalDevice psDevice, std::vector<char*>& names);
	static void layersSupported(vk::PhysicalDevice psDevice, std::vector<char*>& names, std::vector<bool>& resVec);
	// checking physical devices capability
	static bool checkPhysicalDeviceCapability(vk::PhysicalDevice psDevice, hf::Vk::CreateInfo::Device& demands);
	static bool checkGPUFeatures(vk::PhysicalDevice psDevice, vk::PhysicalDeviceFeatures featuresToCheck);

};

/* ----------- Info Class ----------- */
class hf::Vk::Info::Device {
// attributes
public:
	vk::SurfaceKHR usedSurface;
	bool useValidationLayers;
	bool abundantInfo;
	bool verboseMode;

// virtual dtor
public:
	virtual ~Device() = default;

};

class hf::Vk::FurtherInfo::Device : public hf::Vk::Info::Device {
// attributes
public:
	void* pNext = nullptr;
	std::vector<Info::RequestQueues> initializedQueuesInfo;
	vk::PhysicalDeviceFeatures requieredDeviceFeatures;
	std::vector<char*> requieredExtensionNames;
	std::vector<char*> requieredLayerNames;

	Device& setNextPtr(void* pNext_) {
		pNext = pNext_;
		return *this;
	}

	Device& setUsedSurface(vk::SurfaceKHR usedSurface_) {
		usedSurface = usedSurface_;
		return *this;
	}

	Device& setUseValidationLayers(bool useValidationLayers_) {
		useValidationLayers = useValidationLayers_;
		return *this;
	}

	Device& setAbundantInfo(bool abundantInfo_) {
		abundantInfo = abundantInfo_;
		return *this;
	}

	Device& setVerboseMode(bool verboseMode_) {
		verboseMode = verboseMode_;
		return *this;
	}

	Device& setRequieredDeviceFeatures(vk::PhysicalDeviceFeatures requieredDeviceFeatures_) {
		requieredDeviceFeatures = requieredDeviceFeatures_;
		return *this;
	}

};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::Device : public hf::Vk::FurtherInfo::Device {
// attributes
private:
	std::vector<vk::DeviceQueueCreateInfo> queuesInfo;

public:
	hf::Vk::Instance* from; // will be needed when searching for device's physical device
	void(*checkGPUCapability)(std::vector<vk::PhysicalDevice>& hostGPUs);
	//void* FunAdditional_data;

// methods
public:
	Device() { memset(&requieredDeviceFeatures, 0, sizeof(requieredDeviceFeatures)); }
	operator vk::DeviceCreateInfo();

	Device& setInstance(hf::Vk::Instance * from_) {
		from = from_;
		return *this;
	}

};

/* ------- Core Class ------- */
class hf::Vk::Device : private Auxiliary::Device {
// attributes
private:
	hf::Vk::Info::Device* _description = nullptr;
	vk::PhysicalDevice _physicalDevice;
	std::unordered_map<uint32_t, Info::QueueFamily> _qHandles; // uint32_t qFamilyIdx
	vk::Device _device;

// methods
private:
	void init(CreateInfo::Device& createInfo);
	void deinit();
	
	// Fills handles vector based upon selected vkDevice and current qFamilyIdx, where queues_count must be equal to
	void obtainQueueHandles(CreateInfo::Device& createInfo);
	// Based upon& createInfo selects proper GPU to host Vulkan
	void selectPhysicalDevice(CreateInfo::Device& createInfo);
	// select gpus throught extensions and layer compatibility
	void extLayCompatibleGPUs(std::vector<vk::PhysicalDevice>& fromGPUs, CreateInfo::Device& demands);
	int findSuitableQueueFamilyIndex(vk::PhysicalDevice fromGPU, VkQueueFlagBits properties, uint32_t& queuesCount, vk::SurfaceKHR& renderSurface);
	void findSuitableQueueFamilyIndex(std::vector<vk::PhysicalDevice>& fromGPUs, std::vector<int>& psDeviceQFamilies, VkQueueFlagBits properties, uint32_t queuesCount = 1, vk::SurfaceKHR renderSurface = vk::SurfaceKHR());
	void featuresSupported(std::vector<vk::PhysicalDevice>& fromGPUs, vk::PhysicalDeviceFeatures reqFeatures);

public:
	// Ctor 'n Dtor
	Device() {}
	Device(CreateInfo::Device& createInfo) { init(createInfo); }
	~Device() { deinit(); }

	/* Getters  -> make them */
	vk::Device& getVkHandle() { return _device; }
	vk::PhysicalDevice& getPhysicalDevice() { return _physicalDevice; }
	Info::QueueFamily& getQueueHandles(uint32_t familyIdx) { return _qHandles[familyIdx]; };
	Info::Device& getDescription() { return *_description; }
	bool verboseActivated() { return _description->verboseMode; }
	bool abundantInfo() { return _description->abundantInfo; }

	/* Casters */
	operator vk::Device() { return _device; }

	/* Core Methods */
	// Initialization on demand
	void create(CreateInfo::Device& createInfo) { init(createInfo); }
	// Objects dtor on demand
	void destroy() { deinit(); } 

	/* Capatibility checkers */
	// enumerate extensions
	void enumerateAvailableExtensions(std::vector<vk::ExtensionProperties>& props) { Auxiliary::Device::enumerateAvailableExtensions(_physicalDevice, props); }
	// enumerate layers
	void enumerateAvailableLayers(std::vector<vk::LayerProperties>& props) { Auxiliary::Device::enumerateAvailableLayers(_physicalDevice, props); }
	// Query for Support
	bool extensionSupported(std::string name) { return Auxiliary::Device::extensionSupported(_physicalDevice, name); }
	void extensionsSupported(std::vector<char*>& names, std::vector<bool>& resVec) { hf::Vk::Auxiliary::Device::extensionsSupported(_physicalDevice, names, resVec); }
	// returns number of available extensions from provided names
	int extensionsSupported(std::vector<char*>& names) { return hf::Vk::Auxiliary::Device::extensionsSupported(_physicalDevice, names); }
	bool layerSupported(std::string name) { return Auxiliary::Device::layerSupported(_physicalDevice, name); }
	void layersSupported(std::vector<char*>& names, std::vector<bool>& resVec) { hf::Vk::Auxiliary::Device::layersSupported(_physicalDevice, names, resVec); }
	int layersSupported(std::vector<char*>& names) { return hf::Vk::Auxiliary::Device::layersSupported(_physicalDevice, names); }
	bool checkGPUFeatures(vk::PhysicalDeviceFeatures featuresToCheck) { return hf::Vk::Auxiliary::Device::checkGPUFeatures(_physicalDevice, featuresToCheck); };

};

#endif
