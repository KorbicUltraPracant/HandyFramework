/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Inlines for memory manager's methods
*/

#ifndef HFVKMEMANAGER_INL
#define HFVKMEMANAGER_INL

#include <hfVk/MemoryManager.h>
#include <hfVk/Buffer.h>
#include <hfVk/Image.h>

using namespace hf::Vk;

inline void MemoryManager::releaseImageMemory(Image& image) {
	restoreMemoryBlock(image.getMemoryInfo()); 
}

inline void MemoryManager::releaseBufferMemory(Buffer& buffer) {
	restoreMemoryBlock(buffer.getMemoryInfo()); 
}

#endif