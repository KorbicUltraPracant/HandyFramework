/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Info structures for Descriptor Set methods
*/

#ifndef HFVKDESCRSETAX
#define HFVKDESCRSETAX

#include <hfVk/hfVk.h>
#include <vector>

/* Auxiliary Class */
class hf::Vk::Info::WriteDescriptorSet {
	// attributes
public:
	vk::DescriptorSet dstSet;
	uint32_t dstBinding;
	uint32_t dstArrayElement;
	uint32_t descriptorCount;
	vk::DescriptorType descriptorType;
	vk::DescriptorImageInfo* pImageInfo = nullptr;
	vk::DescriptorBufferInfo* pBufferInfo = nullptr;
	vk::BufferView* pTexelBufferView = nullptr;

	// methods
public:
	operator vk::WriteDescriptorSet();
	
	WriteDescriptorSet& setDstSet(vk::DescriptorSet dstSet_) {
		dstSet = dstSet_;
		return *this;
	}

	WriteDescriptorSet& setDstBinding(uint32_t dstBinding_) {
		dstBinding = dstBinding_;
		return *this;
	}

	WriteDescriptorSet& setDstArrayElement(uint32_t dstArrayElement_) {
		dstArrayElement = dstArrayElement_;
		return *this;
	}

	WriteDescriptorSet& setDescriptorCount(uint32_t descriptorCount_) {
		descriptorCount = descriptorCount_;
		return *this;
	}

	WriteDescriptorSet& setDescriptorType(vk::DescriptorType descriptorType_) {
		descriptorType = descriptorType_;
		return *this;
	}

	WriteDescriptorSet& setPImageInfo(vk::DescriptorImageInfo* pImageInfo_) {
		pImageInfo = pImageInfo_;
		return *this;
	}

	WriteDescriptorSet& setPBufferInfo(vk::DescriptorBufferInfo* pBufferInfo_) {
		pBufferInfo = pBufferInfo_;
		return *this;
	}

	WriteDescriptorSet& setPTexelBufferView(vk::BufferView* pTexelBufferView_) {
		pTexelBufferView = pTexelBufferView_;
		return *this;
	}
};

class hf::Vk::Info::UpdateDescriptorSets {
// attributes
public:
	std::vector<vk::DescriptorBufferInfo> buffersInfo;
	std::vector<vk::DescriptorImageInfo> imagesInfo;
	std::vector<hf::Vk::Info::WriteDescriptorSet> writeInfo; // write info references items from buffer or images Infos.

	UpdateDescriptorSets& setBuffersInfo(std::vector<vk::DescriptorBufferInfo> buffersInfo_) {
		buffersInfo = buffersInfo_;
		return *this;
	}

	UpdateDescriptorSets& setImagesInfo(std::vector<vk::DescriptorImageInfo> imagesInfo_) {
		imagesInfo = imagesInfo_;
		return *this;
	}

	UpdateDescriptorSets& setWriteInfo(std::vector<hf::Vk::Info::WriteDescriptorSet> writeInfo_) {
		writeInfo = writeInfo_;
		return *this;
	}
};

#endif // !HFVKDESCRSETAX


