/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief usefull macros used inside this lib
*/

#ifndef HFVK_MACROS_H
#define HFVK_MACROS_H

#include <iostream>
#include <limits>
#include <UsefulMacros.h>

const uint32_t MAX_WAIT_TIMEOUT = std::numeric_limits<uint32_t>::max();

// uniform vk funs result check 
#define vkRC(f_res, err_msg, ret_res, verbose_mode) if (f_res != vk::Result::eSuccess && verbose_mode) { \
		std::cerr << err_msg << std::endl; \
		return ret_res; \
	} 

#define IS_BITSET(bitField, bitVariable) (bitField & bitVariable) == bitVariable

#endif
