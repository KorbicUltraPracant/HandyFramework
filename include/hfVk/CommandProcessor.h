/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief
*/

#ifndef HFVKCOMMANDPROCESSOR_H
#define HFVKCOMMANDPROCESSOR_H

#include <hfVk/hfVk.h>
#include <hfVk/Device.h>
#include <vector>
#include <unordered_map>
#include <hfVk/CommandBuffer.h>

/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief 
	This class serves as interface between commandBufferSets or single cmdbuffers and device's queues
	User will via flushCmdBuffer, pass command buffer which want to execute in queue and CmdProcessor object
	will find first suitable queue to post commands in (must have same queueFamilyIdx and optionaly should idle(so commands
	will be executed as soon as GPU allowes it, it will be upon user to define this behavior).

	Use notes:
	Should be one per application
*/

/* ------- Info Classes ------- */
class hf::Vk::Info::PostSingleBuffer {
// attributes
public:
	hf::Vk::CommandBuffer* pBuffer; // which cmd buffer to post
	// separate vectors, inside vk Submit info it is also described separately
	std::vector<vk::Semaphore> waitOn;
	std::vector<vk::PipelineStageFlags> insideStages;
	std::vector<vk::Semaphore> signalUponFinish;

// methods
public:
	operator vk::SubmitInfo();

	PostSingleBuffer& setPtrBuffer(hf::Vk::CommandBuffer* pBuffer_) {
		pBuffer = pBuffer_;
		return *this;
	}
};

class hf::Vk::Info::PostCommandBuffers {
// attributes
public:
	std::vector<PostSingleBuffer> bulk;

};

class hf::Vk::Info::CommandProcessor {
// attributes
public:
	hf::Vk::Device* pDevice; // ptr to parent device(the one which has compatible queues), ptr because user doesn't have to specify it in object's ctor calling

	CommandProcessor& setDevicePtr(hf::Vk::Device* pDevice_) {
		pDevice = pDevice_;
		return *this;
	}
};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::CommandProcessor : public hf::Vk::Info::CommandProcessor {
// attributes
public:

};

/* ------- Core Class ------- */
class hf::Vk::CommandProcessor {
// attributes
private:
	Info::CommandProcessor* _description = nullptr;

// methods
private:
	void init(CreateInfo::CommandProcessor& createInfo);
	void deinit();
	//bool initCompleted() { return _description != nullptr; }

	vk::Queue findQueueToSubmit(uint32_t queueFamilyIdx);

public:
	/* Ctors 'n Dtor */
	CommandProcessor() {}
	CommandProcessor(CreateInfo::CommandProcessor& createInfo) { init(createInfo); }
	~CommandProcessor() { deinit(); }

	/* Getters */
	Info::CommandProcessor& getDescription() { return *_description; }

	/* Setters */

	/* Core Methods */
	void startUp(CreateInfo::CommandProcessor& createInfo) { init(createInfo); }
	void shutdown() { deinit(); }

	// post cmd buffer into queue and when it is processed, then return it (make it available) into CommandBufferSet
	bool flushCmdBuffer(Info::PostSingleBuffer& info, Fence* signalUponFinish = nullptr);
	bool flushCmdBuffer(hf::Vk::CommandBuffer& buffer);
	bool flushCmdBufferNWait(hf::Vk::CommandBuffer& buffer) { bool ret = flushCmdBuffer(buffer); buffer.waitUntilExecutionFinished(); return ret; }

	/* 
		unconditionally all commands must be created from same pool, otherwise use variant with single Info::CommandsExec
		singleQueueCommandPost posts all cmd buffers into single queue, multiQueueCommandPost redistribute cmd buffers over all available queues from QFamily
		if useFences:
			for every queue where commands will be posted is provided one fence from intern pool and that fence
			is than assigned to cmd buffer object which can check its status afterwards...
	*/

	// version without signaling fences per each queue's work done
	bool multiQueueCommandsPost(Info::PostCommandBuffers& buffers);
	// user must guarantee that pFenceVec.size() >= min(availableQueues[acrossAllFamilies].size(), buffers.size())
	bool multiQueueCommandsPost(Info::PostCommandBuffers& buffers, std::vector<Fence*>& pFenceVec);
	bool singleQueueCommandsPost(Info::PostCommandBuffers& buffers, Fence* signalUponFinish = nullptr);

	/* Operators */	

	/* Status Checkers */
	
};


#endif