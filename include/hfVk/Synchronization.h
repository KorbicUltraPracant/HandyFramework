/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Header containing all sync primitives
*/

#ifndef HFVKSYNC_H
#define HFVKSYNC_H

#include <hfVk/Fence.h>
#include <hfVk/Event.h>
#include <hfVk/Semaphore.h>

#endif