/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Compute Pipeline class
*/

#ifndef HFVKCOMPUTEPIPE_H
#define HFVKCOMPUTEPIPE_H

#include <hfVk/Pipeline.h>
#include <hfVk/PipelineLayout.h>
#include <hfVk/PipelineComponents.inl>

/* ------- Info Class ------- */
class hf::Vk::Info::ComputePipeline : public hf::Vk::Info::Pipeline {
// attributes
public:

// virtual methods
public:
	virtual ~ComputePipeline() {}
	// virtual ComputePipeline& operator=(ComputePipeline& assign) = default; nemusi byt posunuju akorat ptr na strukturu v tech objektech ...

};

class hf::Vk::FurtherInfo::ComputePipeline : public hf::Vk::Info::ComputePipeline {
// attributes
public:
	// shader stage info
	hf::Vk::Info::PipelineSetUp::ShaderStage shaderStage;

	ComputePipeline& setShaderStage(hf::Vk::Info::PipelineSetUp::ShaderStage shaderStage_) {
		shaderStage = shaderStage_;
		return *this;
	}

};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::ComputePipeline : public hf::Vk::FurtherInfo::ComputePipeline, public hf::Vk::CreateInfo::Pipeline {
// attributes
public:

// methods
public:
	operator vk::ComputePipelineCreateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::ComputePipeline : public hf::Vk::Pipeline {
// attributes
private:
	// pipeline's info
	hf::Vk::Info::ComputePipeline* _description = nullptr;

// methods
private:
	void init(CreateInfo::ComputePipeline& createInfo);
	void deinit();
	void move(ComputePipeline& withdraw);

public:
	// Ctor 'n Dtor
	ComputePipeline() {}
	ComputePipeline(CreateInfo::ComputePipeline& createInfo) { init(createInfo); } // testnout jak se bude chovat vector na move-assignment(p
	ComputePipeline(ComputePipeline&& init) { move(init); }
	~ComputePipeline() { deinit(); }

	/* getters */
	Info::ComputePipeline& getDescription() { return *_description; }

	/* operators */
	ComputePipeline& operator=(ComputePipeline& assign) { move(assign); return *this; }

	/* Core Methods */
	// object's lifetime
	void assemble(CreateInfo::ComputePipeline& createInfo) { init(createInfo); }
	bool assemble(VkPipeline vkpipe, Info::ComputePipeline info); // used in pipelineDB
	void dismantle() { deinit(); }

	// Explicit Object's Reset, object is keept in same memory, but attributes are restored in state as in the Start of Ctor. 
	bool rebuild(CreateInfo::ComputePipeline*& createInfo); // if& createInfo == nullptr -> reset else rebuild via& createInfo

};

#endif
