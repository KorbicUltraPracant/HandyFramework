/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Event object
*/

#ifndef HFVKEVENT_H
#define HFVKEVENT_H

#include <hfVk/hfVk.h>

/* ------ Event Info Classes ------ */
class hf::Vk::Info::Event {
// attributes
public:
	hf::Vk::Device* pDevice;

	Event& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}
};

class hf::Vk::CreateInfo::Event : public hf::Vk::Info::Event {
// methods
public:
	Event() {}
	Event(Info::Event info) : Info::Event(info) {}
	operator vk::EventCreateInfo();

};

/* ------ Event Core Class ------ */
class hf::Vk::Event {
// attributes
private:
	Info::Event* _description = nullptr;
	vk::Event _event;

// methods
private:
	void init(CreateInfo::Event& createInfo);
	void deinit();
	void move(Event& withdraw);

public:
	/* Ctors 'n Dtor */
	Event() {}
	Event(CreateInfo::Event& createInfo) { init(createInfo); }
	Event(Event&& init) { move(init); }
	~Event() { deinit(); }

	/* Getters */
	vk::Event& getVkHandle() { return _event; } // _used = true; 
	Info::Event& getDescription() { return *_description; }

	/* Core Methods */
	void create(CreateInfo::Event& createInfo) { init(createInfo); }
	void destroy() { deinit(); }
	void waitUntilSignaled();
	void set();
	void reset();

	/* Operators */
	operator vk::Event() { return _event; }
	Event& operator=(Event& assign) { move(assign); return *this; }

	/* Status Checkers */
	bool isSet();
	bool isReset();

};

#endif
