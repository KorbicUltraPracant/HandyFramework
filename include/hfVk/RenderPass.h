/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Render Pass wrapper
*/

#ifndef HFVKRENDERPASS_H
#define HFVKRENDERPASS_H

#include <hfVk/hfVk.h>
#include <vector>

/* ------- Info Class ------- */
class hf::Vk::Info::RenderPass {
// attributes
public:
	// parent Device
	hf::Vk::Device* pDevice; 

// virtual dtor
public:
	virtual ~RenderPass() = default;

	RenderPass& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}
};

class hf::Vk::FurtherInfo::RenderPass : public hf::Vk::Info::RenderPass {
// attributes
public:
	std::vector<vk::AttachmentReference> attachmentReferences; // what attachment(idx) will have during subpass which layout
	std::vector<vk::AttachmentDescription> attachments; //attachments available during this renderpass
	std::vector<vk::SubpassDescription> subpasses;
	std::vector<vk::SubpassDependency> subpassDependencies;

};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::RenderPass : public hf::Vk::FurtherInfo::RenderPass {
// attributes
public:

// methods
public:
	inline operator vk::RenderPassCreateInfo();

};

/* ------- Core Class ------- */
class hf::Vk::RenderPass {
// attributes
private:
	hf::Vk::Info::RenderPass* _description = nullptr;
	vk::RenderPass _renderPass;

// methods
private:
	void init(CreateInfo::RenderPass& createInfo);
	void deinit();
	void move(RenderPass& withdraw);

public:
	/* Ctors 'n Dtor */
	RenderPass() {}
	RenderPass(CreateInfo::RenderPass& createInfo) { init(createInfo); }
	RenderPass(RenderPass&& init) { move(init); }
	~RenderPass() { deinit(); }

	/* Operators */
	RenderPass& operator=(RenderPass& assign) { move(assign); return *this; }
	operator vk::RenderPass() { return _renderPass; }

	/* Getters */
	vk::RenderPass& getVkHandle() { return _renderPass; }

	/* Core Methods */
	void describe(CreateInfo::RenderPass& createInfo) { init(createInfo); }
	void dismantle() { deinit(); }

};
#endif