/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief usefull macros used inside this lib
*/

#ifndef HF_MACROS_H
#define HF_MACROS_H

//#include <memory>

#define UPTR(type, param) std::unique_ptr<type>(new type(param))
#define SPTR_ALLOC(type, param) std::shared_ptr<type>(new type(param))

#define PTR(type) std::unique_ptr<type>
#define SPTR(type) std::shared_ptr<type>

#define CREATECONTAINER(object, count, constrExpr) do { \
	for (int i = 0; i < count; i++) \
		object.push_back(constrExpr); \
} while(0) 

#endif
