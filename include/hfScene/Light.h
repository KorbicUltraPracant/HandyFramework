#ifndef HFSCENELIGHT_H
#define HFSCENELIGHT_H

#include <hfScene/hfScene.h>
#include <glm/vec3.hpp>

class hf::Scene::Light {
public:
	glm::vec3 color;
	glm::vec3 position;
	glm::vec3 direction;
	float cutOff;
	enum LightSource : uint32_t { DIRECTIONAL = 0, POINT = 1, SPOTLIGHT = 2 } sourceType; // DIRECTIONAL = 0, POINT = 1, SPOTLIGHT = 2
	enum LightingTechnique : uint32_t { NONE = 0, PHONG = 1, BLINN = 2 } lightingMode; // NONE = 0, PHONG = 1, PHONG_BLINN = 2
	// hf::Vk::Mesh* usedModel = nullptr;
};

#endif