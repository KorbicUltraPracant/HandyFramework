#ifndef HFSCENE_INL
#define HFSCENE_INL

#include <hfScene/Scene.h>
#include <fstream>
#include <iostream>
#include <regex>
#include <unordered_map>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>

struct vertex {
	glm::vec3 pos;
	glm::vec3 normal;
	glm::vec2 uv;

	bool operator==(const vertex& other) const {
		return pos == other.pos && normal == other.normal;
	}
};

namespace std {
	template<> struct hash<vertex> {
		size_t operator()(vertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.normal) << 1)) >> 1)
				^(hash<glm::vec2>()(vertex.uv) << 1);
		}
	};
}

template<typename APIMesh>
inline void hf::Scene::Graph::Root<APIMesh>::tinyObjLoadRoutine(std::string path, std::vector<APIMesh*>* addedMeshes, bool* hasTextures) {
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> tmpMaterials;

	/* tady je problem ze se pro odlisne modely nacita stejna cesta, na kks!! */
	std::string baseDir_ = GetBaseDir(path);

	// fill tiny obj vecs
	std::string err;
	bool ret = tinyobj::LoadObj(&attrib, &shapes, &tmpMaterials, &err, (const char*)path.c_str(), baseDir_.c_str(), true);
	if (err.size()) {
		std::cerr << "problem with loading scene: " << err << ":(.\n Press any key to exit...\n";
		std::cin.get();
		exit(-1);
	}

	// create n assign info to meshes
	uint32_t idxCounter(indices.size());
	uint32_t indicePosOffset(sceneData->vertices.size() / 3); // cause xyz pos 
	std::unordered_map<vertex, uint32_t> uniqueVertices;
	std::vector<vertex> tmpVec; // totalne zkurvena vec... zabere to moc mista a spoustu cpy casu, ale co se da delat -> zasrany tinyobj kreten...
	for (auto& shape : shapes) {
		// create new empty mesh
		meshes.emplace(shape.name, APIMesh());

		// if user wants to know which meshes will added by this call
		if (addedMeshes)
			addedMeshes->push_back(&meshes[shape.name]);
		
		//meshes[shape.name].baseDir = baseDir_;
		meshes[shape.name].indicesPos.start = idxCounter;
		idxCounter += shape.mesh.indices.size();
		meshes[shape.name].indicesPos.end = idxCounter;

		// texcoords and indices must be crafted inside here
		// Loop over faces(polygon)
		size_t index_offset = 0;
		for (size_t f = 0; f < shape.mesh.num_face_vertices.size(); f++) {
			int fv = shape.mesh.num_face_vertices[f];

			// Loop over vertices in the face.
			for (size_t v = 0; v < fv; v++) {
				// access to vertex
				tinyobj::index_t idx = shape.mesh.indices[index_offset + v];
				vertex tmp;
				tmp.pos = glm::vec3(
					attrib.vertices[3 * idx.vertex_index + 0],
					attrib.vertices[3 * idx.vertex_index + 1],
					attrib.vertices[3 * idx.vertex_index + 2]);

				tmp.normal = glm::vec3(
					attrib.normals[3 * idx.normal_index + 0],
					attrib.normals[3 * idx.normal_index + 1],
					attrib.normals[3 * idx.normal_index + 2]
				);

				if (idx.texcoord_index < 0)
					tmp.uv = glm::vec2(0, 0);
				else
					tmp.uv = glm::vec2(attrib.texcoords[2 * idx.texcoord_index + 0], 1.0f - attrib.texcoords[2 * idx.texcoord_index + 1]);

				if (uniqueVertices.count(tmp) == 0) {
					uniqueVertices[tmp] = static_cast<uint32_t>(tmpVec.size());
					tmpVec.push_back(tmp);
				}

				indices.push_back(uniqueVertices[tmp] + indicePosOffset);
			}
			index_offset += fv;

			// per-face material
			meshes[shape.name].material_name = tmpMaterials[shape.mesh.material_ids[f]].diffuse_texname;
		}
	}

	/*if (vertices.size() / 3 != texcoords.size() / 2) {
		std::cerr << "hf::Scene::Load -> not every vertex have texture coords\n";
		exit(-1);
	}*/

	// redistribute tmpVertexVec into actual buffers
	for (auto& vertex : tmpVec) {
		sceneData->vertices.insert(sceneData->vertices.end(), { vertex.pos.x, vertex.pos.y, vertex.pos.z });
		sceneData->normals.insert(sceneData->normals.end(), { vertex.normal.x, vertex.normal.y, vertex.normal.z });
		sceneData->texcoords.insert(sceneData->texcoords.end(), { vertex.uv.x, vertex.uv.y });
	}

	if (tmpMaterials.size()) 
		*hasTextures = true;

	// store texture info
	for (auto& mat : tmpMaterials) {
		sceneData->materials[mat.name] = UPTR(hf::Scene::Material, mat);
		sceneData->materials[mat.name]->baseDir = baseDir_;
	}

}

template <typename APIMesh>
void hf::Scene::Graph::Root<APIMesh>::load(std::string path) {
	// user trying to load model which has already been loaded
	if (meshes.find(path) != meshes.end()) // jeste otestovat jestli se ta podminka vazne chyti !!
		return;

	// clear previous scene
	clear();
	tinyObjLoadRoutine(path);
}

template<typename APIMesh>
inline void hf::Scene::Graph::Root<APIMesh>::clear() {
	sceneData = UPTR(hf::Scene::Data,);
	meshes.clear();
	root = nullptr;
}

template<typename APIMesh>
inline void hf::Scene::Graph::Root<APIMesh>::addModel(std::string path, std::vector<APIMesh*>* addedMeshes, bool* hasTextures) {
	tinyObjLoadRoutine(path, addedMeshes, hasTextures);
}

template<typename APIMesh>
inline hf::Scene::Graph::Node<APIMesh>* hf::Scene::Graph::Root<APIMesh>::addModelChild(hf::Scene::Graph::Node<APIMesh>* parent, hf::Scene::Graph::Node<APIMesh>* exemplar) {
	hf::Scene::Graph::Node<APIMesh> tmp(*exemplar);
	tmp.rootPtr = this;
	parent->derivedNodes.push_back(UPTR(hf::Scene::Graph::Node<APIMesh>, std::move(tmp)));
	return &*parent->derivedNodes.back();
}

#endif

template<typename APIMesh>
inline hf::Scene::Graph::Node<APIMesh>& hf::Scene::Graph::Node<APIMesh>::operator=(const hf::Scene::Graph::Node<APIMesh>& copy) {
	name = copy.name;
	rootPtr = copy.rootPtr;
	indices = copy.indices;
	meshes.clear();
	meshes = copy.meshes;
	worldTranslation = copy.worldTranslation;
	hasTextures = copy.hasTextures;
	derivedNodes.clear();
	for (auto& child : copy.derivedNodes)
		derivedNodes.push_back(UPTR(hf::Scene::Graph::Node<APIMesh>, *child));

	return *this;
}

/*
	Must not allow to vertex be without texture coord -- as it is shown in my bc thesis, it will slow down (broke) Vk's rendering preparations.
	http://syoyo.github.io/tinyobjloader/

	az budu nacitat textury sceny, pokud se nacte kompletne beztexturova scena, potreba vytvorit defaultni treba i jedno pixelovou texturu z niz se bude cist...

	// Create an empty stack and push root to it
	stack<node *> nodeStack;
	nodeStack.push(root);

	Pop all items one by one. Do following for every popped item
	a) print it
	b) push its right child
	c) push its left child
	Note that right child is pushed first so that left is processed first 
	while (nodeStack.empty() == false)
	{
		// Pop the top item from stack and print it
		struct node *node = nodeStack.top();
		printf("%d ", node->data);
		nodeStack.pop();

		// Push right and left children of the popped node to stack
		if (node->right)
			nodeStack.push(node->right);
		if (node->left)
			nodeStack.push(node->left);
	}

*/