#ifndef HFSCENEMATERIAL_H
#define HFSCENEMATERIAL_H

#include <hfScene/hfScene.h>
#include <vector>
#include <tiny_obj_loader.h>

class hf::Scene::Material {
// attributes
private:
public:
	// base dir
	std::string baseDir;

	// textures
	//std::string ambientTexname;             // map_Ka
	std::string diffuseTexname;             // map_Kd
	/*std::string specularTexname;            // map_Ks
	std::string specularHighlightTexname;  // map_Ns
	std::string bumpTexname;                // map_bump, map_Bump, bump
	std::string displacementTexname;        // disp
	std::string alphaTexname;               // map_d
	std::string reflectionTexname;          // refl*/

	//tinyobj::material_t neco;
// methods
private:
public:
	Material(tinyobj::material_t& convert);

};

#endif