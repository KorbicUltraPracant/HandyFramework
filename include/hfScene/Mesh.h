#ifndef HFSCENEMESH_H
#define HFSCENEMESH_H

#include <hfScene/hfScene.h>
#include <vector>

struct Range {
	uint32_t start;
	uint32_t end;
};

class hf::Scene::Mesh {
// attributes
public:
	// range inside parent's scene indices vector
	Range indicesPos;

	// per-face material ID, idx to parent's material vector
	std::string material_name = "";

	// base dir
	//std::string baseDir;

// methods
public:

};

#endif