/**
* @author Radek Blahos
* @project Handy Framework's Vulkan Library
*
* @brief hfVkRender Forward declarations
*/

#ifndef HFSCENE_H
#define HFSCENE_H

#include <UsefulMacros.h>

namespace hf {
	 namespace Scene {
		class Data;
		class Mesh;
		class Camera;
		class Light;
		class Model;
		class Material;

		// universal scene graph
		namespace Graph {
			template <typename APIMesh>
			class Node;
			template <typename APIMesh>
			class Root;
			template<typename T> class Iterator;

		}

		namespace CreateInfo {

		}

		namespace FurtherInfo {

		}

		namespace Info {

		}

		namespace Utility {

		}

		namespace Enums {
			enum NodeType { ROOT, MODEL };

		}
	}
}

#endif

/* Class Template

class hf::Scene::class {
// attributes
private:
public:

// methods
private:
public:

};

*/