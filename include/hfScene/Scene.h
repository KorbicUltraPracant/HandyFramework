#ifndef HFSCENESCENE_H
#define HFSCENESCENE_H

#include <unordered_map>
#include <string>
#include <vector>
#include <glm/mat4x4.hpp>
#include <tiny_obj_loader.h>
#include <hfScene/Camera.h>
#include <hfScene/Light.h>
#include <hfScene/GraphNode.h>
#include <memory>

/* You definetelly wanna store scene on heap !!*/
class hf::Scene::Data {
// attributes
public:
	// with all this one can render whole scene inside vulkan at once... only if he wants animations, there will need be a scene graph and proper position calculation, which will need rendering per mesh. Or instanced rendering where rendering per model is often desired...
	std::vector<float> vertices;   // packed as stated here http://syoyo.github.io/tinyobjloader/
	std::vector<float> normals;    // 'vn'
	std::vector<float> texcoords;  // 'vt'

	std::unordered_map<std::string, PTR(hf::Scene::Material)> materials; // for now after storing textures inside GPU i will clean this

};

template <typename APIMesh>
class hf::Scene::Graph::Root {
// attributes
private:

public:
	PTR(hf::Scene::Data) sceneData;
	std::vector<uint32_t> indices; // i hope that indices can be defined in 4byte unsigned , store it inside mesh

	// projection matrix
	glm::mat4 projection;

	// camera
	hf::Scene::Camera camera;

	// scene's light
	hf::Scene::Light light;

	// meshes db
	std::unordered_map<std::string, APIMesh> meshes;

	// root node 
	std::unique_ptr<hf::Scene::Graph::Node<APIMesh>> root; // unique ptr because I can dtor whole tree with one assign :)

// methods
private:
public:
	void load(std::string path);
	void clear();
	void addModel(std::string path, std::vector<APIMesh*>* addedMeshes = nullptr, bool* hasTextures = nullptr); // adds models data at the and of current buffers
	void tinyObjLoadRoutine(std::string path, std::vector<APIMesh*>* addedMeshes = nullptr, bool* hasTextures = nullptr);
	/* 
		for now precompute world translation matrices for all nodes
	*/
	// graph methods
	hf::Scene::Graph::Node<APIMesh>* addModelChild(hf::Scene::Graph::Node<APIMesh>* parent, hf::Scene::Graph::Node<APIMesh>* exemplar);

};

std::string GetBaseDir(std::string& filepath);

#endif