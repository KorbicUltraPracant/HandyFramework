#include <hfScene/Mesh.h>
#include <vector>
#include <memory>
#include <glm/mat4x4.hpp>
#include <hfAx/hfAx.h>
#include <UsefulMacros.h>

/* Node is representing one single model inside scene */
template <typename APIMesh>
class hf::Scene::Graph::Node {
// attributes
public:
	std::string name; // models name debug only...

	// ptr to root which contains all unique data 
	hf::Scene::Graph::Root<APIMesh>* rootPtr = nullptr;

	Range indices; // undef values

	// textures flag
	bool hasTextures = false;

	// meshes representing this node's model
	std::vector<APIMesh*> meshes; // ptr to root's mesh vector or if null it is root for whole sceen or root for meshes -> all its child are part of same model

	// world translation mat
	glm::mat4 worldTranslation = glm::mat4(1.0);

	// child container
	hf::Ax::PtrVector<hf::Scene::Graph::Node<APIMesh>> derivedNodes; // !! include hfAx

	// change flag
	// bool changed = false; // if changed true process ... signals nothing, computation is needed anyway because child might have changed

// methods 
public:
	Node() {}
	Node(APIMesh& mesh_) : mesh(&mesh_) {}
	Node(hf::Scene::Graph::Root<APIMesh>* root) : rootPtr(root) {}
	Node(APIMesh& mesh_, hf::Scene::Graph::Root<APIMesh>* root) : mesh(&mesh_), rootPtr(root) {}
	Node(const Node& copy) { this->operator=(copy); }

	Node& operator=(const Node& copy);

};