/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Set builded over Command buffers...
*/

#ifndef HFVKCOMMANDBUFFERSET_H
#define HFVKCOMMANDBUFFERSET_H

#include <hfVkRender/Set.h>
#include <hfVk/CommandPool.h>
#include <hfVk/CommandBuffer.inl>
#include <hfVk/Device.h>
#include <hfAx/UsageRegister.h>
#include <hfVk/Synchronization.h>

/* ------- Info Class ------- */
class hf::Vk::Info::CommandBufferSet {
	// attributes
public:
	hf::Vk::Device* pDevice;
	uint32_t familyQueueIdx; // to which queues can be commands submited

	CommandBufferSet& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	CommandBufferSet& setFamilyQueueIdx(uint32_t familyQueueIdx_) {
		familyQueueIdx = familyQueueIdx_;
		return *this;
	}
};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::CommandBufferSet : public Info::CommandBufferSet {
// attributes
public:
	//bool createFences = false;
	uint32_t initialBufferCount;

	CommandBufferSet& setInitialBufferCount(uint32_t initialBufferCount_) {
		initialBufferCount = initialBufferCount_;
		return *this;
	}
};

/* ------- Core Class ------- */
template<>
class Utility::Set<CommandBuffer> {
	// attributes
private:
	CommandPool _pool;
	hf::Ax::PtrVector<CommandBuffer> _buffers;
	hf::Ax::UsageRegister _bufferUsage;
	Info::CommandBufferSet* _description = nullptr;

	// methods
private:
	void init(CreateInfo::CommandBufferSet& createInfo);
	void deinit();
	//bool initCompleted();
	int getNextBufferIdx(); // returns idx of the first free cmd buff

public:
	/* Ctors 'n Dtor */
	Set() {}
	Set(CreateInfo::CommandBufferSet& createInfo) { init(createInfo); }
	~Set() { deinit(); }

	/* Getters */
	CommandBuffer& getBuffer(unsigned idx) { return *_buffers[idx]; }
	CommandBuffer& getLastBuffer() { return *_buffers.back(); }

	// return cmd buffer which idx has been lastly assigned by getNextBuffer
	// User cannot call to consequence getCurrentBuffer calls without calling returnCurrentBuffer between them !
	CommandBuffer& getNextBuffer() { return *_buffers[getNextBufferIdx()]; }
	CommandBuffer& getCurrentBuffer() { return *_buffers[_bufferUsage.lastAssigned()]; } // neni osetreno, protoze chci aby to vyhodilo vyjimku, jelikoz to to znamena ze uzivatel pouziva cmd set spatne!
	int getCurrentBufferIdx() { return _bufferUsage.lastAssigned(); }
	void returnBuffer(uint32_t idx) { _bufferUsage.unsetIndex(idx); }
	void returnCurrentBuffer() { _bufferUsage.unsetIndex(_bufferUsage.lastAssigned()); } // mark buffer on idx as unused

	/* Core Methods */
	void create(CreateInfo::CommandBufferSet& createInfo) { init(createInfo); }
	void destroy() { return deinit(); }

	// insert count more cmd buffers into set
	void addBuffers(uint32_t count);

	// reset _bufferUsage
	void usageRegisterReset() { _bufferUsage.unsetAllIndexes(); }
	//void deleteFencePointers();

	size_t getSize() { return _buffers.size(); }

};

#endif
