/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Utility uniform buffer object -> This class will contain only HFVKBuffer located in host memory(RAM)
	but will be treated as typename T
*/

#ifndef HFVKUNIFORMBUFFER_H
#define HFVKUNIFORMBUFFER_H

#include <hfVkRender/hfVkRender.h>
#include <hfVk/hfVk.h>
#include <hfVk/Buffer.h>

template <typename T>
class hf::Vk::Render::UniformBuffer {
// attributes
private:
	T* _viewPtr = nullptr; 
	hf::Vk::Buffer _buffer;

public:
	UniformBuffer() {}
	UniformBuffer(CreateInfo::Buffer& createInfo);
	~UniformBuffer() { if (_viewPtr != nullptr) _buffer.unmap(); }
	void create(CreateInfo::Buffer& createInfo);
	
	void flushData() { _buffer.unmap(); _viewPtr = static_cast<T*>(_buffer.map()); }
	hf::Vk::Buffer& getBuffer() { return _buffer; }
	T& accessData() { return *_viewPtr; }
	void copy(T& data);

};

#endif
