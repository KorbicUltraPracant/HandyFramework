#ifndef HFVKRENDERCMDBUFFERRECORDING_H
#define HFVKRENDERCMDBUFFERRECORDING_H

#include <hfVkRender/hfVkRender.h>
#include <hfVk/CommandBufferInfo.h>
#include <hfScene/Scene.h>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>

class hf::Vk::CommandBufferRecording::Draw::Core {
// attributes
public:
	hf::Vk::Info::BindVertexBuffers vertexBuffersInfo;
	hf::Vk::Info::BindDescriptorSets descriptorSetsInfo;
	hf::Vk::Info::PushConstants pushConstants;
	hf::Vk::GraphicPipeline* pGraphicPipeline;
	uint32_t renderWidth;
	uint32_t renderHeight;
	float minDepth = 0.0f;
	float maxDepth = 1.0f;

// methods
public:
	virtual ~Core() {}
	virtual void record(hf::Vk::CommandBuffer& keepCommands);

	virtual Core& setVertexBuffersInfo(hf::Vk::Info::BindVertexBuffers vertexBuffersInfo_) {
		vertexBuffersInfo = vertexBuffersInfo_;
		return *this;
	}

	virtual Core& setDescriptorSetsInfo(hf::Vk::Info::BindDescriptorSets descriptorSetsInfo_) {
		descriptorSetsInfo = descriptorSetsInfo_;
		return *this;
	}

	virtual Core& setPushConstants(hf::Vk::Info::PushConstants pushConstants_) {
		pushConstants = pushConstants_;
		return *this;
	}

	virtual Core& setPGraphicPipeline(hf::Vk::GraphicPipeline * pGraphicPipeline_) {
		pGraphicPipeline = pGraphicPipeline_;
		return *this;
	}

	virtual Core& setRenderWidth(uint32_t renderWidth_) {
		renderWidth = renderWidth_;
		return *this;
	}

	virtual Core& setRenderHeight(uint32_t renderHeight_) {
		renderHeight = renderHeight_;
		return *this;
	}

	operator hf::Vk::Info::SetViewport() { return { 0, 0, (float)renderWidth, (float)renderHeight, minDepth, maxDepth }; }

};

class hf::Vk::CommandBufferRecording::Draw::Draw : public hf::Vk::CommandBufferRecording::Draw::Core {
// attributes
public:
	hf::Vk::Info::Draw drawInfo;

// methods
public:
	void record(hf::Vk::CommandBuffer& keepCommands);

	Draw& setDrawInfo(hf::Vk::Info::Draw drawInfo_) {
		drawInfo = drawInfo_;
		return *this;
	}
};

class hf::Vk::CommandBufferRecording::Draw::Indexed : public hf::Vk::CommandBufferRecording::Draw::Core {
// attributes
public:
	hf::Vk::Info::BindIndexBuffer indexBufferInfo;
	hf::Vk::Info::DrawIndexed drawInfo;

// methods
public:
	void record(hf::Vk::CommandBuffer& keepCommands);

	Indexed& setDrawInfo(hf::Vk::Info::DrawIndexed drawInfo_) {
		drawInfo = drawInfo_;
		return *this;
	}

	Indexed& setIndexBufferInfo(hf::Vk::Info::BindIndexBuffer indexBufferInfo_) {
		indexBufferInfo = indexBufferInfo_;
		return *this;
	}

};

class hf::Vk::CommandBufferRecording::Draw::IndexedIndirect : public hf::Vk::CommandBufferRecording::Draw::Core {
// attributes
public:
	hf::Vk::Info::BindIndexBuffer indexBufferInfo;
	hf::Vk::Info::DrawIndexedIndirect drawInfo;

// methods
public:
	void record(hf::Vk::CommandBuffer& keepCommands);

	IndexedIndirect& setDrawInfo(hf::Vk::Info::DrawIndexedIndirect drawInfo_) {
		drawInfo = drawInfo_;
		return *this;
	}

	IndexedIndirect& setIndexBufferInfo(hf::Vk::Info::BindIndexBuffer indexBufferInfo_) {
		indexBufferInfo = indexBufferInfo_;
		return *this;
	}

};

template<typename T>
class hf::Vk::CommandBufferRecording::RenderPass {
// attributes
public:
	hf::Vk::Info::BeginRenderPass renderPassInfo;
	std::vector<T> drawInfos; // resize properly before usage..., draw info per each subpass

// methods
public:
	void record(hf::Vk::CommandBuffer& keepCommands) {
		/* begin renderPass */
		keepCommands.beginRenderPass(renderPassInfo);

		/* record cmd commands */
		int counter(0);
		for (auto& subpass : drawInfos) {
			subpass.record(keepCommands);

			// single and last entry is not finished by next subpass cmd
			if (drawInfos.size() > 1 && counter < drawInfos.size() - 1)
				keepCommands.nextSubpass();

			counter++;
		}

		/* end renderpass */
		keepCommands.endRenderPass();
	}

	RenderPass& setRenderPassInfo(hf::Vk::Info::BeginRenderPass renderPassInfo_) {
		renderPassInfo = renderPassInfo_;
		return *this;
	}

};

template<typename T>
class hf::Vk::CommandBufferRecording::Batch {
// attributes
public:
	vk::CommandBufferUsageFlagBits cmdBufferUsage = vk::CommandBufferUsageFlagBits::eSimultaneousUse;
	bool clearImage = false;
	hf::Ax::PtrVector<hf::Vk::CommandBufferRecording::RenderPass<T>> renderPasses;	

// methods
public:
	void record(hf::Vk::CommandBuffer& keepCommands, hf::Vk::Render::Renderer* rendererPtr = nullptr, uint32_t swapchainImageIdx = 0) {
		/* begin recording */
		keepCommands.beginRecording(cmdBufferUsage);

		/* clear resources if requiered */
		if (rendererPtr != nullptr && clearImage)
			rendererPtr->clearRPAttachments(swapchainImageIdx, keepCommands);

		for (auto& rp : renderPasses)
			rp->record(keepCommands);

		/* finish recording */
		keepCommands.finishRecording();
	}

	Batch& setBufferUsage(vk::CommandBufferUsageFlagBits cmdBufferUsage_) {
		cmdBufferUsage = cmdBufferUsage_;
		return *this;
	}

	Batch& setClearImage(bool clearImage_) {
		clearImage = clearImage_;
		return *this;
	}
};

#endif // hfVkWIDGET_H
