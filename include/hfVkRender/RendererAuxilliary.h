#ifndef HFVKRENDERERAXSTRUCTS_H
#define HFVKRENDERERAXSTRUCTS_H

#include <hfVkRender/hfVkRender.h>
#include <hfVk/CommandBufferInfo.h>
#include <hfScene/Scene.h>
#include <glm/mat4x4.hpp>
#include <glm/vec4.hpp>

struct hf::Vk::ShaderResources::UniformBuffer::VertexShader {
	glm::mat4 viewProjection;
	glm::mat4 worldSpaceTr;
	glm::mat4 normalsFix; // must by mat4 for gpu memory layout but actualy is used only mat 3x3 

	VertexShader() : viewProjection(glm::mat4(1.0)), worldSpaceTr(glm::mat4(1.0)), normalsFix(glm::mat3(1.0)) {}
};

struct hf::Vk::ShaderResources::UniformBuffer::GeometryShader {
	std::array<glm::mat4, 6> shadowCastMats;

};

struct hf::Vk::ShaderResources::UniformBuffer::FragmentShader {
	// Vec4 everywhere because stupid layout reqs on gpu's KHR API's side
	glm::vec4 color;
	glm::vec4 objectUniformColor;
	glm::vec4 position;
	glm::vec4 direction;
	glm::vec4 eyePos;
	float cutOff;
	//uint32_t sourceType; // DIRECTIONAL = 0, POINT = 1, SPOTLIGHT = 2
	uint32_t lightingMode;
	uint32_t samplerIdx = -1; // if idx is -1 than there is no binded sampler to current mesh
	float farPlane = 0.0f; // used when computing shadow map
	//FragmentShaderBuffer() { memset(this, 0, sizeof(VertexShaderBuffer)); }
};

struct hf::Vk::ShaderResources::PushConstants {
	uint32_t matIdx[6] = { 0,1,2,3,4,5 };
};

#endif // hfVkWIDGET_H
