/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief utility vertex buffer object
* Class pairing hf::Vk::Info::PipelineSetUp::VertexInputDescription with its hf::Vk::Buffer representation.
* Used by HFVKScene class.
*/

#ifndef HFVK_SHAREDBUFFER_H
#define HFVK_SHAREDBUFFER_H

#include <hfVkRender/hfVkRender.h>
#include <hfVk/Buffer.h>

/* ------- Core Class ------- */
class hf::Vk::Render::SharedBuffer {
// attributes
private:
	hf::Vk::Buffer _data;
	void* _viewPtr = nullptr;

// methods
public:
	SharedBuffer() {}
	~SharedBuffer() { if (_viewPtr != nullptr) _data.unmap(); }

	void create(vk::DeviceSize size, vk::BufferUsageFlagBits usage, hf::Vk::MemoryManager* managerPtr);
	template<typename T>
	T& accessData() { return *static_cast<T*>(_viewPtr); }
	void* accessDataPtr() { return _viewPtr; }
	hf::Vk::Buffer& getBuffer() { return _data; }
	void flushData();

};

#endif
