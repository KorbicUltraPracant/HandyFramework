/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Render Context -> This class is something like template to HFVK users. Encapsulates all HFVK objets 
	required to Start rendering. And defines abstract methods which user have to implement in
	order to HFVK Start doing something(Render to a screen).
*/

#ifndef HFVK_RENDERSTRATEGY_H
#define HFVK_RENDERSTRATEGY_H

#include <hfVkRender/hfVkRender.h>
#include <hfVk/PipelineLayout.h>
#include <hfVkRender/CommandBufferSet.h>

class hf::Vk::Render::Strategy {
// attributes
public:	
	hf::Ax::PtrVector<ShaderModule> shaders; // used shaders
	hf::Ax::PtrVector<DescriptorSet> descriptorSets; 
	hf::Vk::Utility::Set<CommandBuffer> commandBuffers; // commnad buffers for this strategy

};

#endif