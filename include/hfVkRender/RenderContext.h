/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Render Context -> This class is something like template to HFVK users. Encapsulates all HFVK objets 
	required to Start rendering. And defines abstract methods which user have to implement in
	order to HFVK Start doing something(Render to a screen).
*/

#ifndef HFVK_RENDERCONTEXT_H
#define HFVK_RENDERCONTEXT_H

#include <hfVk/Instance.inl>
#include <hfVk/Device.h>
#include <hfVk/MemoryManager.inl>
#include <hfVkRender/Presenter.h>
#include <hfVkRender/RenderComponentsDB.h>
#include <hfVkRender/PipelineDatabase.h>
#include <hfVk/Swapchain.h>
#include <hfVk/CommandProcessor.h>
#include <hfVkRender/CommandBufferSet.h>
#include <hfVk/DescriptorManager.h>

/* ------- Abstract Class ------- */
class hf::Vk::Render::Context {
// attributes
public:
	//hf::Ax::Window window; // OS window include/inherit in external class together with render context 
	
	// Vulkan Objects
	hf::Vk::Instance instance;
	hf::Vk::Device device;
	hf::Vk::MemoryManager memManager;
	hf::Vk::DescriptorManager descManager;
	hf::Vk::PipelineDatabase pipelineDB;
	hf::Vk::Render::ComponentsDB componentsDB;
	hf::Vk::Presenter presenter;
	hf::Vk::CommandProcessor commandProcessor;	
};

#endif