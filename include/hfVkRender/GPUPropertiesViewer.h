/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief GPU properties viewer
*/

#ifndef HFVKPROPERTIESVIEW_H
#define HFVKPROPERTIESVIEW_H

#include <hfVk/Instance.h>
#include <hfVk/Device.h>

class hf::Vk::Utility::GPUPropertiesViewer : 
	public hf::Vk::Auxiliary::Instance, 
	public hf::Vk::Auxiliary::Device
{

};

#endif
