/**
* @author Radek Blahos
* @project hfVkWidget Library
*
* @brief Maps geSG Scene Meshes and Images into hfVk provided structures.
* This class provides the link between implementation independent scene graph
* and Vulkan.
* Meshes are mapped to a collection of hfVkBuffers and MaterialImageComponents
* are mapped to hfVkImages.
*/

#ifndef HFVKRENDERSCENE_H
#define HFVKRENDERSCENE_H

#include <hfVk/Image.inl>
#include <hfVkRender/hfVkRender.h>
#include <string>
#include <glm/vec3.hpp>
#include <hfVkRender/Mesh.h>
#include <hfScene/Scene.inl>
#include <hfScene/Material.h>
#include <hfAx/hfAx.h>

class hf::Vk::CreateInfo::Scene {
//attributes
public:
	// these object will be needed for Vulkan's ge::sg::scene representation 
	hf::Vk::MemoryManager* pManager = nullptr;
	hf::Vk::CommandProcessor* pCmdProcessor = nullptr;
	hf::Vk::Utility::Set<hf::Vk::CommandBuffer>* pCmdSet = nullptr;
	// image load function ptr

	Scene& setManagerPtr(hf::Vk::MemoryManager * pManager_) {
		pManager = pManager_;
		return *this;
	}

	Scene& setPCmdProcessor(hf::Vk::CommandProcessor * pCmdProcessor_) {
		pCmdProcessor = pCmdProcessor_;
		return *this;
	}

	Scene& setPCmdSet(hf::Vk::Utility::Set<hf::Vk::CommandBuffer> * pCmdSet_) {
		pCmdSet = pCmdSet_;
		return *this;
	}

};

class hf::Vk::Render::Scene : public hf::Scene::Graph::Root<hf::Vk::Render::Mesh> {
// attributes
private:
	CreateInfo::Scene* _description = nullptr; // need to keep ptrs to some hfVk Objects

public:
	// std::vector<hf::Vk::Render::Mesh> gpuMeshes; currently not needed everything will be stored inside main buffers
	hf::Ax::PtrVector<hf::Vk::Buffer> buffers;

	// all scenes textures are baked here, image can have more layers inside it, Image per every component, mainly will be used one image per diffuse component
	hf::Ax::PtrVector<hf::Vk::Image> bakedTextures;

// methods
private:
	void storeTextures();
	void stashTextureOnGPU(void* data, uint32_t width, uint32_t height);
	void storeMeshes();
	void storeDummyTexture();

public:
	Scene() {};
	Scene(CreateInfo::Scene& createInfo) { init(createInfo); }
	~Scene();

	/* Core */
	void init(CreateInfo::Scene& createInfo) { _description = new CreateInfo::Scene(createInfo); }
	
	// create scene graph via some rules
	void createSceneGraph(); // need to use some better format than obj to actualy have somewhere stored graph scene... I will replace with assimp anyway !!!

	// stores hf::Scene
	void stashOnGPU(); // delete previous scene if exists and replace it with defined one
	// void addModelToScene(std::string path); because static Vulkan sources init, when scene loading for maximum rendering speed, dynamicaly adding obj to scene is impossible for now. 
	
	// offer ability to choose which models attribs to use (vertex, textureCoords, index buffer) and store them inside buffer 
	//void processMeshes();
	
	// typeflags defines which textures to load
	//void storeTextures(); 
	
	// to every hfVkMesh assign texture references which belongs to it...
	//void distributeRefs();

};

#endif