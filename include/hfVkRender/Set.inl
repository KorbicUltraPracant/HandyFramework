/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#include <hfVkRender/Set.h>
#include <hfAx/Exception.h>

using namespace hf::Vk;
using namespace hf::Ax;
using namespace std;

template<typename T>
inline hf::Vk::Utility::Set<T>::~Set() {
	// init function was not called upon object
	if (!_container.size())
		return;

	_containerUsage.reset();
}

template <typename T>
inline void Utility::Set::addItem(T item) {
	_container.push_back(UPTR(T, std::move(item)));

	_containerUsage.resize(_containerUsage.size() + 1);
}

template <typename T>
int Utility::Set::getNextItemIdx() {
	int freeIdx;
	while (1) {
		freeIdx = _containerUsage.getNextFreeIndex();
		// search every available containers and find one which is not operated with 
		if (freeIdx != -1) {
			if (!_containers[freeIdx]->isSubmited())
				break;
			// else search continues
		}
		else {
			freeIdx = 0;
			break;
		}
	}
	
	return freeIdx;
}