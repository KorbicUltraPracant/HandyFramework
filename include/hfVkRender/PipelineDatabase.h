/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Pipeline Database object
*/

#ifndef HFVKPIPEDB_H
#define HFVKPIPEDB_H

#include <hfVkRender/hfVkRender.h>
#include <hfVk/Device.h>
#include <hfVk/PipelineCache.h>
#include <hfVk/GraphicPipeline.h>
#include <hfVk/ComputePipeline.h>
#include <string>
#include <unordered_map>
#include <vector>

/* Aux structs */
struct hf::Vk::CreateInfo::GraphicPipeDBEntry {
	hf::Vk::CreateInfo::GraphicPipeline graphicPipeCreateInfo;
	std::string name;

	GraphicPipeDBEntry& setGraphicPipeCreateInfo(hf::Vk::CreateInfo::GraphicPipeline graphicPipeCreateInfo_) {
		graphicPipeCreateInfo = graphicPipeCreateInfo_;
		return *this;
	}

	GraphicPipeDBEntry& setName(std::string name_) {
		name = name_;
		return *this;
	}
};

struct hf::Vk::CreateInfo::ComputePipeDBEntry {
	hf::Vk::CreateInfo::ComputePipeline computePipeCreateInfo;
	std::string name;

	ComputePipeDBEntry& setComputePipeCreateInfo(hf::Vk::CreateInfo::ComputePipeline computePipeCreateInfo_) {
		computePipeCreateInfo = computePipeCreateInfo_;
		return *this;
	}

	ComputePipeDBEntry& setName(std::string name_) {
		name = name_;
		return *this;
	}
};

/* ------- Info Class ------- */
class hf::Vk::Info::PipelineDatabase {
// attributes
public:
	hf::Vk::Device* pDevice;
	std::string cacheStoragePath;

	PipelineDatabase& setDevicePtr(hf::Vk::Device * pDevice_) {
		pDevice = pDevice_;
		return *this;
	}

	PipelineDatabase& setCacheStoragePath(std::string cacheStoragePath_) {
		cacheStoragePath = cacheStoragePath_;
		return *this;
	}
};

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::PipelineDatabase : public hf::Vk::Info::PipelineDatabase {
// attributes
public:
	// potreba pred pouzitim resiznout...
	std::vector<GraphicPipeDBEntry> graphicPipesCreateInfos; // string will be name used inside DB
	std::vector<ComputePipeDBEntry> computePipesCreateInfos;

// methods
public:

};

/* ------- Core Class ------- */
class hf::Vk::PipelineDatabase {
// attributes
private:
	std::unordered_map<std::string, GraphicPipeline> _graphicPipesDB;
	std::unordered_map<std::string, ComputePipeline> _computePipesDB;
	Info::PipelineDatabase* _description = nullptr;


// methods
private:
	void init(CreateInfo::PipelineDatabase& createInfo);
	void deinit();
	//bool initCompleted();

	// pipelines ctor
	static void assembleGraphicPipelinesWorker(std::vector<CreateInfo::GraphicPipeDBEntry>* graphicPipesCreateInfos, Device* pDevice, uint32_t from, uint32_t to, vk::PipelineCache cache = vk::PipelineCache(), std::vector<vk::Pipeline>* pipesPtr = nullptr);
	static void assembleComputePipelinesWorker(std::vector<CreateInfo::ComputePipeDBEntry>* computePipesCreateInfos, Device* pDevice, uint32_t from, uint32_t to, vk::PipelineCache cache = vk::PipelineCache(), std::vector<vk::Pipeline>* pipesPtr = nullptr);

public:
	/* Ctors 'n Dtor */
	PipelineDatabase() {}
	PipelineDatabase(CreateInfo::PipelineDatabase& createInfo) { init(createInfo); }
	~PipelineDatabase() { deinit(); }

	/* Getters */

	/* Core Methods */
	void build(CreateInfo::PipelineDatabase& createInfo) { init(createInfo); }
	void destroy() { deinit(); }
	// in order to be able to properly use these methods DB object must be already initialized! 
	void buildGraphicPipelines(std::vector<CreateInfo::GraphicPipeDBEntry>& graphicPipesCreateInfos);
	void buildComputePipelines(std::vector<CreateInfo::ComputePipeDBEntry>& computePipesCreateInfos);
	void destroyGraphicPipelines() { _graphicPipesDB.clear(); }
	void destroyComputePipelines() { _computePipesDB.clear(); }

	// adds object to pipeline, referenced object will be moved into DB -> DB responds for objects deallocation
	void addComputePipeline(std::string name, ComputePipeline& pipeline) { _computePipesDB[name] = pipeline; } // ano vidim ze tady se musi objekt vytvorit a jeste se pak do nej ta pipeline movne ale v insertu se taky stejne vola pipeline.move() a na emplace nemam& createInfo te pipeline ... takze tohle je jedine reseni
	void addGraphicPipeline(std::string name, GraphicPipeline& pipeline) { _graphicPipesDB[name] = pipeline; }
	void addComputePipeline(CreateInfo::ComputePipeDBEntry& info);
	void addGraphicPipeline(CreateInfo::GraphicPipeDBEntry& info);

	// returns and pop pipeline object from DB, referenced HFVK must be uninitialized
	ComputePipeline& obtainComputePipeline(std::string name) { return _computePipesDB[name]; }
	GraphicPipeline& obtainGraphicPipeline(std::string name) { return _graphicPipesDB[name]; }
	// just destroy pipeline with provided key if existed
	void popComputePipeline(std::string name) { _computePipesDB.erase(name); }
	void popGraphicPipeline(std::string name) { _graphicPipesDB.erase(name); }

	/* Operators */

	/* Status Checkers */
	

};

#endif