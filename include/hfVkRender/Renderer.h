#ifndef HFVKRENDERER_H
#define HFVKRENDERER_H

#include <hfVkRender/hfVkRender.h>
#include <hfVkRender/RenderContext.h>
#include <hfVkRender/RendererAuxilliary.h>
#include <hfVkRender/UniformBuffer.inl>
#include <hfVkRender/Scene.h>
#include <hfVkRender/SharedBuffer.h>

namespace hf {
	namespace Vk {
		namespace Render {
			using Node = hf::Scene::Graph::Node<hf::Vk::Render::Mesh>;
		}
	}
}

/* Create Info */
class hf::Vk::CreateInfo::Renderer {
// attributes
public:
	// uint32_t surfaceWidth, surfaceHeight;
	struct {
		bool axisRendering = false;
		bool normalRendering = false;
		bool shadowRendering = false;
		bool wireframeRendering = false;
	} requieredFeatures;

};

/* Core Class */
class hf::Vk::Render::Renderer : protected hf::Vk::Render::Context {
protected:
	// enabled routines
	hf::Vk::CreateInfo::Renderer description;

	// current fov
	float currentFOV = 90;

	// scene cpu n gpu representation
	hf::Vk::Render::Scene scene;

	// shader's data
	hf::Vk::Render::UniformBuffer<hf::Vk::ShaderResources::UniformBuffer::VertexShader> vsUbo;
	hf::Vk::Render::UniformBuffer<hf::Vk::ShaderResources::UniformBuffer::FragmentShader> fsUbo;
	hf::Vk::Render::UniformBuffer<hf::Vk::ShaderResources::UniformBuffer::GeometryShader> gsUbo;
	hf::Vk::Render::UniformBuffer<vk::DrawIndexedIndirectCommand> indirectDrawParameters;
	hf::Vk::Image shadowCubeMap;
	glm::mat4 shadowProj;
	hf::Vk::ShaderResources::PushConstants pushConstants;

	// rendering technique
	enum Techique : uint32_t { SOLID = 1, WIREFRAME = 2, NORMALS = 4, SHOWAXIS = 8 };
	uint32_t renderingTechnique = SOLID;
	/*
		PERMODEL -- WILL BE INSTANCED
	*/
	enum Strategy : uint32_t { BAKEDSCENE, PERMODEL } strategy = BAKEDSCENE;

// methods
protected:
	/* Core Methods */
	// all around init
	virtual void init(const hf::Vk::CreateInfo::Renderer& info) = 0;

	// init individual hf::Vk components
	virtual void initInstance();
	virtual void createVkSurface() = 0;
	void deleteVkSurface() { instance.getVkHandle().destroySurfaceKHR(presenter.getSurface()); }
	virtual void initDevice(uint32_t queuesCount) = 0;
	void initManagers(); // cmdprocessor ds, mem Managers
	virtual void createBuffers();
	virtual void createCmdBuffers();
	virtual void setUpDescriptorSets();
	virtual void loadShaders();
	// creates static pipeline layouts (layouts are still the same no matter loaded scene)
	virtual void createPipelineLayouts();
	virtual void initPipelineDB() = 0;
	virtual void setUpRenderPasses();
	virtual void createSamplers();

	/* First user initializes presenter, than creates renderpass and with that renderpass calls presneter's createFramebuffers method. */
	void initPresenter(uint32_t width, uint32_t height, uint32_t swapchainImgCount);
	virtual void fillPipelineDBCreateInfo(hf::Vk::CreateInfo::PipelineDatabase& createInfo);
	virtual void writeDescriptorSets();
	virtual void recordCmdBuffers();
	virtual void recordSceneCmdBuffers();
	static void checkGPUFeatures(std::vector<vk::PhysicalDevice>& hostGPUs);
	// transform all images to proper layout
	void imagesLayoutTransfer();
	virtual void resizePresenter(uint32_t width, uint32_t height);
	virtual void afterSceneLoadedActions();

	/* describing each single rendering strategy */
	// -- Axis Rendering -- //
	void axis_FillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo);
	void axis_SetUpDS();
	void axis_WriteDS();
	void axis_CreatePipelineLayout();
	void axis_LoadShaders();
	void axis_RecordCommands();
	// -- Baked Scene -- //
	void bakedScene_FillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo);
	void bakedScene_SetUpDS();
	void bakedScene_SetUpTextureDS();
	void bakedScene_WriteDS();
	void bakedScene_WriteTextureDS();
	void bakedScene_CreatePipelineLayout();
	void bakedScene_LoadShaders();
	void bakedScene_RecordCommands();

	// -- Normals Scene -- //
	void normals_FillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo);
	void normals_SetUpDS();
	void normals_WriteDS();
	void normals_CreatePipelineLayout();
	void normals_LoadShaders();
	void normals_RecordCommands();
	// -- Wireframe Scene -- //
	void wireframe_FillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo);
	void wireframe_SetUpDS();
	void wireframe_WriteDS();
	void wireframe_CreatePipelineLayout();
	void wireframe_LoadShaders();
	void wireframe_RecordCommands();
	// -- SceneGraph -- //
	void shadowMapping_GenerateMapFillPipelineInfo(std::vector<hf::Vk::CreateInfo::GraphicPipeDBEntry>& graphicPipesCreateInfos);
	void shadowMapping_RenderSceneFillPipelineInfo(hf::Vk::CreateInfo::GraphicPipeline& createInfo);
	void shadowMapping_SetUpDS();
	void shadowMapping_WriteDS();
	void shadowMapping_CreatePipelineLayouts();
	void shadowMapping_CreateFramebuffers();
	void shadowMapping_SetUpRenderPass();
	//void shadowMapping_RenderSceneSetUpRenderPass();
	void shadowMapping_LoadShaders();
	void shadowMapping_ShadowMap_RecordCommands();
	void shadowMapping_Render_RecordCommands();
	void shadowMapping_switchCubeImageLayout(CommandBuffer& commandBuffer);

	// scene representation
	virtual void createSceneGraph() { scene.createSceneGraph(); } // create scene graph via rules for current renderer ... general vk renderer will use that one provided by vkScene

	/* Auxils */
	void obtainRequiredInstanceExtensions(std::vector<std::string>& extensionNames);

public:
	/* Ctors 'n Dtor */
	// ctor is always virtual
	virtual ~Renderer() { presenter.getSwapchain().dismantle(); deleteVkSurface(); }

	/* Getters */
	hf::Scene::Camera& getCamera() { return scene.camera; }

	/* because batch cmd buffer recording */
	virtual void clearRPAttachments(uint32_t swapchainImageIdx, hf::Vk::CommandBuffer& buffer);

	/* Operators */
	void operator()(hf::Vk::Render::Scene* scene);

};

#endif
