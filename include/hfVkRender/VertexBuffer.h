/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief utility vertex buffer object
* Class pairing hf::Vk::Info::PipelineSetUp::VertexInputDescription with its hf::Vk::Buffer representation.
* Used by HFVKScene class.
*/

#ifndef HFVK_VertexINPUT_H
#define HFVK_VertexINPUT_H

#include <hfVkRender/hfVkRender.h>
#include <hfVk/Buffer.h>
#include <hfVk/PipelineComponents.inl>

/* -------CreateInfoClass ------- */
class hf::Vk::CreateInfo::VertexBuffer {
public:
	CreateInfo::Buffer bufferInfo;
	Info::PipelineSetUp::VertexInputDescription vertexInputdescription;
	// copy src data inside newly created buffer
	void* sourceData = nullptr;
	uint32_t dataLenInBytes = 0;
	// used only when copying data to GPU heap
	hf::Vk::CommandProcessor* copyAgent = nullptr;
	hf::Vk::CommandBuffer* execBuffer = nullptr;

	VertexBuffer& setBufferInfo(CreateInfo::Buffer bufferInfo_) {
		bufferInfo = bufferInfo_;
		return *this;
	}

	VertexBuffer& setVertexInputdescription(Info::PipelineSetUp::VertexInputDescription vertexInputdescription_) {
		vertexInputdescription = vertexInputdescription_;
		return *this;
	}

	VertexBuffer& setSourceData(void * sourceData_) {
		sourceData = sourceData_;
		return *this;
	}

	VertexBuffer& setDataLenInBytes(uint32_t dataLenInBytes_) {
		dataLenInBytes = dataLenInBytes_;
		return *this;
	}

	VertexBuffer& setCopyAgent(hf::Vk::CommandProcessor * copyAgent_) {
		copyAgent = copyAgent_;
		return *this;
	}

	VertexBuffer& setExecBuffer(hf::Vk::CommandBuffer * execBuffer_) {
		execBuffer = execBuffer_;
		return *this;
	}
};

/* ------- Core Class ------- */
class hf::Vk::Render::VertexBuffer {
// attributes
private:
	Info::PipelineSetUp::VertexInputDescription _description;
	hf::Vk::Buffer _data;
	void* _viewPtr = nullptr;

// methods
public:
	VertexBuffer() {}
	VertexBuffer(CreateInfo::VertexBuffer& createInfo) { create(createInfo); }
	~VertexBuffer() { if (_viewPtr != nullptr) _data.unmap(); }

	void create(CreateInfo::VertexBuffer& createInfo);
	template<typename T>
	T& accessData() { return *static_cast<T*>(_viewPtr); }
	//template<typename T>
	void* accessDataPtr() { return _viewPtr; }
	hf::Vk::Buffer& getBuffer() { return _data; }
	Info::PipelineSetUp::VertexInputDescription& getInputDescription() { return _description; }

};

#endif
