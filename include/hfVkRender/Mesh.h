/**
* @author Radek Blahos
* @project hfVkWidget Library
*
* @brief
*/

#ifndef HFVKRENDERMESH_H
#define HFVKRENDERMESH_H

//#include <hfScene/hfScene.h>
#include <hfVkRender/VertexBuffer.h>
#include <hfVk/GraphicPipeline.h>
#include <hfScene/Mesh.h>
#include <hfVkRender/CommandBufferSet.h>

class hf::Vk::Render::Mesh : public hf::Scene::Mesh {
// attributes
public:
	//hf::Vk::Utility::Set<hf::Vk::CommandBuffer> cmdBuffers;
	uint32_t bakedTextureId = -1; // texture not assigned

// methods
public:
	Mesh() {}
	~Mesh() {}

};

#endif