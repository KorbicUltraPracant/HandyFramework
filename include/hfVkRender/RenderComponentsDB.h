/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Render Compontents storage
*/

#ifndef HFVKPIPECOMPONENTSDB_H
#define HFVKPIPECOMPONENTSDB_H

#include <hfVk/hfVk.h>
#include <hfVkRender/hfVkRender.h>
#include <hfVk/DescriptorSet.inl>
#include <hfVk/PipelineLayout.h>
#include <hfVk/ShaderModule.h>
#include <hfVk/Sampler.h>
#include <hfVk/RenderPass.inl>
#include <unordered_map>
#include <string>
#include <vector>
#include <hfVkRender/RenderStrategy.h>

class hf::Vk::Render::ComponentsDB {
//attributes
public:
	std::unordered_map<std::string, Strategy> renderStrategies;
	std::unordered_map<std::string, hf::Vk::PipelineLayout> pipelineLayouts;
	std::unordered_map<std::string, RenderPass> renderPasses;
	std::unordered_map<hf::Vk::RenderPass*, hf::Ax::PtrVector<hf::Vk::Framebuffer>> renderPass_FramebuffersMap; // stores framebuffers for whole program
	hf::Vk::Utility::Set<CommandBuffer> commonCommandBufferSet; // common buffer set
	hf::Ax::PtrVector<Semaphore> semaphores;
	hf::Ax::PtrVector<Sampler> samplers; 
	std::unordered_map<VkQueueFlagBits, uint32_t> queueFamilyTags; // keeps mapping from flags to queueFamilyIndex
};

#endif

// watchout for ptrvector... its intern realloc should cause a problem!!! ?? find out more about this assumption