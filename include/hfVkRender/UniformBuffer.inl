/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Uniform buffer's inlines
*/

#ifndef HFVKUNIFORMBUFFER_INL
#define HFVKUNIFORMBUFFER_INL

#include <hfVkRender/UniformBuffer.h>

using namespace hf::Vk;

template<typename T>
Render::UniformBuffer<T>::UniformBuffer(CreateInfo::Buffer& createInfo) {
	createInfo.size = sizeof(T);
	createInfo.assignMemory = true;
	_buffer.create(createInfo);
	
	// initialize data memory to default for object T
	_viewPtr = static_cast<T*>(_buffer.map());
	T init;
	memcpy(_viewPtr, &init, sizeof(T));
}

template<typename T>
void Render::UniformBuffer<T>::create(CreateInfo::Buffer& createInfo) {
	createInfo.size = sizeof(T);
	createInfo.assignMemory = true;
	_buffer.create(createInfo);

	// initialize data memory to default for object T
	_viewPtr = static_cast<T*>(_buffer.map());
	T init;
	memcpy(_viewPtr, &init, sizeof(T));
}

template<typename T>
void hf::Vk::Render::UniformBuffer<T>::copy(T& data) {
	memcpy(_viewPtr, &data, sizeof(T));
}

#endif // !HFVKUNIFORMBUFFER_INL
