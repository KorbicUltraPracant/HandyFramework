#ifndef HFVKPRESENTER_H
#define HFVKPRESENTER_H

#include <hfVk/hfVk.h>
#include <hfVkRender/hfVkRender.h>
#include <hfVk/Swapchain.h>
#include <hfVk/Framebuffer.h>
#include <hfVk/RenderPass.h>
#include <memory>

/* ------- Info class ------- */
class hf::Vk::Info::Presenter {
// attributes
public:
	hf::Vk::MemoryManager* pMemManager; // parrent Device ptr, used when creating depth buffer, includes ptr to Device
	hf::Vk::Queue* presentationQueue;
	std::unordered_map<hf::Vk::RenderPass*, hf::Ax::PtrVector<hf::Vk::Framebuffer>>* renderPass_FramebuffersMapPtr;

// methods
public:
	Presenter& setPMemManager(hf::Vk::MemoryManager * pMemManager_) {
		pMemManager = pMemManager_;
		return *this;
	}

	Presenter& setPresentationQueue(hf::Vk::Queue * presentationQueue_) {
		presentationQueue = presentationQueue_;
		return *this;
	}
};

/* ------- Create Info ------- */
class hf::Vk::CreateInfo::Presenter : public Info::Presenter {
// attributes
public:
	hf::Vk::CreateInfo::Swapchain swapchainCreateInfo;

// methods
public:
	Presenter& setSwapchainCreateInfo(hf::Vk::CreateInfo::Swapchain swapchainCreateInfo_) {
		swapchainCreateInfo = swapchainCreateInfo_;
		return *this;
	}

};

/* ------- Core ------- */
class hf::Vk::Presenter {
// attributes
private:
	Info::Presenter* _description = nullptr;
	
	vk::SurfaceKHR _presentSurface; // save here, but created externaly e.g. obtained from 3rd party lib or when its done from geAx Window
	std::unique_ptr<Swapchain> _swapchain;
	std::unique_ptr<Image> _depthbuffer;
	RenderPass* _currentRenderPass; // signals for which renderpass use framebuffers
	

public:

// methods
private:
	void deleteFramebuffers(RenderPass& usedRenderPass) { _description->renderPass_FramebuffersMapPtr->erase(&usedRenderPass); } // delete framebuffers for defined renderpass																							 //void setCurrentRenderPass(RenderPass& usedRenderPass) { if (_description->framebuffers->find(&usedRenderPass) != _description->framebuffers->end()) _currentRenderPass = &usedRenderPass; } // assign renderpass only if framebuffers for it exists ...

public:
	Presenter() {}
	Presenter(hf::Vk::CreateInfo::Presenter& createInfo) { init(createInfo); }
	void init(hf::Vk::CreateInfo::Presenter& createInfo);
	void createDepthBuffer();
	void addFramebuffers(RenderPass& renderPass);

	void present() { getSwapchain().presentImage(*_description->presentationQueue); }
	void transformImagesToProperLayout(hf::Vk::CommandBuffer& buffer);
	void clearRenderResources(uint32_t swapchainImageIdx, hf::Vk::CommandBuffer& buffer);

	Swapchain& getSwapchain() { return *_swapchain; }
	Framebuffer& getFramebuffer() { if (_swapchain->getCurrentImageIdx() != -1) return *(*_description->renderPass_FramebuffersMapPtr)[_currentRenderPass][_swapchain->getCurrentImageIdx()]; }
	Framebuffer& getFramebuffer(RenderPass& renderPass, uint32_t idx) { return *(*_description->renderPass_FramebuffersMapPtr)[&renderPass][idx]; }
	Image& getDepthbuffer() { return *_depthbuffer; }
	vk::SurfaceKHR& getSurface() { return _presentSurface; }
	uint32_t getImageCount() { return _swapchain->getDescription().minImageCount; }
	Info::Presenter& getDescription() { return *_description; }

};

#endif
