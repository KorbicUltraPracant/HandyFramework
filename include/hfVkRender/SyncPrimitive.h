/**
* @author Radek Blahos
* @project hfVk Library
*
* @brief Abstract factory for Vk Sync primitives
*/

#ifndef HFVKSYNCPRIMITIVE_H
#define HFVKSYNCPRIMITIVE_H

#include <hfVk/Event.h>
#include <hfVk/Fence.h>

/* ------ Sync Primitive ------ */
class hf::Vk::Utility::SyncPrimitive {
// attributes
public:
	void* _lockPtr = nullptr; // stores ptr to Fence or Event

// methods
public:
	SyncPrimitive() {};
	SyncPrimitive(hf::Vk::Device& device) {} // if createFence false -> event is used
	virtual ~SyncPrimitive() {}

	virtual void create(hf::Vk::Device& device) = 0;
	virtual void destroy() = 0;

	template<typename T>
	T& getPrimitive() { return *(T*)_lockPtr; }
	void*& getPrimitivePtr() { return _lockPtr; }

	virtual void waitUntilSignaled() = 0;
	virtual void reset() = 0;
	virtual void signal() = 0;
	virtual void markAsUsed() = 0;
	virtual bool isUsed() = 0;
	virtual bool isSignaled() = 0;

};

class FencePrimitive : public hf::Vk::Utility::SyncPrimitive {
public:
	FencePrimitive() {};
	FencePrimitive(hf::Vk::Device& device) { create(device); }
	virtual ~FencePrimitive() { destroy(); }

	virtual void create(hf::Vk::Device& device);
	virtual void destroy();

	virtual void waitUntilSignaled() { hf::Vk::Fence* ptr = (hf::Vk::Fence*)_lockPtr; ptr->waitUntilSignaled(); }
	virtual void reset() { hf::Vk::Fence* ptr = (hf::Vk::Fence*)_lockPtr; ptr->reset(); }
	virtual void signal() { return; }
	virtual void markAsUsed() { hf::Vk::Fence* ptr = (hf::Vk::Fence*)_lockPtr; ptr->markAsUsed(); }
	virtual bool isUsed() { hf::Vk::Fence* ptr = (hf::Vk::Fence*)_lockPtr; return ptr->isUsed(); }
	virtual bool isSignaled() { hf::Vk::Fence* ptr = (hf::Vk::Fence*)_lockPtr; return ptr->isSignaled(); }

};

class EventPrimitive : public hf::Vk::Utility::SyncPrimitive {
public:
	EventPrimitive() {};
	EventPrimitive(hf::Vk::Device& device) { create(device); }
	virtual ~EventPrimitive() { destroy(); }

	virtual void create(hf::Vk::Device& device);
	virtual void destroy();

	virtual void waitUntilSignaled() { hf::Vk::Event* ptr = (hf::Vk::Event*)_lockPtr; ptr->waitUntilSignaled(); }
	virtual void reset() { hf::Vk::Event* ptr = (hf::Vk::Event*)_lockPtr; ptr->reset(); }
	virtual void signal() { hf::Vk::Event* ptr = (hf::Vk::Event*)_lockPtr; ptr->set(); }
	virtual void markAsUsed() { return; }
	virtual bool isUsed() { hf::Vk::Event* ptr = (hf::Vk::Event*)_lockPtr; return ptr->isSet(); }
	virtual bool isSignaled() { hf::Vk::Event* ptr = (hf::Vk::Event*)_lockPtr; return ptr->isSet(); }

};

#endif