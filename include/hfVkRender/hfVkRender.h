/**
* @author Radek Blahos
* @project Handy Framework's Vulkan Library
*
* @brief hfVkRender Forward declarations
*/

#ifndef HFVKRENDER_H
#define HFVKRENDER_H

namespace hf {
	namespace Vk {
		class PipelineDatabase;
		// nejaka class co bude zastresovat framebuffery, dpt buff tak aby mohla existovat cista swapchain
		class Presenter;

		namespace Render {
			class Context;
			class ComponentsDB;
			class Renderer;
			class VertexBuffer;
			class SharedBuffer; // vk::buffer with host visible and coherent flag set
			template <typename T> class UniformBuffer;
			class Mesh;
			class Scene;
			class Strategy;
			class ComplexStrategy;

		}

		namespace CreateInfo {
			class PipelineDatabase;
			class Presenter;
			class Scene;
			class CommandBufferSet;
			class VertexBuffer;
			class Renderer;

		}

		namespace FurtherInfo {
			
		}

		namespace Info {
			class PipelineDatabase;
			class Presenter;
			class CommandBufferSet;

		}

		// Utility 
		namespace Utility {
			template <typename T>
			class Set;
			
		}

		namespace ShaderResources {
			namespace UniformBuffer {
				struct VertexShader;
				struct FragmentShader;
				struct GeometryShader;
			}
			struct PushConstants;
		}

		namespace CommandBufferRecording {
			template<typename T> class RenderPass;
			//class Subpass;
			template<typename T> class Batch; // need to find out how to pass into single vector different types of RenderPass template class instances
			namespace Draw {
				class Core;
				class Draw;
				class Indexed;
				class IndexedIndirect;
			}
		}
	}
}

#endif

/* Class Template

class hf::Vk::Render::class {
// attributes
private:
public:

// methods
private:
public:

};

*/