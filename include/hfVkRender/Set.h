/* template vec, ktera bude tvorit zastresovat set pro libovolnou entitu eg. command containery, sync primitiva...
avsak kazda entity vec musí mít vlastní info strukturu... */

#ifndef HFVKENTITYSET_H
#define HFVKENTITYSET_H

#include <hfVk/hfVk.h>
#include <hfVkRender/hfVkRender.h>
#include <hfVk/CommandPool.h>
#include <hfVk/Device.h>
#include <hfAx/UsageRegister.h>
#include <hfVk/Synchronization.h>

/* ------- Core Class ------- */
template <typename T>
class hf::Vk::Utility::Set {
// attributes
private:
	hf::Ax::PtrVector<T> _container;
	hf::Ax::UsageRegister _containerUsage;

// methods
private:
	int getNextItemIdx();

public:
	/* Ctors 'n Dtor */
	Set() {}
	~Set();

	/* Getters */
	T& getItem(unsigned idx) { return *_container[idx]; }

	// return cmd container which idx has been lastly assigned by getNextcontainer
	// User cannot call to consequence getCurrentcontainer calls without calling returnCurrentcontainer between them !
	T& getNextItem() { return *_container[getNextItemIdx()]; }
	T& getCurrentItem() { return *_container[_containerUsage.lastAssigned()]; } // neni osetreno, protoze chci aby to vyhodilo vyjimku, jelikoz to to znamena ze uzivatel pouziva cmd set spatne!
	int getCurrentItemIdx() { return _containerUsage.lastAssigned(); }
	void returnItem(uint32_t idx) { _containerUsage.unsetIndex(idx); }
	void returnCurrentItem() { _containerUsage.unsetIndex(_containerUsage.lastAssigned()); } // mark container on idx as unused

	// insert count more items into set
	inline void addItem(T item);

	// reset _containerUsage
	void usageRegisterReset() { _containerUsage.unsetAllIndexes(); }

};

#define SET_ADD_ITEMS(container, param, count) do { \
	for (int i = 0; i < count; i++) { \
		container.addItem(param); \
	}\
} while (0);

#endif // !
