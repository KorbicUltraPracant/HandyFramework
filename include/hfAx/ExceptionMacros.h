/**
* @author Radek Blahos
* @project hfAx Library
*
* @brief excepction macros
*/

#ifndef hfAxEXCEPTIONMACROS_H
#define hfAxEXCEPTIONMACROS_H

#include <hfAx/Exception.h>
#include <iostream>

#define CATCHEXIT(returnCode) \
		std::cout << "press any key to exit..."; \
		std::cin.get(); \
		exit(-1) // vypustim ; kvuli zvyku ho stejne za to makro v kodu napsat...

#endif
