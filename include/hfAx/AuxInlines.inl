/**
* @author Radek Blahos
* @project hfAx Library
*
* @brief This file contains implementation of thisFilename's methods
*/

#ifndef hfAX_INL
#define hfAX_INL

#include <hfAx/hfAx.h>

template<typename T>
T hf::Ax::ceil(T number) {
	if (number == int(number)) 
		return T(number);

	return T(int(number + 1));
}

template<typename T>
bool hf::Ax::in(T val, T* array, int arr_size) {
	for (int i = 0; i < arr_size; i++) {
		if (val == array[i])
			return true;
	}

	return false;
}

#endif
