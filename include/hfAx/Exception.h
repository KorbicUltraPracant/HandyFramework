/**
* @author Radek Blahos
* @project hfAx Library
*
* @brief Exception classes definition
*/

#ifndef hfAxEXCEPTION_H
#define hfAxEXCEPTION_H

#include <hfAx/hfAx.h>
#include <exception>
#include <stdexcept>
#include <new>
#include <string>

class hf::Ax::Exception::Base {
public:
	std::string errorName;

protected:
	std::string getName() noexcept { return errorName + std::string(".\n"); }

public:
	Base(std::string error) : errorName(error) {}
	virtual ~Base() {}
	virtual std::string what() { return (errorName + std::string(".\n")).c_str(); }

};

class hf::Ax::Exception::Bad::Allocation : public Base, public std::bad_alloc {
public:
	Allocation(std::string error) : Base(error) {}
	std::string what() { return (std::string("BadAllocation: ") + Base::getName()).c_str(); }

};

class hf::Ax::Exception::Bad::Creation : public Base, public std::exception {
public:
	Creation(std::string error) : Base(error) {}
	std::string what() { return (std::string("BadCreation: ") + Base::getName()).c_str(); }

};

class hf::Ax::Exception::Bad::Load : public Base, public std::runtime_error {
public:
	Load(std::string error) : Base(error), runtime_error(error) {}
	std::string what() { return (std::string("BadLoad: ") + Base::getName()).c_str(); }

};

class hf::Ax::Exception::Bad::Select : public Base {
public:
	Select(std::string error) : Base(error) {}
	std::string what() { return (std::string("BadSelect: ") + Base::getName()).c_str(); }

};

class hf::Ax::Exception::Bad::Execution : public Base {
public:
	Execution(std::string error) : Base(error) {}
	std::string what() { return (std::string("BadExecution: ") + Base::getName()).c_str(); }

};

#endif
